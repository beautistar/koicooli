import React, { Component } from 'react';
import { Platform, StyleSheet, ScrollView, Text, View,SafeAreaView, TouchableOpacity, TextInput, Image, ActivityIndicator, Animated, KeyboardAvoidingView, AsyncStorage } from 'react-native';
import I18n, { getLanguages } from 'react-native-i18n';
import { Actions } from 'react-native-router-flux'
const logo = require("../assets/images/logo.png")
import Api from '../components/api'
import WorkAreaDetails from '../components/WorkAreasDetails'
import { setDeviceLanguage, getDeviceLanguage, setLoginData, getLoginData } from '../components/Language'

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appLanguage: '',
      userName: '',
      password: '',
      userNameErr: false,
      passwordErr: false,
      loading: false,
      loginLoading:false,
      errMessage: '',
      errorHeight: new Animated.Value(0),
      errorOpacity: new Animated.Value(0),
      errorMode: false,
     
    }
  }
  _launchError() {
    if (!this.state.errorMode) {
      Animated.sequence([
        Animated.parallel([
          Animated.timing(this.state.errorHeight, { toValue: 35, duration: 300 }),
          Animated.timing(this.state.errorOpacity, { toValue: 1, duration: 300 })
        ])
      ]).start();
      setTimeout(() => {
        Animated.sequence([
          Animated.parallel([
            Animated.timing(this.state.errorHeight, { toValue: 0, duration: 300 }),
            Animated.timing(this.state.errorOpacity, { toValue: 0, duration: 300 })
          ])
        ]).start();
      }, 5000)
    }
  }
  componentWillMount() {
    
    getDeviceLanguage().then(lng =>{
      if(!lng)
      setDeviceLanguage().then(lng => this.setState({ appLanguage: lng }))
    } )
    this.setState({loginLoading:true})
    getLoginData().then(data=>{
      console.log('login Data : ',data)
      if(data){
        this.setState({
          userName:data.email,
          password:data.password
        })
        this.login()
      }
      else{
        this.setState({loginLoading:false})
      }
    })
  }

  login() {

    let { userName, password } = this.state
    if (!userName)
      this.setState({ userNameErr: true})
    if (!password)
      this.setState({ passwordErr: true })
    if (userName && password) {
      this.setState({ loading: true })
      var data = {
        email: userName,
        password: password
      }
      
      Api.post('/login',data).then((res)=>{
        console.log('req : ',res)
        if(res.message=="success"){
            setLoginData({
              blocks:res.data.blocks,
              email:res.data.email,
              firstName:res.data.firstName,
              lastName:res.data.lastName,
              name:res.data.name,
              mobile:res.data.mobile,
              password:password,
              subjects:res.data.subjects,
              type:res.data.type,
              user_id:res.data._id,
              instituteName:res.data.instituteId.title,
              instituteId:res.data.instituteId._id,
              token:res.token
            })
            this.setState({loading:false})
            console.log("data : ",res)
            if(res.data.type==3){
              // Actions.Home({menu:true})
               Actions.reset("Home",{menu:true})
            }
            if(res.data.type==4){
              Actions.reset("Home",{page:<WorkAreaDetails title='Areas of Works'/>,menu:true,back:false})
               //Actions.Home({page:<WorkAreaDetails title='Areas of Works'/>,menu:true,back:false})
             
            }
            else
            {
                this.setState({loading:false,errMessage:"Please Check Your UserName And Password "})
                this._launchError() 
            } 
        }
        else{
          console.log("Errror..")
          this.setState({loading:false,errMessage:res.message})
          this._launchError()
      
        }
      })
    }
  }
  render() {
    console.log("languge : ", this.state.appLanguage)
    I18n.locale = this.state.appLanguage;
    let { userNameErr, passwordErr, loading } = this.state
    if (this.state.loginLoading)
      return (
        <View style={styles.loading}>
          <ActivityIndicator size='large' color='#7f3f98' />
        </View>
      )
    else
    return (
        <SafeAreaView style={styles.container}>
        <KeyboardAvoidingView style={styles.container} behavior={Platform.OS == 'ios' ? 'padding' : null} >
          <Animated.View style={{ width: '100%', height: this.state.errorHeight, opacity: this.state.errorOpacity, backgroundColor: '#d91e18', justifyContent: 'center', alignItems: 'center', padding: 10,position:'absolute',zIndex:1}}>
            <Text style={{ color: 'white', fontSize: 15 }}>{this.state.errMessage}</Text>
          </Animated.View>
          <ScrollView style={{ width: '100%' }} contentContainerStyle={{ alignItems: 'center'}}>
          <Image source={logo} style={styles.logo} />
            <Text style={styles.title}>KolCooli</Text>

            <View style={styles.inputView}>
              {/* <Text style={{textAlign:'center',}}></Text> */}
              <TextInput style={{ borderBottomWidth: 1,paddingVertical:Platform.OS=='ios'?10:0, textAlign: this.state.appLanguage == 'en' ? 'left' : 'right' }}
                placeholder={I18n.t('userName')}
                onChangeText={(value) => this.setState({ userName: value, userNameErr: value ? false : true })}
                value={this.state.userName}
                onSubmitEditing={() => this.password.focus()} />
              {userNameErr ? <Text style={styles.errorMsg}>{I18n.t('userNameErr')}</Text> : null}
              <TextInput style={{ borderBottomWidth: 1,paddingVertical:Platform.OS=='ios'?10:0, textAlign: this.state.appLanguage == 'en' ? 'left' : 'right', marginTop: Platform.OS == 'ios' ? 20 : 10 }}
                ref={(ref) => this.password = ref}
                placeholder={I18n.t('password')}
                onChangeText={(value) => this.setState({ password: value, passwordErr: value ? false : true })}
                secureTextEntry={true}
                value={this.state.password}
                onSubmitEditing={() => this.login()} />
              {passwordErr ? <Text style={styles.errorMsg}>{I18n.t('passwordErr')}</Text> : null}
            </View>

            <TouchableOpacity 
            onPress={()=>Actions.ForgotPassword()}
            style={styles.forgotButtonView}  >
              <Text>{I18n.t('forgotPassword')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.loginButtonView} onPress={() => this.login()}>
              {
                loading ? <ActivityIndicator color='white' />
                  : <Text style={styles.loginButtonText}>{I18n.t('login')}</Text>
              }
            </TouchableOpacity>
           
            {/* <TouchableOpacity style={{alignItems:'center',}} onPress={()=>this.setLangauge()}>
          <Text>Change language</Text>
        </TouchableOpacity> */}
          </ScrollView>
        </KeyboardAvoidingView>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  loading: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 75,
    marginTop: Platform.OS == 'ios' ? 90 : 40
  },
  title: {
    fontSize: 35,
    marginVertical: 20,
    color: '#7f3f98'
  },
  inputView: {
    width: '75%',

  },
  forgotButtonView: {
    alignItems: 'center',
    marginTop: 50,
    marginBottom: 20
  },
  loginButtonView: {
    alignItems: 'center',
    width: '75%',
    backgroundColor: '#7f3f98',
    padding: 10,
    borderRadius: 50,
    elevation: 2
  },
  loginButtonText: {
    fontSize: 20,
    color: 'white'
  },
  errorMsg: {
    fontWeight: 'bold',
    color: 'red',
    marginTop: 5
  }
});
