import React, {Component} from 'react';
import {Platform, StyleSheet, ScrollView, Text,Dimensions,SafeAreaView, View,TouchableOpacity,TextInput,Image,ActivityIndicator,Animated,KeyboardAvoidingView} from 'react-native';
import I18n, { getLanguages } from 'react-native-i18n';
import {Actions} from 'react-native-router-flux'
const logo =require("../assets/images/logo.png")
import Api from '../components/api'

//I18n.fallbacks = true;
// Available languages
I18n.translations = {
  'en': require('../assets/translations/en'),
  'he': require('../assets/translations/hebrew'),
};

export default class ForgotPassword extends Component{
 constructor(props){
   super(props);
   this.state={
     language:'en',
     email:'',
     emailErr:false,
     loading:false,
     errMessage:'',
     errorHeight:new Animated.Value(0),
     errorOpacity:new Animated.Value(0),
     errorMode:false,
     bgcolor:'#d91e18',
   }
 }
 _launchError(){
  if(!this.state.errorMode){
    Animated.sequence([
      Animated.parallel([
        Animated.timing(this.state.errorHeight,{toValue:35,duration:300}),
        Animated.timing(this.state.errorOpacity,{toValue:1,duration:300})
      ])
    ]).start();
    setTimeout(()=>{
      Animated.sequence([
        Animated.parallel([
          Animated.timing(this.state.errorHeight,{toValue:0,duration:300}),
          Animated.timing(this.state.errorOpacity,{toValue:0,duration:300})
        ])
      ]).start();
     
    },5000)
  }
  }

componentDidMount(){
  getLanguages().then(languages => {
    //I18n.locale =languages[0].slice(0,2);
    console.log('did mount',languages[0].slice(0,2)); // ['en-US', 'en']
    this.setState({language:languages[0].slice(0,2)})
  });
 
}
login(){

  let {email}=this.state
  if(!email)
    this.setState({emailErr:true})
  if(email)
  {
    this.setState({loading:true})
      var data={
        email:email,
      }
      Api.post('/forgot-password',data).then((res)=>{
        console.log('req : ',res)
        if(res.status=="Error"){
          console.log("Errror..")
          this.setState({loading:false,errMessage:res.message,bgcolor:'#d91e18'})
          this._launchError()
          
        }
        else{
          console.log("success..")
          this.setState({loading:false,errMessage:res.message,bgcolor:'green'})
          this._launchError()
          setTimeout(()=>{
            Actions.reset("Login")
          },5000)
        }
      })
  }
}
  render() {
    console.log("languge : ",this.state.language)
    I18n.locale =this.state.language;
    let{emailErr,loading}=this.state
    return (
       <SafeAreaView style={styles.container}>
         <Animated.View style={{ opacity:this.state.errorOpacity,backgroundColor:this.state.bgcolor,justifyContent:'center',alignItems:'center',padding:10}}>
         <Text style={{color:'white',fontSize:15,textAlign:'center',paddingHorizontal:5}}>{this.state.errMessage}</Text>
         </Animated.View>
         
       <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'padding' : null} >

        <View style={{alignItems:'center',marginTop:50}}>
        <Image source={logo} style={styles.logo}/>
        <Text style={styles.title}>KolCooli</Text>
       
         <View style={styles.inputView}>
            <TextInput style={{borderBottomWidth:1,textAlign:this.state.language=='en'?'left':'right',marginTop:10,padding:5}}
            ref={(ref)=>this.password=ref}
            placeholder={'Email'}
            onChangeText={(value)=>this.setState({email:value,emailErr:value?false:true})}
            value={this.state.email}
            onSubmitEditing={()=>this.login()}/>
             {emailErr?<Text style={styles.errorMsg}>Please Enter Your Email.</Text>:null}
        </View>
        <TouchableOpacity style={styles.loginButtonView} onPress={()=>this.login()}>
         {
           loading? <ActivityIndicator size='large' color='white'/>
           :<Text style={styles.loginButtonText}>Submit</Text>
         } 
        </TouchableOpacity>
        </View>
     </KeyboardAvoidingView>
       </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
   //alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  logo:{
    width:150,
    height:150,
    borderRadius:75,
    marginTop:20
  },
  title:{
    fontSize:35,
    marginVertical:20,
    color:'#7f3f98'
  },
  inputView:{
    marginVertical:20,
    width:Dimensions.get('window').width/1.4
  },
  forgotButtonView:{
    alignItems:'center',
    marginTop:50,
    marginBottom:20
  },
  loginButtonView:{
    marginTop:20,
    alignItems:'center',
    width:'75%',
    backgroundColor:'#7f3f98',
    padding:10,
    borderRadius:50,
    elevation:2
  },
  loginButtonText:{
    fontSize:20,
    color:'white'
  },
  errorMsg:{
    fontWeight:'bold',
    color:'red',
    marginTop:5
  }
});
