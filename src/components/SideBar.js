import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, FlatList, ActivityIndicator, TouchableOpacity,AsyncStorage} from 'react-native';
import I18n from 'react-native-i18n';
import { Icon } from 'native-base';
import { setLayout } from './Layout'
import About from '../home/About'
import { getAppLanguage, getDeviceLanguage, setAppLanguage,getLoginData } from './Language'
import { Actions } from 'react-native-router-flux';
import WorkAreaDetails from '../components/WorkAreasDetails'
export default class WorkAreasDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: '',
      appLanguage:'',
      loginType:''
    }
  }
  componentWillMount() {

    getDeviceLanguage().then(lng => {
      console.log('device Langauge: ', lng)
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => {
      console.log('APP Langauge: ', lng)
      this.setState({ appLanguage: lng })
    })
    getLoginData().then(data=>this.setState({loginType:data.type}))
  }
  setLangauge(language) {
    I18n.locale = language;
    setAppLanguage(language)
    //Actions.pop()
    //Actions.Home()
    if(this.state.loginType==3)
    Actions.reset("Home",{menu:true})
    else
    Actions.reset("Home",{page:<WorkAreaDetails title='Areas of Works'/>,menu:true})
    //this.setState({appLanguage:language})
    console.log('language : ', language)
  }

  render() {
    I18n.locale = this.state.appLanguage;
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
      return (
        <View style={styles.container}>
         <TouchableOpacity 
            onPress={()=>this.state.loginType==3?Actions.reset("Home",{menu:true}):Actions.reset("Home",{page:<WorkAreaDetails title='Areas of Works'/>,menu:true})}
            style={{padding:10}}>
                <Text>
                    Home
                </Text>
            </TouchableOpacity>
            <TouchableOpacity 
            onPress={()=>this.setLangauge(this.state.appLanguage == 'en' ? 'he' : 'en')}
            style={{padding:10}}>
                <Text>
                    Change Langauge
                </Text>
            </TouchableOpacity>
            <TouchableOpacity 
            onPress={()=> Actions.Home({page:<About/>,headerTitle:'About',back:true})}
            style={{padding:10}}>
                <Text>
                    About
                </Text>
            </TouchableOpacity>
            <TouchableOpacity 
            onPress={()=>{Actions.reset("Login"),AsyncStorage.clear()}}
            style={{padding:10}}>
                <Text>
                    Logout
                </Text>
            </TouchableOpacity>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#ffffff',
  },
});

