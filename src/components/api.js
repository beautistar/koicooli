import {Alert} from 'react-native'
class Api {
  static headers(token) {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'token':token,
      'dataType': 'json',
    }
  }
  static get(route,token) {
    return this.kolcooli(route, null,token, 'GET');
  }
  static getWithParams(route,param, token) {
    return this.kolcooli(route, params,token, 'GET',)
    //return this.kolcooli(route, null,token, 'GET');
  }

  static put(route, params,token) {
    return this.kolcooli(route, params,token,'PUT')
  }

  static post(route, params) {
    return this.kolcooli(route, params,null, 'POST')
  }
  static postWithToken(route, params,token) {
    return this.kolcooli(route, params,token, 'POST',)
  }

  static delete(route, params,token) {
    return this.kolcooli(route, params,token,'DELETE')
  }
  static kolcooli(route, params,token,verb,) {
    //const host = 'http://192.168.0.119:1365'
   //const host = 'https://kolcooli-uygdztrkpx.now.sh'
   const host = 'https://kolcooli-server-back-end-1.herokuapp.com'
    
    const url = `${host}${route}`
    console.log(url)
    let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
    options.headers = Api.headers(token)
    //console.log('---------Data inside api-----',params)
    //console.log('-------header called-------',Api.headers(token))
    return fetch(url, options).then( resp => {
      //console.log('-----my Response-----',resp)
      let json = resp.json();
    console.log('-----inside api-----',json)
    //console.log('-----inside api-----',resp)
      if (resp.ok) {
        //console.log('-----inside api-----',resp)
        return json;
          //console.log('-----inside api-----',resp)
      }
      return json.then(err => {throw err});

    }).then( json => json)
    .catch(e=>{
      console.log('Error',e)
      Alert.alert(
        'Erorr',
        'Server not reponding Please try again later ',
        [
          {text: 'Yes', onPress: () =>this.kolcooli(route, params,token,verb)},
          {text: 'No', onPress: () => console.log("press No")},
        ],
        { cancelable: false }
      )
    });
  }
}
export default Api
