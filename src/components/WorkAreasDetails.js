import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, Animated,FlatList,RefreshControl, ActivityIndicator, TouchableOpacity,SafeAreaView,TouchableWithoutFeedback} from 'react-native';
import Api from '../components/api'
import Header from '../components/Header'
import I18n from 'react-native-i18n';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
const defaultPic = require('../assets/images/logo.png')
import { setLayout } from '../components/Layout'
import EditWorkAreasDetails from '../components/editWorkAreasDetails'
import Task from '../home/Task'
import WorksAreas from '../home/WorksAreas'
import NoDataFound from '../components/NoDataFound'
import Swipeout from 'react-native-swipeout'
import { getAppLanguage, getDeviceLanguage,getLoginData } from '../components/Language'
export default class WorkAreasDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: '',
      appLanguage: this.props.lng ? this.props.lng : 'en',
      loading: false,
      revers: false,
      subjects: [
        // { tid: 1, title: 'Play Guitar', updatedDate: new Date(), activeTask: 4 },
        // { tid: 2, title: 'לנגן גיטרה', updatedDate: new Date(), activeTask: 2 },
      ],
      close:false,
      refreshing: false
    }
    this.bounce=new Animated.Value(0)
    this.getData=this.getData.bind(this)
  }
  _onRefresh = () => {
    this.setState({refreshing: true});
    var params={
      studentId:this.props.studentId,
      block:this.props.blockId
    }
    this.getData(params,true)
  }
  getData(params,refresh,newsubject){
    console.log('paramiter: ',params)
    this.setState({loading:refresh?false:true})
    getLoginData().then(data=>{
      this.setState({loginType:data.type})
        Api.postWithToken('/api/subject-list',params,data.token).then((res)=>{
          console.log('req : ',res)
          if(res.message=="success"){ 
            this.setState({subjects:res.data,refreshing: false},()=>{
             if(newsubject){
                setTimeout(()=>{
                  Animated.spring(
                    this.bounce,
                    {
                      toValue:1,
                      friction:0.5
                    }
                  ).start()
                },1000)
              }
            })
            this.bounce.setValue(0)
            this.props.getBlockData?this.props.getBlockData():null
          }
          else{
            this.setState({subjects:[],refreshing: false})
          }
         this.setState({loading:false})
        })
     })
     this.props.getBlockData?this.props.getBlockData():null
     this.props.getStudents?this.props.getStudents():null 
  }
  componentWillMount() {
    console.log('data ======= : ',this.props)
    getDeviceLanguage().then(lng => {
      console.log('device Langauge: ', lng)
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => {
      console.log('APP Langauge: ', lng)
      this.setState({ appLanguage: lng })
    })
    //var params=this.props.blockId?{block:this.props.blockId}:{studentId:this.props.studentId}
    var params={
      studentId:this.props.studentId,
      block:this.props.blockId
    }
    this.getData(params)
  }
  deleteTask(subjectId){
   // alert(subjectId)
    getLoginData().then(data=>{
      this.setState({loginType:data.type})
      Api.delete('/api/subject-delete/'+subjectId,null,data.token).then((res)=>{
        console.log('req : ',res)
        if(res.message=="success"){ 
          var params={
            studentId:this.props.studentId,
            block:this.props.blockId
          }
          this.getData(params)
        }
      })
  })
  }
  openInfo(item){
    this.setState({close:true})
    Actions.EditWorkAreasDetails({ title:this.props.title ,
      getData:this.getData,
      subTitle:item.title,
      firstName:this.props.firstName,
      lastName:this.props.lastName,
      pic:this.props.pic,
      studentId:this.props.studentId,
      actionPlan:item.tasks,
      studentSubject:this.props.studentSubject,
      subjectId:item._id,back:true})
  }
  render() {
    I18n.locale = this.state.appLanguage;
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    console.log('Data : ',this.state.subjects)
    if (this.state.loading)
      return (
        <View style={styles.loading}>
          <ActivityIndicator size='large' color='#7f3f98' />
        </View>
      )
    else
      return (
        <SafeAreaView style={styles.container}>
          <View style={styles.subHeader}>
          <View style={{flexDirection:'row'}}>
              <View style={{width:this.state.loginType==4?'100%':'90%',alignItems:'center',paddingLeft:this.state.loginType==4?0:'10%'}}>
                 <Text style={styles.subHeaderText}>{this.state.loginType==4?'My':this.props.firstName +" "+this.props.lastName} {this.props.title}</Text>
              </View>
            {this.state.loginType==4?null:<TouchableOpacity  onPress={()=>Actions.Home({page:<WorksAreas data={{firstName: this.props.firstName, lastName: this.props.lastName,profile:this.props.pic}}
              studentId={this.props.studentId}
              blockId={this.props.blockId}
            />,menu:true,back:true})}>
              <Image source={require('../assets/icons/cubeIcon.png')} style={{ height: 30, width: 30 }} />
            </TouchableOpacity>}
            </View>
          </View>

          <View style={styles.container2}>
            <FlatList
              extraData={this.state}
              contentContainerStyle={this.state.subjects.length==0?{flex:0.9}:null}
              showsVerticalScrollIndicator={false}
              data={this.state.subjects}
              renderItem={({ item, index }) =>
                <View
                  style={[styles.listCard, { marginTop: index == 0 ? 20 : 10, marginBottom: index == this.state.subjects.length - 1 ? 50 : null }]}>
                   <Swipeout
                   close={this.state.close}
                    backgroundColor='white'
                     right={this.state.loginType==4?null:[{component:<View style={{flex:1,justifyContent:'center'}}>
                          <Icon name='delete-forever' type='MaterialIcons' style={{alignSelf:'center',color:'white',fontSize:40}} /></View>
                          ,backgroundColor:'red',
                       onPress:()=>this.deleteTask(item._id)}]}
                       >
                       <TouchableWithoutFeedback
                       onPress={()=>this.openInfo(item)}>
                      <View style={[styles.listCol, { flexDirection: chnageLayout ? 'row' : 'row-reverse',paddingTop:5 }]}>
                        <View style={styles.listCol2}>
                          <Text style={[styles.name, { textAlign: chnageLayout ? 'left' : 'right' }]}>{item.title}</Text>
                          <View style={{ flexDirection: chnageLayout ? 'row' : 'row-reverse', alignItems: 'center', marginTop: 10, borderBottomWidth: 1.5, borderBottomColor: '#f4f4f5', paddingBottom: 10 }}>
                            <Icon name='calendar' type='Octicons' style={chnageLayout ? { paddingLeft: 5 } : { paddingRight: 5 }} />
                            <Text style={styles.activeDate}>{I18n.t('updatedOn')} </Text>
                            <Text style={styles.activeDate}>{ new Date(item.updatedDate ?item.updatedDate:item.createdDate).toLocaleDateString()}</Text>
                          </View>
                          <TouchableOpacity
                            onPress={() => Actions.Home({ page:<Task
                              subjectName={item.title}
                              blockName={item.block.title}
                              getData={this.getData}
                              studentId={item.studentId}
                              blockId={this.props.studentSubject?null:item.block._id}
                              subjectId={item._id}
                              data={{firstName: this.props.firstName, lastName: this.props.lastName, profile:this.props.pic}}/>,menu:true,back:true})}
                            style={[styles.listSubCol, { flexDirection: chnageLayout ? 'row' : 'row-reverse',paddingBottom:10 }]}>
                            <Icon name='file-document' type='MaterialCommunityIcons' style={{ fontSize: 30 }} />
                            {item.tasks.length>0?<Animated.View style={[styles.circleView,index==0?{transform:[{scale:this.bounce}]}:null,{ position: 'absolute', left: -15, top: 25 }]}>
                              <Text style={styles.circleText}>{item.tasks.length}</Text>
                            </Animated.View >:null}
                            <Text style={styles.subTitle}>{I18n.t('task')}</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                      </TouchableWithoutFeedback>
                      </Swipeout>
                </View>
              
              }
              refreshControl={
                <RefreshControl
                  colors={['#7d4094']}
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
              ListEmptyComponent={
                <NoDataFound/>
              }
              keyExtractor={(item, index) => item._id} />
          
           {this.state.loginType==4?null: <TouchableOpacity
              onPress={() => Actions.EditWorkAreasDetails({  add:true,
                getData:this.getData,
                studentSubject:this.props.studentSubject,
                blockId:this.props.blockId,
                studentId:this.props.studentId,back:true })}
              style={{
                padding: 10, backgroundColor: '#7f3f98', borderRadius: 25, width: 50, height: 50,
                alignSelf: 'flex-start', marginRight: 10, justifyContent: 'center', alignItems: 'center',
                position: 'absolute', bottom: 10, right: chnageLayout ? 10 : null, left: chnageLayout ? null : 10
              }}>
              <Icon name='add' type='MaterialIcons' style={{ fontSize: 25, color: 'white' }} />
            </TouchableOpacity>}
          </View>

        </SafeAreaView>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
  },
  loading: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 100,
    marginTop: 20
  },
  title: {
    fontSize: 35,
    marginVertical: 20,
    color: '#7f3f98'
  },
  headerBar: {
    flexDirection: 'row',
    paddingTop: 10,
    padding: 10,
    alignSelf: 'center',
    backgroundColor: 'white',
  },
  container2: {
    flex: 1,
    paddingHorizontal: 10
  },
  listCard: {
    backgroundColor: 'white',
    //paddingVertical: 5,
  },
  listCol: {
    flexDirection: 'row',
    padding: 5
  },
  imageView: {
    width: '25%',
    alignItems: 'center'
  },
  pic: {
    width: 70,
    height: 70,
    borderRadius: 35
  },
  listCol2: {
    width: '100%',
    paddingHorizontal: 15,
    paddingBottom: 5
  },
  name: {
    fontWeight: 'bold',
    fontSize: 25,
    color: '#7f4296'
  },
  activeDate: {
    fontSize: 20,
    marginLeft: 5
  },
  subTitle: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: 17
  },
  row: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 35,
    marginTop: Platform.OS == 'ios' ? 90 : 20
  },
  title: {
    fontSize: 30,
    marginTop: 5,
    color: '#7f3f98',
    fontWeight: 'bold'
  },
  listSubCol: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10
  },
  subTitle: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: 17
  },
  circleView: {
    width: 25,
    height: 25,
    backgroundColor: '#f78f5b',
    borderRadius: 13,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10
  },
  circleText: {
    color: 'white'
  },
  subHeader: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00aeeb',
    paddingVertical: 15
  },
  subHeaderText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 22
  }
});

