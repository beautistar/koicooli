import React, { Component } from 'react';
import { Text, View, Image,StyleSheet} from 'react-native';

export default class Task extends Component {
render(){
    return(
        <View style={styles.container}>
            <Image source={require('../assets/images/nodata2.png')} style={styles.image} />
            {/* <Text style={styles.text}>{this.props.message}</Text> */}
        </View>
    )
}
}
const styles = StyleSheet.create({
    container: {
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        flex:1,
        paddingBottom:20,
    },
    image:{
        width:200,
        height:200,
    },
    text:{
        fontSize:20,
         marginTop:10,
       // color:'#7d4094',
        fontWeight:'bold'
    }
})