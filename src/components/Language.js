import React, { Component } from 'react';
import { Platform, AsyncStorage } from 'react-native';
import I18n, { getLanguages } from 'react-native-i18n';
I18n.translations = {
    'en': require('../assets/translations/en'),
    'he': require('../assets/translations/hebrew'),
};

export function setAppLanguage(lng) {
    AsyncStorage.setItem('appLanguage', lng)
}
export function getAppLanguage() {
    return AsyncStorage.getItem('appLanguage').then(lng => {
        return lng
    })
}
export function setDeviceLanguage() {
    return getLanguages().then(languages => {

        if (languages[0].slice(0, 2) == 'en' || languages[0].slice(0, 2) == 'he') {
            console.log('Device @@@@@@@@@@V :', languages[0].slice(0, 2));
            AsyncStorage.setItem('deviceLanguage', languages[0].slice(0, 2))
            setAppLanguage(languages[0].slice(0, 2))
        }
        else {
            AsyncStorage.setItem('deviceLanguage', 'en')
            setAppLanguage('en')
        }
        return  languages[0].slice(0, 2) == 'he'?'he':'en'
    });
}
export function getDeviceLanguage() {
    return AsyncStorage.getItem('deviceLanguage').then(lng => {
        return lng
    })
}
export function setLoginData(data) {
    AsyncStorage.setItem('loginData',JSON.stringify(data))
}
export function getLoginData() {
    return AsyncStorage.getItem('loginData').then(loginData => {
        return JSON.parse(loginData)
    })
}
