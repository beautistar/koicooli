import React, { Component } from 'react';
import { Platform, StyleSheet, StatusBar, View, Image,TouchableOpacity} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text } from 'native-base';
import {Actions} from 'react-native-router-flux'
import WorksAreas from '../home/WorksAreas'
export default class Head extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDrawer:true
    }
  }
openDrawer(){
  if(this.state.openDrawer){
    this.props.openDrawer()
    this.setState({openDrawer:false})
  }
  else{
    this.props.closeDrawer()
    this.setState({openDrawer:true})
  }
}
  render() {
    return (
      <View style={styles.Container}>
         <Header style={[styles.header]}>
       {/* <Header style={[styles.header, { flexDirection: this.props.lng ? 'row' : 'row-reverse' }]}> */}
          <View style={{width: '15%',flexDirection:'row',justifyContent:'space-between', }}>
          {this.props.back ?  
            <TouchableOpacity onPress={this.props.onBackPress?this.props.onBackPress:()=>Actions.pop()} style={{width:'70%'}} >
              <Icon name='ios-arrow-back' type='Ionicons' style={{ fontSize: 30, color: 'white',marginLeft:10 }} />
            </TouchableOpacity>:null
          }
          </View>
          <View style={{ width:'70%' }}>
            <Title style={{ color: 'white' }}>{this.props.title}</Title>
          </View>
          <View style={{ width: '15%',justifyContent:'center',alignItems:'center' }}>
           { this.props.menu? 
            <TouchableOpacity transparent onPress={()=>this.openDrawer()} >
                <Icon name='menu' type='Entypo' style={{ fontSize: 30, color: 'white' }} />
              </TouchableOpacity>:null}
            {/* {this.props.matrix?
            <Button transparent onPress={()=>Actions.Home({page:<WorksAreas data={{firstName: 'Tutor', lastName: '', pic: require('../assets/images/profile2.jpg')}}/>})}>
              <Image source={require('../assets/icons/cubeIcon.png')} style={{ height: 30, width: 30 }} />
            </Button>:null} */}
          </View>
        </Header>
        <StatusBar backgroundColor='#7d4094' barStyle='light-content' />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    // width:'100%',
    backgroundColor: '#7d4094',
    alignItems: 'center'
  },
  Container: {
    width: '100%',
  }

});
