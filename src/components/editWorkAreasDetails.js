import React, { Component } from 'react';
import { Platform, Dimensions, StyleSheet, Alert, BackHandler, ScrollView, Text, View, TouchableOpacity, KeyboardAvoidingView, Animated, TextInput, Image, FlatList, Modal, ActivityIndicator, Keyboard } from 'react-native';
import I18n from 'react-native-i18n';
import Header from '../components/Header'
import { Icon, Radio } from 'native-base'
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../components/Layout'
import { getAppLanguage, getDeviceLanguage, getLoginData } from '../components/Language'
import WorksAreas from '../home/WorksAreas'
import Api from '../components/api'
import WorkAreasDetails from '../components/WorkAreasDetails'
export default class EditWorkAreasDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      deviceLanguage: 'en',
      appLanguage: 'en',
      changeLng: '',
      title: '',
      que1: '',
      que2: '',
      que3: '',
      que4: '',
      que5: '',
      que1Err: false,
      que2Err: false,
      que3Err: false,
      que4Err: false,
      que5Err: false,
      titleErr: false,
      blockErr: false,
      expand: [],
      loginType: '',
      block: [],
      blockName: 'Select Work area',
      tempBlock: '',
      tempBlockId: '',
      blockId: "",
      openModel: false,
      errMessage: '',
      errorHeight: new Animated.Value(0),
      errorOpacity: new Animated.Value(0),
      errorMode: false,
      sendLoading: false,
      keybord_open: false,
      changes: false
    }
    this._keyboardDidShow = this._keyboardDidShow.bind(this)
    this._keyboardDidHide = this._keyboardDidHide.bind(this)
    this.onBackPress = this.onBackPress.bind(this)
  }
  _launchError() {
    if (!this.state.errorMode) {
      Animated.sequence([
        Animated.parallel([
          Animated.timing(this.state.errorHeight, { toValue: 35, duration: 1000 }),
          Animated.timing(this.state.errorOpacity, { toValue: 1, duration: 1000 })
        ])
      ]).start();
      setTimeout(() => {
        Animated.sequence([
          Animated.parallel([
            Animated.timing(this.state.errorHeight, { toValue: 0, duration: 1000 }),
            Animated.timing(this.state.errorOpacity, { toValue: 0, duration: 1000 })
          ])
        ]).start();
      }, 5000)
    }
  }
  onBackPress() {
    if (this.state.changes) {
      Alert.alert(
        '',
        'Do You Want To Save Changes ?',
        [
          { text: I18n.t('cancel'), onPress: () => this.setState({ changes: true }) },
          { text: I18n.t('yes'), onPress: () => this.send() },
          { text: I18n.t('no'), onPress: () => this.setState({ changes: false }, () => Actions.pop()) },
        ],
        { cancelable: false }
      )
    }
    else {
      Actions.pop()
    }
  }
  componentDidMount() {
    BackHandler.addEventListener('backPress', () => {
      console.log("chnages : ", this.state.changes)
      if (this.state.changes) {
        Alert.alert(
          '',
          'Do You Want To Save Changes ?',
          [
            { text: I18n.t('cancel'), onPress: () => this.setState({ changes: true }) },
            { text: I18n.t('yes'), onPress: () => this.send() },
            { text: I18n.t('no'), onPress: () => this.setState({ changes: false }, () => Actions.pop()) },
          ],
          { cancelable: false }
        )
        return true
      }
    })
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  _keyboardDidShow() {
    console.log('Keyboard Shown');
    this.setState({ keybord_open: true })
  }

  _keyboardDidHide() {
    console.log('Keyboard Hidden');
    this.setState({ keybord_open: false })
  }
  componentWillMount() {
    console.log('sid : ', this.props)
    actionPlan = ''
    if (this.props.actionPlan)
      this.props.actionPlan.map(item => {
        actionPlan += item.title + "\n"
      })
    // this.setState({actionPlan:actionPlan})
    console.log("actionPlan : ", actionPlan)
    getDeviceLanguage().then(lng => {
      //console.log('device Langauge==========: ', lng)
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
    getLoginData().then(data => {
      let { que1, que2, que3, que4, que5, title } = this.state
      this.setState({ loginType: data.type })
      if (this.props.add)
        Api.postWithToken('/api/block-list', null, data.token).then((res) => {
          console.log('req : ', res)
          if (res.message == "success") {
            var data = []
            res.data.map((item) => {
              data[item.sequence - 1] = item
            })
            console.log('data: ', data)
            this.setState({ block: data, loading: false })
          }
        })
      else {
        this.setState({ loading: true });
        console.log("daata")
        Api.get('/api/subject-detail/' + this.props.subjectId, data.token).then((res) => {
          console.log('req : ', res)
          console.log("block name : ", res.data.block.title)
          if (res.message == "success") {
            this.workAreasName(res.data.block.title)
            this.setState({
              //blockname: res.data.block.title,
              title: res.data.title,
              que1: res.data.title1,
              que2: res.data.title2,
              que3: res.data.title3,
              que4: res.data.title4,
              que5: actionPlan,
              blockId: res.data.block._id,
              studentId: res.data.studentId,
              loading: false
            })
          }
        })
      }
    })
  }
  send() {
    let { que1, que2, que3, que4, que5, title, blockErr } = this.state
    if (!title)
      this.setState({ titleErr: true })
    if (!que1)
      this.setState({ que1Err: true })
    if (!que2)
      this.setState({ que2Err: true })
    if (!que3)
      this.setState({ que3Err: true })
    if (!que4)
      this.setState({ que4Err: true })
    if (!que5)
      this.setState({ que5Err: true })
    if (!this.state.blockId && !this.props.blockId || this.state.block == 'Select Work area') {
      this.setState({ sendLoading: false, blockErr: true, errMessage: 'Please Select Work area!' })
      this._launchError()
    }
    console.log("data : ", que5.split('\n'))
    if (title, que1, que2, que3, que4, que5, !blockErr) {
      this.setState({ sendLoading: true, changes: false })
      //alert("hii")
      getLoginData().then(data => {
        // console.log("token : ",data)
        params = {
          title: title,
          title1: que1,
          title2: que2,
          title3: que3,
          title4: que4,
          title5: que5,
          block: this.props.blockId ? this.props.blockId : this.state.blockId,
          studentId: this.state.studentId ? this.state.studentId : this.props.studentId
        }
        if (this.props.add)
          Api.postWithToken('/api/subject', params, data.token)
            .then((res) => {
              console.log('add responce  : ', res)
              if (res.message == 'success') {
                this.setState({ sendLoading: false })
                que5.split('\n').map(task => {
                  Api.postWithToken('/api/subject-task',
                    {
                      title: task,
                      blockId: this.props.blockId ? this.props.blockId : this.state.blockId,
                      studentId: this.state.studentId ? this.state.studentId : this.props.studentId,
                      subjectId: res.data._id,
                      followUpId: data.user_id,
                    }
                    , data.token)
                    .then((res) => {
                      console.log('add responce  : ', res)
                      if (res.message == 'success') {
                        var params = this.props.studentSubject ? {
                          studentId: this.props.studentId
                        } : { block: this.props.blockId ? this.props.blockId : this.state.blockId, studentId: this.props.studentId }
                        this.props.getData(params, null, true)
                        Actions.pop()
                      }
                    })
                })


              }
              else {
                this.setState({ sendLoading: false, errMessage: res.message })
                this._launchError()
              }
            })
            .catch(e => {
              console.log('Error', e)
              Alert.alert(
                'Erorr',
                'Server not reponding Please try again later ',
                [
                  { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
              )
            })
        else
          Api.put('/api/subject-update/' + this.props.subjectId, params, data.token)
            .then((res) => {
              console.log('req : ', res)
              if (res.message == 'Updated SuccessFully') {
                this.setState({ sendLoading: false })
                //console.log("subject :   ", this.props.studentSubject)
                var params = this.props.studentSubject ? {
                  studentId: this.props.studentId
                } : { block: this.props.blockId ? this.props.blockId : this.state.blockId, studentId: this.props.studentId }
                this.props.getData(params)
                Actions.pop()
              }
              else {
                this.setState({ sendLoading: false, errMessage: res.message })
                this._launchError()
              }
            })
            .catch(e => {
              console.log('Error', e)
              Alert.alert(
                'Erorr',
                'Server not reponding Please try again later ',
                [
                  { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
              )
            })
      })
    }
  }
  workAreasName(name) {
    var areaname=''
    if (name == "Areas Of Growth")
    areaname=I18n.t('areasForGrowth')
    else if (name ==  "Areas Of Strength")
    areaname= I18n.t('areasOfStrength')
    else if (name == "Areas Of Interest")
    areaname= I18n.t('areasOfInterest')
    else if (name == "Intra Personal")
    areaname= I18n.t('intraPersonal')
    else if (name == "Inter Personal")
    areaname=I18n.t('interPersonal')
    else if (name == "Cognitive")
    areaname=I18n.t('cognitive')
    else if (name == "Body and movement")
    areaname=I18n.t('bodyAndMovement')
    else if (name == "Community")
    areaname=I18n.t('community')
    else if (name =="Presentation Skills")
    areaname=I18n.t('presentationSkills')
    this.setState({blockname:areaname})
  }
  renderTextBox(name, value, onPress, error, text) {
    // console.log('expands : ',!this.state.expand.includes(name))
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.colView}>
        <View style={[styles.inputView, { flexDirection: chnageLayout ? 'row' : 'row-reverse', borderColor: error ? 'red' : '#cacbcc' }]}>
          <View style={[styles.leftLine, chnageLayout ? { marginRight: 5 } : { marginLeft: 5 }]} />
          <View style={styles.inputRowView}>
            <Text style={{ textAlign: chnageLayout ? 'left' : 'right', color: 'black' }}>{I18n.t(name)}</Text>
            <View style={{ width: '100%', flexDirection: chnageLayout ? 'row' : 'row-reverse', alignItems: 'center' }}>
              <View style={{ width: '93%' }}>
                {text ?
                  <TouchableOpacity onPress={onPress}>
                    <Text >{value}</Text>
                  </TouchableOpacity> :
                  <TextInput style={[styles.inputBox, { textAlign: chnageLayout ? 'left' : 'right' }]}
                    onChangeText={onPress}
                    multiline={!this.state.expand.includes(name)}
                    value={value}
                    editable={this.state.loginType == 3}
                  />}
              </View>
              <TouchableOpacity
                onPress={() => this.setState(state => (this.state.expand.includes(name) ? this.state.expand.splice(this.state.expand.indexOf(name), 1) : this.state.expand.push(name), state))}>
                <Icon name={this.state.expand.includes(name) ? 'chevron-small-down' : chnageLayout ? 'chevron-small-right' : 'chevron-small-left'} type='Entypo' style={{ color: '#b3b6b9', fontSize: 40 }} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    )
  }
  reabderModal() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.state.openModel}
        onRequestClose={() => { }}>

        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
          <View style={{ backgroundColor: 'white', width: '90%', height: Dimensions.get('window').height / 1.5, borderRadius: 5 }}>
            <View style={{ padding: 10, borderBottomWidth: 0.5 }}>
              <Text style={{ fontWeight: 'bold', fontSize: 22 }}>Select Block</Text>
            </View>
            <FlatList
              extraData={this.state}
              data={this.state.block}
              renderItem={({ item }) =>
                <View style={{ marginHorizontal: 15, paddingVertical: 5, flexDirection: 'row', alignItems: 'center' }}>
                  <Radio
                    selectedColor='#7d4094'
                    selected={item._id == this.state.tempBlockId}
                    onPress={() => this.setState({ tempBlockId: item._id, tempBlock: item.title })} />
                  <TouchableOpacity style={{ margin: 10 }} onPress={() => this.setState({ tempBlockId: item._id, tempBlock: item.title })}>
                    <Text style={{ fontSize: 18 }}>{item.title}</Text>
                  </TouchableOpacity>
                </View>
              }
              keyExtractor={(item) => item._id}
            />
            <View style={{ padding: 10, borderTopWidth: 0.5, alignItems: 'flex-end' }}>
              <View style={{ flexDirection: 'row', width: '60%', justifyContent: 'space-around' }}>
                <TouchableOpacity
                  onPress={() => this.setState({ openModel: false, tempBlockId: this.state.blockId })} >
                  <Text style={{ padding: 5, color: '#7d4094', fontWeight: 'bold', fontSize: 20 }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.setState({ openModel: false, blockErr: false, blockName: this.state.tempBlock, blockId: this.state.tempBlockId })} >
                  <Text style={{ padding: 5, color: '#7d4094', fontWeight: 'bold', fontSize: 20 }}>Ok</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

        </View>

      </Modal>
    )
  }
  render() {
    console.log('expand ', this.state.expand)
    I18n.locale = this.state.appLanguage
    console.log("layout : ", setLayout(this.state.deviceLanguage, this.state.appLanguage))
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    if (this.state.loading)
      return (
        <View style={styles.loading}>
          <ActivityIndicator size='large' color='#7f3f98' />
        </View>
      )
    return (
      <View style={styles.container}>
        <Header
          title={this.props.headerTitle ? this.props.headerTitle : 'KolCooli'}
          lng={chnageLayout} matrix={this.props.matrix}
          back={this.props.back}
          menu={this.props.menu}
          onBackPress={() => this.onBackPress()}
        />
        {this.props.add ? null : <View style={styles.subHeader}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ width: '90%', alignItems: 'center', paddingLeft: '10%' }}>
              <Text style={styles.subHeaderText}>{this.state.loginType == 4 ? 'My' : this.props.firstName + " " + this.props.lastName} </Text>
              <Text style={styles.subHeaderText}>{this.state.blockname}</Text>
            </View>
            {this.state.loginType == 4 ? null : <TouchableOpacity onPress={() => Actions.Home({
              page: <WorksAreas data={{ firstName: this.props.firstName, lastName: this.props.lastName, profile: this.props.pic }}
                studentId={this.props.studentId}
                blockId={this.props.blockId}
              />, menu: true, back: true
            })}>
              <Image source={require('../assets/icons/cubeIcon.png')} style={{ height: 30, width: 30 }} />
            </TouchableOpacity>}
          </View>
        </View>}
        <Animated.View style={{ width: '100%', height: this.state.errorHeight, opacity: this.state.errorOpacity, backgroundColor: '#d91e18', justifyContent: 'center', alignItems: 'center', padding: 10, position: 'absolute', zIndex: 1 }}>
          <Text style={{ color: 'white', fontSize: 15 }}>{this.state.errMessage}</Text>
        </Animated.View>


        <View style={[styles.subContainer]}>
          {/* <View style={styles.row}>
              <Text style={styles.title}>{this.props.subTitle}</Text>
            </View> */}

          <ScrollView
            contentContainerStyle={{ paddingBottom: this.state.keybord_open ? Platform.OS == 'ios' ? 240 : 50 : 0 }}
            showsVerticalScrollIndicator={false}
          >
            {this.props.studentSubject && this.props.add ? this.renderTextBox("worksAreas", this.state.blockName, () => this.setState({ openModel: true }), this.state.blockErr, true) : null}
            {this.renderTextBox("title", this.state.title, (value) => this.setState({ title: value, titleErr: value ? false : true, changes: true }), this.state.titleErr)}
            {this.renderTextBox("que1", this.state.que1, (value) => this.setState({ que1: value, que1Err: value ? false : true, changes: true }), this.state.que1Err)}
            {this.renderTextBox("que2", this.state.que2, (value) => this.setState({ que2: value, que2Err: value ? false : true, changes: true }), this.state.que2Err)}
            {this.renderTextBox("que3", this.state.que3, (value) => this.setState({ que3: value, que3Err: value ? false : true, changes: true }), this.state.que3Err)}
            {this.renderTextBox("que4", this.state.que4, (value) => this.setState({ que4: value, que4Err: value ? false : true, changes: true }), this.state.que4Err)}
            {this.renderTextBox("que5", this.state.que5, (value) => this.setState({ que5: value, que5Err: value ? false : true, changes: true }), this.state.que5Err)}

            <TouchableOpacity style={styles.buttonView}
              disabled={this.state.loginType == 4}
              onPress={() => this.send()}>
              {this.state.sendLoading ?
                <ActivityIndicator size='large' color='white' /> :
                <Text style={styles.buttonText}>{I18n.t('save')}</Text>}
            </TouchableOpacity>
          </ScrollView>
        </View>


        {this.reabderModal()}
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c5eaf9',

  },
  loading: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginTop: Platform.OS == 'ios' ? 20 : 10
  },
  title: {
    fontSize: 25,
    marginVertical: 5,
    color: '#7f3f98',
    fontWeight: 'bold'
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    margin: 15,
    marginTop: 25,
  },
  row: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5
  },
  buttonView: {
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f78c54',
    marginTop: 5
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 28
  },
  inputView: {
    width: '100%',
    borderBottomWidth: 1,
    //marginTop:10,
  },
  leftLine: {
    borderLeftWidth: 3,
    borderLeftColor: '#00b2ec',
    height: 45,
  },
  inputRowView: {
    width: '95%'
  },
  inputRowView2: {
    width: Platform.OS == 'ios' ? '87%' : '89%'
  },
  inputBox: {
    //height: 40
  },
  colView: {
    //margin:40,
    width: '85%',
    alignSelf: 'center',
    marginTop: 15
  },
  errorMsg: {
    fontWeight: 'bold',
    color: 'red',
    marginTop: 5
  },
  subHeader: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00aeeb',
    paddingVertical: 15
  },
  subHeaderText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 22
  }

});
