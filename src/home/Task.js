import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image,RefreshControl,FlatList, ActivityIndicator, ScrollView,TouchableOpacity,TouchableWithoutFeedback,Animated} from 'react-native';
import Api from '../components/api'
import Header from '../components/Header'
import I18n from 'react-native-i18n';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
const defaultPic = require('../assets/images/logo.png')
import { setLayout } from '../components/Layout'
import TasksDetails from '../home/TaskDetails'
import WorksAreas from '../home/WorksAreas'
import NoDataFound from '../components/NoDataFound'
import Swipeout from 'react-native-swipeout';
import { getAppLanguage, getDeviceLanguage,getLoginData } from '../components/Language'
const defaultProfile = require('../assets/images/blank-profile.png')
export default class Task extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: '',
      appLanguage:'',
      loading: true,
      revers: false,
      tasks: [
        // { tid: 1, title: 'Find a used guitar', updatedDate: new Date() },
        // { tid: 2, title: 'Task #2', ç: new Date() },
        // { tid: 3, title: 'Task #3', updatedDate: new Date() },
        // { tid: 4, title: 'לחפש גיטרה יד 2', updatedDate: new Date() }
      ],
      loginType:'',
      close:false,
      openModel:false,
      errMessage: '',
      errorHeight: new Animated.Value(0),
      errorOpacity: new Animated.Value(0),
      errorMode: false,
      refreshing:false,
      blockname:'',
    }
    this.getTaskData=this.getTaskData.bind(this)
  }
  _launchError() {
    if (!this.state.errorMode) {
      Animated.sequence([
        Animated.parallel([
          Animated.timing(this.state.errorHeight, { toValue: 35, duration: 1000 }),
          Animated.timing(this.state.errorOpacity, { toValue: 1, duration: 1000 })
        ])
      ]).start();
      setTimeout(() => {
        Animated.sequence([
          Animated.parallel([
            Animated.timing(this.state.errorHeight, { toValue: 0, duration: 1000 }),
            Animated.timing(this.state.errorOpacity, { toValue: 0, duration: 1000 })
          ])
        ]).start();
      }, 5000)
    }
  }
  _onRefresh = () => {
    this.setState({refreshing: true});
    var params=this.props.allTask?{studentId:this.props.studentId}:{studentId:this.props.studentId,subjectId:this.props.subjectId}
    this.getTaskData(params,true)
  }
  getTaskData(params,refresh,update){
    this.setState({loading:refresh?false:true})
      getLoginData().then(data=>{
        this.setState({loginType:data.type})
        Api.postWithToken('/api/subject-task-list',params,data.token).then((res)=>{
          console.log('req : ',res)
          if(res.message=="success"){ 
            this.setState({tasks:res.data,refreshing:false})
          }
          else{
            this.setState({tasks:[],refreshing:false})
          }
          this.setState({loading:false})
        })
    })
    if(!update){
      this.props.getStudents?this.props.getStudents():null
      this.props.getData?this.props.getData({studentId:this.props.studentId,subjectId:this.props.subjectId,block:this.props.blockId}):null
    }
  }
  workAreasName(name) {
    console.log("name---------: ",name)
    var areaname=''
    if (name == "Areas Of Growth")
    areaname=I18n.t('areasForGrowth')
    else if (name ==  "Areas Of Strength")
    areaname= I18n.t('areasOfStrength')
    else if (name == "Areas Of Interest")
    areaname= I18n.t('areasOfInterest')
    else if (name == "Intra Personal")
    areaname= I18n.t('intraPersonal')
    else if (name == "Inter Personal")
    areaname=I18n.t('interPersonal')
    else if (name == "Cognitive")
    areaname=I18n.t('cognitive')
    else if (name == "Body and movement")
    areaname=I18n.t('bodyAndMovement')
    else if (name == "Community")
    areaname=I18n.t('community')
    else if (name =="Presentation Skills")
    areaname=I18n.t('presentationSkills')
    console.log("work are name : ",areaname)
    //this.setState({blockname:areaname})
    return areaname
  }
  componentWillMount() {
   
    console.log('data ======= : ',this.props)
    
    getDeviceLanguage().then(lng => {
      console.log('device Langauge: ', lng)
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => {
      console.log('APP Langauge: ', lng)
      this.setState({ appLanguage: lng })
    })
    var params=this.props.allTask?{studentId:this.props.studentId}:{studentId:this.props.studentId,subjectId:this.props.subjectId}
    this.getTaskData(params,false,true)
  }
  deleteTask(taskId){
    //alert(taskId)
    getLoginData().then(data=>{
      this.setState({loginType:data.type,close:true})
      Api.delete('/api/subject-task-delete/'+taskId,null,data.token).then((res)=>{
        console.log('req : ',res)
        if(res.message=="success"){ 
          var params=this.props.allTask?{studentId:this.props.studentId}:{studentId:this.props.studentId,subjectId:this.props.subjectId}
          this.getTaskData(params)
          this.setState({loading:true,tasks:[]})
        }
        else{
          this.setState({loading:false,errMessage:res.message})
          this._launchError()
        }
      })
  })
  }
  componentDidMount(){
    
  }
  openInfo(item){
    console.log("item : ",item)
    this.setState({close:true})
    Actions.TasksDetails({
      subjectNam:this.props.subjectName,
      blockName:this.props.blockName,
      editTask:this.state.loginType==4?true:false,
      allTask:this.props.allTask,
      getTaskData:this.getTaskData,
      data:[item],
      subjectTaskId:item._id,
      studentId:this.props.studentId, 
      pic:this.props.pic?this.props.pic:this.props.data.profile,
      title:item.title, 
      subjectId:this.props.subjectId,
      blockId:this.props.blockId,
      loginType:this.state.loginType, 
      name:this.props.data.firstName+' '+this.props.data.lastName,
      firstName:this.props.data.firstName, 
      lastName:this.props.data.lastName,back:true})
  }
  render() {
    console.log("applagauge : ",this.state.appLanguage)
    I18n.locale = this.state.appLanguage;
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    let name=this.props.name?this.props.name:(this.props.data.firstName+" "+this.props.data.lastName)
    let userpic=this.props.pic?this.props.pic:this.props.data.profile
    if (this.state.loading)
      return (
        <View style={styles.loading}>
          <ActivityIndicator size='large' color='#7f3f98' />
        </View>
      )
    else
       
      return (
        <View style={styles.container}>
          <View style={styles.subHeader}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
            <View style={{width:this.state.loginType==4?'100%':'90%',alignItems:'center',paddingLeft:this.state.loginType==4?0:'10%'}}>
              <Text style={styles.subHeaderText}>{this.state.loginType==4?I18n.t("myTasks"):name+" "+I18n.t("task")}</Text>
              {this.props.blockName?<Text style={styles.subHeaderText2}>{this.workAreasName(this.props.blockName)}</Text>:null}
              {this.props.subjectName?<Text style={styles.subHeaderText2}>{this.props.subjectName}</Text>:null}
              </View>
             {this.state.loginType==4?null:<TouchableOpacity  onPress={()=>Actions.Home({page:<WorksAreas studentId={this.props.studentId} data={{firstName: this.props.data.firstName, lastName: this.props.data.lastName, profile: userpic}}/>,menu:true,back:true})}>
              <Image source={require('../assets/icons/cubeIcon.png')} style={{ height: 30, width: 30 }} />
            </TouchableOpacity>}
            </View>
          </View>
          {this.state.tasks.length==0?
           <NoDataFound message='No Tasks Found !'/>
         :
          <ScrollView>
           {/* {this.state.loginType==4?null: <View style={styles.row}>
               <Image source={userpic?{uri:userpic}:defaultProfile} style={styles.logo} /> */}
               {/* <Image source={defaultProfile} style={styles.logo} /> */}
              {/* <Text style={styles.title}>{name}</Text>
            </View>} */}
            <Animated.View style={{ width: '100%', height: this.state.errorHeight, opacity: this.state.errorOpacity, backgroundColor: '#d91e18', justifyContent: 'center', alignItems: 'center', padding: 10,position:'absolute',zIndex:1}}>
            <Text style={{ color: 'white', fontSize: 15 }}>{this.state.errMessage}</Text>
          </Animated.View>
            <View style={styles.container2}>
              <FlatList
                extraData={this.state}
                showsVerticalScrollIndicator={false}
                data={this.state.tasks}
                renderItem={({ item, index }) =>
                <View
                  style={[styles.listCard, { marginTop: index == 0 ? 20 : 10, marginBottom: index == this.state.tasks.length - 1 ? 50 : null }]}>
                    <Swipeout
                    close={this.state.close}
                    sensitivity={20}
                    backgroundColor='white'
                     right={[{component:<View style={{flex:1,justifyContent:'center'}}>
                       <Icon name='delete-forever' type='MaterialIcons' style={{alignSelf:'center',color:'white',fontSize:35}} /></View>
                       ,backgroundColor:'red',
                       onPress:()=>this.deleteTask(item._id)}]}>
                    <TouchableWithoutFeedback onPress={()=>this.openInfo(item)}>
                    <View style={[styles.listCol, { flexDirection: chnageLayout ? 'row' : 'row-reverse',paddingTop:5 }]}>
                      <View style={styles.listCol2}>
                        <Text style={[styles.name, { textAlign: chnageLayout ? 'left' : 'right' }]}>{item.title}</Text>
                        <View style={{ flexDirection: chnageLayout ? 'row' : 'row-reverse', alignItems: 'center', marginTop: 10,paddingBottom:5 }}>
                          <Icon name='calendar' type='Octicons' style={chnageLayout ? { paddingLeft: 5 } : { paddingRight: 5 }} />
                          <Text style={styles.activeDate}>{I18n.t('updatedOn')} </Text>
                          <Text style={styles.activeDate}>{new Date(item.updatedDate ?item.updatedDate:item.createdDate).toLocaleDateString()}</Text>
                        </View>
                      </View>
                    </View>
                    </TouchableWithoutFeedback>
                    </Swipeout>
                  </View>
                }
                refreshControl={
                  <RefreshControl
                    colors={['#7d4094']}
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
                keyExtractor={(item, index) => item._id} />
            </View>
           
          </ScrollView>
          }
         {    this.state.loginType==4 ||!this.props.allTask && this.props.blockId?<TouchableOpacity
              onPress={() => Actions.TasksDetails({ 
                allTask:this.props.allTask, 
                getTaskData:this.getTaskData,
                studentId:this.props.studentId,
                subjectId:this.props.subjectId,
                blockId:this.props.blockId,
                pic:this.props.pic?this.props.pic:this.props.data.pic,  
                add:true,
                firstName:this.props.data.firstName, 
                lastName:this.props.data.lastName,back:true })}
              style={{
                padding: 10, backgroundColor: '#7f3f98', borderRadius: 25, width: 50, height: 50,
                alignSelf: 'flex-start', marginRight: 10, justifyContent: 'center', alignItems: 'center',
                position: 'absolute', bottom: 10, right: chnageLayout ? 10 : null, left: chnageLayout ? null : 10
              }}>
              <Icon name='add' type='MaterialIcons' style={{ fontSize: 25, color: 'white' }} />
            </TouchableOpacity>:null}
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
  },
  loading: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 35,
    marginVertical: 20,
    color: '#7f3f98'
  },
  headerBar: {
    flexDirection: 'row',
    paddingTop: 10,
    padding: 10,
    alignSelf: 'center',
    backgroundColor: 'white',
  },
  container2: {
    flex: 1,
    paddingHorizontal: 10
  },
  listCard: {
    backgroundColor: 'white',
    //paddingVertical: 5,
  },
  listCol: {
    flexDirection: 'row',
    padding: 5
  },
  imageView: {
    width: '25%',
    alignItems: 'center'
  },
  pic: {
    width: 70,
    height: 70,
    borderRadius: 35
  },
  listCol2: {
    width: '100%',
    paddingHorizontal: 15,
    paddingBottom: 5
  },
  name: {
    fontWeight: 'bold',
    fontSize: 25,
    color: '#7f4296'
  },
  activeDate: {
    fontSize: 20,
    marginTop: 5
  },
  subTitle: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: 17
  },
  row: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginTop: Platform.OS == 'ios' ? 40 : 20
  },
  title: {
    fontSize: 30,
    marginTop: 5,
    color: '#7f3f98',
    fontWeight: 'bold'
  },
  subHeader: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00aeeb',
    paddingVertical: 15
  },
  subHeaderText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 22
  },
  subHeaderText2: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18
  }
});
