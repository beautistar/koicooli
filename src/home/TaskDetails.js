import React, { Component } from 'react';
import { Platform, StyleSheet, SafeAreaView,BackHandler, Alert,ScrollView, DatePickerIOS, Text, Animated,KeyboardAvoidingView, View, TouchableOpacity, TextInput, Image, Modal,ActivityIndicator, Dimensions, DatePickerAndroid, Keyboard, FlatList } from 'react-native';
import I18n from 'react-native-i18n';
import Header from '../components/Header'
import { Icon, CheckBox ,Radio} from 'native-base'
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../components/Layout'
import { getAppLanguage, getDeviceLanguage, getLoginData } from '../components/Language'
import WorksAreas from '../home/WorksAreas'
const happy = require("../assets/icons/happy.png")
const natural = require("../assets/icons/natural.png")
const sad = require("../assets/icons/sad.png")
import { Dropdown } from 'react-native-material-dropdown';
//import Carousel from "react-native-carousel-control";
import Carousel from 'react-native-snap-carousel';
import Api from '../components/api'
import moment from 'moment'
//import Slider from "react-native-slider";
import Slider from '../lib/index'
const SCREEN_WIDTH = 250;

export default class TasksDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      deviceLanguage: 'en',
      appLanguage: 'en',
      changeLng: '',
      taskName: '',
      status:0,
      que1: '',
      que2: '',
      selectDate:false,
      que3: new Date(),
      tutorName: 'Select Tutor',
      tempDate: new Date(),
      taskNameErr: false,
      que1Err: false,
      que2Err: false,
      que3Err: false,
      tutorNameErr: false,
      completed: false,
      expand: [],
      loginType: '',
      open: false,
      color:'#d91e18',
      openDatePicker: false,
      tasks: [],
      tutors:[],
      tempTutorId:'',
      tutorId:'',
      tempTutorName:'',
      currentPage: 0,
      blockId:'',
      subjectId:'',
      errMessage: '',
      errorHeight: new Animated.Value(0),
      errorOpacity: new Animated.Value(0),
      errorMode: false,
      sendLoading:false,
      changes:false,
      sliding:'',
    }
    this.xOffset = new Animated.Value(0);
    this.Left = new Animated.Value(0);
    this.Right = new Animated.Value(0);
    this.onBackPress=this.onBackPress.bind(this)
  }
  _launchError() {
    if (!this.state.errorMode) {
      Animated.sequence([
        Animated.parallel([
          Animated.timing(this.state.errorHeight, { toValue: 35, duration: 1000 }),
          Animated.timing(this.state.errorOpacity, { toValue: 1, duration: 1000 })
        ])
      ]).start();
      setTimeout(() => {
        Animated.sequence([
          Animated.parallel([
            Animated.timing(this.state.errorHeight, { toValue: 0, duration: 1000 }),
            Animated.timing(this.state.errorOpacity, { toValue: 0, duration: 1000 })
          ])
        ]).start();
      }, 5000)
    }
  }
  onBackPress(){
    if(this.state.changes){
      Alert.alert(
        '',
        'Do You Want To Save Changes ?',
        [
          {text: I18n.t('cancel'), onPress: () =>this.setState({changes:true})},
          {text: I18n.t('yes'), onPress: () => this.send()},
          {text: I18n.t('no'), onPress: () =>this.setState({changes:false},()=>Actions.pop())},
        ],
        { cancelable: false }
      )
    }
    else{
      Actions.pop()
    }
  }
  componentDidMount(){
    BackHandler.addEventListener('backPress', ()=> {
    console.log("chnages : ",this.state.changes)
      if(this.state.changes){
        Alert.alert(
          '',
          'Do You Want To Save Changes ?',
          [
            {text: I18n.t('cancel'), onPress: () =>this.setState({changes:true})},
            {text: I18n.t('yes'), onPress: () => this.send()},
            {text: I18n.t('no'), onPress: () =>this.setState({changes:false},()=>Actions.pop())},
          ],
          { cancelable: false }
        )
        return true
      }
    })
  }
  componentWillMount() {
   console.log('data ======= : ',this.props.data)
   data=[],
   this.props.data?this.props.data.map((item,index)=>{
        data[index]={
          taskId:item._id,
          taskName:item.title,
          que1:item.questionOne,
          que2:item.questionTwo,
          que3:item.questionThree,
          status:item.taskStatus,
          tempStatus:item.taskStatus,
          studentOpinion:item.studentOpinion,
          tutorName:item.followUpId?item.followUpId.firstName+' '+item.followUpId.lastName:null,
          tutorId:item.followUpId?item.followUpId._id:null,
          block:item.blockId._id,
          subjectId:item.subjectId._id,
          studentId:item.studentId._id,
          taskNameErr:false,
          que1Err:false,
          que2Err:false,

        }
    }): 
    getLoginData().then(data=>{
   //   console.log("data ------------- : ",data)
      this.setState({
        tutorName:data.firstName+" "+data.lastName,
              tempTutorId:data.user_id,
              tutorId:data.user_id,
      }) 
    })
    this.setState({tasks:data})
   // console.log("data :============== ",data)
    getDeviceLanguage().then(lng => {
     // console.log('device Langauge==========: ', lng)
      this.setState({ deviceLanguage: lng })
    })
 
    getLoginData().then(data => 
      {
            this.getTasks()
            Api.postWithToken('/api/user-all-tutor',{instituteId:data.instituteId},data.token).then((res)=>{
              //console.log('req : ',res)
              if(res.message=="success"){ 
                var data=[]
                res.data.map((item,index)=>{
                    data[index]={tid:item._id,name:item.firstName+' '+item.lastName}
                })
               // console.log('data: ',data)
                this.setState({tutors:data,loading:false})
              }
            })
        this.setState({ loginType: data.type })
      }
    )
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
  }
  getTasks(){
    getLoginData().then(data => 
      {
          console.log("task id : ",this.props.subjectTaskId)
            Api.postWithToken('/api/subject-task-phase-list',{subjectTaskId:this.props.subjectTaskId},data.token).then((res)=>{
              console.log('req : ',res)
              if(res.message=="success"){ 
                var data=[]
                res.data.map((item,index)=>{
                  data[index]={
                    taskId:item._id,
                    taskName:item.title,
                    que1:item.questionOne,
                    que2:item.questionTwo,
                    que3:item.questionThree,
                    status:item.taskStatus,
                    tempStatus:item.taskStatus,
                    studentOpinion:item.studentOpinion,
                    tutorName:item.followUpId?item.followUpId.firstName+' '+item.followUpId.lastName:null,
                    tutorId:item.followUpId?item.followUpId._id:this.state.tutorId,
                    block:item.blockId._id,
                    subjectId:item.subjectId._id,
                    studentId:item.studentId._id,
                    taskNameErr:false,
                    que1Err:false,
                    que2Err:false,
          
                  }
                })
              // console.log('data: ',data)
                this.setState({tasks:data.reverse(),loading:false})
              }
              else{

              }
            })
      })
  }
  send() {
    //console.log('date validete :',moment().isBefore(this.state.que3))
    let { que1, que2, selectDate, tutorName, status, taskName,tutorId } = this.state
    if(this.props.add){
      if (!taskName)
      this.setState({ taskNameErr: true ,sendLoading:false})
    if (!que1)
      this.setState({ que1Err: true,sendLoading:false })
    if (!que2)
      this.setState({ que2Err: true,sendLoading:false })
    if (!selectDate)
    { this.setState({ errMessage: 'Please Select Milestone Date !' ,sendLoading:false,que3Err:true})
    this._launchError()}
    if( moment().isAfter(this.state.que3))
    { this.setState({ errMessage: 'Please Select Valid Milestone Date !' ,sendLoading:false,que3Err:true})
    this._launchError()}
    if (tutorName =='Select Tutor') 
      this.setState({ tutorNameErr: true ,sendLoading:false})
    if (status==0)
     { this.setState({ errMessage: 'Please Select Progress Status!' })
     this._launchError()}
    if(que1 && que2 && selectDate && tutorName && tutorId && status!=0 && taskName &&  moment().isBefore(this.state.que3) ){
      this.setState({sendLoading:true,changes:true})
      getLoginData().then(data=>{
        //console.log("token : ",data)
           params={
            title:this.state.taskName,
            questionOne:this.state.que1,
            questionTwo:this.state.que2,
            questionThree:this.state.que3,
            taskStatus:1,
            studentOpinion:this.state.status,
            followUpId:this.state.tutorId,
            subjectId:this.props.subjectId?this.props.subjectId:this.state.subjectId,
            blockId:this.props.blockId?this.props.blockId:this.state.blockId,
            studentId:this.props.studentId
           }
          
           Api.postWithToken('/api/subject-task',params,data.token)
           .then((res)=>{
            // console.log('add responce  : ',res)
             if(res.message=='success'){
               this.setState({sendLoading:false})
              
              var params=this.props.allTask?{studentId:this.props.studentId}:{studentId:this.props.studentId,subjectId:this.props.subjectId}
              
              this.props.getTaskData(params)
                Actions.pop()
             }
             else{
               this.setState({sendLoading:false,errMessage:res.message})
               this._launchError()
             }
           })
         
        })
      }
    }
   else{
      this._scroll.scrollTo({x:0,y:0,animated:true})
      var index=this.state.currentPage
       // console.log("date : ",moment().isBefore(this.state.tasks[index].que3))
      if (!this.state.tasks[index].que1)
        this.setState(state=>( this.state.tasks[index].que1Err=true,state))
      if (!this.state.tasks[index].que2)
        this.setState(state=>( this.state.tasks[index].que2Err=true,state))
      if (this.state.tasks[index].studentOpinion==0)
       { this.setState({ errMessage: 'Please Select Progress Status!',color:'#d91e18' })
       this._launchError()}
       if (!moment().isBefore(this.state.tasks[index].que3))
       { this.setState({ errMessage: 'Please Select Valid Milestone Date !',color:'#d91e18' })
       this._launchError()}
       
       this.setState({sendLoading:false})
      if(this.state.tasks[index].que1 && this.state.tasks[index].que2 && this.state.tasks[index].studentOpinion >0 && moment().isBefore(this.state.tasks[index].que3))
        getLoginData().then(data=>{
          this.setState({sendLoading:true,changes:false})
          console.log("data: ",this.props.subjectTaskId)
             params={
              subjectTaskId:this.props.subjectTaskId,
              title:this.state.tasks[index].taskName,
              questionOne:this.state.tasks[index].que1,
              questionTwo:this.state.tasks[index].que2,
              questionThree:this.state.tasks[index].que3,
              taskStatus:this.state.tasks[this.state.currentPage].tempStatus,
              studentOpinion:this.state.tasks[index].studentOpinion,
              followUpId:this.state.tasks[index].tutorId,
              subjectId:this.state.tasks[index].subjectId,
              blockId:this.state.tasks[index].block,
              studentId:this.state.tasks[index].studentId,
              //updateFollowUp:true
             }
             Api.postWithToken('/api/subject-task-phase/',params,data.token)
              .then((res)=>{
                console.log('task phase responce  : ',res)
                if(res.message=='success'){
                  this.getTasks()
                  tasks=this.state.tasks
                  tasks.push(data)
                  this.setState({tasks:tasks})
                 this.setState((state)=>(this.state.tasks[this.state.currentPage].status=this.state.tasks[this.state.currentPage].tempStatus,state))
                  this._scroll.scrollTo({x: 0, y: 0, animated: true})
                  this.setState({sendLoading:false,errMessage:'Successfully Update',color:'green'})
                  this._launchError()
                  var params=this.props.allTask?{studentId:this.props.studentId}:{studentId:this.props.studentId,subjectId:this.props.subjectId}
                  this.props.getTaskData(params)
                
                }
                else{
                  this.setState({sendLoading:false,errMessage:res.message,color:'#d91e18'})
                  this._launchError()
                }
              })
              
          })
      }
  }
  async openDatePicker() {
    Keyboard.dismiss()
    if (Platform.OS == 'ios') {
      this.setState({ openDatePicker: !this.state.openDatePicker })
    }
    else {
      try {
        const { action, year, month, day } = await DatePickerAndroid.open({
          date: new Date()
        });
        if (action !== DatePickerAndroid.dismissedAction) {
          var index=this.state.currentPage
          // Selected year, month (0-11), day
          console.log('date : ', year + "-" + month + "-" + day)
        this.props.add?this.setState({ que3: new Date(year ,month,day), que3Err: false ,selectDate:true,changes:true}):
        this.setState((state)=>(this.state.tasks[index].que3=new Date(year ,month,day),this.state.tasks[index].que3Err=false,this.state.changes=true,state))
        }
      } catch ({ code, message }) {
        console.warn('Cannot open date picker', message);
      }
    }
  }
  renderTextBox(name, value, onPress, error, dropdown, focus,editable) {
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.colView}>
        <View style={[styles.inputView, { flexDirection: chnageLayout ? 'row' : 'row-reverse', borderColor: error ? 'red' : '#cacbcc' }]}>
          <View style={[styles.leftLine, chnageLayout ? { marginRight: 5 } : { marginLeft: 5 }]} />
          <View style={styles.inputRowView}>
            <Text style={{ textAlign: chnageLayout ? 'left' : 'right', color: 'black' }}>{I18n.t(name)}</Text>
            <View style={{ width: '100%', flexDirection: chnageLayout ? 'row' : 'row-reverse', alignItems: 'center' }}>
              <View style={[{ width: '95%', marginTop: -10 }]}
              >
                {
                  dropdown ?
                    <Text style={{ marginTop: 10, height: 35, textAlign: chnageLayout ? 'left' : 'right',color:!editable?'grey':'black' }} onPress={() =>editable? this.setState({ open: !this.state.open,tempTutorId:this.props.add?this.state.tutorId:this.state.tasks[this.state.currentPage].tutorId}):null
                  }>{value}</Text>
                    :
                    <TextInput style={[styles.inputBox, { textAlign: chnageLayout ? 'left' : 'right' }]}
                      onChangeText={onPress}
                      multiline={!this.state.expand.includes(name)}
                      onFocus={focus}
                      value={value}
                      editable={editable}
                    />
                }
              </View>
              {dropdown ?
                <TouchableOpacity onPress={() => this.setState({ open: !this.state.open })}>
                  <Icon name='chevron-small-down' type='Entypo' style={{ color: '#b3b6b9', fontSize: 35 }} />
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={() => this.setState(state => (this.state.expand.includes(name) ? this.state.expand.splice(this.state.expand.indexOf(name), 1) : this.state.expand.push(name), state))}>
                  <Icon name={chnageLayout ? 'chevron-small-right' : 'chevron-small-left'} type='Entypo' style={{ color: '#b3b6b9', fontSize: 30 }} />
                </TouchableOpacity>}
            </View>
          </View>
        </View>
      </View>
    )
  }
  reabderModal() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.state.open}
        onRequestClose={() => { }}>
        <View style={{justifyContent:'center',alignItems:'center',flex:1,backgroundColor:'rgba(0,0,0,0.5)'}}>
            <View style={{backgroundColor:'white',width:'90%',height:Dimensions.get('window').height/1.5,borderRadius:5}}>
            <View style={{padding:10,borderBottomWidth:0.5}}>
                <Text style={{fontWeight:'bold',fontSize:22}}>Select Tutor</Text>
            </View>
              <FlatList
              extraData={this.state}
              data={this.state.tutors}
              renderItem={({item})=>
                <View style={{marginHorizontal:15,paddingVertical:5,flexDirection:'row',alignItems:'center'}}>
                  <Radio 
                  selectedColor='#7d4094'
                  selected={item.tid==this.state.tempTutorId}
                  onPress={()=>this.setState({tempTutorId:item.tid,tempTutorName:item.name})}/>
                  <TouchableOpacity style={{margin:10}} onPress={()=>this.setState({tempTutorId:item.tid,tempTutorName:item.name})}>
                    <Text style={{fontSize:18}}>{item.name}</Text>
                  </TouchableOpacity>
                  </View>
              }
              keyExtractor={(item)=>item.tid}
              />
              <View style={{padding:10,borderTopWidth:0.5,alignItems:'flex-end'}}>
                <View style={{flexDirection:'row',width:'60%',justifyContent:'space-around'}}>
                  <TouchableOpacity 
                  onPress={()=>this.setState({open:false,tempTutorId:this.state.tutorId})} >
                      <Text style={{padding:5,color:'#7d4094',fontWeight:'bold',fontSize:20}}>Cancel</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                  onPress={()=>
                  this.props.add?this.setState({open:false,tutorName:this.state.tempTutorName,tutorId:this.state.tempTutorId,tutorNameErr:false,changes:true})
                  :this.setState(state=>(this.state.open=false,
                    this.state.changes=true,
                    this.state.tasks[this.state.currentPage].tutorName=this.state.tempTutorName,
                    this.state.tasks[this.state.currentPage].tutorId=this.state.tempTutorId,state))
                  } >
                      <Text style={{padding:5,color:'#7d4094',fontWeight:'bold',fontSize:20}}>Ok</Text>
                  </TouchableOpacity>
                </View>  
              </View>
            </View>
            
          </View>
      </Modal>
    )
  }
  _renderItem(item, index) {
  // console.log('item data : ',item,index)
    let lastindex=0
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (

      <View key={index} style={[styles.subContainer,this.state.currentPage+1==index?{marginLeft:-20}:this.state.currentPage-1==index?{marginRight:-20}:null]}>
        <ScrollView
          ref={(ref)=> this._scroll=ref}
          showsVerticalScrollIndicator={false}>
          {this.props.add ? this.renderTextBox("taskName", this.state.taskName, (value) => this.setState({ taskName: value, taskNameErr: value ? false : true }), this.state.taskNameErr) :
            <View style={styles.row}>
              <Text style={[styles.title,{textAlign:chnageLayout ?'left':'right'}]}>{item.taskName}</Text>
            </View>}
          {this.renderTextBox("taskQue1", 
          this.props.add?this.state.que1:item.que1, 
          (value) =>this.props.add? 
          this.setState({que1:value,que1Err:value ?false : true,changes:true}):
          this.setState((state)=>(this.state.tasks[index].que1= value,this.state.changes=true,this.state.tasks[index].que1Err=value ? false : true,state )),
          this.props.add?this.state.que1Err:this.state.tasks[index].que1Err,
          false,
          null,
          !this.props.add?lastindex!=index?false:this.state.tasks[this.state.currentPage].status!=3:true)}
          {this.renderTextBox("taskQue2",  this.props.add?this.state.que2:item.que2, 
          (value) => this.props.add? 
          this.setState({que2:value,que2Err:value ? false : true,changes:true}):
          this.setState((state)=>(this.state.tasks[index].que2= value,
          this.state.changes=true,
          this.state.tasks[index].que2Err=value ? false : true,state)), 
          this.props.add?this.state.que2Err:this.state.tasks[index].que2Err,
          false,
          null,
          !this.props.add?lastindex!=index?false:this.state.tasks[this.state.currentPage].status!=3:true)}
          {this.renderTextBox("taskQue3", 
          this.props.add?new Date(this.state.que3).toLocaleDateString():item.que3?new Date(item.que3).toLocaleDateString():new Date().toLocaleDateString(),
          null, 
          this.state.que3Err,
          null, 
          () => this.openDatePicker(),
          !this.props.add?lastindex!=index?false:this.state.tasks[this.state.currentPage].status!=3:true,
         )}
          {this.renderTextBox("taskQue4", 
          this.props.add?this.state.tutorName:item.tutorName,
          null,
           this.state.tutorNameErr, 
           !this.props.add?lastindex!=index?false:this.state.tasks[this.state.currentPage].status!=3:true,
           null,
           !this.props.add ?lastindex!=index?false:this.state.tasks[this.state.currentPage].status!=3:true
          )}
          <View style={styles.progressView}>
            <Text style={{ fontSize: 20 }}>{I18n.t("progressStatus")}</Text>
            <View style={[styles.progressBarView, { flexDirection: chnageLayout ? 'row-reverse' : 'row' }]}>
              <View style={styles.iconView}>
                <Image source={happy} style={[styles.progressIcon, { tintColor:this.props.add?this.state.status== 3 ?'#35bcaf' : '#d1d2d3':item.studentOpinion == 3 ? '#35bcaf' : '#d1d2d3' }]} />
                <View style={styles.line} />
              </View>
              <View style={styles.iconView}>
                <Image source={natural} style={[styles.progressIcon, { tintColor: this.props.add?this.state.status== 2 ?'#35bcaf' : '#d1d2d3':item.studentOpinion== 2 ? '#35bcaf' : '#d1d2d3' }]} />
                <View style={styles.line} />
              </View>
              <View style={styles.iconView}>
                <Image source={sad} style={[styles.progressIcon, { tintColor:this.props.add?this.state.status == 1?'#35bcaf' : '#d1d2d3':item.studentOpinion == 1 ? '#35bcaf' : '#d1d2d3' }]} />
                <View style={styles.line} />
              </View>
            </View>
          </View>
          
            <Slider
            style={styles.slider}
            value={this.state.status}
            animateTransitions={true}
            thumbTintColor='white'
            thumbStyle={{borderWidth:2,borderColor:'#d1d2d3'}}
            minimumValue={0}
            maximumValue={100}
            step={50}
            minimumTrackTintColor={'#00adeb'}
            maximumTrackTintColor={'#00adeb'}
            value={
              this.props.add?null:
              this.state.sliding?null:item.studentOpinion==1?0:item.studentOpinion==2?50:item.studentOpinion==3?100:0
            }
            disabled={!this.props.add?lastindex!=index?true:this.state.tasks[this.state.currentPage].status==3:null}
            onValueChange={(value) => 
             {
              if (value == 0)
              this.props.add?this.setState({ status:1,changes:true })
              :
              this.setState((state)=>(this.state.changes=true,this.state.tasks[this.state.currentPage].studentOpinion=chnageLayout?1:3,state))
            else
              this.props.add?this.setState({ status: 0,changes:true }):
              this.setState((state)=>(this.state.changes=true,this.state.tasks[this.state.currentPage].studentOpinion=0,state))
            if (value == 50)
              this.props.add?this.setState({ status: 2 ,changes:true}):
              this.setState((state)=>(this.state.changes=true,this.state.tasks[this.state.currentPage].studentOpinion=2,state))
            if (value==100)
              this.props.add?this.setState({ status:3,changes:true }):
              this.setState((state)=>(this.state.changes=true,this.state.tasks[this.state.currentPage].studentOpinion=chnageLayout?3:1,state)) 
             }
             }/>
         { this.props.add?null:<View>
            <View style={[styles.complete, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
              <Text style={{ fontSize: 20 }}>{I18n.t("completed")}</Text>
              <CheckBox
              disabled={lastindex!=index?true:this.state.tasks[this.state.currentPage].status==3}
              checked={this.state.tasks[this.state.currentPage].tempStatus == 3 ?true:false}
                onPress={() => 
                  this.setState((state)=>(this.state.changes=true,this.state.tasks[this.state.currentPage].tempStatus=this.state.tasks[this.state.currentPage].tempStatus==3?1:3,state))} />
            </View>
            <View style={[styles.complete, { flexDirection: chnageLayout ? 'row' : 'row-reverse', marginBottom: 20, marginTop: 10 }]}>
              <Text>{I18n.t("completeNote")}</Text>
            </View>
          </View>}
          {lastindex!=index?null:<TouchableOpacity style={styles.buttonView}
            disabled={!this.props.add?lastindex!=index?true:this.state.tasks[this.state.currentPage].status==3:null}
            onPress={() => this.send()}>
          { this.state.sendLoading?
               <ActivityIndicator size='large' color='white' />:
               <Text style={styles.buttonText}>{I18n.t('save')}</Text>}
          </TouchableOpacity>}
        </ScrollView>
      </View>

    );
  }
  transitionAnimation(rotedY) {
    return {
      transform: [
        { perspective: 1000 },
        {
          rotateY: rotedY
        }
      ]
    };
  };
  workAreasName(name) {
    console.log("name---------: ",name)
    var areaname=''
    if (name == "Areas Of Growth")
    areaname=I18n.t('areasForGrowth')
    else if (name ==  "Areas Of Strength")
    areaname= I18n.t('areasOfStrength')
    else if (name == "Areas Of Interest")
    areaname= I18n.t('areasOfInterest')
    else if (name == "Intra Personal")
    areaname= I18n.t('intraPersonal')
    else if (name == "Inter Personal")
    areaname=I18n.t('interPersonal')
    else if (name == "Cognitive")
    areaname=I18n.t('cognitive')
    else if (name == "Body and movement")
    areaname=I18n.t('bodyAndMovement')
    else if (name == "Community")
    areaname=I18n.t('community')
    else if (name =="Presentation Skills")
    areaname=I18n.t('presentationSkills')
    console.log("work are name : ",areaname)
    //this.setState({blockname:areaname})
    return areaname
  }
  render() {
    let { currentPage } = this.state
    var params=this.props.allTask?{studentId:this.props.studentId}:{studentId:this.props.studentId,subjectId:this.props.subjectId}
    console.log("params : ",params)
    //console.log("current tasks : ",this.state.tasks[currentPage])
    I18n.locale = this.state.appLanguage
    //console.log("layout : ", setLayout(this.state.deviceLanguage, this.state.appLanguage))
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
  //   if (this.state.loading)
  //   return (
  //     <View style={styles.loading}>
  //       <ActivityIndicator size='large' color='#7f3f98' />
  //     </View>
  //   )
  // else
    return (
      // <SafeAreaView style={styles.container}>
      <View style={styles.container}>
       <Header 
            title={this.props.headerTitle?this.props.headerTitle:'KolCooli'} 
            lng={chnageLayout} matrix={this.props.matrix}
            back={this.props.back}
            menu={this.props.menu}
            onBackPress={()=>this.onBackPress()}
           />
        <View style={styles.subHeader}>
          <View style={{ flexDirection: 'row',alignItems:'center' }}>
            <View style={{ width: '90%', alignItems: 'center', paddingLeft: '10%' }}>
              <Text style={styles.subHeaderText}>{this.props.name ? this.props.loginType == 4 ? 'My Task' : this.props.name + " Task" : this.props.editTask?'My Task':'Add Task'}</Text>
              {this.props.blockName?<Text style={styles.subHeaderText2}>{this.workAreasName(this.props.blockName)}</Text>:null}
              {this.props.subjectName?<Text style={styles.subHeaderText2}>{this.props.subjectName}</Text>:null}
            </View>
            {this.state.loginType == 4 ? null : <TouchableOpacity onPress={() => Actions.Home({ page: <WorksAreas studentId={this.props.studentId} data={{ firstName: this.props.firstName, lastName: this.props.lastName, profile: this.props.pic }} />,menu:true,back:true })}>
              <Image source={require('../assets/icons/cubeIcon.png')} style={{ height: 30, width: 30 }} />
            </TouchableOpacity>}
          </View>
        </View>
        <Animated.View style={{ width: '100%', height: this.state.errorHeight, opacity: this.state.errorOpacity, backgroundColor:this.state.color, justifyContent: 'center', alignItems: 'center', padding: 10,position:'absolute',zIndex:1}}>
            <Text style={{ color: 'white', fontSize: 15 }}>{this.state.errMessage}</Text>
        </Animated.View>
        {this.props.add ?
          this._renderItem({ title: '' }, 0) :
         
          <Carousel
          ref={(c) => { this._carousel = c; }}
          data={this.state.tasks}
          renderItem={(item)=>this._renderItem(item.item,item.index)}
          sliderWidth={Dimensions.get('window').width}
          itemWidth={Dimensions.get('window').width-40}
          onSnapToItem={(index) => this.setState({ currentPage: index }) }
          />
        }
        {
          this.reabderModal()
        }
        {
          this.state.openDatePicker ?
            <View style={{ backgroundColor: 'white', marginHorizontal: 5, marginTop: 5, borderRadius: 10 }}>
              <DatePickerIOS
                mode='date'
                date={this.state.tempDate}
                onDateChange={(date) => this.setState({ tempDate: date, })}
              />
              <View style={{ flexDirection: 'row', width: '50%', padding: 15, alignSelf: 'flex-end', justifyContent: 'space-around' }}>
                <TouchableOpacity onPress={() => this.setState({ openDatePicker: false })}>
                  <Text style={styles.datePickerBtn}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() =>this.props.add?this.setState({ que3: this.state.tempDate, que3Err: false, openDatePicker: false ,selectData:true,changes:true}):
                 this.setState((state)=>(this.state.tasks[this.state.currentPage].que3=this.state.tempDate,this.state.tasks[this.state.currentPage].que3Err=false,this.state.openDatePicker= false,changes=true,state))}>
                  <Text style={styles.datePickerBtn}>Done</Text>
                </TouchableOpacity>
              </View>
            </View> : null
        }
      {/* </SafeAreaView> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c5eaf9',

  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginTop: Platform.OS == 'ios' ? 20 : 10
  },
  title: {
    fontSize: 25,
    marginVertical: 5,
    color: '#7f3f98',
    fontWeight: 'bold',
    width:'80%'
  },
  subContainer: {
    flex: 1,
    // width:350,
    backgroundColor: 'white',
    margin: 10,
    marginTop: 25
  },
  row: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5
  },
  buttonView: {
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f78c54',
    marginTop: 5
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 28
  },
  inputView: {
    width: '100%',
    borderBottomWidth: 1,
    //marginTop:10,
  },
  leftLine: {
    borderLeftWidth: 3,
    borderLeftColor: '#00b2ec',
    height: 45,
  },
  line: {
    borderLeftWidth: 3,
    borderLeftColor: '#d1d2d3',
    height: 25,
    marginTop: 5,
    marginTop:-1,
    marginBottom: -17,
  },
  inputRowView: {
    width: '95%',
    // height:55,
    //justifyContent:'space-between',
    //borderWidth:1
  },
  inputRowView2: {
    width: Platform.OS == 'ios' ? '87%' : '89%'
  },
  inputBox: {
    //height: 45,
    //fontSize:20,
  },
  colView: {
    //margin:40,
    width: '85%',
    alignSelf: 'center',
    marginTop: 15
  },
  errorMsg: {
    fontWeight: 'bold',
    color: 'red',
    marginTop: 5
  },
  subHeader: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00aeeb',
    paddingVertical: 15
  },
  subHeaderText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 22
  },
  progressView: {
    marginTop: 30,
    width: '85%',
    alignSelf: 'center'
  },
  progressBarView: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    marginTop: 10,
    alignSelf: 'center',
    //borderWidth:1
  },
  iconView: {
    alignItems: 'center'
  },
  progressIcon: {
    width: 30,
    height: 30,
  },
  slider: {
    width: '83%',
    alignSelf: 'center',
    marginBottom:20,
  },
  complete: {
    //marginTop: 30,
    width: '85%',
    alignSelf: 'center',
  },
  buttonView: {
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f78c54',
    marginTop: 5
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 28
  },
  datePickerBtn: {
    fontSize: 18,
    color: '#7d4094',
    fontWeight: 'bold'
  },
  subHeaderText2: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18
  },
  loading: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
