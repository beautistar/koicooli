import React, { Component } from 'react';
import { Platform, StyleSheet,BackHandler, Alert,SafeAreaView,ActivityIndicator,Animated,Dimensions,FlatList,ScrollView,KeyboardAvoidingView, Text, View, TouchableOpacity, TextInput, Image, DatePickerAndroid, DatePickerIOS,Modal,TouchableWithoutFeedback} from 'react-native';
import I18n, { getLanguages } from 'react-native-i18n';
import Header from '../components/Header'
import { Icon, CheckBox ,Radio} from 'native-base'
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../components/Layout'
import Api from '../components/api'
import { setAppLanguage, getAppLanguage, getDeviceLanguage,getLoginData} from '../components/Language'
const logo = require("../assets/images/logo.png")
import ImagePicker from 'react-native-image-crop-picker';
const defaultProfile = require('../assets/images/blank-profile.png')
import moment from 'moment';
export default class StudentDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      profile:'',
      ProfileUri:'',
      deviceLanguage: 'en',
      appLanguage: 'en',
      changeLng: '',
      tutorName: this.props.tutorName,
      firstName:'',
      lastName:'',
      email:'',
      instituteName:this.props.instituteName,
      standard: '',
      temDate:new Date(moment().subtract(9,'years').format('YYYY,MM,DD')),
      dateOfBirth: new Date(moment().subtract(9,'years')),
      tutorNameErr: false,
      standardErr: false,
      dateOfBirthErr: false,
      openDatePicker: false,
      firstNameErr:false,
      lastNameErr:false,
      emailErr:false,
      profileErr:false,
      instituteNameErr:false,
      open:false,
      chooseOption:false,
      tutorList:[],
      password:'',
      passwordErr:false,
      sendLoading:false,
      dob:'',
      errMessage: '',
      errorHeight: new Animated.Value(0),
      errorOpacity: new Animated.Value(0),
      errorMode: false,
      tutors:[],
      tempTutorId:'',
      tutorId:'',
      tempTutorName:'',
      changes:false,
    }
    this.onBackPress=this.onBackPress.bind(this)
  }
  _launchError() {
    if (!this.state.errorMode) {
      Animated.sequence([
        Animated.parallel([
          Animated.timing(this.state.errorHeight, { toValue: 35, duration: 1000 }),
          Animated.timing(this.state.errorOpacity, { toValue: 1, duration: 1000 })
        ])
      ]).start();
      setTimeout(() => {
        Animated.sequence([
          Animated.parallel([
            Animated.timing(this.state.errorHeight, { toValue: 0, duration: 1000 }),
            Animated.timing(this.state.errorOpacity, { toValue: 0, duration: 1000 })
          ])
        ]).start();
      }, 5000)
    }
  }
  async openDatePicker() {
    console.log('date : ', moment().subtract(9,'years').format('YYYY,MM,DD'))
    if (Platform.OS == 'ios') {
      this.setState({ openDatePicker: !this.state.openDatePicker })
    }
    else {
      try {
        const { action, year, month, day } = await DatePickerAndroid.open({
          date:this.state.dateOfBirth?new Date(this.state.dateOfBirth):new Date(moment().subtract(9,'years'))
        });
        if (action !== DatePickerAndroid.dismissedAction) {
          // Selected year, month (0-11), day
          console.log('date : ', new Date(year,month,day))
          this.setState({ dateOfBirth:new Date(year,month,day),dateOfBirthErr: false,changes:true })
        }
      } catch ({ code, message }) {
        console.warn('Cannot open date picker', message);
      }
    }
  }
  
  componentWillMount() {
    getDeviceLanguage().then(lng => {
      console.log('device Langauge==========: ', lng)
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
    
  
      this.setState({loading:true})
      getLoginData().then(data=>{
        console.log("data ------------- : ",data)
        this.setState({
          tutorName:data.firstName+" "+data.lastName,
                tempTutorId:data.user_id,
                tutorId:data.user_id,
        })
        Api.postWithToken('/api/user-all-tutor',{instituteId:data.instituteId},data.token).then((res)=>{
          console.log('req : ',res)
          if(res.message=="success"){ 
            var data=[]
            res.data.map((item,index)=>{
                data[index]={tid:item._id,name:item.firstName+' '+item.lastName}
            })
            this.setState({tutors:data,loading:false})
          }
        })
        if(!this.props.add){
          Api.get('/api/user-detail/'+this.props.studentId,data.token).then((res)=>{
            console.log('req : ',res)
            if(res.message=="success"){ 
              this.setState({
                ProfileUri:res.data.profile,
                profile:res.data.profile,
                tutorName:res.data.tutorId.firstName+" "+res.data.tutorId.lastName,
                tempTutorId:res.data.tutorId._id,
                tutorId:res.data.tutorId._id,
                firstName:res.data.firstName,
                lastName:res.data.lastName,
                email:res.data.email,
                instituteName:res.data.instituteId.title,
                standard:res.data.class,
                dateOfBirth:res.data.birthdate,
                dob:new Date(res.data.birthdate).toLocaleDateString(),
                loading:false
              })
            }
          })
         }
       })
    

  }
  onBackPress(){
    if(this.state.changes){
      Alert.alert(
        '',
        'Do You Want To Save Changes ?',
        [
          {text: I18n.t('cancel'), onPress: () =>this.setState({changes:true})},
          {text: I18n.t('yes'), onPress: () => this.send()},
          {text: I18n.t('no'), onPress: () =>this.setState({changes:false},()=>Actions.pop())},
        ],
        { cancelable: false }
      )
    }
    else{
      Actions.pop()
    }
  }
  componentDidMount(){
   this.BackHandler= BackHandler.addEventListener('backPress', ()=> {
      console.log("chnages : ",this.state.changes)
        if(this.state.changes){
          Alert.alert(
            '',
            'Do You Want To Save Changes ?',
            [
              {text: I18n.t('cancel'), onPress: () =>this.setState({changes:true})},
              {text: I18n.t('yes'), onPress: () => this.send()},
              {text: I18n.t('no'), onPress: () =>this.setState({changes:false},()=>Actions.pop())},
            ],
            { cancelable: false }
          )
          return true
        }
      })
  }
  componentWillUnmount(){
    this.BackHandler.remove()
    console.log("willunmount......::")
    
  }
  send() {
    let { standard, dateOfBirth,firstName,lastName,instituteName,email,tutorName,profile,password} = this.state
    //console.log("date ------ : ",dateOfBirth,moment(dateOfBirth).isAfter(moment().subtract(18,'years')))
    bdate=moment(moment().subtract(9,'years')).isAfter(moment(dateOfBirth));
    //bdate2=moment(moment().subtract(18,'years').format('YYYYY,MM,DD')).isBefore(moment(dateOfBirth));
    console.log("is after",bdate,":",dateOfBirth,":",this.state.dob)
   // console.log("is before",bdate2)
   
    if (!firstName)
      this.setState({ firstNameErr: true })
    if (!lastName)
      this.setState({ lastNameErr: true })
    if (!instituteName)
      this.setState({ instituteNameErr: true })
    if (!email)
      this.setState({ emailErr: true })
    if (!profile)
      this.setState({ profileErr: true })
    if (!standard)
      this.setState({ standardErr: true })
    if (!tutorName)
      this.setState({ tutorNameErr: true })
    if (!password)
      this.setState({ passwordErr: true })
    if(!bdate){
      this.setState({ dateOfBirthErr: true ,errMessage:'Your Age Must be 9 or Above'})
      this._launchError()
    }
    
    if(standard && firstName && lastName && instituteName && email && tutorName && bdate){
      this.setState({sendLoading:true})
      getLoginData().then(data=>{
       // console.log("token : ",data)
       console.log('date', this.state.tutorId)
          params={
            profile:profile,
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
            type: 4,
            tutorId: this.state.tutorId,
            instituteId:data.instituteId,
            class:standard,
            birthdate: dateOfBirth
          }
          
          if(this.props.add)
          Api.postWithToken('/api/user-register',params,data.token)
          .then((res)=>{
            console.log('req : ',res)
            if(res.message=='Success'){
              this.setState({sendLoading:false})
              Actions.reset("Home",{menu:true})
            }
            else{
              this.setState({sendLoading:false,errMessage:res.message})
              this._launchError()
            }
          })
          else
          Api.put('/api/user-update/'+this.props.studentId,params,data.token)
          .then((res)=>{
            console.log('req : ',res)
            if(res.message=='Updated SuccessFully'){
              this.setState({sendLoading:false})
              Actions.reset("Home",{menu:true})
            }
            else{
              this.setState({sendLoading:false,errMessage:res.message})
              this._launchError()
            }
          })
       })
    }
  }
  open(name){
    if(name==1){
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          cropping: true,
          includeBase64:true
        }).then(image => {
          console.log(image);
          this.setState({profile:'data:image/jpeg;base64,'+image.data,ProfileUri:image.path,chooseOption:false,changes:true})
        });
    }
    else{
        ImagePicker.openPicker({
          width: 300,
          height: 400,
          cropping: true,
          includeBase64:true
        }).then(image => {
          console.log(image);
          this.setState({profile:'data:image/jpeg;base64,'+image.data,ProfileUri:image.path,chooseOption:false,changes:true})
        });      
    } 
  }
  getImage(){
    return(
      <Modal
      animationType="none"
      transparent={true} 
      visible={this.state.chooseOption}
      onRequestClose={()=>{}}>
      <TouchableWithoutFeedback onPress={()=>this.setState({chooseOption:false})}>
        <View style={{justifyContent:'center',alignItems:'center',flex:1,backgroundColor:'rgba(0,0,0,0.5)'}}>
          <View style={{backgroundColor:'white',width:'70%',padding:10,borderRadius:5}}>
            <TouchableOpacity style={{margin:5,flexDirection:'row',alignItems:'center'}} onPress={()=>this.open(1)}>
              <Icon name='camera-iris' type='MaterialCommunityIcons'/>
              <Text style={{marginLeft:10,fontWeight:'bold'}}>Camera</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:5,flexDirection:'row',alignItems:'center'}}onPress={()=>this.open(2)}>
            <Icon name='folder-image' type='MaterialCommunityIcons'/>
              <Text style={{marginLeft:10,fontWeight:'bold'}}>Galary</Text>
            </TouchableOpacity>
          </View>
        </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
  reanderInputBox(name,value,onChangeText,error,chnageLayout,style,disable,calendar,keybordType){
    return(
      <View style={[styles.inputView,style,{ flexDirection: chnageLayout ? 'row' : 'row-reverse', borderColor: error ? 'red' : '#cacbcc' }]}>
      <View style={[styles.leftLine, chnageLayout ? { marginRight: 5 } : { marginLeft: 5 }]} />
      <View style={calendar?styles.inputRowView2:styles.inputRowView}>
        <Text style={{ textAlign: chnageLayout ? 'left' : 'right',fontSize:18}}>{I18n.t(name)}</Text>
        {disable?
          <Text  onPress={onChangeText} style={{paddingVertical:5,textAlign: chnageLayout ? 'left' : 'right',fontSize:18}} >{value}</Text>:
          <TextInput style={[styles.inputBox, { textAlign: chnageLayout ? 'left' : 'right' }]}
          onChangeText={onChangeText}
          multiline
          secureTextEntry={name=='password'?true:false}
          keyboardType={keybordType}
          value={value}
        />
        }
      </View>
      {/* {calendar?
        <TouchableOpacity
            onPress={onChangeText}>
            <Icon name='calendar' type='Octicons' style={{ marginTop: 25 ,marginBottom:5}} />
        </TouchableOpacity>:null
      } */}
    </View>
    )
  }
  reabderModal() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.state.open}
        onRequestClose={() => { }}>
        <View style={{justifyContent:'center',alignItems:'center',flex:1,backgroundColor:'rgba(0,0,0,0.5)'}}>
            <View style={{backgroundColor:'white',width:'90%',height:Dimensions.get('window').height/1.5,borderRadius:5}}>
            <View style={{padding:10,borderBottomWidth:0.5}}>
                <Text style={{fontWeight:'bold',fontSize:22}}>Select Tutor</Text>
            </View>
              <FlatList
              extraData={this.state}
              data={this.state.tutors}
              renderItem={({item})=>
                <View style={{marginHorizontal:15,paddingVertical:5,flexDirection:'row',alignItems:'center'}}>
                  <Radio 
                  selectedColor='#7d4094'
                  selected={item.tid==this.state.tempTutorId}
                  onPress={()=>this.setState({tempTutorId:item.tid,tempTutorName:item.name})}/>
                  <TouchableOpacity style={{margin:10}} onPress={()=>this.setState({tempTutorId:item.tid,tempTutorName:item.name})}>
                    <Text style={{fontSize:18}}>{item.name}</Text>
                  </TouchableOpacity>
                  </View>
              }
              keyExtractor={(item)=>item.tid}
              />
              <View style={{padding:10,borderTopWidth:0.5,alignItems:'flex-end'}}>
                <View style={{flexDirection:'row',width:'60%',justifyContent:'space-around'}}>
                  <TouchableOpacity 
                  onPress={()=>this.setState({open:false,tempTutorId:this.state.tutorId})} >
                      <Text style={{padding:5,color:'#7d4094',fontWeight:'bold',fontSize:20}}>Cancel</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                  onPress={()=>
                  this.setState({open:false,tutorName:this.state.tempTutorName,tutorId:this.state.tempTutorId,tutorNameErr:false,changes:true})
                  } >
                      <Text style={{padding:5,color:'#7d4094',fontWeight:'bold',fontSize:20}}>Ok</Text>
                  </TouchableOpacity>
                </View>  
              </View>
            </View>
            
          </View>
      </Modal>
    )
  }
  render() {
    console.log("chnage data : ",this.state.changes)
    I18n.locale = this.state.appLanguage
   // console.log("layout : ", setLayout(this.state.deviceLanguage, this.state.appLanguage))
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if (this.state.loading)
      return (
        <View style={styles.loading}>
          <ActivityIndicator size='large' color='#7f3f98' />
        </View>
      )
    else
    return (
      <View style={styles.container}>
       <KeyboardAvoidingView behavior={Platform.OS=='ios'?'padding':null}>
       <Header 
            title={this.props.headerTitle?this.props.headerTitle:'KolCooli'} 
            lng={chnageLayout} matrix={this.props.matrix}
            back={this.props.back}
            menu={this.props.menu}
            onBackPress={()=>this.onBackPress()}
           />
       <Animated.View style={{ width: '100%', height: this.state.errorHeight, opacity: this.state.errorOpacity, backgroundColor: '#d91e18', justifyContent: 'center', alignItems: 'center', padding: 10,position:'absolute',zIndex:1}}>
              <Text style={{ color: 'white', fontSize: 15 }}>{this.state.errMessage}</Text>
        </Animated.View>
       <ScrollView 
        showsVerticalScrollIndicator={false}>
        <View style={styles.subContainer}>
          
           { 
           this.props.data?<View style={styles.row}>
             <TouchableOpacity style={{alignSelf:'center',marginTop:20}} onPress={()=>this.setState({chooseOption:true})}>
                 <Image defaultSource={defaultProfile} source={{uri:this.state.ProfileUri}} style={{width:100,height:100,borderRadius:50,}}/>
             </TouchableOpacity>
              <Text style={styles.title}>{this.props.data.lastName} {this.props.data.firstName}</Text>
            </View>:
             <View>
             <TouchableOpacity style={{alignSelf:'center',marginTop:20}} onPress={()=>this.setState({chooseOption:true})}>
                 <Image defaultSource={defaultProfile}  source={this.state.ProfileUri?{uri:this.state.ProfileUri}:defaultProfile} style={{width:100,height:100,borderRadius:50,}}/>
             </TouchableOpacity>
         </View>
          }
            <View style={styles.colView}>
                 
                  {this.reanderInputBox('fname',this.state.firstName,
                  (value)=>this.setState({ firstName: value, firstNameErr: value ? false : true,changes:value ? true : false }),
                  this.state.firstNameErr,chnageLayout)}

                  {this.reanderInputBox('lname',this.state.lastName,
                  (value)=>this.setState({ lastName: value, lastNameErr: value ? false : true,changes:value ? true : false }),
                  this.state.lastNameErr,chnageLayout,{marginTop:25})}

                  {this.reanderInputBox('email',this.state.email,
                  (value)=>this.setState({ email: value, emailErr:reg.test(value)? false : true,changes:value ? false : true,changes:value ?true : false }),
                  this.state.emailErr,chnageLayout,{marginTop:25},null,null,"email-address")}

                  {this.reanderInputBox('instituteName',this.state.instituteName,null,null,chnageLayout,{marginTop:25},true)}

                  {this.reanderInputBox('myTutor',this.state.tutorName,()=>this.setState({open:true}),null,chnageLayout,{marginTop:25},true)}

                  {this.reanderInputBox('class',this.state.standard,
                  (value)=>this.setState({ standard: value,standardErr: value ? false : true,changes:value ? true : false}),
                  this.state.standardErr,chnageLayout,{marginTop:25})}

                  {this.reanderInputBox('dateOfBirth',new Date(this.state.dateOfBirth).toLocaleDateString(),
                  ()=>this.openDatePicker(),
                  this.state.dateOfBirthErr,chnageLayout,{marginTop:25},true,true)}

                  {this.props.add?this.reanderInputBox('password',this.state.password,
                  (value)=>this.setState({ password: value,passwordErr: value ? false : true,changes:value ?true : false}),
                  this.state.passwordErr,chnageLayout,{marginTop:25}):null}
            </View>
            <TouchableOpacity style={styles.buttonView}
              onPress={() => this.send()}
            >
            {this.state.sendLoading?
               <ActivityIndicator size='large' color='white' />:  
               <Text style={styles.buttonText}>{I18n.t('save')}</Text>
            } 
            </TouchableOpacity>
         
        </View>
        </ScrollView>
        {
          this.reabderModal() 
        }
        {
          this.getImage()
        }
         {
            this.state.openDatePicker ?
              <View  style={{backgroundColor:'white',marginHorizontal:5,marginTop:5,borderRadius:10,position:'absolute',bottom:0,width:'98%'}}>
              <DatePickerIOS
                mode='date'
                date={this.state.dateOfBirth?new Date(this.state.dateOfBirth):this.state.temDate}
                onDateChange={(date) => this.setState({ temDate: date,changes:true})}
              />
               <View style={{flexDirection:'row',width:'50%',padding:15,alignSelf:'flex-end',justifyContent:'space-around'}}>
                    <TouchableOpacity onPress={()=>this.setState({openDatePicker:false})}>
                      <Text style={styles.datePickerBtn}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.setState({dateOfBirth:this.state.temDate,dob:this.state.temDate,dateOfBirthErr: false,openDatePicker:false})}>
                      <Text style={styles.datePickerBtn}>Done</Text>
                    </TouchableOpacity>
                </View>
               </View>: null
          }
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c5eaf9',

  },
  logo: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginTop: Platform.OS == 'ios' ? 20 : 10
  },
  loading: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 25,
    marginVertical: 5,
    color: '#7f3f98',
    fontWeight: 'bold'
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    margin: 15,
    marginTop: 25,
    marginBottom:10,
    paddingBottom:'15%'
  },
  row: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  buttonView: {
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f78c54',
    marginTop: 5
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 28
  },
  inputView: {
    width: '100%',
    borderBottomWidth: 1,
    marginTop: 5,
  },
  leftLine: {
    borderLeftWidth: 3,
    borderLeftColor: '#00b2ec',
    height: 45,
  },
  inputRowView: {
    width: '95%'
  },
  inputRowView2: {
    width: Platform.OS == 'ios' ? '87%' : '89%'
  },
  inputBox: {
    marginTop:-10,
    fontSize:18,
   // height: 40
  },
  colView: {
    margin: 40,
    marginTop: 40
  },
  errorMsg: {
    fontWeight: 'bold',
    color: 'red',
    marginTop: 5
  },
  datePickerBtn:{
    fontSize:18,
    color:'#7d4094',
    fontWeight:'bold'
  }

});
