import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, TextInput, Image, FlatList, AsyncStorage, ActivityIndicator } from 'react-native';
import Drawer from 'react-native-drawer'
import Home from './Home'
import WorkAreaDetails from '../components/WorkAreasDetails'
import I18n, { getLanguages } from 'react-native-i18n';
import { setLayout } from '../components/Layout'
import SideBar from '../components/SideBar'
import Header from '../components/Header'

import { getAppLanguage, getDeviceLanguage, getLoginType } from '../components/Language'
export default class AppDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: '',
      appLanguage: this.props.lng ? this.props.lng : 'en',
      loading:true,
    }
    this.openDrawer=this.openDrawer.bind(this)
    this.closeDrawer=this.closeDrawer.bind(this)
    
    console.log('props : ',this.props)
  }
  closeDrawer(){
    this.drawer.close()
  }
  openDrawer(){
    this.drawer.open()
  }
  componentWillMount() {
   
    getDeviceLanguage().then(lng => {
      console.log('device Langauge: ', lng)
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => {
      console.log('APP Langauge: ', lng)
      this.setState({ appLanguage: lng, })
    })

    if(this.props.page)
      this.setState({loading:false})
    else{
      this.setState({loading:false})
    }
  }
 
  render() {
    console.log('matrix : ',this.props.matrix)
    I18n.locale = this.state.appLanguage;
    //console.log("layout chnage Copmponent : ", setLayout(this.state.deviceLanguage, this.state.appLanguage))
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    if(this.state.loading)
      return(
        <View style={styles.loading}>
          <ActivityIndicator size='large' color='#7d4094' />
        </View>
      )
    else
    return(
          <View style={{flex:1,backgroundColor:'white'}}>
            <Header 
            title={this.props.headerTitle?this.props.headerTitle:'KolCooli'} 
            lng={chnageLayout} matrix={this.props.matrix}
            openDrawer={this.openDrawer} closeDrawer={this.closeDrawer} 
            back={this.props.back}
            menu={this.props.menu}
           />
           <Drawer
             type='overlay'
             panThreshold={0.5}
            // style={styles.drawer}
            // side={chnageLayout?'left':'right'}
            ref={(ref) => { this.drawer = ref }}
            panOpenMask={0.5}
            openDrawerOffset={0.2} // 20% gap on the right side of drawer
            tapToClose={true}
            content={<SideBar />}
            tweenHandler={(ratio) => ({
              mainOverlay: { opacity:ratio/1.5,backgroundColor:'black' }
            })}
            >
            {
              this.props.page?
                this.props.page
                :
                this.props.studentHome?
                  <WorkAreaDetails title='Areas of strength'  name={'student'}/>
                :
                <Home home={false}/>
            }
           
          </Drawer>
          </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
  },
  drawer: { 
    shadowColor: '#000000', 
    shadowOpacity: 0.8,
     shadowRadius: 3
    },
  loading: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
