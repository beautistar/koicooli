import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity,RefreshControl, TextInput, Image, FlatList, AsyncStorage, ActivityIndicator } from 'react-native';
import Api from '../components/api'
import Header from '../components/Header'
import I18n, { getLanguages } from 'react-native-i18n';
import { Icon, Card, CardItem } from 'native-base';
import { Actions } from 'react-native-router-flux';
const defaultPic = require('../assets/images/logo.png')
import { setLayout } from '../components/Layout'
import { getAppLanguage, getDeviceLanguage, setAppLanguage, getLoginData } from '../components/Language'
import WorksAreas from '../home/WorksAreas'
import Tasks from '../home/Task'
import StudentDetails from '../home/StudentDetails'
import WorkAreaDetails from '../components/WorkAreasDetails'
const defaultProfile = require('../assets/images/blank-profile.png')
import NoDtataFound from '../components/NoDataFound'
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceLanguage: '',
      appLanguage: this.props.lng ? this.props.lng : 'en',
      loading: true,
      revers: false,
      tutorName:'',
      instituteName:'',
      studentId:'',
      students: [],
      refreshing:false,
      actionPlan:''
    }
    this.getStudents=this.getStudents.bind(this)
  }
  componentWillMount() {

    getDeviceLanguage().then(lng => {
      console.log('device Langauge: ', lng)
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => {
      console.log('APP Langauge: ', lng)
      this.setState({ appLanguage: lng })
    })
     this.getStudents()
  }
  getStudents(){
    getLoginData().then(data=>{
      this.setState({
        tutorName:data.firstName +' '+data.lastName,
        instituteName:data.instituteName,
      })
      Api.postWithToken('/api/user-list',null,data.token).then((res)=>{
        console.log('req : ')
        if(res.message=="success"){ 
          this.setState({students:res.data,loading:false,refreshing:false})
        }
        else{
          this.setState({students:[],loading:false,refreshing:false})
        }
      })
   })
  }
  setLangauge(language) {
    I18n.locale = language;
    setAppLanguage(language)
    Actions.pop()
    Actions.Home()
    //this.setState({appLanguage:language})
    console.log('language : ', language)
  }
  _onRefresh = () => {
    this.setState({refreshing: true});
    this.getStudents()
  }
  render() {
    I18n.locale = this.state.appLanguage;
    console.log("layout chnage Copmponent : ", setLayout(this.state.deviceLanguage, this.state.appLanguage))
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    if (this.state.loading)
      return (
        <View style={styles.loading}>
            <View style={{bcakgroundColor:'white'}}>
               <ActivityIndicator size='large' color='#7f3f98' /> 
           </View>
      </View>
      )
    else
      return (
        <View style={styles.container}>
          <View style={{ padding: 10, backgroundColor: '#00aeeb',justifyContent:'center',alignItems:'center', paddingVertical: 15 }}>
            <View >
              <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 22 }}>{I18n.t("headerTitle")} ({this.state.students.length>0?this.state.students.length:null})</Text>
            </View>
           
          </View>

          <View style={styles.container2}>
           {this.state.students.length>0?
            <FlatList
              extraData={this.state}
              showsVerticalScrollIndicator={false}
              data={this.state.students}
              renderItem={({ item, index }) =>
                <TouchableOpacity
                  onPress={() =>
                   Actions.Home({ page: <WorkAreaDetails 
                   //title='Areas of strength'
                   //name={'student'} 
                   studentSubject={true}
                   studentId={item._id}
                   firstName={item.firstName} 
                   lastName={item.lastName}
                   pic={item.profile} />,menu:true,back:true })}
                  style={[styles.listCard, { marginTop: index == 0 ? 20 : 10, marginBottom: index == this.state.students.length - 1 ? 50 : null }]}>
                  <View style={[styles.listCol, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                    <TouchableOpacity style={styles.imageView}
                      onPress={() => Actions.StudentDetails({ studentId:item._id, back: true })}>
                      <Image defaultSource={defaultProfile} source={item.profile?{uri:item.profile}:defaultProfile} style={styles.pic} />
                      {/* <Image source={defaultProfile} style={styles.pic} /> */}
                    </TouchableOpacity>
                    <View style={styles.listCol2}>
                      <Text style={[styles.name, { textAlign: chnageLayout ? 'left' : 'right' }]}
                        onPress={() =>Actions.StudentDetails({ studentId:item._id, back: true })}
                      >{item.lastName}  {item.firstName}</Text>
                      <View style={{ flexDirection: chnageLayout ? 'row' : 'row-reverse', alignItems: 'center', marginTop: 10, marginLeft: -5 }}>
                        <Icon name='calendar' type='Octicons' style={{ padding: 5, paddingLeft: 0 }} />
                        <Text style={styles.activeDate}>{I18n.t('updatedOn')} </Text>
                        <Text style={styles.activeDate}>{item.updatedDate ? new Date(item.updatedDate).toLocaleDateString() : new Date(item.createdDate).toLocaleDateString()}</Text>
                      </View>
                    </View>
                  </View>
                  <View style={[styles.listRowView, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                    <TouchableOpacity
                      onPress={() => Actions.Home({ page: <WorksAreas data={item}  
                        getStudents={this.getStudents} 
                        studentId={item._id}/>,menu:true,back:true })}
                      style={[styles.listSubCol, , { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                      <Image source={require('../assets/icons/cube.png')} style={{ width: 30, height: 35 }} />
                      {/* <View style={styles.circleView}>
                          <Text style={styles.circleText}>{item.activeTask}</Text>
                        </View>  */}
                      <Text style={styles.subTitle}>{I18n.t('areas')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.listSubCol, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}
                      onPress={() => Actions.Home({ page: <WorkAreaDetails
                      getStudents={this.getStudents} 
                      studentId={item._id}
                      studentSubject={true}
                      firstName={item.firstName} lastName={item.lastName} pic={item.profile} />,menu:true,back:true })}>
                      <Icon name='star' type='SimpleLineIcons' style={{ fontSize: 30 }} />
                     {item.subjects.length>0?<View style={[styles.circleView, { position: 'absolute', left: -15, top: 18 }]}>
                        <Text style={styles.circleText}>{item.subjects.length}</Text>
                      </View>:null}
                      <Text style={styles.subTitle}>{I18n.t('subject')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => Actions.Home({ page: <Tasks
                        getStudents={this.getStudents} 
                        data={item} studentId={item._id}
                        allTask={true}/>,menu:true,back:true})}
                      style={[styles.listSubCol, { flexDirection: chnageLayout ? 'row' : 'row-reverse' }]}>
                      <Icon name='file-document' type='MaterialCommunityIcons' style={{ fontSize: 30 }} />
                     {item.tasks.length>0? <View style={[styles.circleView, { position: 'absolute', left: -15, top: 18 }]}>
                        <Text style={styles.circleText}>{item.tasks.length}</Text>
                      </View>:null}
                      <Text style={styles.subTitle}>{I18n.t('task')}</Text>
                    </TouchableOpacity>

                  </View>
                </TouchableOpacity>
              }
              refreshControl={
                <RefreshControl
                  colors={['#7d4094']}
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
              keyExtractor={(item, index) => item._id} />
              :<NoDtataFound/>}
            <TouchableOpacity
              onPress={() => Actions.StudentDetails({ tutorName:this.state.tutorName, instituteName:this.state.instituteName, add:true,back:true})}
              style={{
                padding: 10, backgroundColor: '#7f3f98', borderRadius: 25, width: 50, height: 50,
                alignSelf: 'flex-start', marginRight: 10, justifyContent: 'center', alignItems: 'center',
                position: 'absolute', bottom: 10, right: chnageLayout ? 10 : null, left: chnageLayout ? null : 10
              }}>
              <Icon name='add-user' type='Entypo' style={{ fontSize: 25, color: 'white' }} />
            </TouchableOpacity>
           
          </View>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
  },
  loading: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 100,
    marginTop: 20
  },
  title: {
    fontSize: 35,
    marginVertical: 20,
    color: '#7f3f98'
  },
  headerBar: {
    flexDirection: 'row',
    paddingTop: 10,
    padding: 10,
    alignSelf: 'center',
    backgroundColor: 'white',
  },
  sreachView: {
    width: '75%',
    height: 45,
    flexDirection: 'row',
    borderWidth: 2,
    borderRadius: 50,
    alignItems: 'center',
    borderColor: 'black'
  },
  searchIcon: {
    padding: 5,
    color: 'grey'
  },
  searchInput: {
    width: '70%'
  },
  button: {
    width: '15%',
    alignItems: 'center'
  },
  container2: {
    flex: 1,
    paddingHorizontal: 10
  },
  listCard: {
    backgroundColor: 'white',
    paddingVertical: 15,
  },
  listCol: {
    flexDirection: 'row',
    padding: 5
  },
  imageView: {
    width: '25%',
    alignItems: 'center'
  },
  pic: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  listCol2: {
    width: '75%',
    paddingBottom: 5
  },
  name: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#7f4296'
  },
  activeDate: {
    fontSize: 17,
    marginTop: 5
  },
  date: {
    fontSize: 15,
    //fontWeight:'bold'
  },
  listRowView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    alignSelf: "center",
    paddingVertical: 10
  },
  listSubCol: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  subTitle: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: 17
  },
  circleView: {
    width: 25,
    height: 25,
    backgroundColor: '#f78f5b',
    borderRadius: 13,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10
  },
  circleText: {
    color: 'white'
  }
});
