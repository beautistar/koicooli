import React, { Component } from 'react';
import { Platform, StyleSheet, ScrollView, Text, View, TouchableOpacity, Image, ActivityIndicator, Animated, KeyboardAvoidingView, AsyncStorage } from 'react-native';
import I18n, { getLanguages } from 'react-native-i18n';
import Header from '../components/Header'
import { Actions } from 'react-native-router-flux'
import { setLayout } from '../components/Layout'
import {  getAppLanguage, getDeviceLanguage,getLoginData } from '../components/Language'
import Api from '../components/api'
import WorkAreasDetails from '../components/WorkAreasDetails'
const logo = require("../assets/images/logo.png")
const Icon1 = require('../assets/icons/blockIcon1.png')
const Icon2 = require('../assets/icons/blockIcon2.png')
const Icon3 = require('../assets/icons/blockIcon3.png')
const Icon4 = require('../assets/icons/blockIcon4.png')
const Icon5 = require('../assets/icons/blockIcon5.png')
const Icon6 = require('../assets/icons/blockIcon6.png')
const Icon7 = require('../assets/icons/blockIcon7.png')
const Icon8 = require('../assets/icons/blockIcon8.png')
const Icon9 = require('../assets/icons/blockIcon9.png')
const defaultProfile = require('../assets/images/blank-profile.png')
export default class WorksAreas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      deviceLanguage: 'en',
      appLanguage: 'en',
      changeLng: '',
      block:[],
      blockId:'',
    }
    this.bounce=new Animated.Value(0)
    this.getBlockData=this.getBlockData.bind(this)
  }
  componentWillMount() {
    ///api/block-list
    getDeviceLanguage().then(lng => {
      console.log('device Langauge==========: ', lng)
      this.setState({ deviceLanguage: lng })
    })
    getAppLanguage().then(lng => this.setState({ appLanguage: lng }))
 
  }
  getBlockData(block){
    getLoginData().then(data=>{
      Api.postWithToken('/api/block-list',{studentId:this.props.studentId},data.token).then((res)=>{
        console.log('req : ',res)
        if(res.message=="success"){ 
          var data=[]
          res.data.map((item)=>{
              data[item.sequence-1]=item
          })
          console.log('data: ',data)
          this.setState({block:data,loading:false,blockId:block})
          Animated.spring(
            this.bounce,
            {
              toValue:1,
              friction:0.5
            }
          ).start()
        }
      })
      this.props.getStudents?this.props.getStudents():null 
   })
  }
  componentDidMount(){
    this.getBlockData() 
  }

  renderBlock(icon, name,block) {
    console.log("block : ",block.subjects)
    console.log("block : ",block._id)
    return (
      <TouchableOpacity
        onPress={() => Actions.Home({page:<WorkAreasDetails title={name} 
          getBlockData={this.getBlockData}
          blockId={block._id} 
          pic={this.props.data.profile} 
          lastName={this.props.data.lastName} 
          firstName={this.props.data.firstName} 
          name={this.props.data.lastName+" "+this.props.data.firstName} 
          studentId={this.props.studentId}
          />,menu:true,back:true})}
        style={styles.blockView}>
        <Image source={icon} style={styles.blockIcon}  />
        <Text style={styles.blockName}>{name}</Text>
       { block.subjects.length > 0?<Animated.View
            style={[styles.circleView,{transform:[{scale:this.bounce}]}]}>
              <Text style={styles.circleText}>{block.subjects.length}</Text>
            </Animated.View>:null}
      </TouchableOpacity>
    )
  }

  render() {
    let {block}=this.state
    I18n.locale = this.state.appLanguage
    console.log("layout : ", setLayout(this.state.deviceLanguage, this.state.appLanguage))
    let chnageLayout = setLayout(this.state.deviceLanguage, this.state.appLanguage)
    return (
      <View style={styles.container}>
      <ScrollView>
        <View style={styles.subContainer}>
          <View style={styles.row}>
            <Image defaultSource={defaultProfile} source={this.props.data.profile?{uri:this.props.data.profile}:null} style={styles.logo} />
            {/* <Image source={defaultProfile} style={styles.logo} /> */}
            <Text style={styles.title}>{this.props.data.lastName} {this.props.data.firstName}</Text>
          </View>
          <View style={styles.titleView}>
            <View >
              <Text style={styles.title2}>{I18n.t('worksAreas')}</Text>
            </View>
          </View>
          {block.length>0?<View style={[styles.blockRowView, { flexDirection: chnageLayout ? this.state.deviceLanguage == 'he' ? 'row-reverse' : 'row' : this.state.deviceLanguage == 'he' ? 'row-reverse' : 'row', }]}>
            {this.renderBlock(Icon1, I18n.t('areasForGrowth'),block[0])}
            {this.renderBlock(Icon2, I18n.t('areasOfStrength'),block[1])}
            {this.renderBlock(Icon3, I18n.t('areasOfInterest'),block[2])}
            {this.renderBlock(Icon4, I18n.t('intraPersonal'),block[3])}
            {this.renderBlock(Icon5, I18n.t('interPersonal'),block[4])}
            {this.renderBlock(Icon6, I18n.t('cognitive'),block[5])}
            {this.renderBlock(Icon7, I18n.t('bodyAndMovement'),block[6])}
            {this.renderBlock(Icon8, I18n.t('community'),block[7])}
            {this.renderBlock(Icon9, I18n.t('presentationSkills'),block[8])}
          </View>:null}
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c5eaf9',

  },
  logo: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginTop: Platform.OS == 'ios' ? 20 : 10,
   
  },
  title: {
    fontSize: 25,
    marginVertical: 5,
    color: '#7f3f98',
    fontWeight: 'bold'
  },
  blockView: {
    width: '30%',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
  blockIcon: {
    width: 60,
    height: 60
  },
  blockName: {
    textAlign: 'center',
    marginHorizontal: 10,
    color: 'black',
    fontSize: 14
  },
  subContainer: {
    paddingVertical:15,
    backgroundColor: 'white',
    margin: 15,
    marginTop: 25
  },
  row: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleView: {
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00aeeb',
    marginTop: 5
  },
  title2: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 28
  },
  blockRowView: {
    alignSelf: 'center',
    justifyContent: 'space-between',
    width: '100%',
    flexWrap: 'wrap'
  },
  circleView: {
    width: 25,
    height: 25,
    backgroundColor: '#f78f5b',
    borderRadius: 13,
    alignItems: 'center',
    justifyContent: 'center',
   position:'absolute',
   right:25,
   top:0

  },
  circleText: {
    color: 'white'
  }

});
