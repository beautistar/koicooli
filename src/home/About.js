import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, TextInput, Image, FlatList, AsyncStorage, ActivityIndicator } from 'react-native';
const logo = require("../assets/images/logo.png")
import Header from '../components/Header'

export default class About extends Component {


  render() {

    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 0.9, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={logo} style={styles.logo} />
          <Text style={{ marginTop: 20, fontSize: 20 }}>App Version: 0.5</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
    backgroundColor: '#c5eaf9',
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 75,
    marginTop: Platform.OS == 'ios' ? 90 : 40
  },
  title: {
    fontSize: 35,
    marginVertical: 20,
    color: '#7f3f98'
  },
});
