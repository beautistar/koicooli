import React, { Component } from 'react'
import { Scene, Router, Stack } from 'react-native-router-flux'
import Login from './login'
import Home from './home'
import ForgotPassword from './login/ForgotPassword'
import WorksAreas from './home/WorksAreas'
import Tasks from './home/Task'
import WorkAreasDetails from './components/WorkAreasDetails'
import StudentDetails from './home/StudentDetails'
import EditWorkAreasDetails from './components/editWorkAreasDetails'
import TasksDetails from './home/TaskDetails'
export default class Route extends Component {
    render() {
        return (
            <Router>
                <Stack key="root" hideNavBar>
                    <Scene key='Login' component={Login} initial/>
                    <Scene key='ForgotPassword' component={ForgotPassword} />
                    <Scene key='Home' component={Home} />
                    <Scene key='WorksAreas' component={WorksAreas} />
                    <Scene key='Tasks' component={Tasks} />
                    <Scene key='WorkAreasDetails' component={WorkAreasDetails} />
                    <Scene key='StudentDetails' component={StudentDetails} />
                    <Scene key='EditWorkAreasDetails' component={EditWorkAreasDetails} />
                    <Scene key='TasksDetails' component={TasksDetails} />
                </Stack>
            </Router>
        )
    }
}
