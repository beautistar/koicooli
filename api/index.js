var express = require('express');
const moment = require('moment');
//var database = require('./config/database');
var bodyParser = require('body-parser');
var fs = require('fs');
var port = process.env.PORT || 1365;
var app = express();
var cors = require('cors')
var path = require('path');

app.use(cors());
app.use(bodyParser.json({
    limit: '50mb'
}));
//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));
var enableCORS = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, X-Requested-With, *');
    if ('OPTIONS' === req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
};

app.all("/*", (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, X-Requested-With, *');
    next();
});
app.use(enableCORS);
app.use(express.static(__dirname + '/images'));
app.set('view engine', 'ejs')
// Schemas

var models_path = __dirname + '/model';
fs.readdirSync(models_path).forEach((file) => {
    if (~file.indexOf('.js')) require(models_path + '/' + file);
});

// Routes

const allRoutes = require('./routes')(app);

app.listen(port, () => {
    console.log("Express server listening on port", port);
});

module.exports = app, allRoutes;