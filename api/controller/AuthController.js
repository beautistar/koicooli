const mongoose = require('mongoose');
const User = mongoose.model("user");
const async = require('async');
const UserCons = require('../constant/user');
const CommonService = require('../service/CommonService.js')
const ValidateService = require('../service/ValidatorService.js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');
let config = require('../config/config.js');
const nodemailer = require("nodemailer");

module.exports = {

    /**
     * @desc : login user
     * @param req = {
     *     "email":"",
     *     "password":""
     * }
     * @param res => return user with token
     */
    login: function (req, res) {

        let params = req.body;
        console.log("In---Login", params);
        //  res.send(JSON.stringify({ a: 123456789 }));

        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkLoginReqValidation(params);

        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {

            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                //check user exist or not
                function (wcb) {

                    let userFilter = {
                        $or: [
                            {
                                email: params.email
                            },
                            {
                                email: (params.email).toLowerCase()
                            },
                            {
                                name: params.email
                            },
                            {
                                name: (params.email).toLowerCase()
                            }
                        ]
                    }
                    User
                        .findOne(userFilter)
                        .exec(function (err, user) {
                            if (err) throw err;

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "User not found"
                                });
                            }
                        })
                },

                //compare and approve
                function (user, wcb) {
                    let option = {
                        user: user,
                        password: params.password
                    }

                    //decrypt and compare password
                    let checkPassword = CommonService.compareHash(option);

                    if (!checkPassword) {
                        return res.json({
                            "code": 403,
                            "status": "Error",
                            "message": "Your password does not match,please try again"
                        });
                    }
                    else {

                        let token = jwt.sign({
                            id: user._id,
                            email: user.email,
                            mobile: user.mobile,
                            type: user.type
                        }, config.secrect);


                        User
                            .findOne({_id: user._id})
                            .populate('createdBy')
                            .populate('deletedBy')
                            .populate('tutorId')
                            .populate('instituteId')
                            .exec(function (err, student) {
                                if (err) throw err;

                                if (student && student) {

                                    return res.json({
                                        "code": 200,
                                        "message": "success",
                                        "data": student,
                                        "token": token
                                    });
                                }
                                else {
                                    return res.json({
                                        "code": 200,
                                        "message": "success",
                                        "data": user,
                                        "token": token
                                    });
                                }

                            })


                    }
                }
            ])
        }
        ;
    },

    /**
     *
     * @param req
     * @param res
     */
    forgotPassword: function (req, res) {

        let params = req.body;
        //let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        let fullUrl = req.protocol + '://' + req.get('host')

        let path = require('path');
        let appDir = path.dirname(require.main.filename);
        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkForgotReqValidation(params);

        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {

            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {
            //check email is in valid format
            /*  let valid = CommonService.emailValidator(params.email);

              if (!valid) {
                  return res.json({
                      "code": 403,
                      "status": "Error",
                      "message": "Please enter valid email address"
                  });
              }
              else {*/

            async.waterfall([

                //check user exist or not
                function (wcb) {

                    let userFilter = {
                        $or: [
                            {
                                email: params.email
                            },
                            {
                                email: (params.email).toLowerCase()
                            }
                        ]
                    }

                    User
                        .findOne(userFilter)
                        .exec(function (err, user) {
                            if (err) throw err;

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "User not found"
                                });
                            }
                        })
                },

                function (user, wcb) {

                    let linkExpirationCode = CommonService.randomStringGenerator();
                    // create reusable transport method (opens pool of SMTP connections)
                    let smtpTransport = nodemailer.createTransport({
                        service: "Gmail",
                        //host: req.headers.host, // hostname
                        secureConnection: false, // use SSL
                        port: 587, // port for secure SMTP
                        auth: {
                            user: "jacktech88@gmail.com",
                            pass: "jackTech@123"
                        }
                    });

                    // setup e-mail data with unicode symbols
                    let mailOptions = {
                        from: "support@kolcooli.info", // sender address
                        to: user.email, // list of receivers
                        subject: "Forgot Password", // Subject line
                        text: 'Click on the link to reset your password..' + fullUrl + '/forgot-password-page/' + user._id + '/' + linkExpirationCode, // plaintext body
                        //text: 'Click on the link to reset your password..' + fullUrl + '/home/js2/Desktop/Projects/KolCooli/forgot-password-page/' + user._id + '/' + linkExpirationCode, // plaintext body
                        //html: "<b>Hello world ✔</b>" // html body
                    }

                    // send mail with defined transport object
                    smtpTransport.sendMail(mailOptions, function (error, response) {
                        if (error) {
                            console.log("Errr--", error)
                            return res.json({"code": 403, "message": "Mail Not Sent"});
                        } else {
                            return res.json({
                                "code": 200,
                                "message": "link for reset password is sent on " + user.email
                            });
                        }

                    });
                }
            ])
            // }
        }
    },

    forgotPassword1: function (req, res) {

        let params = req.body;
        //let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        let fullUrl = req.protocol + '://' + req.get('host')

        let path = require('path');
        let appDir = path.dirname(require.main.filename);
        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkForgotReqValidation(params);

        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {

            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {
            //check email is in valid format
            /*  let valid = CommonService.emailValidator(params.email);

              if (!valid) {
                  return res.json({
                      "code": 403,
                      "status": "Error",
                      "message": "Please enter valid email address"
                  });
              }
              else {*/

            async.waterfall([

                //check user exist or not
                function (wcb) {

                    let userFilter = {
                        $or: [
                            {
                                email: params.email
                            },
                            {
                                email: (params.email).toLowerCase()
                            }
                        ]
                    }

                    User
                        .findOne(userFilter)
                        .exec(function (err, user) {
                            if (err) throw err;

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "User not found"
                                });
                            }
                        })
                },

                function (user, wcb) {

                    let linkExpirationCode = CommonService.randomStringGenerator();

                    let options = {
                        subject: "Forgot Password",
                        text: 'Click on the link to reset your password..' + fullUrl + '/forgot-password-page/' + user._id + '/' + linkExpirationCode, // plaintext body;
                        host: req.headers.host,
                        user: user,
                        //templateName: 'UserResetPasswordLink'
                    }

                    CommonService.mailSender(options, function (err, result) {
                        if (err) {
                            return res.json({
                                "code": 403,
                                "message": "Mail not send"
                            });
                        }
                        else if (result == false) {
                            return res.json({
                                "code": 403,
                                "message": "Mail not send"
                            });
                        }
                        else {
                            return res.json({
                                "code": 200,
                                "message": "link for reset password is sent on " + user.email
                            });

                        }

                    })


                }
            ])
            // }
        }
    },

    forgotPasswordPage: function (req, res) {

        let params = req.params;

        let userId = params.userId;
        let linkCode = params.code;

        let userFilter = {
            _id: userId
        }

        User
            .findOne(userFilter)
            .exec(function (err, user) {
                if (err) throw  err;

                if (user) {
                    if (user.forgotPasswordCode == linkCode) {
                        res.send('This link is expired !!! <br> please generate new one to reset your password.')
                    }
                    else {
                        res.render('forgotPassword', {userId: userId, status: false, message: "", linkCode: linkCode})
                    }
                }
                else {
                    return res.json({
                        "code": 403,
                        "status": "Error",
                        "message": "User not found"
                    });
                }
            })
    },

    updatePassword: function (req, res) {

        let params = req.body;
        console.log("In----Update")

        if (params.password === params.cpassword) {

            let userFilter = {
                _id: params.userId
            }

            User
                .findOne(userFilter)
                .exec(function (err, user) {
                    if (err) throw  err;

                    if (user) {

                        let newPassword = CommonService.generateHash(params.password);
                        user.password = newPassword;
                        user.forgotPasswordCode = params.linkCode;

                        user.save(function (err, updatedUser) {
                            if (err) throw err

                            if (updatedUser) {
                                res.render('forgotPassword', {
                                    message: "password changed successfully",
                                    status: true,
                                    userId: "",
                                    linkCode: ""
                                });
                            }
                            else {
                                res.render('forgotPassword', {
                                    message: "can't reset password at this moment",
                                    status: true,
                                    userId: "",
                                    linkCode: ""
                                });
                            }
                        })
                    }
                    else {
                        return res.json({
                            "code": 403,
                            "status": "Error",
                            "message": "User not found"
                        });
                    }
                })
        }
        else {
            res.render('forgotPassword', {
                message: "Confirm Password Doesn't Matched !!!",
                status: true,
                userId: params.userId,
                linkCode: params.linkCode
            });
        }

    },


}