const mongoose = require('mongoose');
const User = mongoose.model("user");
const Block = mongoose.model("block");
const Subject = mongoose.model("subject");
const SubjectTask = mongoose.model("subjecttask");
const async = require('async');
const UserConstant = require('../constant/user');
const CommonService = require('../service/CommonService');
const bcrypt = require('bcrypt-nodejs');
const ValidateService = require('../service/ValidatorService.js');
const _ = require('lodash');
var ObjectId = mongoose.Types.ObjectId();
const config = require('../config/config');
var path = require('path');
const fs = require('fs');
module.exports = {


    home: function (req, res) {
        res.send(JSON.stringify({a: 1}));
    },

    /**
     *
     * @param req
     * @param res
     */
    paginateAgreeDemo: function (req, res) {

        let collection = db.collection('user')

        collection.aggregate([
            {$match: {age: {$lt: 30}}},
            //{$match: {skype: "jmacleen"}},
            {
                $lookup: {
                    from: "Department",
                    localField: "department",
                    foreignField: "_id",
                    as: "Data_of_Department"
                }
            },
            {$match: {gender: "male"}},

            //   -----For groupwise Counting....
            {$group: {_id: "$nationality", total: {$sum: 1}}},

            //  -------For set the limit number of output....
            {$limit: 5},

            //  ------For divide the array of result.....
            {$unwind: "$name"},

            //  ----------Skip on perticular number $skip
            {$skip: 3},

            //  --------For counting all number of data.
            {$count: "Total"}

        ]).toArray((err, data) => {
            if (err) throw err;
            res.json(data);
        });

    },

    /**
     * @desc : getting list of student as per login
     * @param req
     * @param res
     */
    paginate: function (req, res) {

        let params = req.body;
        let loginUser = req.user;
        //console.log("params---", JSON.stringify(params))

        async.waterfall([

            //getting tutor and students which is created by admins
            function (wcb) {
                if (loginUser && loginUser.type === UserConstant.userType.ADMIN) {

                    let adminFilter = {
                        createdBy: mongoose.Types.ObjectId(loginUser.id),
                        type: {$in: [UserConstant.userType.STUDENT, UserConstant.userType.TUTOR]}
                    }

                    User
                        .find(adminFilter)
                        .exec(function (err, studentsTutors) {
                            if (err) throw err;

                            if (studentsTutors && studentsTutors.length) {

                                let studentIds = _.filter(studentsTutors, {type: 4});
                                studentIds = _.map(studentIds, '_id')
                                let tutorsIds = _.filter(studentsTutors, {type: 3});
                                tutorsIds = _.map(tutorsIds, '_id')


                                wcb(null, studentIds, tutorsIds)

                            }
                            else {
                                wcb(null, [], [])
                            }

                        })

                }
                else {
                    wcb(null, [], [])
                }

            },

            //getting students which is created by tutors of admin
            function (studentIds, tutorsIds, wcb) {
                if (loginUser && loginUser.type === UserConstant.userType.ADMIN) {

                    let tutorStudentIds = _.map(tutorsIds, function (subject) {
                        return mongoose.Types.ObjectId(subject)
                    })
                    let adminFilter = {
                        tutorId: {$in: tutorStudentIds},
                        type: [UserConstant.userType.STUDENT]
                    }

                    User
                        .find(adminFilter)
                        .exec(function (err, students) {
                            if (err) throw err;

                            if (students && students.length) {

                                let tutorStudnetsIds = _.map(students, '_id');
                                let finalStudentIds = studentIds.concat(tutorStudnetsIds);
                                let finalWithStudAndTutor = finalStudentIds.concat(tutorsIds)

                                wcb(null, finalWithStudAndTutor)

                            }
                            else {
                                let finalWithStudAndTutor = studentIds.concat(tutorsIds)
                                wcb(null, finalWithStudAndTutor)
                            }

                        })

                }
                else {
                    wcb(null, [])
                }

            },


            function (studentIds, wcb) {

                let filter = {};
                let skipPage;
                let limitPage;
                let sort;

                //paginate logic
                if (params.page && params.limit) {
                    skipPage = (params.page - 1) * params.limit;
                    limitPage = params.limit;
                }
                else {
                    skipPage = 0;
                    limitPage = 0;
                }

                //sorting
                if (params.sort && JSON.stringify(params.sort) !== {}) {
                    sort = params.sort
                }
                else {
                    sort = {createdDate: -1}
                }

                if (loginUser && loginUser.type == UserConstant.userType.TUTOR) {
                    filter.$or = [];
                    filter.$or.push(
                        {tutorId: mongoose.Types.ObjectId(loginUser.id)},
                        {followUpId: mongoose.Types.ObjectId(loginUser.id)})
                    //filter.tutorId = mongoose.Types.ObjectId(loginUser.id)
                    filter.isDeleted = false
                }
                /*if (loginUser && loginUser.type == UserConstant.userType.ADMIN) {
                    filter.createdBy = mongoose.Types.ObjectId(loginUser.id)
                    //filter.isDeleted = false
                }*/

                //admin users filter
                if (loginUser && loginUser.type == UserConstant.userType.ADMIN) {
                    filter._id = {$in: studentIds}
                    //filter.isDeleted = false
                }
                if (params.tutorId) {
                    filter.tutorId = mongoose.Types.ObjectId(params.tutorId)
                }

                if (params.createdBy) {
                    filter.createdBy = mongoose.Types.ObjectId(params.createdBy)
                }

                if (params.type) {
                    delete filter.tutorId
                    delete filter.isDeleted
                    filter.type = {$in: params.type}
                }

                // filter by start with
                if (params.search
                    && params.search.keys
                    && params.search.keyword) {
                    _.forEach(params.search.keys, function (key) {
                        if (key) {
                            filter[key] = {
                                $regex: "^" + params.search.keyword,
                                $options: "$i"
                            };
                        }
                    });
                }


                //filter.isDeleted = false;
                User.aggregate([
                    {
                        $match: filter,

                    },
                    /*{
                        "$lookup": {
                            "from": "users",
                            "localField": "createdBy",
                            "foreignField": "_id",
                            "as": "createdObjects"
                        }
                    },
                    {"$unwind": "$createdObjects"},
                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "tutorId",
                            "foreignField": "_id",
                            "as": "tutorObjects"
                        }
                    },
                    {"$unwind": "$tutorObjects"},*/

                    //  -------For set the limit number of output....
                    /*{
                        $limit: limitPage
                    },
                    //  ----------Skip on perticular number $skip
                    {
                        $skip: skipPage
                    },*/
                    {
                        $sort: sort
                    }
                    //  --------For counting all number of data.
                ], (err, userdata) => {
                    if (err) throw err;

                    if (userdata && userdata.length) {
                        wcb(null, userdata)
                        /* return res.json({
                             "code": 200,
                             "status": "success",
                             "data": userdata,
                         });*/
                    }
                    else {
                        return res.json({
                            "code": 403,
                            "status": "Error",
                            "message": "List not found"
                        });
                    }
                })
            },

            //getting dependent data
            function (users, wcb) {

                let allUserIds = [];

                _.each(users, function (user) {
                    if (user && user.createdBy) {
                        allUserIds.push(user.createdBy)
                    }
                    if (user && user.tutorId) {
                        allUserIds.push(user.tutorId)
                    }
                    if (user && user.followUpId) {
                        allUserIds.push(user.followUpId)
                    }
                })

                let userFilter = {
                    _id: {$in: allUserIds}
                }

                if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                    userFilter.isDeleted = false
                }

                User
                    .find(userFilter, function (err, userWithRefs) {
                        if (err) throw err;

                        if (userWithRefs && userWithRefs.length) {

                            _.each(users, function (user) {
                                if (user && user.createdBy) {
                                    let findRegCreated = _.find(userWithRefs, {_id: user.createdBy});
                                    if (findRegCreated && findRegCreated != undefined) {
                                        user.createdBy = findRegCreated
                                    }
                                }
                                if (user && user.tutorId) {
                                    let findRegTutor = _.find(userWithRefs, {_id: user.tutorId});
                                    if (findRegTutor && findRegTutor != undefined) {
                                        user.tutorId = findRegTutor

                                    }
                                }
                                if (user && user.followUpId) {
                                    let findRegFollowedUp = _.find(userWithRefs, {_id: user.followUpId});
                                    if (findRegFollowedUp && findRegFollowedUp != undefined) {
                                        user.followUpId = findRegFollowedUp

                                    }
                                }
                            });

                            wcb(null, users)
                        }
                        else {
                            wcb(null, users)
                        }
                    })
            },

            //getting tutor blocks
            function (users, wcb) {

                let blocksIds = [];
                _.each(users, function (user) {
                    if (user && user.blocks && user.blocks.length > 0) {
                        _.each(user.blocks, function (block) {
                            blocksIds.push(mongoose.Types.ObjectId(block))
                        })
                    }
                });

                if (blocksIds && blocksIds.length > 0) {

                    let blockFilter = {
                        _id: {$in: blocksIds}
                    }
                    Block
                        .find(blockFilter)
                        .exec(function (err, allBlocks) {
                            if (err) throw err

                            if (allBlocks && allBlocks.length) {
                                _.each(users, function (user) {
                                    if (user && user.blocks && user.blocks.length > 0) {
                                        _.each(user.blocks, function (block, index) {
                                            let findRegBlocks = _.find(allBlocks, {_id: mongoose.Types.ObjectId(block)});
                                            if (findRegBlocks) {
                                                user.blocks[index] = findRegBlocks
                                            }
                                        })
                                    }
                                })
                                wcb(null, users)
                            }
                            else {
                                wcb(null, users)
                            }
                        })

                }
                else {
                    wcb(null, users)
                }
            },

            //getting user subjects
            function (users, wcb) {

                let studentIds = [];
                _.each(users, function (user) {
                    if (user && user._id) {
                        studentIds.push(user._id)
                    }
                })

                let studSubjectFilter = {
                    studentId: {$in: studentIds}
                }

                if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                    studSubjectFilter.isDeleted = false
                }

                Subject
                    .find(studSubjectFilter)
                    .populate('block')
                    .exec(function (err, subjects) {
                        if (err) throw err;

                        if (subjects && subjects.length) {

                            _.each(users, function (user) {
                                if (user && user._id) {
                                    let findRegSubjects = _.filter(subjects, {studentId: user._id});
                                    if (findRegSubjects) {
                                        user.subjects = findRegSubjects;
                                        if (user && user.subjects.length) {
                                            _.each(user.subjects, function (subject) {
                                                if (!subject.updatedDate) {
                                                    subject.updatedDate = null
                                                }
                                            })
                                        }
                                    }
                                    else {
                                        user.subjects = []
                                    }
                                }
                            })
                            wcb(null, users)

                        }
                        else {
                            wcb(null, users)
                        }
                    })
            },

            //getting user task of subjects
            function (users, wcb) {

                let studTaskIds = [];
                let studIds = [];
                _.each(users, function (user) {
                    if (user && user._id) {
                        studIds.push(user._id)
                    }
                    if (user.subjects && user.subjects.length) {
                        _.each(user.subjects, function (subject) {
                            if (subject && subject._id) {
                                studTaskIds.push(subject._id)
                            }
                        })
                    }
                })

                let studMapIds = _.map(studIds, function (stud) {
                    return mongoose.Types.ObjectId(stud)
                })
                let subjectMapIds = _.map(studTaskIds, function (task) {
                    return mongoose.Types.ObjectId(task)
                })

                let taskFilter = {
                    studentId: {$in: studMapIds},
                    subjectId: {$in: subjectMapIds}
                }

                if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                    taskFilter.isDeleted = false
                }

                SubjectTask
                    .find(taskFilter)
                    .populate('block')
                    .exec(function (err, tasks) {
                        if (err) throw err;

                        if (tasks && tasks.length) {
                            _.each(users, function (user) {
                                if (user && user._id) {
                                    let findRegTasks = _.filter(tasks, {studentId: user._id});

                                    if (findRegTasks) {
                                        user.tasks = findRegTasks
                                        if (user && user.tasks.length) {
                                            _.each(user.tasks, function (subject) {
                                                if (!subject.updatedDate) {
                                                    subject.updatedDate = null
                                                }
                                            })
                                        }
                                    }
                                    else {
                                        user.tasks = []
                                    }
                                }
                            });

                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": users,
                            });

                        }
                        return res.json({
                            "code": 200,
                            "message": "success",
                            "data": users,
                        });
                    })
            }
        ])
    },

    allTutors: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkInstTutorReqValidation(params);

        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {
            let filter = {};
            let skipPage;
            let limitPage;
            let sort;

            //paginate logic
            if (params.page && params.limit) {
                skipPage = (params.page - 1) * params.limit;
                limitPage = params.limit;
            }
            else {
                skipPage = 0;
                limitPage = 0;
            }

            //sorting
            if (params.sort && JSON.stringify(params.sort) !== {}) {
                sort = params.sort
            }
            else {
                sort = {createdDate: -1}
            }

            if (params.createdBy) {
                filter.createdBy = mongoose.Types.ObjectId(params.createdBy)
            }
            if (params.instituteId) {
                filter.instituteId = mongoose.Types.ObjectId(params.instituteId),
                    filter.type = UserConstant.userType.TUTOR,
                    filter.isDeleted = false
            }

            if (params.type) {
                delete filter.tutorId
                delete filter.isDeleted
                filter.type = {$in: params.type}
            }

            // filter by start with
            if (params.search
                && params.search.keys
                && params.search.keyword) {
                _.forEach(params.search.keys, function (key) {
                    if (key) {
                        filter[key] = {
                            $regex: "^" + params.search.keyword,
                            $options: "$i"
                        };
                    }
                });
            }


            //filter.isDeleted = false;
            User.aggregate([
                {
                    $match: filter,

                },
                {
                    $sort: sort
                }
                //  --------For counting all number of data.
            ], (err, userdata) => {
                if (err) throw err;

                if (userdata && userdata.length) {
                    //wcb(null, userdata)
                    return res.json({
                        "code": 200,
                        "message": "success",
                        "data": userdata,
                    });
                }
                else {
                    return res.json({
                        "code": 403,
                        "status": "Error",
                        "message": "List not found"
                    });
                }
            })
        }
    },

    paginateV1: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        async.waterfall([

            function (wcb) {

                let filter = {};
                let skipPage;
                let limitPage;
                let sort;

                //paginate logic
                if (params.page && params.limit) {
                    filter.skipPage = (params.page - 1) * params.limit;
                    filter.limitPage = params.limit;
                }
                /*else {
                    skipPage = 0;
                    limitPage = 0;
                }*/

                //sorting
                if (params.sort) {
                    filter.sort = params.sort
                }
                else {
                    filter.sort = {createdDate: -1}
                }

                if (loginUser && loginUser.type == UserConstant.userType.TUTOR) {
                    filter.tutorId = mongoose.Types.ObjectId(loginUser.id)
                    filter.isDeleted = false
                }

                if (params.tutorId) {
                    filter.tutorId = mongoose.Types.ObjectId(params.tutorId)
                }

                if (params.createdBy) {
                    filter.createdBy = mongoose.Types.ObjectId(params.createdBy)
                }

                User
                    .find(filter)
                    .populate('createdBy')
                    .populate('deletedBy')
                    .populate('tutorId')
                    .exec(function (err, userdata) {

                        if (userdata && userdata.length) {
//                            wcb(null, userdata)
                            return res.json({
                                "code": 200,
                                "status": "success",
                                "data": userdata,
                            });

                        }
                        else {
                            return res.json({
                                "code": 403,
                                "status": "Error",
                                "message": "List not found"
                            });
                        }
                    })


                /* User.aggregate([
                     {
                         $match: filter,

                     },

                     //  -------For set the limit number of output....
                     /!*{
                         $limit: limitPage
                     },
                     //  ----------Skip on perticular number $skip
                     {
                         $skip: skipPage
                     },*!/
                     {
                         $sort: sort
                     }
                     //  --------For counting all number of data.
                 ], (err, userdata) => {
                     if (err) throw err;

                     if (userdata && userdata.length) {
                         wcb(null, userdata)
                     }
                     else {
                         return res.json({
                             "code": 403,
                             "status": "Error",
                             "message": "List not found"
                         });
                     }
                 })*/
            },

            //getting dependent data
            /*function (users, wcb) {

                let allUserIds = [];

                _.each(users, function (user) {
                    if (user && user.createdBy) {
                        allUserIds.push(user.createdBy)
                    }
                    if (user && user.tutorId) {
                        allUserIds.push(user.tutorId)
                    }
                })

                let userFilter = {
                    _id: {$in: allUserIds}
                }

                User
                    .find(userFilter, function (err, userWithRefs) {
                        if (err) throw err;

                        if (userWithRefs && userWithRefs.length) {

                            _.each(users, function (user) {
                                if (user && user.createdBy) {
                                    let findRegCreated = _.find(userWithRefs, {_id: user.createdBy});
                                    if (findRegCreated && findRegCreated != undefined) {
                                        user.createdBy = findRegCreated
                                    }
                                }
                                if (user && user.tutorId) {
                                    let findRegTutor = _.find(userWithRefs, {_id: user.tutorId});
                                    if (findRegTutor && findRegTutor != undefined) {
                                        user.tutorId = findRegTutor
                                    }
                                }
                            })

                            return res.json({
                                "code": 200,
                                "status": "success",
                                "data": users,
                            });

                        }
                        else {
                            return res.json({
                                "code": 200,
                                "status": "success",
                                "data": users,
                            });
                        }
                    })
            }*/

        ])


    },
    /**
     *
     * @param req
     * @param res
     */
    create: function (req, res) {

        let params = req.body;
        let loginUser = req.user
        console.log("In----req", params)

        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkCreateReqValidation(params);


        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                function (wcb) {

                    let checkWhere = {
                        $or: []
                    };
                    if (params.mobile) {
                        checkWhere.$or.push({
                            mobile: params.mobile
                        })
                    }
                    if (params.email) {
                        checkWhere.$or.push({
                            email: params.email
                        })
                    }

                    User
                        .findOne(checkWhere, function (err, userExist) {

                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },

                function (wcb) {


                    //image convertor
                    if (params.profile) {
                        let imagePath = CommonService.convertBase64Image(params.profile);

                        //let env = process.env.NODE_ENV || 'dev';
                        if (config.proEnv == 'dev') {
                            var appDir = path.dirname(require.main.filename);
                            params.profile = req.protocol + "://" + req.headers.host + imagePath.split('./images')[1];
                            //params.profile = appDir + "/images" + imagePath.split('./images')[1];
                        }
                        else if (config.proEnv == 'prod') {
                            params.profile = req.protocol + "://" + req.headers.host + imagePath.split('./images')[1];
                        }
                        //params.profile = config.storeImage + imagePath.split('./images')[1];
                    }
                    //decrypt password for security
                    let password = CommonService.generateHash(params.password);
                    if (password) {
                        params.password = password
                    }


                    params.createdDate = new Date();
                    params.updatedDate = null;

                    if (params.isAdmin) {
                        params.createdBy = params.isAdmin
                    }
                    else {
                        params.createdBy = loginUser.id
                    }

                    if (loginUser && loginUser.type == UserConstant.userType.TUTOR) {
                        params.tutorId = loginUser.id
                    }

                    if (params.firstName) {
                        params.name = (params.firstName).toLowerCase()
                    }

                    User
                        .create(params, function (err, user) {
                            if (err) throw err

                            if (user) {
                                wcb(null, user)
                                /*return res.json({
                                    "code": 200,
                                    "status": "success",
                                    "data": user,
                                    "message": "Success"
                                    //"token": token
                                });*/
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "data": {},
                                    "message": "Failed"
                                    //"token": token
                                });
                            }

                        })
                },

                function (user, wcb) {

                    if (user.type == UserConstant.userType.TUTOR) {

                        let getBlockFilter = {
                            isDeleted: false
                        }

                        Block
                            .find(getBlockFilter)
                            .exec(function (err, blocks) {
                                if (err) throw err

                                if (blocks && blocks.length) {

                                    _.each(blocks, function (block) {
                                        if (block) {
                                            if (user.blocks && user.blocks.length > 0) {
                                                user.blocks.push(block._id).toString()
                                            }
                                            else {
                                                user.blocks = [];
                                                user.blocks.push(block._id).toString()
                                            }
                                        }
                                    })

                                    user.save(function (err) {

                                    })

                                    return res.json({
                                        "code": 200,
                                        "status": "success",
                                        "data": user,
                                        "message": "Success"
                                        //"token": token
                                    });

                                }
                                else {
                                    return res.json({
                                        "code": 200,
                                        "status": "success",
                                        "data": user,
                                        "message": "Success"
                                        //"token": token
                                    });
                                }
                            })

                    }
                    else {
                        //wcb(null, user)
                        return res.json({
                            "code": 200,
                            "status": "success",
                            "data": user,
                            "message": "Success"
                            //"token": token
                        });
                    }

                },
            ])
        }
    },
    /**
     *
     * @param req
     * @param res
     */
    view: function (req, res) {

        let params = req.params;
        let loginUser = req.user;


        async.waterfall([

            function (wcb) {

                let userFilter = {
                    "_id": params.id
                };

                User
                    .findOne(userFilter)
                    .populate('createdBy')
                    .populate('deletedBy')
                    .populate('tutorId')
                    .populate('instituteId')
                    .exec(function (err, user) {
                        if (err) return err;

                        if (user) {

                            if (user && !user.profile) {
                                user.profile = null
                            }
                            wcb(null, user)
                        }
                        else {
                            return res.json({
                                "code": 403,
                                "status": "Error",
                                "message": "User not found"
                            });
                        }

                    })
            },
            //getting user subjects
            function (user, wcb) {

                let studentIds = [];

                if (user && user._id) {
                    studentIds.push(user._id)
                }

                let studSubjectFilter = {
                    studentId: {$in: studentIds},
                }

                if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                    studSubjectFilter.isDeleted = false
                }

                Subject
                    .find(studSubjectFilter)
                    .populate('block')
                    .populate('createdBy')
                    .exec(function (err, subjects) {
                        if (err) throw err;

                        if (subjects && subjects.length) {
                            user.subjects = subjects
                            wcb(null, user)

                        }
                        else {
                            wcb(null, user)
                        }
                    })
            },

            //getting user task of subjects
            function (user, wcb) {
                let studTaskIds = [];
                let studIds = [];
                if (user && user._id) {
                    studIds.push(user._id)
                }
                if (user.subjects && user.subjects.length) {
                    _.each(user.subjects, function (subject) {
                        if (subject && subject._id) {
                            studTaskIds.push(subject._id)
                        }
                    })
                }

                let studMapIds = _.map(studIds, function (stud) {
                    return mongoose.Types.ObjectId(stud)
                })
                let subjectMapIds = _.map(studTaskIds, function (task) {
                    return mongoose.Types.ObjectId(task)
                })
                let taskFilter = {
                    studentId: {$in: studMapIds},
                    subjectId: subjectMapIds
                }

                if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                    taskFilter.isDeleted = false
                }

                SubjectTask
                    .find(taskFilter)
                    .populate('blockId')
                    .populate('createdBy')
                    .populate('subjectId')
                    .exec(function (err, tasks) {
                        if (err) throw err;

                        if (tasks && tasks.length) {

                            user.tasks = tasks


                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": user,
                            });

                        }
                        return res.json({
                            "code": 200,
                            "message": "success",
                            "data": user,
                        });
                    })
            }

        ])


    },

    /**
     *
     * @param req
     * @param res
     */
    update: function (req, res) {
        let params = req.body;
        let loginUser = req.user;
        let idParams = req.params;
        let id = idParams.id
        params.id = id;
        /*if (params.isAdmin) {
            loginUser.id = params.isAdmin
        }*/
        console.log("Paras---", JSON.stringify(params))
        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkUpdateReqValidation(params);


        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                //checking duplication data
                function (wcb) {

                    let checkWhere = {
                        _id: {$ne: mongoose.Types.ObjectId(id)},
                        $or: []
                    };
                    if (params.mobile) {
                        checkWhere.$or.push({
                            mobile: params.mobile
                        })
                    }
                    if (params.email) {
                        checkWhere.$or.push({
                            email: params.email
                        })
                    }


                    User
                        .findOne(checkWhere, function (err, userExist) {
                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },

                //finding which data going to update
                function (wcb) {

                    let userFilter = {
                        "_id": id
                    }

                    User
                        .findOne(userFilter)
                        .exec(function (err, user) {
                            if (err) throw err;

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "User not found"
                                });
                            }
                        })
                },

                function (user, wcb) {

                    //profile convertor
                    if (params.profile) {
                        let imagePath = CommonService.convertBase64Image(params.profile);

                        //let env = process.env.NODE_ENV || 'dev';
                        if (config.proEnv == 'dev') {
                            var appDir = path.dirname(require.main.filename);
                            params.profile = appDir + "/images" + imagePath.split('./images')[1];
                        }
                        else if (config.proEnv == 'prod') {
                            params.profile = req.protocol + "://" + req.headers.host + imagePath.split('./images')[1];
                        }
                        //params.profile = config.storeImage + imagePath.split('./images')[1];
                    }

                    //decrypt password for security
                    /*  let password = CommonService.generateHash(params.password);
                      if (password) {
                          params.password = password
                      }

  */

                    if (params.tutorId == "123456") {
                        delete params.tutorId
                    }
                    params.updatedDate = new Date();

                    /*if (loginUser && loginUser.id) {
                        params.createdBy = loginUser.id
                    }*/

                    if (params.isAdmin && (params.isAdmin != "" || params.isAdmin != '')) {
                        params.createdBy = params.isAdmin
                    }
                    else {
                        params.createdBy = loginUser.id
                    }

                    if (params.type == UserConstant.userType.STUDENT && loginUser && loginUser.type == UserConstant.userType.TUTOR) {
                        if (!params.tutorId) {
                            params.tutorId = loginUser.id
                        }

                    }

                    if (params.firstName) {
                        params.name = (params.firstName).toLowerCase()
                    }
                    delete params.id

                    _.each(params, function (val, key) {
                        user[key] = val
                    });

                    user.save(function (err) {
                        if (err) throw err
                        wcb(null, user)
                    })
                },

                //sending final response
                function (user, wcb) {

                    let findWhere = {
                        _id: user._id
                    };

                    User
                        .findOne(findWhere)
                        .populate('createdBy')
                        .populate('deletedBy')
                        .populate('tutorId')
                        .exec(function (err, finalUser) {
                            if (err) throw err;

                            if (finalUser) {
                                return res.json({
                                    "code": 200,
                                    "message": "Updated SuccessFully",
                                    "data": finalUser,
                                });
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Failed to update"
                                });
                            }
                        });
                }
            ])
        }
    },
    /**
     *
     * @param req
     * @param res
     */
    destroy: function (req, res) {

        let params = req.params;
        let loginUser = req.user;

        let delFilter = {
            "_id": params.id
        };

        let options = {
            new: false
        };

        let updateData = {
            isDeleted: true,
            deletedBy: loginUser.id
        }

        User
            .findOneAndUpdate(delFilter, {$set: updateData}, options, function (err, deletedUser) {
                if (err) throw err;

                if (deletedUser) {
                    return res.json({
                        "code": 200,
                        "status": "success",
                        "data": deletedUser,
                    });
                }
                else {
                    return res.json({
                        "code": 403,
                        "status": "Error",
                        "message": "Not Deleted"
                    });
                }
            })
    },

    /**
     * @desc find and remove user subjects
     * @param req :{
     *     "studentId":"",
     *     "subjectIds":["",""]
     * }
     * @param res
     * @returns {*|Promise<any>}
     */
    removeSubject: function (req, res) {

        let params = req.body;
        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkRemSubReqValidation(params);


        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                //getting students
                function (wcb) {

                    let studFilter = {
                        _id: mongoose.Types.ObjectId(params.studentId)
                    }

                    User
                        .findOne(studFilter, function (err, user) {
                            if (err) throw  err

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Student not found"
                                });
                            }
                        })
                },
                function (user, wcb) {

                    let rejectedData = _.reject(user.subjects, function (subject) {
                        return subject == params.subjectIds
                    })
                    user.subjects = rejectedData
                    user.updatedDate = new Date();
                    // var options = { new: true }; //so that user returned is the updated not original doc
                    //save the final data
                    user.save((err, updatedInts) => {

                        if (err) throw err;
                        if (!err) {
                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": updatedInts
                            });
                        }

                    });

                }
            ])
        }
    }


}