const mongoose = require('mongoose');
const Subject = mongoose.model("subject");
const User = mongoose.model("user");
const Block = mongoose.model("block");
const SubjectTask = mongoose.model("subjecttask");
const SubjectTaskPhase = mongoose.model("subjecttaskphase");
const async = require('async');
const UserConstant = require('../constant/user');
const CommonService = require('../service/CommonService');
const config = require('../config/config');
const bcrypt = require('bcrypt-nodejs');
const ValidateService = require('../service/ValidatorService.js');
const _ = require('lodash');
var ObjectId = mongoose.Types.ObjectId();
var path = require('path');
module.exports = {

    /**
     * @desc : getting list of student as per login
     * @param req
     * @param res
     */
    paginate: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        if (params.studentId) {
            loginUser.id = params.studentId
            loginUser.type = UserConstant.userType.STUDENT
        }
        async.waterfall([

            //getting student for its subjects data
            function (wcb) {
                if (loginUser && loginUser.type == UserConstant.userType.STUDENT
                    || loginUser && loginUser.type == UserConstant.userType.TUTOR) {

                    let tutorFilter = {
                        _id: mongoose.Types.ObjectId(loginUser.id),
                        isDeleted: false
                    }

                    User
                        .findOne(tutorFilter)
                        .exec(function (err, user) {
                            if (err) throw err

                            if (user.type && user.type == UserConstant.userType.TUTOR && (!user.blocks.length)) {

                                // wcb(null, user)
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Block not found"
                                });
                            }
                            else {

                                wcb(null, user)
                            }
                        })
                }
                else {
                    wcb(null, {})
                }
            },

            function (user, wcb) {
                if (loginUser && loginUser.type === UserConstant.userType.ADMIN) {

                    let adminFilter = {
                        createdBy: mongoose.Types.ObjectId(loginUser.id),
                        type: [UserConstant.userType.TUTOR],
                        isDeleted: false
                    }

                    User
                        .find(adminFilter)
                        .exec(function (err, adminTutors) {
                            if (err) throw err;

                            if (adminTutors && adminTutors.length) {

                                let tutorsIds = _.map(adminTutors, '_id');
                                wcb(null, user, tutorsIds)

                            }
                            else {
                                wcb(null, user, [])
                            }

                        })

                }
                else {
                    wcb(null, user, [])
                }

            },


            function (user, tutorsIds, wcb) {
                let filter = {};
                let skipPage;
                let limitPage;
                let sort;

                //paginate logic
                if (params.page && params.limit) {
                    skipPage = (params.page - 1) * params.limit;
                    limitPage = params.limit;
                }
                else {
                    skipPage = 0;
                    limitPage = 0;
                }

                //sorting
                if (params.sort) {
                    sort = params.sort
                }
                else {
                    sort = {createdDate: -1}
                }


                /*  if (user && user.subjects && user.subjects.length > 0) {

                      let subjectIds = _.map(user.subjects, function (subject) {
                          return mongoose.Types.ObjectId(subject)
                      })
                      if (loginUser && loginUser.type == UserConstant.userType.STUDENT) {
                          filter._id = {$in: subjectIds}
                          filter.isDeleted = false
                      }
                  }
  */

                if (user && user.type == UserConstant.userType.STUDENT) {
                    filter.studentId = mongoose.Types.ObjectId(user._id)
                    filter.isDeleted = false
                }
                if (user && user.blocks && user.blocks.length > 0) {
                    let blockIds = _.map(user.blocks, function (subject) {
                        return mongoose.Types.ObjectId(subject)
                    })
                    if (user && user.type == UserConstant.userType.TUTOR) {
                        filter.block = {$in: blockIds}
                        filter.isDeleted = false
                        //filter.createdBy = user._id
                    }
                }
                if (loginUser && loginUser.type === UserConstant.userType.ADMIN) {
                    let allTutorIds = _.map(tutorsIds, function (subject) {
                        return mongoose.Types.ObjectId(subject)
                    });
                    filter.createdBy = {$in: allTutorIds}
                    filter.isDeleted = false
                }


                if (params.block) {
                    filter.block = mongoose.Types.ObjectId(params.block)
                    filter.isDeleted = false
                }

                if (params.createdBy) {
                    filter.createdBy = mongoose.Types.ObjectId(params.createdBy)
                }
                // filter by start with
                if (params.search
                    && params.search.keys
                    && params.search.keyword) {
                    _.forEach(params.search.keys, function (key) {
                        if (key) {
                            filter[key] = {
                                $regex: "^" + params.search.keyword,
                                $options: "$i"
                            };
                        }
                    });
                }

                Subject.aggregate([
                    {
                        $match: filter,

                    },
                    /*    {
                            "$lookup": {
                                "from": "users",
                                "localField": "createdBy",
                                "foreignField": "_id",
                                "as": "createdObjects"
                            }
                        },
                        {"$unwind": "$createdObjects"},
                        {
                            "$lookup": {
                                "from": "users",
                                "localField": "tutorId",
                                "foreignField": "_id",
                                "as": "tutorObjects"
                            }
                        },
                        {"$unwind": "$tutorObjects"},*/

                    //  -------For set the limit number of output....
                    /*{
                        $limit: limitPage
                    },
                    //  ----------Skip on perticular number $skip
                    {
                        $skip: skipPage
                    },*/
                    {
                        $sort: sort
                    }
                    //  --------For counting all number of data.
                ], (err, subjectData) => {
                    if (err) throw err;

                    if (subjectData && subjectData.length) {
                        wcb(null, subjectData)
                        /*return res.json({
                            "code": 200,
                            "status": "success",
                            "data": subjectData,
                        });*/
                    }
                    else {
                        return res.json({
                            "code": 403,
                            "status": "Error",
                            "message": "List not found"
                        });
                    }
                })
            },

            function (subjects, wcb) {

                let allblockIds = [];

                _.each(subjects, function (user) {
                    if (user && user.block) {
                        allblockIds.push(user.block)
                    }
                })

                let userFilter = {
                    _id: {$in: allblockIds}
                }

                if (loginUser && (loginUser.type == UserConstant.userType.TUTOR)) {
                    userFilter.isDeleted = false
                }

                Block
                    .find(userFilter, function (err, blocks) {
                        if (err) throw err;

                        if (blocks && blocks.length) {

                            _.each(subjects, function (user) {
                                if (user && user.block) {
                                    let findRegCreated = _.find(blocks, {_id: user.block});
                                    if (findRegCreated && findRegCreated != undefined) {
                                        user.block = findRegCreated
                                    }
                                }
                            })

                            wcb(null, subjects)

                        }
                        else {
                            wcb(null, subjects)
                        }
                    })

            },

            function (subjects, wcb) {

                let studTaskIds = [];
                _.each(subjects, function (subject) {
                    if (subject && subject._id) {
                        studTaskIds.push(subject._id)
                    }
                })

                let subjectMapIds = _.map(studTaskIds, function (task) {
                    return mongoose.Types.ObjectId(task)
                })

                let taskFilter = {
                    subjectId: {$in: subjectMapIds}
                }

                if (params.studentId) {
                    taskFilter.studentId = {$in: [params.studentId]}
                }

                if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT || loginUser.type == UserConstant.userType.ADMIN)) {
                    taskFilter.isDeleted = false
                }


                SubjectTask
                    .find(taskFilter)
                    .populate('block')
                    .exec(function (err, tasks) {
                        if (err) throw err;

                        if (tasks && tasks.length) {
                            _.each(subjects, function (subject) {
                                if (subject && subject._id) {
                                    let findRegTasks = _.filter(tasks, {subjectId: subject._id});

                                    if (findRegTasks) {
                                        subject.tasks = findRegTasks
                                        if (subject && subject.tasks.length) {
                                            _.each(subject.tasks, function (subject) {
                                                if (!subject.updatedDate) {
                                                    subject.updatedDate = null
                                                }
                                            })
                                        }
                                    }
                                    else {
                                        subject.tasks = []
                                    }
                                }
                            });

                            wcb(null, subjects)
                        }
                        else {
                            if (subjects && subjects.length) {
                                _.each(subjects, function (subject) {
                                    if (!subject.tasks) {
                                        subject.tasks = []
                                    }
                                })
                            }
                            wcb(null, subjects)
                        }
                    })
            },

            //getting dependent data
            function (subjects, wcb) {

                let allUserIds = [];

                _.each(subjects, function (user) {
                    if (user && user.createdBy) {
                        allUserIds.push(user.createdBy)
                    }
                    if (user && user.tutorId) {
                        allUserIds.push(user.tutorId)
                    }
                })

                let userFilter = {
                    _id: {$in: allUserIds}
                }

                if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT || loginUser.type == UserConstant.userType.ADMIN)) {
                    userFilter.isDeleted = false
                }

                User
                    .find(userFilter, function (err, userWithRefs) {
                        if (err) throw err;

                        if (userWithRefs && userWithRefs.length) {

                            _.each(subjects, function (user) {
                                if (user && user.createdBy) {
                                    let findRegCreated = _.find(userWithRefs, {_id: user.createdBy});
                                    if (findRegCreated && findRegCreated != undefined) {
                                        user.createdBy = findRegCreated
                                    }
                                }
                                if (user && user.tutorId) {
                                    let findRegTutor = _.find(userWithRefs, {_id: user.tutorId});
                                    if (findRegTutor && findRegTutor != undefined) {
                                        user.tutorId = findRegTutor
                                    }
                                }
                            });

                            _.each(subjects, function (subject) {
                                if (!subject.updatedDate) {
                                    subject.updatedDate = null
                                }
                            })

                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": subjects,
                            });

                        }
                        else {
                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": subjects,
                            });
                        }
                    })
            }

        ])


    },

    /**
     *
     * @param req
     * @param res
     */
    create: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkSubjCreateReqValidation(params);

        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                function (wcb) {

                    let checkWhere = {};
                    if (params.block || params.title) {
                        checkWhere.$and = [
                            {
                                title: params.title
                            },
                            {
                                block: mongoose.Types.ObjectId(params.block)
                            },
                            {
                                studentId: mongoose.Types.ObjectId(params.studentId)
                            }
                        ]
                    }

                    if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                        checkWhere.isDeleted = false
                    }
                    Subject
                        .findOne(checkWhere, function (err, userExist) {

                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },

                function (wcb) {

                    //image convertor
                    if (params.image) {
                        let imagePath = CommonService.convertBase64Image(params.image);

                        //let env = process.env.NODE_ENV || 'dev';
                        if (config.proEnv == 'dev') {
                            var appDir = path.dirname(require.main.filename);
                            params.image = appDir + "/images" + imagePath.split('./images')[1];
                        }
                        else if (config.proEnv == 'prod') {
                            params.image = config.storeImage + imagePath.split('./images')[1];
                        }
                        //params.image = config.storeImage + imagePath.split('./images')[1];
                    }
                    params.createdDate = new Date();

                    if (loginUser && loginUser.id) {
                        params.createdBy = loginUser.id
                    }

                    Subject
                        .create(params, function (err, subject) {

                            if (!err && subject) {
                                /* var token = jwt.sign({
                                     id: register_user._id,
                                     title: register_user.title
                                 }, config.secrect);*/

                                // wcb(null, subject)
                                return res.json({
                                    "code": 200,
                                    "message": "success",
                                    "data": subject,
                                    //"token": token
                                });
                            }

                        })
                },

            ])
        }
    },

    /**
     *
     * @param req
     * @param res
     */
    view: function (req, res) {

        let params = req.params;
        let loginUser = req.user;

        let userFilter = {
            "_id": params.id
        };

        Subject
            .findOne(userFilter)
            .populate('createdBy')
            .populate('deletedBy')
            .populate('block')
            .exec(function (err, subject) {
                if (err) return err;

                if (subject) {

                    return res.json({
                        "code": 200,
                        "message": "success",
                        "data": subject,
                    });
                }
                else {
                    return res.json({
                        "code": 403,
                        "status": "Error",
                        "message": "Subject not found"
                    });
                }

            })

    },

    /**
     *
     * @param req
     * @param res
     */
    update: function (req, res) {
        let params = req.body;
        let loginUser = req.user;
        let idParams = req.params;
        let id = idParams.id
        params.id = id
        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkCommonUpdateReqValidation(params);


        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                //checking duplication data
                function (wcb) {

                    let checkWhere = {
                        _id: {$ne: mongoose.Types.ObjectId(id)}
                    };

                    if (params.block || params.title) {
                        checkWhere.$and = [
                            {
                                title: params.title
                            },
                            {
                                block: mongoose.Types.ObjectId(params.block)
                            },
                            {
                                studentId: mongoose.Types.ObjectId(params.studentId)
                            }
                        ]

                    }
                    if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                        checkWhere.isDeleted = false
                    }

                    Subject
                        .findOne(checkWhere, function (err, userExist) {
                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },

                //finding which data going to update
                function (wcb) {

                    let userFilter = {
                        "_id": id
                    }
                    if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                        userFilter.isDeleted = false
                    }
                    Subject
                        .findOne(userFilter)
                        .exec(function (err, subject) {
                            if (err) throw err;

                            if (subject) {
                                wcb(null, subject)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Subject not found"
                                });
                            }
                        })
                },

                function (subject, wcb) {

                    //image convertor
                    if (params.image) {
                        let imagePath = CommonService.convertBase64Image(params.image);

                        //let env = process.env.NODE_ENV || 'dev';
                        if (config.proEnv == 'dev') {
                            var appDir = path.dirname(require.main.filename);
                            params.image = appDir + "/images" + imagePath.split('./images')[1];
                        }
                        else if (config.proEnv == 'prod') {
                            params.image = config.storeImage + imagePath.split('./images')[1];
                        }
                        //params.image = config.storeImage + imagePath.split('./images')[1];
                    }

                    //decrypt password for security
                    /*  let password = CommonService.generateHash(params.password);
                      if (password) {
                          params.password = password
                      }
  */

                    params.updatedDate = new Date();

                    if (loginUser && loginUser.id) {
                        params.createdBy = loginUser.id
                    }

                    delete params.id

                    _.each(params, function (val, key) {
                        subject[key] = val
                    });

                    subject.save(function (err) {
                        if (err) throw err
                        wcb(null, subject)
                    })
                },

                //sending final response
                function (subject, wcb) {

                    let findWhere = {
                        _id: subject._id
                    };


                    if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                        findWhere.isDeleted = false
                    }
                    Subject
                        .findOne(findWhere)
                        .populate('createdBy')
                        .populate('deletedBy')
                        .exec(function (err, subjectData) {
                            if (err) throw err;

                            if (subjectData) {
                                return res.json({
                                    "code": 200,
                                    "message": "Updated SuccessFully",
                                    "data": subjectData,
                                });
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Failed to update"
                                });
                            }
                        });
                }
            ])
        }
    },

    /**
     *
     * @param req
     * @param res
     */
    destroy: function (req, res) {

        let params = req.params;
        let loginUser = req.user;

        async.waterfall([

            //deleting main subjects
            function (wcb) {
            console.log("In--Subject")
                let delFilter = {
                    "_id": mongoose.Types.ObjectId(params.id)
                };

                let options = {
                    new: false
                };

                let updateData = {
                    isDeleted: true,
                    deletedBy: loginUser.id
                }

                Subject
                    .findOneAndUpdate(delFilter, {$set: updateData}, options, function (err, deletedSubject) {
                        if (err) throw err;

                        if (deletedSubject) {
                            /*return res.json({
                                "code": 200,
                                "message": "success",
                                "data": deletedUser,
                            });*/
                            wcb(null, deletedSubject)
                        }
                        else {
                            return res.json({
                                "code": 403,
                                "status": "Error",
                                "message": "Not Deleted"
                            });
                        }
                    })
            },

            //deleting subject task
            function (deletedSubject, wcb) {
                console.log("In--SubjectTask")
                let delFilter = {
                    "subjectId": mongoose.Types.ObjectId(deletedSubject._id)
                };

                let options = {
                    new: false
                };

                let updateData = {
                    isDeleted: true,
                    deletedBy: loginUser.id
                }

                console.log("In--SubjectTask-Filter",JSON.stringify(delFilter))

                SubjectTask
                    .findOneAndUpdate(delFilter, {$set: updateData}, options, function (err, deletedTask) {
                        if (err) throw err;

                        if (deletedTask) {
                            /*return res.json({
                                "code": 200,
                                "message": "success",
                                "data": deletedUser,
                            });*/
                            wcb(null, deletedSubject, deletedTask)
                        }
                        else {
                            return res.json({
                                "code": 403,
                                "status": "Error",
                                "message": "Not Deleted"
                            });
                        }
                    })
            },

            //deleting subject task
            function (deletedSubject, deletedTask, wcb) {
                console.log("In--SubjectTaskPhase")
                let delFilter = {
                    "subjectTaskId": mongoose.Types.ObjectId(deletedTask._id)
                };

                let options = {
                    new: false
                };

                let updateData = {
                    isDeleted: true,
                    deletedBy: loginUser.id
                }

                SubjectTaskPhase
                    .findOneAndUpdate(delFilter, {$set: updateData}, options, function (err, deletedTaskPhase) {
                        if (err) throw err;

                        if (deletedTaskPhase) {
                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": deletedSubject,
                            });
                        }
                        else {
                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": deletedSubject,
                            });
                        }
                    })
            }
        ])


    },

    /**
     * @desc : get student list by login
     * or by tutor request
     * @param req
     * @param res
     */
    studSubjectList: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        if (params.studentId) {
            loginUser.id = params.studentId
        }

        async.waterfall([

            //getting student for its subjects data
            function (wcb) {
                if (loginUser && loginUser.type == UserConstant.userType.STUDENT
                    || loginUser && loginUser.type == UserConstant.userType.TUTOR) {

                    let tutorFilter = {
                        _id: loginUser.id
                    }

                    User
                        .findOne(tutorFilter)
                        .exec(function (err, user) {
                            if (err) throw err

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                wcb(null, {})
                            }
                        })
                }
                else {
                    wcb(null, {})
                }
            },

            function (user, wcb) {

                let filter = {};
                let skipPage;
                let limitPage;
                let sort;

                //paginate logic
                if (params.page && params.limit) {
                    skipPage = (params.page - 1) * params.limit;
                    limitPage = params.limit;
                }
                else {
                    skipPage = 0;
                    limitPage = 0;
                }

                //sorting
                if (params.sort) {
                    sort = params.sort
                }
                else {
                    sort = {createdDate: -1}
                }

                if (user && user.subjects && user.subjects.length) {

                    let subjectIds = _.map(user.subjects, function (subject) {
                        return mongoose.Types.ObjectId(subject)
                    })
                    if (loginUser && loginUser.type == UserConstant.userType.STUDENT) {
                        filter._id = {$in: subjectIds}
                        filter.isDeleted = false
                    }
                }


                if (params.blockId) {
                    filter.block = mongoose.Types.ObjectId(params.blockId)
                }

                if (params.createdBy) {
                    filter.createdBy = mongoose.Types.ObjectId(params.createdBy)
                }


                Subject.aggregate([
                    {
                        $match: filter,

                    },
                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "createdBy",
                            "foreignField": "_id",
                            "as": "createdObjects"
                        }
                    },
                    {"$unwind": "$createdObjects"},
                    /*{
                        "$lookup": {
                            "from": "users",
                            "localField": "tutorId",
                            "foreignField": "_id",
                            "as": "tutorObjects"
                        }
                    },
                    {"$unwind": "$tutorObjects"},*/

                    //  -------For set the limit number of output....
                    /*{
                        $limit: limitPage
                    },
                    //  ----------Skip on perticular number $skip
                    {
                        $skip: skipPage
                    },*/
                    {
                        $sort: sort
                    }
                    //  --------For counting all number of data.
                ], (err, subjectData) => {
                    if (err) throw err;

                    if (subjectData && subjectData.length) {
                        //wcb(null, subjectData)
                        return res.json({
                            "code": 200,
                            "message": "success",
                            "data": subjectData,
                        });
                    }
                    else {
                        return res.json({
                            "code": 403,
                            "status": "Error",
                            "message": "List not found"
                        });
                    }
                })
            },
        ])


    },


}