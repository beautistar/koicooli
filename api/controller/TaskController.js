const mongoose = require('mongoose');
const Subject = mongoose.model("subject");
const User = mongoose.model("user");
const Block = mongoose.model("block");
const SubjectTask = mongoose.model("subjecttask");
const async = require('async');
const UserConstant = require('../constant/user');
const CommonService = require('../service/CommonService');
const bcrypt = require('bcrypt-nodejs');
const ValidateService = require('../service/ValidatorService.js');
const _ = require('lodash');
var ObjectId = mongoose.Types.ObjectId();
const config = require('../config/config');
var path = require('path');
module.exports = {

    /**
     * @desc : getting list of subject
     * task as per login
     * @param req
     * @param res
     */
    paginate: function (req, res) {

        let params = req.body;
        let loginUser = req.user;


        async.waterfall([
            // Getting tutors and students which is created by admin
            function (wcb) {
                if (loginUser && loginUser.type === UserConstant.userType.ADMIN) {

                    let adminFilter = {
                        createdBy: mongoose.Types.ObjectId(loginUser.id),
                        type: [UserConstant.userType.STUDENT, UserConstant.userType.TUTOR],
                        isDeleted: false
                    }

                    User
                        .find(adminFilter)
                        .exec(function (err, studentsTutors) {
                            if (err) throw err;

                            if (studentsTutors && studentsTutors.length) {

                                let studentIds = _.filter(studentsTutors, {type: 4});
                                studentIds = _.map(studentIds, '_id')
                                let tutorsIds = _.filter(studentsTutors, {type: 3});
                                tutorsIds = _.map(tutorsIds, '_id')


                                wcb(null, studentIds, tutorsIds)

                            }
                            else {
                                wcb(null, [], [])
                            }

                        })

                }
                else {
                    wcb(null, [], [])
                }

            },

            //getting students which is created by admin's tutor
            function (studentIds, tutorsIds, wcb) {
                if (loginUser && loginUser.type === UserConstant.userType.ADMIN) {

                    let adminFilter = {
                        tutorId: {$in: tutorsIds},
                        type: [UserConstant.userType.STUDENT],
                        isDeleted: false
                    }

                    User
                        .find(adminFilter)
                        .exec(function (err, students) {
                            if (err) throw err;

                            if (students && students.length) {

                                let tutorStudnetsIds = _.map(students, '_id');
                                let finalStudentIds = studentIds.concat(tutorStudnetsIds)

                                wcb(null, finalStudentIds)

                            }
                            else {
                                wcb(null, [])
                            }

                        })

                }
                else {
                    wcb(null, [])
                }

            },

            function (studentIds, wcb) {

                let filter = {};
                let skipPage;
                let limitPage;
                let sort;

                //paginate logic
                if (params.page && params.limit) {
                    skipPage = (params.page - 1) * params.limit;
                    limitPage = params.limit;
                }
                else {
                    skipPage = 0;
                    limitPage = 0;
                }

                //sorting
                if (params.sort) {
                    sort = params.sort
                }
                else {
                    sort = {createdDate: -1}
                }

                if (loginUser && loginUser.type == UserConstant.userType.TUTOR && !params.studentId) {
                    filter.createdBy = mongoose.Types.ObjectId(loginUser.id)
                    filter.isDeleted = false
                }
                if (loginUser && loginUser.type == UserConstant.userType.STUDENT) {
                    filter.studentId = mongoose.Types.ObjectId(loginUser.id)
                    filter.isDeleted = false
                }
                if (loginUser && loginUser.type == UserConstant.userType.ADMIN) {
                    filter.studentId = {$in: studentIds}
                    filter.isDeleted = false
                }

                if (params.blockId) {
                    filter.blockId = mongoose.Types.ObjectId(params.blockId)
                }
                if (params.subjectId) {
                    filter.subjectId = mongoose.Types.ObjectId(params.subjectId)
                }
                if (params.studentId) {
                    filter.studentId = mongoose.Types.ObjectId(params.studentId)
                    filter.isDeleted = false
                }

                if (params.createdBy) {
                    filter.createdBy = mongoose.Types.ObjectId(params.createdBy)
                }
                // filter by start with
                if (params.search
                    && params.search.keys
                    && params.search.keyword) {
                    _.forEach(params.search.keys, function (key) {
                        if (key) {
                            filter[key] = {
                                $regex: "^" + params.search.keyword,
                                $options: "$i"
                            };
                        }
                    });
                }

                console.log("req---",JSON.stringify(filter))

                SubjectTask
                    .find(filter)
                    .populate('createdBy')
                    .populate('studentId')
                    .populate('blockId')
                    .populate('subjectId')
                    .populate('followUpId')
                    .populate('createdByStudent')
                    .populate('createdByTutor')
                    .exec(function (err, subjectTasks) {
                        if (err) {
                            return res.json({
                                "code": 403,
                                "status": "Error",
                                "message": "List not found"
                            });
                        }
                        else if (subjectTasks && subjectTasks.length) {

                            _.each(subjectTasks, function (subject) {
                                if (!subject.updatedDate) {
                                    subject.updatedDate = null
                                }
                            })

                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": subjectTasks,
                            });
                        }
                        else {
                            return res.json({
                                "code": 403,
                                "status": "Error",
                                "message": "List not found"
                            });
                        }

                    })

            },


        ])


    },

    /**
     *
     * @param req
     * @param res
     */
    create: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkSubjTaskCreateReqValidation(params);

        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                function (wcb) {

                    let checkWhere = {};
                    if (params.title) {
                        checkWhere.title = params.title
                    }
                    if (params.blockId) {
                        checkWhere.blockId = mongoose.Types.ObjectId(params.blockId)
                    }
                    if (params.subjectId) {
                        checkWhere.subjectId = mongoose.Types.ObjectId(params.subjectId)
                    }
                    if (params.studentId) {
                        checkWhere.studentId = mongoose.Types.ObjectId(params.studentId)
                    }

                    /*if (params.blockId || params.title) {
                        checkWhere.$or = [
                            {
                                blockId: params.blockId
                            },
                            {
                                title: params.title
                            }
                        ]

                    }*/
                    if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                        checkWhere.isDeleted = false
                    }
                    SubjectTask
                        .findOne(checkWhere, function (err, userExist) {

                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },

                function (wcb) {

                    if (loginUser && loginUser.type === UserConstant.userType.STUDENT) {
                        let studFilter = {
                            _id: mongoose.Types.ObjectId(loginUser.id)
                        }

                        User
                            .findOne(studFilter)
                            .exec(function (err, student) {
                                if (err) throw err;

                                if (student && student.tutorId) {
                                    loginUser.tutorId = student.tutorId
                                }
                                wcb()

                            })
                    }
                    else {
                        wcb()
                    }

                },

                function (wcb) {

                    //image convertor
                    if (params.image) {
                        let imagePath = CommonService.convertBase64Image(params.image);

                        //let env = process.env.NODE_ENV || 'dev';
                        if (config.proEnv == 'dev') {
                            var appDir = path.dirname(require.main.filename);
                            params.image = appDir + "/images" + imagePath.split('./images')[1];
                        }
                        else if (config.proEnv == 'prod') {
                            params.image = config.storeImage + imagePath.split('./images')[1];
                        }
                        //params.image = config.storeImage + imagePath.split('./images')[1];
                    }
                    params.createdDate = new Date();

                    /*if (loginUser && loginUser.id) {
                        params.createdBy = loginUser.id
                    }*/
                    if (loginUser && loginUser.type === UserConstant.userType.STUDENT) {
                        params.createdBy = loginUser.tutorId,
                            params.createdByStudent = loginUser.id
                    }
                    else {
                        params.createdBy = loginUser.id,
                            params.createdByTutor = loginUser.id
                    }
                    SubjectTask
                        .create(params, function (err, subject) {
                            if (err) throw err

                            if (subject) {
                                wcb(null, subject)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "message": "failed during create",
                                    "data": {},
                                });
                            }

                        })
                },

                function (subject, wcb) {
                    if (subject.studentId && subject.followUpId) {

                        let studFilter = {
                            _id: subject.studentId
                        }

                        User
                            .findOne(studFilter)
                            .exec(function (err, student) {
                                if (student) {

                                    student.followUpId = subject.followUpId

                                    student.save(function (err) {

                                    })
                                    return res.json({
                                        "code": 200,
                                        "message": "success",
                                        "data": subject,
                                        //"token": token
                                    });

                                }
                                else {
                                    return res.json({
                                        "code": 200,
                                        "message": "success",
                                        "data": subject,
                                        //"token": token
                                    });
                                }
                            })

                    }
                    else {
                        return res.json({
                            "code": 200,
                            "message": "success",
                            "data": subject,
                            //"token": token
                        });
                    }
                }
            ])
        }
    },

    /**
     *
     * @param req
     * @param res
     */
    view: function (req, res) {

        let params = req.params;
        let loginUser = req.user;

        let userFilter = {
            "_id": params.id
        };

        SubjectTask
            .findOne(userFilter)
            .populate('createdBy')
            .populate('studentId')
            .populate('blockId')
            .populate('subjectId')
            .exec(function (err, subject) {
                if (err) return err;

                if (subject) {

                    return res.json({
                        "code": 200,
                        "message": "success",
                        "data": subject,
                    });
                }
                else {
                    return res.json({
                        "code": 403,
                        "status": "Error",
                        "message": "SubjectTask not found"
                    });
                }

            })

    },

    /**
     *
     * @param req
     * @param res
     */
    update: function (req, res) {
        let params = req.body;
        let loginUser = req.user;
        let idParams = req.params;
        let id = idParams.id
        params.id = id

        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkCommonUpdateReqValidation(params);



        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                //checking duplication data
                function (wcb) {

                    let checkWhere = {
                        _id: {$ne: mongoose.Types.ObjectId(params.id)}
                    };
                    if (params.title) {
                        checkWhere.title = params.title
                    }
                    if (params.blockId) {
                        checkWhere.blockId = mongoose.Types.ObjectId(params.blockId)
                    }
                    if (params.subjectId) {
                        checkWhere.subjectId = mongoose.Types.ObjectId(params.subjectId)
                    }
                    if (params.studentId) {
                        checkWhere.studentId = mongoose.Types.ObjectId(params.studentId)
                    }

                    if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                        checkWhere.isDeleted = false
                    }
                    SubjectTask
                        .findOne(checkWhere, function (err, userExist) {
                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },
                function (wcb) {
                    if (loginUser && loginUser.type === UserConstant.userType.STUDENT) {
                        let studFilter = {
                            _id: mongoose.Types.ObjectId(loginUser.id)
                        }

                        User
                            .findOne(studFilter)
                            .exec(function (err, student) {
                                if (err) throw err;

                                if (student && student.tutorId) {
                                    loginUser.tutorId = student.tutorId
                                }
                                wcb()

                            })
                    }
                    else {
                        wcb()
                    }

                },

                //finding which data going to update
                function (wcb) {
                    let userFilter = {
                        "_id": id
                    }
                    if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                        userFilter.isDeleted = false
                    }

                    SubjectTask
                        .findOne(userFilter)
                        .exec(function (err, subject) {
                            if (err) throw err;

                            if (subject) {
                                wcb(null, subject)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "SubjectTask not found"
                                });
                            }
                        })
                },

                function (subject, wcb) {
                    //image convertor
                    if (params.image) {
                        let imagePath = CommonService.convertBase64Image(params.image);

                        //let env = process.env.NODE_ENV || 'dev';
                        if (config.proEnv == 'dev') {
                            var appDir = path.dirname(require.main.filename);
                            params.image = appDir + "/images" + imagePath.split('./images')[1];
                        }
                        else if (config.proEnv == 'prod') {
                            params.image = config.storeImage + imagePath.split('./images')[1];
                        }
                        //params.image = config.storeImage + imagePath.split('./images')[1];
                    }

                    params.updatedDate = new Date();

                    if (loginUser && loginUser.type === UserConstant.userType.STUDENT) {
                        params.createdBy = loginUser.tutorId,
                            params.createdByStudent = loginUser.id
                    }
                    else {
                        params.createdBy = loginUser.id,
                            params.createdByTutor = loginUser.id
                    }

                    delete params.id

                    _.each(params, function (val, key) {
                        subject[key] = val
                    });

                    subject.save(function (err) {
                        if (err) throw err
                        wcb(null, subject)
                    })
                },

                function (subject, wcb) {
                    if (subject.studentId && subject.followUpId) {

                        let studFilter = {
                            _id: subject.studentId
                        }

                        User
                            .findOne(studFilter)
                            .exec(function (err, student) {
                                if (student) {

                                    student.followUpId = subject.followUpId

                                    student.save(function (err) {

                                    })
                                    wcb(null, subject)
                                }
                                else {
                                    wcb(null, subject)
                                }
                            })
                    }
                    else {
                        wcb(null, subject)
                    }
                },

                //sending final response
                function (subject, wcb) {

                    let findWhere = {
                        _id: subject._id
                    };

                    SubjectTask
                        .findOne(findWhere)
                        .populate('createdBy')
                        .populate('deletedBy')
                        .exec(function (err, subjectData) {
                            if (err) throw err;

                            if (subjectData) {
                                return res.json({
                                    "code": 200,
                                    "message": "Updated SuccessFully",
                                    "data": subjectData,
                                });
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Failed to update"
                                });
                            }
                        });
                }
            ])
        }
    },

    /**
     *
     * @param req
     * @param res
     */
    destroy: function (req, res) {

        let params = req.params;
        let loginUser = req.user;
        loginUser.id = mongoose.Types.ObjectId(loginUser.id)

        let delFilter = {
            "_id": mongoose.Types.ObjectId(params.id)
        };

        async.waterfall([

            //getting subject tasks
            function (wcb) {

                SubjectTask
                    .findOne(delFilter)
                    .exec(function (err, task) {
                        if (err) return err;

                        if (task && (task.createdByTutor || task.createdByTutor)) {

                            let loginId = mongoose.Types.ObjectId(loginUser.id)
                            if (_.isEqual(loginId, task.createdByTutor)) {
                                wcb()
                            }
                            /*else if (loginId == task.createdByStudent) {
                                wcb()
                            }*/
                            else {
                                return res.json({
                                    "code": 403,
                                    "message": "Un-Authorized Delete",
                                    "data": {},
                                });
                            }
                            /* wcb()*/
                        }
                        else {
                            wcb()
                        }

                    })

            },

            function (wcb) {
                let options = {
                    new: false
                };

                let updateData = {
                    isDeleted: true,
                    deletedBy: loginUser.id
                }

                SubjectTask
                    .findOneAndUpdate(delFilter, {$set: updateData}, options, function (err, deletedUser) {
                        if (err) throw err;

                        if (deletedUser) {
                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": deletedUser,
                            });
                        }
                        else {
                            return res.json({
                                "code": 403,
                                "status": "Error",
                                "message": "Not Deleted"
                            });
                        }
                    })
            }

        ])


    },

    studSubjectTaskList: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        if (params.studentId) {
            loginUser.id = params.studentId
        }

        async.waterfall([

            //getting student for its subjects data
            function (wcb) {
                if (loginUser && loginUser.type == UserConstant.userType.STUDENT
                    || loginUser && loginUser.type == UserConstant.userType.TUTOR) {

                    let tutorFilter = {
                        _id: mongoose.Types.ObjectId(loginUser.id)
                    }

                    User
                        .findOne(tutorFilter)
                        .exec(function (err, user) {
                            if (err) throw err

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                wcb(null, {})
                            }
                        })
                }
                else {
                    wcb(null, {})
                }
            },

            function (user, wcb) {

                let filter = {};
                let skipPage;
                let limitPage;
                let sort;

                //paginate logic
                if (params.page && params.limit) {
                    skipPage = (params.page - 1) * params.limit;
                    limitPage = params.limit;
                }
                else {
                    skipPage = 0;
                    limitPage = 0;
                }

                //sorting
                if (params.sort) {
                    sort = params.sort
                }
                else {
                    sort = {createdDate: -1}
                }

                if (user && user.subjects && user.subjects.length) {

                    let subjectIds = _.map(user.subjects, function (subject) {
                        return mongoose.Types.ObjectId(subject)
                    })
                    if (loginUser && loginUser.type == UserConstant.userType.STUDENT) {
                        filter.subjectId = {$in: subjectIds}
                        filter.isDeleted = false
                    }
                }


                if (params.blockId) {
                    filter.blockId = mongoose.Types.ObjectId(params.blockId)
                }

                if (params.createdBy) {
                    filter.createdBy = mongoose.Types.ObjectId(params.createdBy)
                }


                SubjectTask.aggregate([
                    {
                        $match: filter,

                    },
                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "createdBy",
                            "foreignField": "_id",
                            "as": "createdObjects"
                        }
                    },
                    {"$unwind": "$createdObjects"},
                    /*{
                        "$lookup": {
                            "from": "users",
                            "localField": "tutorId",
                            "foreignField": "_id",
                            "as": "tutorObjects"
                        }
                    },
                    {"$unwind": "$tutorObjects"},*/

                    //  -------For set the limit number of output....
                    /*{
                        $limit: limitPage
                    },
                    //  ----------Skip on perticular number $skip
                    {
                        $skip: skipPage
                    },*/
                    {
                        $sort: sort
                    }
                    //  --------For counting all number of data.
                ], (err, subjectTasks) => {
                    if (err) throw err;

                    if (subjectTasks && subjectTasks.length) {
                        //wcb(null, subjectTasks)
                        return res.json({
                            "code": 200,
                            "message": "success",
                            "data": subjectTasks,
                        });
                    }
                    else {
                        return res.json({
                            "code": 403,
                            "status": "Error",
                            "message": "List not found"
                        });
                    }
                })
            },
        ])


    },


}