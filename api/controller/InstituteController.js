const mongoose = require('mongoose');
const Institute = mongoose.model("institute");
const User = mongoose.model("user");
const Subject = mongoose.model("subject");
const async = require('async');
const UserConstant = require('../constant/user');
const CommonService = require('../service/CommonService');
const bcrypt = require('bcrypt-nodejs');
const ValidateService = require('../service/ValidatorService.js');
const _ = require('lodash');
var ObjectId = mongoose.Types.ObjectId();
const config = require('../config/config');
var path = require('path');
module.exports = {

    /**
     * @desc : getting list of student as per login
     * @param req
     * @param res
     */
    paginate: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        async.waterfall([


            //getting student for its subjects data
            function (wcb) {
                if (loginUser && loginUser.type == UserConstant.userType.ADMIN
                    || loginUser && loginUser.type == UserConstant.userType.TUTOR) {

                    let tutorFilter = {
                        _id: loginUser.id
                    }

                    User
                        .findOne(tutorFilter)
                        .exec(function (err, user) {
                            if (err) throw err

                            if (!user.instituteId) {

                                // wcb(null, user)
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Institute not found"
                                });
                            }
                            else {

                                wcb(null, user)
                            }
                        })
                }
                else {
                    wcb(null, {})
                }
            },

            //getting institutes
            function (user, wcb) {

                let filter = {};
                let skipPage;
                let limitPage;
                let sort;

                //paginate logic
                if (params.page && params.limit) {
                    skipPage = (params.page - 1) * params.limit;
                    limitPage = params.limit;
                }
                else {
                    skipPage = 0;
                    limitPage = 0;
                }

                //sorting
                if (params.sort) {
                    sort = params.sort
                }
                else {
                    sort = {createdDate: -1}
                }

                if (params.createdBy) {
                    filter.createdBy = mongoose.Types.ObjectId(params.createdBy)
                }

                if (user && user.instituteId) {
                    filter._id = user.instituteId
                }

                console.log("filter----",JSON.stringify(filter))

                // filter by start with
                if (params.search
                    && params.search.keys
                    && params.search.keyword) {
                    _.forEach(params.search.keys, function (key) {
                        if (key) {
                            filter[key] = {
                                $regex: "^" + params.search.keyword,
                                $options: "$i"
                            };
                        }
                    });
                }



                Institute.aggregate([
                    {
                        $match: filter,

                    },
                    /* {
                         "$lookup": {
                             "from": "users",
                             "localField": "createdBy",
                             "foreignField": "_id",
                             "as": "createdObjects"
                         }
                     },
                     {"$unwind": "$createdObjects"},*/

                    //  -------For set the limit number of output....
                    /*{
                        $limit: limitPage
                    },
                    //  ----------Skip on perticular number $skip
                    {
                        $skip: skipPage
                    },*/
                    {
                        $sort: sort
                    }
                    //  --------For counting all number of data.
                ], (err, institutes) => {
                    if (err) throw err;

                    if (institutes && institutes.length) {
                        wcb(null, institutes)
                        /*return res.json({
                            "code": 200,
                            "status": "success",
                            "data": institutes,
                        });*/
                    }
                    else {
                        return res.json({
                            "code": 403,
                            "status": "Error",
                            "message": "List not found"
                        });
                    }
                })
            },

            //getting created by
            function (institutes) {

                let allUserIds = [];

                _.each(institutes, function (user) {
                    if (user && user.createdBy) {
                        allUserIds.push(user.createdBy)
                    }
                })

                let userFilter = {
                    _id: {$in: allUserIds}
                }

                User
                    .find(userFilter, function (err, userWithRefs) {
                        if (err) throw err;

                        if (userWithRefs && userWithRefs.length) {

                            _.each(institutes, function (user) {
                                if (user && user.createdBy) {
                                    let findRegCreated = _.find(userWithRefs, {_id: user.createdBy});
                                    if (findRegCreated && findRegCreated != undefined) {
                                        user.createdBy = findRegCreated
                                    }
                                }
                            });

                            return res.json({
                                "code": 200,
                                "status": "success",
                                "data": institutes,
                            });

                        }
                        else {
                            return res.json({
                                "code": 200,
                                "status": "success",
                                "data": institutes,
                            });
                        }
                    })
            }

        ])


    },

    /**
     *
     * @param req
     * @param res
     */
    create: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkInstCreateReqValidation(params);


        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                function (wcb) {

                    let checkWhere = {};
                    if (params.title) {
                        checkWhere.title = params.title
                    }

                    Institute
                        .findOne(checkWhere, function (err, userExist) {

                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },

                function (wcb) {

                    params.createdDate = new Date();

                    if (loginUser && loginUser.id) {
                        params.createdBy = loginUser.id
                    }

                    Institute
                        .create(params, function (err, institute) {
                            if (err) throw err

                            if (institute) {
                                return res.json({
                                    "code": 200,
                                    "message": "success",
                                    "data": institute,
                                });
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "message": "failed",
                                    "data": {},
                                });
                            }

                        })
                }
            ])
        }
    },
    /**
     *
     * @param req
     * @param res
     */
    view: function (req, res) {

        let params = req.params;
        let loginUser = req.user;

        async.waterfall([

            function (wcb) {

                let userFilter = {
                    "_id": params.id
                };

                Institute
                    .findOne(userFilter)
                    .populate('createdBy')
                    /* .populate('deletedBy')*/
                    .exec(function (err, block) {
                        if (err) return err;

                        if (block) {

                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": block,
                            });
                        }
                        else {
                            return res.json({
                                "code": 403,
                                "status": "Error",
                                "message": "Institute not found"
                            });
                        }

                    })
            },


        ])


    },


    /**
     *
     * @param req
     * @param res
     */
    update: function (req, res) {
        let params = req.body;
        let loginUser = req.user;
        let idParams = req.params;
        let id = idParams.id
        params.id = id

        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkCommonUpdateReqValidation(params);


        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                //checking duplication data
                function (wcb) {

                    let checkWhere = {
                        _id: {$ne: mongoose.Types.ObjectId(id)}
                    };
                    if (params.title) {
                        checkWhere.title = params.title
                    }
                    Institute
                        .findOne(checkWhere, function (err, userExist) {
                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },

                //finding which data going to update
                function (wcb) {

                    let userFilter = {
                        "_id": id
                    }

                    Institute
                        .findOne(userFilter)
                        .exec(function (err, institute) {
                            if (err) throw err;

                            if (institute) {
                                wcb(null, institute)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Institute not found"
                                });
                            }
                        })
                },

                function (institute, wcb) {

                    params.updatedDate = new Date();

                    if (loginUser && loginUser.id) {
                        params.createdBy = loginUser.id
                    }

                    delete params.id

                    _.each(params, function (val, key) {
                        institute[key] = val
                    });

                    institute.save(function (err) {
                        if (err) throw err
                        wcb(null, institute)
                    })
                },

                //sending final response
                function (institute, wcb) {

                    let findWhere = {
                        _id: institute._id
                    };

                    Institute
                        .findOne(findWhere)
                        .populate('createdBy')
                        .exec(function (err, instituteData) {
                            if (err) throw err;

                            if (instituteData) {
                                return res.json({
                                    "code": 200,
                                    "message": "Updated SuccessFully",
                                    "data": instituteData,
                                });
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Failed to update"
                                });
                            }
                        });
                }
            ])
        }
    }
    ,
    /**
     *
     * @param req
     * @param res
     */
    destroy: function (req, res) {

        let params = req.params;
        let loginUser = req.user;

        let delFilter = {
            "_id": mongoose.Types.ObjectId(params.id)
        };

        let options = {
            new: false
        };

        let updateData = {
            isDeleted: true,
            deletedBy: loginUser.id
        }

        Institute
            .findOneAndUpdate(delFilter, {$set: updateData}, options, function (err, deletedUser) {
                if (err) throw err;

                if (deletedUser) {
                    return res.json({
                        "code": 200,
                        "message": "success",
                        "data": deletedUser,
                    });
                }
                else {
                    return res.json({
                        "code": 403,
                        "status": "Error",
                        "message": "Not Deleted"
                    });
                }
            })
    },


}