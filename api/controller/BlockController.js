const mongoose = require('mongoose');
const Block = mongoose.model("block");
const User = mongoose.model("user");
const Subject = mongoose.model("subject");
const async = require('async');
const UserConstant = require('../constant/user');
const CommonService = require('../service/CommonService');
const bcrypt = require('bcrypt-nodejs');
const ValidateService = require('../service/ValidatorService.js');
const _ = require('lodash');
var ObjectId = mongoose.Types.ObjectId();
const config = require('../config/config');
var path = require('path');
module.exports = {

    /**
     * @desc : getting list of student as per login
     * @param req
     * @param res
     */
    paginate: function (req, res) {

        let params = req.body;
        let loginUser = req.user;

        if (params.studentId) {
            loginUser.id = mongoose.Types.ObjectId(params.studentId),
                loginUser.type = 4
        }

        async.waterfall([


            //getting login user tutor
            function (wcb) {
                if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {

                    let tutorFilter = {
                        _id: mongoose.Types.ObjectId(loginUser.id),
                        isDeleted: false
                    }

                    User
                        .findOne(tutorFilter)
                        .exec(function (err, user) {
                            if (err) throw err

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                wcb(null, {})
                            }
                        })
                }
                else {
                    wcb(null, {})
                }
            },


            //getting tutor blocks
            function (user, wcb) {

                let filter = {};
                let skipPage;
                let limitPage;
                let sort;

                //paginate logic
                if (params.page && params.limit) {
                    skipPage = (params.page - 1) * params.limit;
                    limitPage = params.limit;
                }
                else {
                    skipPage = 0;
                    limitPage = 0;
                }

                //sorting
                if (params.sort) {
                    sort = params.sort
                }
                else {
                    sort = {sequence: 1}
                }

                if (user && user.blocks && user.blocks.length) {

                    let blockIds = _.map(user.blocks, function (block) {
                        return mongoose.Types.ObjectId(block)
                    })
                    if (loginUser && (loginUser.type == UserConstant.userType.TUTOR || loginUser.type == UserConstant.userType.STUDENT)) {
                        filter._id = {$in: blockIds}
                        filter.isDeleted = false
                    }
                }


                if (params.tutorId) {
                    filter.tutorId = mongoose.Types.ObjectId(params.tutorId)
                }
                /* if (loginUser.type === UserConstant.userType.STUDENT) {
                     filter.studentId = mongoose.Types.ObjectId(loginUser.id)
                 }*/

                if (params.createdBy) {
                    filter.createdBy = mongoose.Types.ObjectId(params.createdBy)
                }

                // filter by start with
                if (params.search
                    && params.search.keys
                    && params.search.keyword) {
                    _.forEach(params.search.keys, function (key) {
                        if (key) {
                            filter[key] = {
                                $regex: "^" + params.search.keyword,
                                $options: "$i"
                            };
                        }
                    });
                }

                Block.aggregate([
                    {
                        $match: filter,

                    },
                    /* {
                         "$lookup": {
                             "from": "users",
                             "localField": "createdBy",
                             "foreignField": "_id",
                             "as": "createdObjects"
                         }
                     },
                     {"$unwind": "$createdObjects"},*/

                    //  -------For set the limit number of output....
                    /*{
                        $limit: limitPage
                    },
                    //  ----------Skip on perticular number $skip
                    {
                        $skip: skipPage
                    },*/
                    {
                        $sort: sort
                    }
                    //  --------For counting all number of data.
                ], (err, blocks) => {
                    if (err) throw err;

                    if (blocks && blocks.length) {
                        wcb(null, user, blocks)
                        /*return res.json({
                            "code": 200,
                            "status": "success",
                            "data": blocks,
                        });*/
                    }
                    else {
                        return res.json({
                            "code": 403,
                            "status": "Error",
                            "message": "List not found"
                        });
                    }
                })
            },

            //getting block subjects
            function (user, blocks, wcb) {

                let blockIds = [];
                _.each(blocks, function (block) {
                    blockIds.push(block._id)
                })


                let blockSubFilter = {
                    block: {$in: blockIds},
                }
                if (loginUser.type === UserConstant.userType.STUDENT || UserConstant.userType.TUTOR || UserConstant.userType.ADMIN) {
                    blockSubFilter.studentId =  mongoose.Types.ObjectId(loginUser.id),
                        blockSubFilter.isDeleted = false
                }

                Subject
                    .find(blockSubFilter)
                    .exec(function (err, subjects) {
                        if (err) wcb(err)

                        if (subjects && subjects.length) {

                            _.each(blocks, function (blockObj) {
                                if (blockObj && blockObj._id) {
                                    let findRegCreated = _.filter(subjects, {block: blockObj._id});
                                    if (findRegCreated && findRegCreated != undefined) {
                                        blockObj.subjects = findRegCreated
                                    }
                                    else {
                                        blockObj.subjects = []
                                    }
                                }
                            });
                            wcb(null, user, blocks)

                        }
                        else {
                            _.each(blocks, function (block) {
                                block.subjects = []
                            })
                            wcb(null, user, blocks)
                        }
                    })

            },

            //getting created by
            function (user, blocks) {
                let allUserIds = [];

                _.each(blocks, function (user) {
                    if (user && user.createdBy) {
                        allUserIds.push(user.createdBy)
                    }
                })

                let userFilter = {
                    _id: {$in: allUserIds},
                    // isDeleted: false
                }

                if (loginUser.type === UserConstant.userType.STUDENT || UserConstant.userType.TUTOR || UserConstant.userType.ADMIN) {
                    userFilter.isDelete = false
                }
                User
                    .find(userFilter, function (err, userWithRefs) {
                        if (err) throw err;

                        if (userWithRefs && userWithRefs.length) {

                            _.each(blocks, function (user) {
                                if (user && user.createdBy) {
                                    let findRegCreated = _.find(userWithRefs, {_id: user.createdBy});
                                    if (findRegCreated && findRegCreated != undefined) {
                                        user.createdBy = findRegCreated
                                    }
                                }
                            });

                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": blocks,
                            });

                        }
                        else {
                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": blocks,
                            });
                        }
                    })
            }

        ])


    },

    /**
     *
     * @param req
     * @param res
     */
    create: function (req, res) {

        let params = req.body;
        let loginUser = req.user;


        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkBlockCreateReqValidation(params);


        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                function (wcb) {

                    let checkWhere = {};
                    if (params.title) {
                        checkWhere.title = params.title
                    }

                    Block
                        .findOne(checkWhere, function (err, userExist) {

                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },

                function (wcb) {

                    //image convertor
                    if (params.image) {

                        let imagePath = CommonService.convertBase64Image(params.image);
                        //let env = process.env.NODE_ENV || 'dev';

                        if (config.proEnv == 'dev') {
                            var appDir = path.dirname(require.main.filename);
                            params.image = appDir + "/images" + imagePath.split('./images')[1];
                        }
                        else if (config.proEnv == 'prod') {
                            params.image = config.storeImage + imagePath.split('./images')[1];
                        }
                    }

                    params.createdDate = new Date();

                    if (loginUser && loginUser.id) {
                        params.createdBy = loginUser.id
                    }

                    Block
                        .create(params, function (err, block) {

                            if (!err && block) {
                                /* var token = jwt.sign({
                                     id: register_user._id,
                                     title: register_user.title
                                 }, config.secrect);*/

                                return res.json({
                                    "code": 200,
                                    "message": "success",
                                    "data": block,
                                    //"token": token
                                });
                            }

                        })
                }
            ])
        }
    },
    /**
     *
     * @param req
     * @param res
     */
    view: function (req, res) {

        let params = req.params;
        let loginUser = req.user;

        async.waterfall([

            function (wcb) {

                let userFilter = {
                    "_id": params.id
                };

                Block
                    .findOne(userFilter)
                    /* .populate('createdBy')
                     .populate('deletedBy')*/
                    .exec(function (err, block) {
                        if (err) return err;

                        if (block) {

                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": block,
                            });
                        }
                        else {
                            return res.json({
                                "code": 403,
                                "status": "Error",
                                "message": "Block not found"
                            });
                        }

                    })
            },


        ])


    },

    blockSubjects: function (req, res) {

        let params = req.params;
        let loginUser = req.user;

        async.waterfall([

            //getting subjects of blocks
            function (wcb) {

                let blockFilter = {
                    block: mongoose.Types.ObjectId(params.id)
                }
                Subject
                    .find(blockFilter)
                    .exec(function (err, blockSubs) {
                        if (err) throw err

                        if (blockSubs && blockSubs.length) {

                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": blockSubs,
                            });
                        }
                        else {
                            return res.json({
                                "code": 403,
                                "message": "Subject Detail not found",
                                "data": [],
                            });
                        }
                    })

            }

        ])


    },

    /**
     *
     * @param req
     * @param res
     */
    update: function (req, res) {
        let params = req.body;
        let loginUser = req.user;
        let idParams = req.params;
        let id = idParams.id
        params.id = id

        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkCommonUpdateReqValidation(params);


        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                //checking duplication data
                function (wcb) {

                    let checkWhere = {
                        _id: {$ne: mongoose.Types.ObjectId(id)}
                    };
                    if (params.title) {
                        checkWhere.title = params.title
                    }
                    Block
                        .findOne(checkWhere, function (err, userExist) {
                            if (err) throw err;

                            if (userExist) {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "You entered duplicate data,check again"
                                });
                            }
                            else {
                                wcb()
                            }
                        });

                },

                //finding which data going to update
                function (wcb) {

                    let userFilter = {
                        "_id": id
                    }

                    Block
                        .findOne(userFilter)
                        .exec(function (err, block) {
                            if (err) throw err;

                            if (block) {
                                wcb(null, block)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Block not found"
                                });
                            }
                        })
                },

                function (block, wcb) {

                    //image convertor
                    if (params.image) {
                        let imagePath = CommonService.convertBase64Image(params.image);

                        //let env = process.env.NODE_ENV || 'dev';
                        if (config.proEnv == 'dev') {
                            var appDir = path.dirname(require.main.filename);
                            params.image = appDir + "/images" + imagePath.split('./images')[1];
                        }
                        else if (config.proEnv == 'prod') {
                            params.image = config.storeImage + imagePath.split('./images')[1];
                        }
                        //params.image = config.storeImage + imagePath.split('./images')[1];
                    }

                    params.updatedDate = new Date();

                    if (loginUser && loginUser.id) {
                        params.createdBy = loginUser.id
                    }

                    delete params.id

                    _.each(params, function (val, key) {
                        block[key] = val
                    });

                    block.save(function (err) {
                        if (err) throw err
                        wcb(null, block)
                    })
                },

                //sending final response
                function (block, wcb) {

                    let findWhere = {
                        _id: block._id
                    };

                    Block
                        .findOne(findWhere)
                        .populate('createdBy')
                        .populate('deletedBy')
                        .exec(function (err, subjectData) {
                            if (err) throw err;

                            if (subjectData) {
                                return res.json({
                                    "code": 200,
                                    "message": "Updated SuccessFully",
                                    "data": subjectData,
                                });
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Failed to update"
                                });
                            }
                        });
                }
            ])
        }
    }
    ,
    /**
     *
     * @param req
     * @param res
     */
    destroy: function (req, res) {

        let params = req.params;
        let loginUser = req.user;

        let delFilter = {
            "_id": params.id
        };

        let options = {
            new: false
        };

        let updateData = {
            isDeleted: true,
            deletedBy: loginUser.id
        }

        Block
            .findByIdAndUpdate(delFilter, {$set: updateData}, options, function (err, deletedUser) {
                if (err) throw err;

                if (deletedUser) {
                    return res.json({
                        "code": 200,
                        "message": "success",
                        "data": deletedUser,
                    });
                }
                else {
                    return res.json({
                        "code": 403,
                        "status": "Error",
                        "message": "Not Deleted"
                    });
                }
            })
    },

    /**
     * @desc find and remove user subjects
     * @param req :{
     *     "tutorId":"",
     *     "blockIds":["",""]
     * }
     * @param res
     * @returns {*|Promise<any>}
     */
    removeBlock: function (req, res) {

        let params = req.body;
        //check required field
        let isValid = ValidateService.checkCommonValidation(params);
        let reqValid = ValidateService.checkRemBlockReqValidation(params);


        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!reqValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter required data"
            });
        }
        else {

            async.waterfall([

                //getting students
                function (wcb) {

                    let studFilter = {
                        _id: mongoose.Types.ObjectId(params.tutorId)
                    }

                    User
                        .findOne(studFilter, function (err, user) {
                            if (err) throw  err

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "Tutor not found"
                                });
                            }
                        })
                },
                function (user, wcb) {

                    let rejectedData = _.reject(user.blocks, function (subject) {
                        return subject == params.blockIds
                    })
                    user.blocks = rejectedData
                    user.updatedDate = new Date();
                    // var options = { new: true }; //so that user returned is the updated not original doc
                    //save the final data
                    user.save((err, updatedInts) => {

                        if (err) throw err;
                        if (!err) {
                            return res.json({
                                "code": 200,
                                "message": "success",
                                "data": updatedInts
                            });
                        }

                    });

                }
            ])
        }
    }


}