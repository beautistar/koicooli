var mongoose = require('mongoose');
var fs = require('fs');
const request = require('request');
const _ = require('lodash');
var validate = require('../service/ValidatorService.js');
const async = require('async');
var random = require('mongoose-random');
var interest = mongoose.model("user");
var register = mongoose.model("user");


//getting celebs data with images
exports.googleCelebs = function (req, res) {

    let params = req.body
    let existsData = [];
    let successData = [];
    const fs = require('fs')
    let jsonData = {}
    let count = 0;
    let finalResult = []
    // read file for json request
    fs.readFile('./celebrityMaster.json', 'utf-8', (err, data) => {
        if (err) throw err
        //convert into json
        jsonData = JSON.parse(data)

        //getting all details of celebs
        let celebSearches = _.map(jsonData.CelebrityValues)
        //loop for one by one celeb details
        async.eachSeries(celebSearches,
            function (celeb, ecb) {
                console.log("Count++", count++)

                //var google_api_key = "AIzaSyBWdKCs2xEHvpFTm29nvFuaUPYlXN6tzf8";
                //let getUrl = 'https://www.googleapis.com/books/v1/volumes?q=' + book + '&key=' + google_api_key + ''
                let getUrl = 'https://celebritybucks.com/images/celebs/full/' + celeb.celebId + '.jpg'
                //https://www.googleapis.com/books/v1/volumes?q={search terms}
                console.log("GetUrll", getUrl)

                //manupalation of data for save into db
                if (getUrl) {
                    let cat = new interest({
                        category: "Celebrity",
                        title: celeb.name ? celeb.name : "",
                        profile: getUrl ? getUrl : ""
                    })

                    finalResult.push(cat)
                    // console.log("finalResult------------",finalResult)
                    cat.save((err, created) => {
                        if (err) throw err;

                    });
                    // console.log("222222222222")
                    ecb()
                } else {
                    //  console.log("333333333333")
                    ecb()
                }
            },
            //final callback result
            function (err, result) {
                console.log("Final------------")
                res.json({
                    "status": 200,
                    "Data": finalResult
                });

            })
    })


};

//calling for user payement (Stripe gateway)
exports.getUserPayement = function (req, res) {
    console.log("In-------")

    let params = req.body
    let user_id = req.user.id;
    //check required field
    let isValid = validate.checkCommonValidation(params);

    if (!isValid) {
        return res.json({
            "code": 403,
            "status": "Error",
            "message": "Please enter proper data"
        });
    } else {
        //stripe public key
        const keyPublishable = 'pk_test_LSOdm45Mix3B9VHlJ3fbUUsu';
        //stripe secret key
        const keySecret = 'sk_test_KajG24q0WLZa4nfZxmDqsfR8';

        // import and create stripe object
        const stripe = require("stripe")(keySecret);
        let stripeAmount
        let c_amount
        let sb_amount
        let coins
        let validity
        let subType

        async.waterfall([

            //getting login user for update coins
            function (wcb) {

                let getUserFilter = {
                    _id: user_id
                }

                register
                    .findOne(getUserFilter)
                    .exec(function (err, loginUser) {
                        if (err) throw err

                        if (loginUser) {
                            wcb(null, loginUser)
                        } else {
                            return res.json({
                                "status": 500,
                                "message": "User not found"
                            });
                        }
                    })
            },

            function (user, wcb) {


                if (params.type === "coin") {
                    stripeAmount = params.c_amount * 100 // 500 cents means $5 (for test)

                    if (user.coins && user.coins > 0) {
                        console.log("Plus")
                        c_amount = params.c_amount + user.coin_amount
                        coins = params.coins + user.coins
                        subType = "coin"
                    } else {
                        c_amount = params.c_amount,
                            coins = params.coins,
                            subType = "coin"
                    }

                } else {
                    stripeAmount = params.sb_amount * 100; // 500 cents means $5 (for test)
                    sb_amount = params.sb_amount,
                        validity = params.validity,
                        subType = "subscription"
                }

                //console.log("coins-----------", coins)
                //console.log("c_amount-----------", c_amount)
                // create a customer 
                stripe.customers.create({
                    email: params.stripeEmail, // customer email, which user need to enter while making payment
                    source: params.stripeToken // token for the given card
                })
                    .then(customer =>
                        stripe.charges.create({ // charge the customer
                            amount: stripeAmount,
                            description: subType,
                            currency: "usd",
                            customer: customer.id
                        }))
                    .then(charge => {

                        let userFilter = {
                            '_id': user_id
                        }
                        //console.log("User:", userFilter)
                        let updateUser = {}
                        let options = {
                            new: true
                        }

                        if (sb_amount && sb_amount != undefined) {
                            updateUser.subscription_amount = sb_amount
                            updateUser.validity = validity
                        }
                        if (c_amount && c_amount != undefined) {
                            updateUser.coin_amount = c_amount
                            updateUser.coins = coins
                        }


                        register.findOneAndUpdate({
                            _id: user_id
                        }, {
                            $set: updateUser
                        }, options, function (err, updatedInts) {
                            if (err) throw err;
                            if (!err) {
                                return res.json({
                                    "code": 200,
                                    "status": "success",
                                    "data": updatedInts
                                });
                            }
                        })

                    });


            }

        ])

    }

},


    exports.commonImageUploader = function (req, res) {

        let params = req.body;

        var Storage = multer.diskStorage({
            destination: function (req, file, callback) {
                callback(null, "../images");
            },
            filename: function (req, file, callback) {
                callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
            }
        });

        var upload = multer({
            storage: Storage
        }).array("imgUploader", 3); //Field name and max count


        upload(req, res, function (err) {
            if (err) {
                return res.end("Something went wrong!");
            }
            return res.end("File uploaded sucessfully!.");
        });

    }