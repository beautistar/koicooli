var mongoose = require('mongoose');
var User = mongoose.model("user");
let async = require('async');
var validate = require('../service/ValidatorService.js');

module.exports = {

    adduserFilter: function (req, res) {

        let params = req.body
        let user_id = req.user.id

        //check required field
        let isValid = validate.checkCommonValidation(params);

        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!params.user_filter) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else {

            async.waterfall([

                //getting login user data
                function (wcb) {

                    let userFilter = {
                        _id: user_id
                    }

                    User
                        .findOne(userFilter)
                        .exec(function (err, user) {
                            if (err) throw err

                            if (user) {
                                wcb(null, user)
                            }
                            else {
                                return res.json({
                                    "code": 403,
                                    "status": "Error",
                                    "message": "User not found"
                                });
                            }

                        })
                },

                //save user filter
                function (user, wcb) {

                    if (user && !user.user_filter) {
                        user.user_filter = {}
                    }
                    /*              if(params.user_filter && params.user_filter.age_from){
                                    user.user_filter.age_from = params.user_filter.age_from
                                 }
                                 if(params.user_filter && params.user_filter.age_to){
                                    user.user_filter.age_to = params.user_filter.age_to
                                 }
                                 if(params.user_filter && params.user_filter.distance){
                                    user.user_filter.distance = params.user_filter.distance
                                  }
                                  if(params.user_filter && params.user_filter.height){
                                    user.user_filter.height = params.user_filter.height
                                  }
                                  if(params.user_filter && params.user_filter.religion && params.user_filter.religion.length){
                                    user.user_filter.religion.push(params.user_filter.religion)
                                  }
                                  else{
                                     user.user_filter.religion =[];
                                     user.user_filter.religion.push(params.user_filter.religion)
                                  }
                                  if(params.user_filter && params.user_filter.relationship && params.user_filter.relationship.length){
                                      user.user_filter.relationship.push(params.user_filter.relationship)
                                  }
                                  else{
                                      user.user_filter.relationship =[];
                                      user.user_filter.relationship.push(params.user_filter.relationship)
                                  }
                                  if(params.user_filter && params.user_filter.body_type && params.user_filter.body_type.length){
                                      user.user_filter.body_type.push(params.user_filter.body_type)
                                  }
                                  else{
                                       user.user_filter.body_type =[];
                                       user.user_filter.body_type.push(params.user_filter.body_type)
                                  }
                                  if(params.user_filter && params.user_filter.children && params.user_filter.children.length){
                                       user.user_filter.children.push(params.user_filter.children)
                                  }
                                  else{
                                      user.user_filter.children =[];
                                      user.user_filter.children.push(params.user_filter.children)
                                  }
                                  if(params.user_filter && params.user_filter.ethnicity && params.user_filter.ethnicity.length){
                                     user.user_filter.ethnicity.push(params.user_filter.ethnicity)
                                  }
                                  else{
                                     user.user_filter.ethnicity =[];
                                     user.user_filter.ethnicity.push(params.user_filter.ethnicity)
                                  }
                                  if(params.user_filter && params.user_filter.education && params.user_filter.education.length){
                                     user.user_filter.education.push(params.user_filter.education)
                                  }
                                  else{
                                     user.user_filter.education =[];
                                     user.user_filter.education.push(params.user_filter.education)
                                  }
                                  if(params.user_filter && params.user_filter.smoking && params.user_filter.smoking.length){
                                     user.user_filter.smoking.push(params.user_filter.smoking)
                                  }
                                  else{
                                       user.user_filter.smoking =[];
                                       user.user_filter.smoking.push(params.user_filter.smoking)
                                  } */

                    user.user_filter = params.user_filter

                    user.save((err, updatedInts) => {

                        if (err) throw err;
                        if (!err) {
                            return res.json({
                                "code": 200,
                                "status": "success",
                                "data": updatedInts
                            });
                        }

                    });


                }

            ])


        }
    },


    getUserMatches: function (req, res) {
        let params = req.body
        let user_id = req.user.id

        //check required field
        let isValid = validate.checkCommonValidation(params);

        if (!isValid) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else if (!params.filter) {
            return res.json({
                "code": 403,
                "status": "Error",
                "message": "Please enter proper data"
            });
        }
        else {
            let matchFilter = {};

            //filter by the age from
            if (params.filter && params.filter.age_from) {
                matchFilter["user_filter.age_from"] = {$gte: params.filter.age_from}
            }
            //filter by the age to
            if (params.filter && params.filter.age_to) {
                matchFilter["user_filter.age_to"] = {$lte: params.filter.age_to}
            }
            //filter by the the distance
            if (params.filter && params.filter.distance) {
                matchFilter["user_filter.distance"] = params.filter.distance
            }
            //filter by the height
            if (params.filter && params.filter.height) {
                matchFilter["user_filter.height"] = params.filter.height
            }
            //filter by the religion
            if (params.filter && params.filter.religion) {
                matchFilter["user_filter.religion"] = {$in: params.filter.religion}
            }
            //filter by the relationship
            if (params.filter && params.filter.relationship) {
                matchFilter["user_filter.relationship"] = {$in: params.filter.relationship}
            }
            //filter by the bodytype
            if (params.filter && params.filter.body_type) {
                matchFilter["user_filter.body_type"] = {$in: params.filter.body_type}
            }
            //filter by the children
            if (params.filter && params.filter.children) {
                matchFilter["user_filter.children"] = {$in: params.filter.children}
            }
            //filter by the ethnicity
            if (params.filter && params.filter.ethnicity) {
                matchFilter["user_filter.ethnicity"] = {$in: params.filter.ethnicity}
            }
            //filter by the education
            if (params.filter && params.filter.education) {
                matchFilter["user_filter.education"] = {$in: params.filter.education}
            }
            //filter by the smoking habbit
            if (params.filter && params.filter.smoking) {
                matchFilter["user_filter.smoking"] = {$in: params.filter.smoking}
            }

            console.log("Filter------", matchFilter)
            User
                .find(matchFilter)
                .exec(function (err, matchedUsers) {
                    if (err) throw err

                    if (matchedUsers && matchedUsers.length) {
                        return res.json({
                            "code": 200,
                            "status": "success",
                            "data": matchedUsers
                        });
                    }
                    else {
                        return res.json({
                            "code": 200,
                            "status": "Match may be available in future",
                            "data": []
                        });
                    }
                })
        }
    }
}