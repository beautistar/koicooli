let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
let BlockSchema = mongoose.Schema({

    title: {
        type: String
    },
    image:{
        type:String
    },
    code: {
        type: String
    },
    createdDate: {
        type: Date
    },
    updatedDate: {
        type: Date
    },
    //who generate block/super admin/admin/tutor
    createdBy: {
        type: ObjectId,
        ref: 'user'
    },
    isDeleted:{
        type:Boolean,
        default: false
    },
    sequence: {
        type: Number,
        //default: 4
    },
    //which student assigned to which tutor
    deletedBy: {
        type: ObjectId,
        ref: 'user'
    },


});

BlockSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('block', BlockSchema);