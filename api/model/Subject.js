let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
let SubjectSchema = mongoose.Schema({

    title: {
        type: String
    },
    title1: {
        type: String
    },
    title2: {
        type: String
    },
    title3: {
        type: String
    },
    title4: {
        type: String
    },
    title5: {
        type: String
    },
    image: {
        type: String
    },
    code: {
        type: String
    },
    createdDate: {
        type: Date
    },
    updatedDate: {
        type: Date
    },
    //who generate subject/super admin/admin/tutor
    createdBy: {
        type: ObjectId,
        ref: 'user'
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    //which student assigned to which tutor
    deletedBy: {
        type: ObjectId,
        ref: 'user'
    },
    studentId: {
        type: ObjectId,
        ref: 'user'
    },
    block: {
        type: ObjectId,
        ref: 'block'
    },

}, {strict: false});

SubjectSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('subject', SubjectSchema);