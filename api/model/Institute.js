let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
let InstituteSchema = mongoose.Schema({

    title: {
        type: String
    },
    address:{
        type:String
    },
    createdDate: {
        type: Date
    },
    updatedDate: {
        type: Date
    },
    //who generate institute/super admin/admin/tutor
    createdBy: {
        type: ObjectId,
        ref: 'user'
    },
    isDeleted:{
        type:Boolean,
        default: false
    },
    //which student assigned to which tutor
    deletedBy: {
        type: ObjectId,
        ref: 'user'
    },


});

InstituteSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('institute', InstituteSchema);