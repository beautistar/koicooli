let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
let UserSchema = mongoose.Schema({

    type: {
        type: Number,
        default: 4
    },
    name: {
        type: String
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    mobile: {
        type: String
    },
    birthdate: {
        type: Date
    },
    age: {
        type: String
    },
    profile: {
        type: String
    },
    zodiacSign: {
        type: String
    },
    gradeLevel: {
        type: String
    },
    class: {
        type: String
    },
    createdDate: {
        type: Date
    },
    updatedDate: {
        type: Date
    },
    //who generate user/super admin/admin/tutor
    createdBy: {
        type: ObjectId,
        ref: 'user'
    },
    //which student assigned to which tutor
    tutorId: {
        type: ObjectId,
        ref: 'user'
    },
    forgotPasswordCode: {
        type: String
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    //which student assigned to which tutor
    deletedBy: {
        type: ObjectId,
        ref: 'user'
    },
    //for tutors only
    blocks: {
        type: Array
    },
    //for students only
    subjects: {
        type: Array
    },  //for students only
    tasks: {
        type: Array
    },
    isActive: {
        type: Boolean,
        default: true
    },
    instituteId: {
        type: ObjectId,
        ref: 'institute'
    },
    adminId: {
        type: ObjectId,
        ref: 'user'
    },
    followUpId: {
        type: ObjectId,
        ref: 'user'
    },

}, {strict: false});

UserSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('user', UserSchema);