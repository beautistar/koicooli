let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
let TaskSchema = mongoose.Schema({

    title: {
        type: String
    },
    questionOne: {
        type: String
    },
    questionTwo: {
        type: String
    },
    questionThree: {
        type: Date
    },
    image: {
        type: String
    },
    description: {
        type: String
    },
    instructions: {
        type: String
    },
    createdDate: {
        type: Date
    },
    updatedDate: {
        type: Date
    },
    //who generate task/super admin/admin/tutor
    createdBy: {
        type: ObjectId,
        ref: 'user'
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    //task of student
    studentId: {
        type: ObjectId,
        ref: 'user'
    },
    //block of subject
    blockId: {
        type: ObjectId,
        ref: 'block'
    },
    //subject
    subjectId: {
        type: ObjectId,
        ref: 'subject'
    },
    //child-task
    parentTaskId: {
        type: ObjectId,
        ref: 'subjecttask'
    },
    taskStartDate: {
        type: Date
    },
    taskEndDate: {
        type: Date
    },
    followUpId: {
        type: ObjectId,
        ref: 'user'
    },
    taskStatus: {
        type: Number,
    },
    createdByStudent: {
        type: ObjectId,
        ref: 'user'
    },
    createdByTutor: {
        type: ObjectId,
        ref: 'user'
    },
    deletedBy: {
        type: ObjectId,
        ref: 'user'
    },
    studentOpinion: {
        type: Number,
        default: 1
    },

});

TaskSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('subjecttask', TaskSchema);