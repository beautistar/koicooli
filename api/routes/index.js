let common = require('../controller/common.js');
let UserController = require('../controller/UserController.js');
let AuthController = require('../controller/AuthController.js');
let SubjectController = require('../controller/SubjectController.js');
let TaskController = require('../controller/TaskController.js');
let BlockController = require('../controller/BlockController.js');
let InstituteController = require('../controller/InstituteController.js');
let TaskPhaseController = require('../controller/TaskPhaseController.js');
let express = require('express');
let jwt = require('jsonwebtoken');
let apiRoutes = express.Router();
let config = require('../config/config.js');
//let Schema = mongoose.Schema;

module.exports = function (app) {
    apiRoutes.use(function (req, res, next) {
        console.log("My---Host", req.headers.host)
        console.log("My---Protocol", req.protocol)
        let token = req.body.token || req.query.token || req.headers.token;
        if (token) {
            jwt.verify(token, config.secrect, function (err, decoded) {
                console.log("dd", decoded);
                if (err) {
                    return res.json({
                        status: "err",
                        success: false,
                        message: 'Failed to authenticate token.',
                        data: null
                    });
                } else {
                    if (decoded._doc) {
                        req.user = decoded._doc;
                        next();
                    } else {
                        req.user = decoded;
                        // console.log("user",req.user);
                        next();
                    }
                }
            });
        } else {
            return res.status(403).send({
                status: "err",
                success: false,
                message: 'No token provided.',
                data: null
            });
        }
    });
    //Token Route
    app.use('/api', apiRoutes);
    //User Register
    app.get('/', UserController.home);
    app.post('/api/user-register', UserController.create);
    app.post('/api/user-list', UserController.paginate);
    app.post('/api/user-all-tutor', UserController.allTutors);
    app.post('/api/user-list-1', UserController.paginateV1);
    app.get('/api/user-detail/:id', UserController.view);
    app.delete('/api/user-delete/:id', UserController.destroy);
    app.put('/api/user-update/:id', UserController.update);
    app.post('/api/user-subject-update/', UserController.removeSubject);
    //User Auth
    app.post('/login', AuthController.login);
    app.post('/forgot-password', AuthController.forgotPassword);
    app.get('/forgot-password-page/:userId/:code', AuthController.forgotPasswordPage);
    app.post('/update-password', AuthController.updatePassword);
    //Block
    app.post('/api/block', BlockController.create);
    app.post('/api/block-list', BlockController.paginate);
    app.get('/api/block-detail/:id', BlockController.view);
    app.get('/api/block--subjects/:id', BlockController.blockSubjects);
    app.delete('/api/block-delete/:id', BlockController.destroy);
    app.put('/api/block-update/:id', BlockController.update);
    app.post('/api/user-block-update/', BlockController.removeBlock);
    //Subject
    app.post('/api/subject', SubjectController.create);
    app.post('/api/subject-list', SubjectController.paginate);
    app.get('/api/subject-detail/:id', SubjectController.view);
    app.delete('/api/subject-delete/:id', SubjectController.destroy);
    app.put('/api/subject-update/:id', SubjectController.update);
    app.post('/api/student-subject/', SubjectController.studSubjectList);
    //Subject Task
    app.post('/api/subject-task', TaskController.create);
    app.post('/api/subject-task-list', TaskController.paginate);
    app.get('/api/subject-task-detail/:id', TaskController.view);
    app.delete('/api/subject-task-delete/:id', TaskController.destroy);
    app.put('/api/subject-task-update/:id', TaskController.update);
    app.post('/api/student-subject-task/', TaskController.studSubjectTaskList);
    //Subject Task Phase
    app.post('/api/subject-task-phase', TaskPhaseController.create);
    app.post('/api/subject-task-phase-list', TaskPhaseController.paginate);
    //Institute
    app.post('/api/institute', InstituteController.create);
    app.post('/api/institute-list', InstituteController.paginate);
    app.get('/api/institute-detail/:id', InstituteController.view);
    app.delete('/api/institute-delete/:id', InstituteController.destroy);
    app.put('/api/institute-update/:id', InstituteController.update);


    //its for common controller for third party apis
    app.get('/getGoogleCelebs', common.googleCelebs);
    //payement getway api
    app.post('/api/getPayement', common.getUserPayement);
}