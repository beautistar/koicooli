const mongoose = require('mongoose');
const User = mongoose.model("user");
module.exports = {

    checkCommonValidation: (params) => {
        if (JSON.stringify(params) === '{}' || params === undefined || params === null) {
            return false
        } else {
            return true
        }
    },

    checkLoginReqValidation: (params) => {
        if (!params.email ||
            !params.password) {
            return false
        } else {
            return true
        }
    },

    checkForgotReqValidation: (params) => {
        if (!params.email) {
            return false
        } else {
            return true
        }
    },

    checkCreateReqValidation: (params) => {

        if (!params.firstName
            || !params.lastName
            || !params.email
            || !params.password
            || !params.type
        /* || !params.mobile*/) {
            return false
        } else {
            return true
        }
    },

    checkUpdateReqValidation: (params) => {

        if (!params.id) {
            return false
        } else {
            return true
        }
    },

    checkBlockCreateReqValidation: (params) => {

        if (!params.title) {
            return false
        } else {
            return true
        }
    },

    checkCommonUpdateReqValidation: (params) => {

        if (!params.id) {
            return false
        } else {
            return true
        }
    },

    checkSubjCreateReqValidation: (params) => {

        if (!params.title
            || !params.block
            || !params.studentId
        ) {
            return false
        } else {
            return true
        }
    },
    checkSubjTaskCreateReqValidation: (params) => {

        if (!params.title
            || !params.blockId
            || !params.subjectId
            || !params.studentId) {
            return false
        } else {
            return true
        }
    },
    checkSubjTaskPhaseCreateReqValidation: (params) => {

        if (!params.title
            || !params.blockId
            || !params.subjectId
            || !params.studentId
            || !params.subjectTaskId) {
            return false
        } else {
            return true
        }
    },
    checkRemSubReqValidation: (params) => {

        if (!params.subjectIds
            || !params.studentId) {
            return false
        } else {
            return true
        }
    },
    checkRemBlockReqValidation: (params) => {

        if (!params.tutorId
            || !params.blockIds) {
            return false
        } else {
            return true
        }
    },

    checkInstCreateReqValidation: (params) => {

        if (!params.title || !params.address) {
            return false
        } else {
            return true
        }
    },
    checkInstTutorReqValidation: (params) => {

        if (!params.instituteId) {
            return false
        } else {
            return true
        }
    },


}
