const request = require('request');
const fs = require('fs');
const bcrypt = require('bcrypt-nodejs');
const config = require('../config/config');
const nodemailer = require("nodemailer");
var path = require('path')
const templatesDir = path.resolve(__dirname, '../..', 'views');
//const emailTemplates = require('email-templates');
const async = require('async');
const TemplateService = require('./TemplateService')
module.exports = {

    convertBase64Image1: function (profile) {
        //console.log("Prooooo---",profile)

        let imagePath;
        let base64Data;
        let data = profile.split(';');

        let randomNumber = this.myFunction(25, '#aA');

        if (data[0] == "data:image/1") {
            imagePath = './images/' + randomNumber + '.png';
            let base64Data = profile.replace(/^data:image\/1;base64,/, "");
        }
        else if (data[0] == "data:image/*") {
            let base64 = data[2].split(",");
            base64Data = base64[1];
            let data = base64[1].substring(0, 8);
            if (data == "/9j/4AAQ") {
                imagePath = './images/' + randomNumber + '.jpeg';
            } else {
                imagePath = './images/' + randomNumber + '.png';
            }
        } else if (data[0] == "data:image/png") {
            imagePath = './images/' + randomNumber + '.png';
            let base64Data = profile.replace(/^data:image\/png;base64,/, "");
        } else if (data[0] == "data:image/jpeg") {
            imagePath = './images/' + randomNumber + '.jpeg';
            let base64Data = profile.replace(/^data:image\/jpeg;base64,/, "");
        } else {
            console.log("image invalid");
        }


        fs.writeFile(imagePath, base64Data, 'base64', function (err) {
            if (err) {
                throw err
                /*res.json({
                    success: false,
                    message: 'Base64 Image is not converted',
                    data: err
                });*/
            }
        })
        return imagePath;
    },

    convertBase64Image(profile) {
        var imagePath;
        var base64Data;
        //console.log("PROFILE",profile);
        var data = profile.split(';');

        //  console.log("dd",data);
        function myFunction(length, chars) {
            var mask = '';
            if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
            if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            if (chars.indexOf('#') > -1) mask += '0123456789';
            var result = '';
            for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
            return result;
        }

        var randomNumber = myFunction(25, '#aA');
        if (data[0] == "data:image/1") {
            imagePath = './images/' + randomNumber + '.png';
            var base64Data = profile.replace(/^data:image\/1;base64,/, "");
        } else if (data[0] == "data:image/*") {
            var base64 = data[2].split(",");
            base64Data = base64[1];
            var data = base64[1].substring(0, 8);
            if (data == "/9j/4AAQ") {
                imagePath = './images/' + randomNumber + '.jpeg';
            } else {
                imagePath = './images/' + randomNumber + '.png';
            }
        } else if (data[0] == "data:image/png") {
            imagePath = './images/' + randomNumber + '.png';
            base64Data = profile.replace(/^data:image\/png;base64,/, "");
        } else if (data[0] == "data:image/jpeg") {
            imagePath = './images/' + randomNumber + '.jpeg';
            base64Data = profile.replace(/^data:image\/jpeg;base64,/, "");
        } else {
            console.log("image invalid");
        }
        console.log("imagePath-----------1111", imagePath)
        fs.writeFile(imagePath, base64Data, 'base64', function (err) {
            if (err) {
                console.log('err: ', err);
                /*res.json({
                  success: false,
                  message: 'Base64 Image is not converted',
                  data: err
                });*/
            }
        })
        return imagePath;
    },

    myFunction1: function (length, chars) {
        let mask = '';
        if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
        if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if (chars.indexOf('#') > -1) mask += '0123456789';
        let result = '';
        for (let i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
        return result;
    },

    /**
     * @desc : encrypt password
     * @param password
     * @returns {*}
     */
    generateHash: function (password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    },

    /**
     * @desc: decrypt password
     * @param params
     * @returns {boolean}
     */
    compareHash: function (params) {
        let encryptedPassword = params.user && params.user.password ? params.user.password : ""
        return bcrypt.compareSync(params.password, encryptedPassword)
    },

    emailValidator: function (params) {
        let mailFormat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (mailFormat.test(params) == false) {
            return false
        }
        else {
            return true
        }
    },

    randomStringGenerator: function () {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (let i = 0; i < 10; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    },

    imageUpload: function () {


    },

    mailSender: function (params, callback) {

        async.waterfall([

           /* function (wcb) {
                if (params.templateName) {
                    let tempOption = {
                        templatesDir: templatesDir,
                        payload: params
                    }
                    TemplateService.templateGenerator(tempOption, function (err, templateResult) {
                        console.log("Tem---Err",err)
                        if (err) throw new Error(err);

                        else {
                            console.log("Temp::Result",templateResult)
                            wcb(null, templateResult)
                        }

                    })

                }
                else {
                    wcb(null, {})
                }

            },*/
            function (wcb) {


                let user = params.user
                // create reusable transport method (opens pool of SMTP connections)
                let smtpTransport = nodemailer.createTransport({
                    service: "Gmail",
                    host: params.host, // hostname
                    secureConnection: false, // use SSL
                    port: 587, // port for secure SMTP
                    auth: {
                        user: "jacktech88@gmail.com",
                        pass: "jackTech@123"
                    }
                });

                // setup e-mail data with unicode symbols
                let mailOptions = {
                    from: "support@kolcooli.info", // sender address
                    to: user.email, // list of receivers
                    subject: params.subject ? params.subject : "Support", // Subject line
                    text: params.text ? params.text : "Email Notification", // plaintext body
                    //text: 'Click on the link to reset your password..' + fullUrl + '/home/js2/Desktop/Projects/KolCooli/forgot-password-page/' + params._id + '/' + linkExpirationCode, // plaintext body
                    //html: templateResult.html ? templateResult.html : "" // html body
                }

                console.log("Send::::Mail")
                // send mail with defined transport object
                smtpTransport.sendMail(mailOptions, function (error, response) {
                    if (error) {
                        callback(null, false)
                    } else {
                        callback(null, true)
                    }

                });
            }

        ])


    },

    templateGenerator: function (params, callback) {

        async.waterfall([

            function (wcb) {
                emailTemplates(params.templatesDir,
                    (err, template) => {
                        if (err) throw new Error(err)
                        else wcb(null, template)
                    });

            },

            function (template) {
                template(params.templateName, {
                        payload: params.payload
                    },
                    function (err, html, text) {
                        if (err) throw new Error(err)
                        else {
                            let result = {
                                html: html,
                                text: text
                            }
                            // wcb(null, html, text);
                            callback(null, result)
                        }
                    });

            }
        ])

    }

}