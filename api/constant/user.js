module.exports = {

    userType: {
        SUPER_ADMIN: 1,
        ADMIN: 2,
        TUTOR: 3,
        STUDENT: 4
    },
    taskStatus: {
        START: 1,
        RUNNING: 2,
        COMPLETED: 3
    },
    studentOpinion: {
        EASY: 1,
        AVERAGE: 2,
        HARD: 3
    }
}