import { REHYDRATE } from 'redux-persist';

var initState={

    logindata:{},
    graph_data:{},
    customer_data:[]

};

function loginreducer(state=initState,action){


    switch (action.type){

        case 'Login':
            return {
                ...state,
                logindata:action.res
            };

            case 'Logout':
            return {
                ...state,
                logindata:{},
                graph_data:{},
                customer_data:[],
            };
            case 'Graph':
            return {
                ...state,
                graph_data:action.res
            };
            case 'Customer':
            return {
                ...state,
                customer_data:action.res
            };
        case REHYDRATE:
            return {
                ...state,
                logindata: action.payload &&
                action.payload.logindata ? action.payload.logindata : {},
                graph_data: action.payload &&
                action.payload.graph_data ? action.payload.graph_data : {},
                customer_data: action.payload &&
                action.payload.customer_data ? action.payload.customer_data : []
            };
        default:
            return{
                ...state
            }
    }


}

export const reducer =loginreducer;