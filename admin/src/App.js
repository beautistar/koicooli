import React, { Component } from 'react';
import { HashRouter, Route, Switch,BrowserRouter } from 'react-router-dom';

import './App.css';
// Styles
// CoreUI Icons Set
import '@coreui/icons/css/coreui-icons.min.css';
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import './scss/style.css'

// Containers
import { DefaultLayout } from './containers';
// Pages
import { Login } from './views/Pages';
const history = BrowserRouter;

// import { renderRoutes } from 'react-router-config';

class App extends Component {

    componentDidMount(){
       // console.log(browserHistory)
        //const { match, location, history } = this.props
        console.log('history',history)
        //console.log('location',location)
    }

  render() {
      //console.log('history',history)
    return (
      <HashRouter>
        <Switch>
          <Route exact path="/login" name="Login Page" component={Login} />
          <Route path="/" name="Dashboard" component={DefaultLayout} />
        </Switch>
      </HashRouter>
    );
  }
}

export default App;
