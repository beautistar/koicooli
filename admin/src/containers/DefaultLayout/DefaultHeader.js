import React, {Component} from 'react';
import {Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink} from 'reactstrap';
import PropTypes from 'prop-types';

import {AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler} from '@coreui/react';
import logo from '../../views/Pages/Login/logo'
import sygnet from '../../views/Pages/Login/minimize_logo'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {ActCreators} from '../../redux/bindActionCretor'
import {Redirect} from 'react-router-dom';

const propTypes = {
    children: PropTypes.node,
};

const defaultProps = {};

const mapStateToProps = state => {
    console.log('hi', state);
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActCreators, dispatch)
};

class DefaultHeader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
        }
    }

    logout() {
        this.props.Logout();
        localStorage.clear()
        this.setState({redirect: true})
    }


    render() {
        if (this.state.redirect) {
            return <Redirect to={{pathname: '/'}}/>
        }

        // eslint-disable-next-line
        const {children, ...attributes} = this.props;
        let loginUser = JSON.parse(localStorage.getItem('User'));
        let fullName
        if (loginUser && (loginUser.id || loginUser._id)) {
            let firstName = loginUser.firstName ? loginUser.firstName : ""
            let lastName = loginUser.lastName ? loginUser.lastName : "";
            fullName = firstName + " " + lastName;
        }


        return (
            <React.Fragment>
                <AppSidebarToggler className="d-lg-none" display="md" mobile/>
                <AppNavbarBrand
                    full={{src: logo, width: 120, height: 40, alt: 'Kolcooli'}}
                    minimized={{src: sygnet, width: 30, height: 30, alt: 'Kolcooli Logo'}}
                />
                <AppSidebarToggler className="d-md-down-none" display="lg"/>


                <Nav className="ml-auto" navbar>

                    <AppHeaderDropdown direction="down">
                        <DropdownToggle nav>
                            <img src={'assets/img/avatars/6.jpg'} className="img-avatar"
                                 alt="admin@bootstrapmaster.com"/>
                        </DropdownToggle>


                        <DropdownMenu right style={{right: 'auto'}}>

                            <DropdownItem>
                                <a>
                                    <i className="fa fa-user"></i>
                                    {fullName}
                                </a><br/>
                                {/*<a onClick={this.logout.bind(this)}><i className="fa fa-user-circle-o"></i>
                                    Profile
                                </a><br/>*/}
                                <a onClick={this.logout.bind(this)}><i className="fa fa-lock"></i>
                                    Logout
                                </a>

                            </DropdownItem>

                        </DropdownMenu>
                    </AppHeaderDropdown>
                </Nav>
                {/* <AppAsideToggler className="d-md-down-none" />*/}
                {/*<AppAsideToggler className="d-lg-none" mobile />*/}
            </React.Fragment>
        );
    }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;
export default connect(mapStateToProps, mapDispatchToProps)(DefaultHeader);

