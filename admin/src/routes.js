import React from 'react';
import Loadable from 'react-loadable'

import DefaultLayout from './containers/DefaultLayout';

function Loading() {
  return <div>Loading...</div>;
}

const User = Loadable({
    loader: () => import('./views/Admin/User/AdminPage'),
    loading: Loading,
});
const UserAdd = Loadable({
    loader: () => import('./views/Admin/User/AdminAddPage'),
    loading: Loading,
});
const UserEdit = Loadable({
    loader: () => import('./views/Admin/User/AdminEditPage'),
    loading: Loading,
});
const ForgotPassword = Loadable({
    loader: () => import('./views/Admin/User/ForgotPassword'),
    loading: Loading,
});
const Block = Loadable({
    loader: () => import('./views/Admin/Block/BlockPage'),
    loading: Loading,
});
const Institute = Loadable({
    loader: () => import('./views/Admin/Institute/InstitutePage'),
    loading: Loading,
});
const Subject = Loadable({
    loader: () => import('./views/Admin/Subject/SubjectPage'),
    loading: Loading,
});
const SubjectTask = Loadable({
    loader: () => import('./views/Admin/Task/TaskPage'),
    loading: Loading,
});
const Dashboard = Loadable({
    loader: () => import('./views/Dashboard'),
    loading: Loading,
});
const SingleOperatorView = Loadable({
    loader: () => import('./views/SingleOperatorView/SingleOperatorView'),
    loading: Loading,
});

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/dashboard', exact: true, name: 'Dashboard', component: DefaultLayout },
    { path: '/user', exact: true, name: 'User', component: User },
    { path: '/user/add', exact: true, name: 'UserAdd', component: UserAdd},
    { path: '/user/edit',  name: 'UserEdit', component: UserEdit},
    { path: '/user/forgot-password', exact: true, name: 'ForgotPassword', component: ForgotPassword },
    { path: '/institute', exact: true, name: 'Institute', component: Institute },
    { path: '/block', exact: true, name: 'Block', component: Block },
    { path: '/subject', exact: true, name: 'Subject', component: Subject },
    { path: '/subject-task', exact: true, name: 'Task', component: SubjectTask },
    { path: '/dashboard', exact: true, name: 'Dashboard', component: Dashboard },
    { path: '/dashboard/asset', name: 'Single Operator View', component: SingleOperatorView },
];

export default routes;
