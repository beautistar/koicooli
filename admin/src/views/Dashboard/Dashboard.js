import React, {Component} from 'react';

import {
    Col,
    Row,
    Table,
    Card, CardHeader, CardBody,
    Modal, ModalBody, ModalFooter,
    Popover, PopoverBody, PopoverHeader,
    ModalHeader, Button, FormGroup, Label, Input, InputGroup, InputGroupAddon
} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import * as _ from 'underscore';
import {PieChart, BarChart} from 'react-easy-chart';
import {Bar} from 'react-chartjs-2';
import Geocode from "react-geocode";
import LoadingScreen from 'react-loading-screen';
import '../../App.css'
import './Dashboard.css'
import SingleOperatorView from "../SingleOperatorView/SingleOperatorView";
import * as mom from 'moment'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {ActCreators} from '../../redux/bindActionCretor'
import Select from 'react-select';
import {CSVLink, CSVDownload} from 'react-csv';
import yellowdot from './yellow-cirl.png'
import reddot from './redcircle.png';
import greendot from './greencircle.png';
import groupdot from './icons8-circled-2-c-48.png';

Geocode.setApiKey("AIzaSyAoaU5LJSPReuxgZr4WbMrYm0rAsrbOays");

let customer_old_data = [];

const mapStateToProps = state => {
    customer_old_data = [];
    if (state && state.customer_data && state.customer_data.length) {
        customer_old_data = state.customer_data;
    }
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActCreators, dispatch)
};

let color = [];
let el301_pie_chart_key_field = [];
let ed_es_pie_chart_key_field = [];
let export_csv_data = [];
let export_all_headers = [];
let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.redirect = this.redirect.bind(this);
        this.hoverMapData = this.hoverMapData.bind(this);
        this.barGraphDataFn = this.barGraphDataFn.bind(this);
        this.getCityListFn = this.getCityListFn.bind(this);
        this.state = {
            success: false,
            selectedOption: '',
            loaded: true,
            el301_operator_field: '',
            es200_operator_field: '',
            pie_chart_name: 'batteryAlarm',
            el301_pie_data: 0,
            es200_pie_data: 0,
            ed_pie_data: 0,
            customer_service_no: [],
            cityGraphData: [],
            cityGraphLabel: [],
            cityList: [],
            customerList: [],
            assestsList: [],
            filterData: [],
            EL301_operator_field: [],
            ES200_operator_field: [],
            ED_operator_field: [],
            el301_pie_chart_data: [],
            es200_pie_chart_data: [],
            ed_pie_chart_data: [],
            monthWiseGraph: [],
            export_all_headers: [],
            export_csv_data: [],
            dataPoints: [],
            all_data_points: [],
            first_data_points: [],
            dataPointsLabel: [],
            metaDataField: [],
            es200ErrorData: [],
            edErrorData: [],
            allDevicesPieChart: [],
            popoverOpen: false
        };
    }

    redirect(data) {
        this.props.Graph(data);
        this.setState({redirect: true, redirect_data: data})
    }

    hoverMapData(data) {
        this.setState({success: true, modal_data: data})
    }


    initMap() {
        var this_pointer = this;

        var gogle_marker_data = {};
        // Create the map.


        var latitude_data = [];
        var longitude_data = [];


        _.each(this.state.assestsList, function (data) {
            if (data.data_metadata && data.data_metadata.geolocation) {
                latitude_data.push(data.data_metadata.geolocation.split(',')[0]);
                longitude_data.push(data.data_metadata.geolocation.split(',')[1]);
            }
        });

        var zoom = Math.round(Math.log(300 * 360 / (_.max(longitude_data) - _.min(longitude_data)) / 256) / Math.LN2);


        if (this.state.assestsList && this.state.assestsList.length) {
            var intital_location = this.state.assestsList[0].data_metadata.geolocation.split(',');
        }


        var map = new window.google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: {
                lat: parseFloat(intital_location && intital_location.length ? intital_location[0] : 0),
                lng: parseFloat(intital_location && intital_location.length ? intital_location[1] : 0)
            }
            //center: {lat: 47.4521799, lng: 8.535500432}
        });


        //find distance

        console.log('this.state.assestsList', this.state.assestsList);
        var map_data = [];
        var assetdata = this.state.assestsList;
        for (var i = 0; i < assetdata.length; i++) {
            var group_map_data = [];
            _.each(assetdata, function (data, index) {
                if (index != i) {
                    if (data && data.data_metadata && data.data_metadata.geolocation) {
                        var location1 = assetdata[i].data_metadata.geolocation.split(',')
                        var location2 = assetdata[index].data_metadata.geolocation.split(',')


                        var radlat1 = Math.PI * location1[0] / 180;
                        var radlat2 = Math.PI * location2[0] / 180;
                        var radlon1 = Math.PI * location1[1] / 180;
                        var radlon2 = Math.PI * location2[1] / 180;

                        var theta = location1[1] - location2[1];

                        var radtheta = Math.PI * theta / 180;
                        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                        dist = Math.acos(dist);
                        dist = dist * 180 / Math.PI;
                        dist = dist * 60 * 1.1515;
                        dist = dist * 1.609344;
                        if (dist <= 0.5) {
                            group_map_data.push(data)
                        }

                    }

                } else {
                    group_map_data.push(data)
                }
            });
            if (group_map_data && group_map_data.length > 1) {
                map_data.push(group_map_data)
            }


        }


        var groupByData = _.groupBy(map_data, 'length');


        _.each(groupByData, function (val, key) {
            var group_wise_data = {};
            if (parseInt(key) == val.length) {
                assetdata = _.reject(assetdata, function (data) {
                    return _.contains(_.pluck(val[0], 'id'), data.id)
                });

                group_wise_data.id = 'group';
                group_wise_data.model = val[0][0].model;
                group_wise_data.data_metadata = val[0][0].data_metadata;
                group_wise_data.items = [];
                group_wise_data.items = val[0];
                assetdata.push(group_wise_data)
            } else {
                assetdata = _.reject(assetdata, function (data) {
                    return _.contains(_.union(_.pluck(_.flatten(val), 'id')), data.id)
                });

                var total_no_of_loops = val.length / parseInt(key);
                var start_point = 0;
                for (var i = 0; i < total_no_of_loops; i++) {
                    var end_point = start_point + (parseInt(key) - 1);
                    group_wise_data = {};
                    group_wise_data.id = 'group';
                    group_wise_data.model = val[start_point][0].model;
                    group_wise_data.data_metadata = val[start_point][0].data_metadata;
                    group_wise_data.items = [];
                    group_wise_data.items = val[start_point];
                    start_point = end_point + 1;
                    assetdata.push(group_wise_data)
                }

            }
        });
        console.log('df', assetdata);

        //google map cluster

        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        var cluster_image = 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m';
        var markers = assetdata.map(function (data, i) {
            var map_data = {};

            if (data && data.data_metadata && !_.isNull(data.data_metadata)) {
                var map_location = data.data_metadata.geolocation.split(',');
                map_data.lat = parseFloat(map_location[0])
                map_data.lng = parseFloat(map_location[1])

                return new window.google.maps.Marker({
                    position: map_data,
                    map: map,
                    icon: data && data.id == 'group' ? groupdot : (data.model == 'EL301' && data.data && (data.data.serviceDue || data.data.serviceDue == 'true')) ? yellowdot : ((data && data.data && data.model != 'EL301'
                        && data.data.errors && (data.data.errors['0'] != '0' && data.data.errors['0'] != 0 && data.data.errors['0'] != 'noError')) ? reddot : greendot),
                    label: data.id ? (data.id == 'group' ? data.items.length.toString() : data.id) : labels[i % labels.length],
                    data: data,
                    title: data.id ? (data.data_metadata.customer ? data.data_metadata.customer + ',' + data.model : data.model) : labels[i % labels.length]
                });

            }


        });
        markers.map((data) => {

            if (data && !_.isUndefined(data)) {


                if (data.data.id == 'group') {
                    data.hoverData = this.hoverMapData;
                    return window.google.maps.event.addListener(data, 'click', function () {
                        this.hoverData(data.data.items);
                    });
                } else {
                    data.Dtate = this.redirect;
                    return window.google.maps.event.addListener(data, 'click', function () {
                        this.Dtate(data.data);
                    });
                }

            }

        })

        var markerCluster = new window.MarkerClusterer(map, _.without(markers, undefined),

            {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
                maxZoom: 14,
                zoomOnClick: true
            });

    }

    getCityListFn() {
        var city = [];
        _.each(_.uniq(_.pluck(this.state.assestsList, 'city')), function (city_name) {
            city.push({value: city_name, label: city_name})
        });
        this.setState({cityList: city})
    }

    getUniqFilterValues(array, filter_array_name) {
        var this_pointer = this;
        var fields = [];
        _.each(array, function (data) {
            if (data && !_.isEmpty(data.data)) {
                _.each(data.data, function (val, key) {
                    if (val && _.isObject(val)) {
                        _.each(val, function (val_val, val_key) {
                            fields.push('error' + val_key)
                        })
                    }
                    if (_.isBoolean(val) || val == 'True' || val == 'False') {
                        fields.push(key)
                    }
                })
            }
        });
        var uniq_fields = _.uniq(fields);
        if (uniq_fields && uniq_fields.length) {
            _.each(uniq_fields, function (keyname) {
                this_pointer.state[filter_array_name].push({value: keyname, label: keyname})
            })
        }
    }

    componentDidMount() {

        this.setState({loaded: true})
        fetch('https://xwwhnoga55.execute-api.us-west-2.amazonaws.com/Prod/api/Assets', {
            method: 'get',
            headers: {
                Accept: 'application/json'
            }
        }).then((response) => response.text())
            .then((responseText) => {

                var res = JSON.parse(responseText);
                if (res && res.length) {
                    console.log("rumlenaghouse", res)
                    var code = this.barGraphDataFn;
                    var cityFn = this.getCityListFn;
                    var total_city_count = 0;
                    var total_city_response_count = 0;
                    var customer_list = [];
                    var customer_list_value = [];
                    var meta_data_field = [];
                    _.each(res, function (data, index) {

                        if (data && data.metadata && !_.isNull(data.metadata)) {
                            if (_.isObject(data.metadata)) {
                                data.data_metadata = data.metadata;
                            } else {
                                data.data_metadata = JSON.parse(data.metadata);
                            }

                            _.each(data.data_metadata, function (val, key) {
                                meta_data_field.push(key)
                            });

                            if (data.data_metadata && data.data_metadata.customer) {
                                customer_list.push(data.data_metadata.customer)
                            }
                            if (data.data_metadata && data.data_metadata.geolocation) {
                                total_city_count++;
                                var map_location = data.data_metadata.geolocation.split(',');
                                Geocode.fromLatLng(map_location[0], map_location[1]).then(
                                    response => {
                                        if (response) {


                                            total_city_response_count++;
                                            data.data_metadata.city = response.results[0].address_components[response.results[0].address_components.length - 3].long_name;
                                            data.city = response.results[0].address_components[response.results[0].address_components.length - 3].long_name;
                                            if (total_city_response_count == total_city_count) {
                                                code();
                                                cityFn();
                                            }
                                        } else {
                                            total_city_response_count++;
                                        }

                                    },
                                    error => {
                                        total_city_response_count++;
                                    }
                                );
                            }

                        }
                    });


                    res = _.filter(res, function (data) {
                        if (data.data_metadata && (data.data_metadata.active == false || data.data_metadata.active == 'false')) {

                        } else {
                            return data
                        }
                    });
                    console.log("rumlenaghouse2", res)


                    if (customer_list && customer_list.length) {
                        _.each(_.uniq(customer_list), function (uniq_list) {
                            customer_list_value.push({value: uniq_list, label: uniq_list})
                        })
                    }


                    this.setState({
                        assestsList: res,
                        loaded: false, mainList: res,
                        customerList: customer_list_value,
                        metaDataField: _.uniq(meta_data_field),
                        customer_no: customer_old_data && customer_old_data.length ? customer_old_data : []
                    });


                    if (customer_old_data && customer_old_data.length) {
                        this.selecetFn();
                        this.barGraphDataPointsFn();
                    } else {
                        this.initMap();
                        this.pieChartValueFn();
                        this.barGraphDataPointsFn();
                    }
                    this.pieChartErrorWiseFn();
                } else {
                    this.setState({loaded: false})
                }
            }).catch(err => err);
    }


    pieChartErrorWiseFn() {
        fetch('https://9p6bw4lf9h.execute-api.us-west-2.amazonaws.com/latest/ErrorHistoryChart', {
            method: 'post',
            headers: {
                Accept: 'application/json'
            }
        }).then((response) => response.text())
            .then((responseText) => {

                var res = JSON.parse(responseText);
                this.setState({errorHistory: res.Data});

                this.setPieChartErrorValueFn();
            }).catch(err => err);
    }


    setPieChartErrorValueFn() {
        var es200Data = [];
        var edData = [];
        var response = this.state.errorHistory;
        var customer_filter_data = [];
        console.log('response', response);
        if (this.state.customer_no && this.state.customer_no.length) {
            _.each(this.state.customer_no, function (customer_data) {
                customer_filter_data.push(_.where(response, {customer: customer_data.value}));
            });
            response = _.flatten(customer_filter_data)
        } else {
            response = this.state.errorHistory
        }


        var groupByData = _.groupBy(response, 'model');

        var EDData = _.reject(response, function (data) {
            return data.model == 'ES200'
        });


        if (groupByData && groupByData.ES200 && groupByData.ES200.length) {


            var error_data = {};
            _.each(groupByData.ES200, function (data) {
                if (data && !_.isEmpty(data.ErrorCount)) {

                    _.each(data.ErrorCount, function (val, key) {

                        if (error_data[key]) {
                            error_data[key].push(data)
                        } else {
                            error_data[key] = [];
                            error_data[key].push(data)
                        }

                    })
                }
            });


            /*  _.each(groupByData.ES200,function(data){
                  if(data && data.ErrorCount && (data.ErrorCount['0'] || data.ErrorCount['noError'])){
                      data.errorValue=data.ErrorCount['0'] ? data.ErrorCount['0'] :data.ErrorCount['noError']
                  }
              });*/


            _.each(error_data, function (val, key) {
                es200Data.push({key: key, value: val.length, data: val})
            })

        }
        if (EDData && EDData.length) {


            var error_eddata = {};
            _.each(EDData, function (data) {
                if (data && !_.isEmpty(data.ErrorCount)) {

                    _.each(data.ErrorCount, function (val, key) {

                        if (error_eddata[key]) {
                            error_eddata[key].push(data)
                        } else {
                            error_eddata[key] = [];
                            error_eddata[key].push(data)
                        }

                    })
                }
            });


            /* _.each(EDData,function(data){
                 if(data && data.ErrorCount && (data.ErrorCount['0'] || data.ErrorCount['noError'])){
                     data.errorValue=data.ErrorCount['0'] ? data.ErrorCount['0'] :data.ErrorCount['noError']
                 }
             });*/


            /* _.each(_.groupBy(EDData,'errorValue'),function(val,key){
                 edData.push({key:key,value:val.length,data:val})
             });*/
            _.each(error_eddata, function (val, key) {
                edData.push({key: key, value: val.length, data: val})
            })
        }

        this.setState({es200ErrorData: es200Data, edErrorData: edData});


        console.log('es200Data', es200Data);
        console.log('edData', edData);

    }


    barGraphDataFn() {
        var geography = [];
        var bar_graph_data = [];
        var bar_graph_label = [];
        var opt_data = this.state.assestsList;
        _.each(this.state.assestsList, function (a) {
            if (a && a.data_metadata && a.data_metadata.city) {
                geography.push(a.data_metadata.city)
            }

        });

        _.each(_.uniq(geography), function (city) {
            var exist_data = _.filter(opt_data, function (a) {
                if (a && a.data_metadata && a.data_metadata.city) {
                    return a.data_metadata.city == city
                }
            });
            if (exist_data && exist_data.length) {
                bar_graph_data.push(exist_data.length)
                bar_graph_label.push(exist_data[0].data_metadata.city)
            }

        });

        this.setState({cityGraphData: bar_graph_data, cityGraphLabel: bar_graph_label})
    }


    monthWiseBarGraphFn() {
        var this_pointer = this;
        var response = [];
        var stop_graph_counting = false;
        var datapoints_x_label = [];
        var first_datapoints_y_label = [];
        var all_datapoints_y_label = [];
        if (this.state.dataPoints && this.state.dataPoints.length) {
            var customer_filter_data = [];
            if (this.state.customer_no && this.state.customer_no.length) {
                _.each(this.state.customer_no, function (customer_data) {
                    customer_filter_data.push(_.where(this_pointer.state.dataPoints, {Customer: customer_data.value}));
                });
                response = _.flatten(customer_filter_data)
            } else {
                response = this.state.dataPoints
            }

            var current_year = mom().format('YYYY');
            var current_month = mom().format('MMM');
            var start_year = '2018';


            for (var i = parseInt(start_year); i <= parseInt(current_year); i++) {
                _.each(months, function (month_name) {
                    if (current_year == start_year && mom('01' + '-' + month_name + '-' + current_year).format('MM') > mom('01' + '-' + current_month + '-' + current_year).format('MM')) {
                        stop_graph_counting = true;
                    } else if (!stop_graph_counting) {
                        var month_wise_data = _.where(response, {Year: start_year.toString(), Month: month_name});

                        if (month_wise_data && month_wise_data.length) {

                            var all_data_point = _.without(_.pluck(month_wise_data, 'AllDataPoint'), undefined);
                            var first_data_point = _.without(_.pluck(month_wise_data, 'FirstDataPoint'), undefined);


                            if (all_data_point && all_data_point.length) {
                                all_datapoints_y_label.push(_.reduce(all_data_point, function (memo, num) {
                                    return memo + num
                                }, 0));
                            }
                            if (first_data_point && first_data_point.length) {
                                first_datapoints_y_label.push(_.reduce(first_data_point, function (memo, num) {
                                    return memo + num
                                }, 0));
                            }


                        } else {
                            all_datapoints_y_label.push(0)
                            first_datapoints_y_label.push(0)
                        }
                        datapoints_x_label.push(month_name + start_year)
                    }

                });
                start_year = parseInt(start_year) + 1;
            }

            this.setState({
                dataPointsLabel: datapoints_x_label,
                first_data_points: first_datapoints_y_label,
                all_data_points: all_datapoints_y_label
            })

        }
    }


    barGraphDataPointsFn() {

        fetch('https://9p6bw4lf9h.execute-api.us-west-2.amazonaws.com/latest/DoorPilotChart', {
            method: 'get',
            headers: {
                Accept: 'application/json'
            }
        }).then((response) => response.text())
            .then((responseText) => {


                var res = JSON.parse(responseText);
                console.log(res.Data)
                this.setState({dataPoints: res.Data})

                this.monthWiseBarGraphFn()


            }).catch(err => err);
    }


    openModalByBarFn(e) {
        var exist_data = _.filter(this.state.assestsList, function (a) {
            if (a && a.data_metadata && a.data_metadata.city) {
                return a.data_metadata.city == e.label;
            }
        });
        this.setState({
            success: true,
            modal_data: exist_data
        })
    }


    selecetFn(key_name, e) {
        if (key_name == 'clear') {
            this.setState({
                    source_id: '1',
                    city: '1',
                    customer_no: [],
                    es200_operator_field: '',
                    el301_operator_field: '',
                    ed_operator_field: '',
                    filterData: []
                },
            );
            this.setState({assestsList: this.state.mainList});
            this.state.assestsList = this.state.mainList;
            this.initMap();
            this.barGraphDataFn();
            this.state.source_id = '1';
            this.state.el301_operator_field = '';
            this.state.es200_operator_field = '';
            this.state.ed_operator_field = '';
            this.state.customer_no = [];
            this.pieChartValueFn();
            this.monthWiseBarGraphFn();
            this.props.Customer([]);
            this.setPieChartErrorValueFn();
        } else {
            this.state[key_name] = e;
            this.setState({[key_name]: e})

            if (_.isNull(e)) {
                var filterValue = this.state.filterData;
                _.each(filterValue, function (data, index) {
                    if (data && data.type == key_name) {
                        filterValue.splice(index, 1)

                    }
                })
                this.setState({filterData: filterValue})
            } else {
                if (key_name && key_name != 'customer_no') {
                    var exist_data = _.findWhere(this.state.filterData, {type: key_name});
                    var exist_data_index = _.findIndex(this.state.filterData, {type: key_name});

                    if (exist_data && !_.isEmpty(exist_data)) {

                        var filterValue = this.state.filterData;
                        filterValue[exist_data_index] = {type: key_name, key: this.state[key_name].label}
                    } else {
                        var filterValue = this.state.filterData;
                        filterValue.push({type: key_name, key: this.state[key_name].label})
                        this.setState({filterData: filterValue})
                    }
                }

            }


            var exist_operator_data = this.state.mainList;

            if (this.state.source_id && this.state.source_id != '1') {
                if (this.state.source_id.value == '2') {
                    var key = 'IoTConnector'
                } else {
                    key = 'DoorPilot'
                }

                exist_operator_data = _.filter(this.state.mainList, function (data) {
                    if (data && !_.isEmpty(data.data_metadata)) {
                        return data.data_metadata['source'] == key;
                    }
                });
            }


            if (this.state.city && this.state.city != '1') {
                var filter_no = this.state.city.label;
                exist_operator_data = _.filter(exist_operator_data, function (data) {
                    return data.city == filter_no;
                });
            }
            if (this.state.customer_no && this.state.customer_no.length) {
                this.props.Customer(this.state.customer_no);
                var exist_customer_data = [];
                _.each(this.state.customer_no, function (customer) {

                    _.each(exist_operator_data, function (data) {
                        if (data.data_metadata.customer == customer.label) {
                            exist_customer_data.push(data)
                        }
                    })
                });
                exist_operator_data = exist_customer_data;
            } else {
                this.props.Customer([]);
            }
            this.setState({assestsList: exist_operator_data});
            this.state.assestsList = exist_operator_data;
            this.initMap();
            this.barGraphDataFn();
            this.pieChartValueFn();
            this.monthWiseBarGraphFn();
            this.setPieChartErrorValueFn();
        }

    }


    setPieChartValueFn(pie_chart_key_field, color, array, pie_chart_name, device_name) {
        var data = [];
        var key_wise_data = [];
        if (array && array.length) {
            _.each(pie_chart_key_field, function (key, index) {
                var error_key = [];
                if (key) {
                    error_key = key.split('error');
                }
                if (error_key && error_key.length > 1) {
                    key_wise_data = _.filter(array, function (data) {
                        if (data && !_.isEmpty(data.data) && !_.isNull(data.data)) {
                            return data.data.errors[error_key[1]] != '0' &&
                                data.data.errors[error_key[1]] != 0 && data.data.errors[error_key[1]] != "noError"
                        }
                    });


                } else {
                    key_wise_data = _.filter(array, function (data) {
                        if (data && !_.isEmpty(data.data) && !_.isNull(data.data)) {
                            return data.data[key]
                        }
                    });

                }

                if (key_wise_data && key_wise_data.length) {
                    console.log('key_wise_data', key_wise_data)
                    data.push({key: key, value: key_wise_data.length, data: key_wise_data})
                }
            });
        }

        this.setState({[pie_chart_name]: data, [device_name]: array.length})

    }


    pieChartValueFn() {
        var all_devices = [];
        var groupByData = _.groupBy(this.state.assestsList, 'model');
        var EDData = _.reject(this.state.assestsList, function (data) {
            return data.model == 'ES200' || data.model == 'EL301'
        });
        color = ['#FF6384', '#36A2EB', '#8060d8cf', '#4dbd74', '#e3a51a', '#86919c', '#b79304', '#de0f0e'];
        el301_pie_chart_key_field = ['batteryAlarm', 'generalFault', 'lockForced', 'obstruction', 'presenceFail', 'serviceDue'];
        ed_es_pie_chart_key_field = ['breakOpenAttempt', 'emergencyExit', 'maintenanceNecessaryAfterInterval'
            , 'maintenanceNecessaryAfterNCycles', 'sabotage', 'error0', 'error1', 'error2'];


        if (groupByData && groupByData.EL301 && groupByData.EL301.length) {

            //this.setPieChartValueFn(el301_pie_chart_key_field,color,groupByData.EL301,'el301_pie_chart_data','el301_pie_data')
            all_devices.push({
                key: 'EL' + '(' + groupByData.EL301.length + ')',
                value: groupByData.EL301.length,
                data: groupByData.EL301
            })
        } else {
            //this.setPieChartValueFn('','',[],'el301_pie_chart_data','el301_pie_data')
        }

        if (groupByData && groupByData.ES200 && groupByData.ES200.length) {

            //this.setPieChartValueFn(ed_es_pie_chart_key_field,color,groupByData.ES200,'es200_pie_chart_data','es200_pie_data')
            all_devices.push({
                key: 'ES' + '(' + groupByData.ES200.length + ')',
                value: groupByData.ES200.length,
                data: groupByData.ES200
            })
        } else {
            //this.setPieChartValueFn('','',[],'es200_pie_chart_data','es200_pie_data')
        }
        if (EDData && EDData.length) {

            //this.setPieChartValueFn(ed_es_pie_chart_key_field,color,EDData,'ed_pie_chart_data','ed_pie_data')
            all_devices.push({key: 'ED' + '(' + EDData.length + ')', value: EDData.length, data: EDData})
        } else {
            //this.setPieChartValueFn('','',[],'ed_pie_chart_data','ed_pie_data')
        }
        this.setState({allDevicesPieChart: all_devices})

    }

    pieChartViewFn(chartdata) {
        return (

            <div className="chart-wrapper">

                <PieChart
                    size={280}
                    labels
                    styles={{
                        '.chart_lines': {
                            strokeWidth: 0
                        },
                        '.chart_text': {
                            fontFamily: 'serif',
                            fontSize: '1em',
                            fill: '#fff',
                            textAlign: 'center'
                        }
                    }}
                    clickHandler={
                        (d) => this.setState({
                            success: true,
                            modal_data: d.data.data,
                        })
                    }
                    data={this.state[chartdata]}/>

            </div>

        )
    }


    exportAllDataFn() {

        var this_pointer = this;
        var headers = [];
        export_csv_data = [];
        export_all_headers = [
            {
                label: 'id', key: 'id'
            }
        ];

        _.each(this.state.metaDataField, function (val) {
            export_all_headers.push({label: val, key: val})
        })

        _.each(this.state.assestsList, function (data) {
            if (data && data.data && !_.isEmpty(data.data)) {

                _.each(data.data, function (val, key) {
                    if (!_.isObject(val)) {
                        headers.push(key)
                    }
                })

            }
        });

        if (headers && headers.length) {
            _.each(_.uniq(headers), function (key) {
                export_all_headers.push({label: key, key: key})
            })

        }


        _.each(this.state.assestsList, function (data) {
            if (data && data.data && !_.isEmpty(data.data)) {
                var excel_data = {
                    id: data.id ? data.id : '-'
                };
                _.each(this_pointer.state.metaDataField, function (val) {
                    excel_data[val] = (data.data_metadata[val] == 'true' || data.data_metadata[val] == true ? data.data_metadata[val] : (
                        data.data_metadata[val] == 'false' || data.data_metadata[val] == false ? 'false' : (
                            data.data_metadata[val] ? data.data_metadata[val] : '-'
                        )
                    ))
                })
                _.each(headers, function (key) {
                    excel_data[key] =
                        (data.data[key] == 'true' || data.data[key] == true ? data.data[key] : (
                            data.data[key] == 'false' || data.data[key] == false ? 'false' : (
                                data.data[key] ? data.data[key] : '-'
                            )
                        ))
                });
                export_csv_data.push(excel_data)
            }
        });

        this.setState({export_csv_data: export_csv_data, export_all_headers: export_all_headers});
        //document.getElementById("csvlink").click();

    }


    generateBarGraphDataFn(barGraphName, backGroundColor, borderColor, labelName, dataPointsLabel) {

        return {
            labels: this.state[dataPointsLabel],
            datasets: [
                {
                    label: labelName,
                    backgroundColor: backGroundColor,
                    borderColor: borderColor,
                    borderWidth: 1,
                    hoverBackgroundColor: backGroundColor,
                    hoverBorderColor: borderColor,
                    data: this.state[barGraphName],
                },],
        }
    }

    generateBarGraphFn(barGraphName, backGroundColor, borderColor, labelName, dataPointsLabel) {
        return (

            <Bar
                data={this.generateBarGraphDataFn(barGraphName, backGroundColor, borderColor, labelName, dataPointsLabel)}

                options={{
                    legend: {
                        display: true,
                        fullWidth: true
                    },
                    scales: {
                        xAxes: [
                            {
                                scaleLabel: {
                                    display: true,
                                    labelString: labelName == 'City Wise Graph' ? 'City' : 'Months',
                                }
                            }],
                        yAxes: [
                            {
                                scaleLabel: {
                                    display: true,
                                    labelString: labelName == 'City Wise Graph' ? 'Total No of Asset' : 'Total No of Operators'
                                },
                                ticks: {
                                    suggestedMin: 0,
                                    beginAtZero: true,
                                }
                            }
                        ],
                    }
                }}
                onElementsClick={(e) => {
                    labelName == 'City Wise Graph' ? this.openModalByBarFn(e[0]._model) : console.log('hello', e[0]._model)
                }}/>
        )


    }

    searchFn(clear) {
        if (clear == 'clear') {
            this.setState({search: ''})
        } else if (this.state.search && (!_.isNull(this.state.search)
            || !_.isUndefined(this.state.search) || this.state.search == '')) {

            const regex = new RegExp(this.state.search);
            var this_pointer = this;
            var modal_data = _.filter(this.state.mainList, function (data) {
                var first_time_data = true;
                if (data.id.match(regex)) {
                    return data
                } else {
                    var return_data = false;
                    if (data && data.data_metadata) {
                        _.each(data.data_metadata, function (val, key) {
                            if (val.match(regex) && !return_data) {
                                return_data = true;
                            }
                        });
                        if (return_data) {
                            return data
                        }
                    }
                    /*  var exist_data=_.contains(_.values(data.data_metadata),this_pointer.state.search);
                      if(exist_data){
                          return data
                      }*/
                }
            });
            this.setState({success: true, modal_data: modal_data})
        }


    }

    toggle(index) {
        var modalData = this.state.modal_data;
        modalData[index].popoverOpen = !modalData[index].popoverOpen;
        this.setState({modal_data: modalData})
    }

    render() {
        var this_pointer = this;
        if (this.state.redirect) {
            return <Redirect
                to={{pathname: '/dashboard/asset/' + this.state.redirect_data.id, state: this.state.redirect_data}}
                render={<SingleOperatorView/>}/>
        }
        return (

            <div className="animated fadeIn">

                <LoadingScreen
                    loading={this.state.loaded}
                    bgColor='#f1f1f1'
                    spinnerColor='#9ee5f8'
                    textColor='#676767'
                    text='Loading'>
                </LoadingScreen>
                <Row>
                    <Col xs={'12'} sm={'12'} md={'12'} lg={'12'}>

                        <Card>
                            <CardHeader>

                                <Row>

                                    <Col xs="12" lg="12">
                                        Filters
                                    </Col>
                                    {this.state.filterData && this.state.filterData.length ?
                                        <Col>


<span>
                                        <Button type="submit"
                                                size="sm"
                                                className={'btn-pill btnPdng'}
                                                onClick={this.selecetFn.bind(this, 'clear')}
                                                color="primary">
                                            Clear all <i className="fa fa-close"></i>
                                        </Button>
</span>
                                            {this.state.filterData && this.state.filterData.length ? this.state.filterData.map((key, index) => (
                                                <span style={{'margin-left': '10px'}}>
                                        <Button type="submit"
                                                size="sm"
                                                className={'btn-pill btnPdng pinkBtn'}
                                                onClick={this.selecetFn.bind(this, key.type, null)}
                                                color="primary">
                                            {key.key} <i className="fa fa-close"></i>
                                        </Button>
</span>
                                            )) : ''}


                                        </Col>
                                        : ''}


                                </Row>


                            </CardHeader>
                            <CardBody>


                                <div className="chart-wrapper">


                                    <Row>

                                        <Col xs={'12'} sm={'12'} md={'3'} lg={'3'}>

                                            <FormGroup>
                                                <Label htmlFor="ccmonth">Source</Label>

                                                <Select
                                                    value={this.state.source_id}
                                                    onChange={this.selecetFn.bind(this, 'source_id')}
                                                    options={[
                                                        {
                                                            label: 'IoT Connector',
                                                            value: '2'
                                                        },
                                                        {
                                                            label: 'DoorPilot',
                                                            value: '3'
                                                        }
                                                    ]}
                                                />
                                            </FormGroup>


                                        </Col>


                                        <Col xs={'12'} sm={'12'} md={'3'} lg={'3'}>

                                            <FormGroup>
                                                <Label htmlFor="ccmonth">Geography</Label>
                                                <Select
                                                    placeholder={'Everywhere'}
                                                    value={this.state.city}
                                                    onChange={this.selecetFn.bind(this, 'city')}
                                                    options={this.state.cityList}
                                                />
                                            </FormGroup>


                                        </Col>


                                        <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                                            <FormGroup>
                                                <Label htmlFor="ccmonth">Customer No</Label>
                                                <Select
                                                    value={this.state.customer_no}
                                                    isMulti="true"
                                                    onChange={this.selecetFn.bind(this, 'customer_no')}
                                                    options={this.state.customerList}
                                                />
                                            </FormGroup>


                                        </Col>


                                        {
                                            this.state.customer_no && this.state.customer_no.length > 1 ?
                                                <Col xs={'12'} sm={'12'} md={'2'} lg={'2'}>


                                                    <CSVLink data={this.state.export_csv_data}
                                                             headers={this.state.export_all_headers}
                                                             id="csvlink"
                                                             target="_blank"
                                                             filename={'graphData.csv'}>
                                                        <Button type="submit"
                                                                style={{'margin-top': '28px'}}
                                                                onClick={this.exportAllDataFn.bind(this)}
                                                                size="md"
                                                                color="primary">
                                                            Export All
                                                        </Button>
                                                    </CSVLink>


                                                </Col>
                                                : ''

                                        }
                                    </Row>


                                    <Row>
                                        <Col xs={'12'} sm={'12'} md={'11'} lg={'11'}>
                                            <form
                                                name={'form'}
                                                onSubmit={this.searchFn.bind(this)}>
                                                <InputGroup>

                                                    <Input type="text"
                                                           value={this.state.search}
                                                           onChange={(e) => {
                                                               this.setState({search: e.target.value})
                                                           }}
                                                           placeholder="Search"/>


                                                    <InputGroupAddon addonType="prepend">
                                                        <Button type="button"
                                                                onClick={this.searchFn.bind(this)}
                                                                color="primary">
                                                            <i className="fa fa-search"></i>
                                                            Search</Button>


                                                        <Button
                                                            type="button"
                                                            color="danger"
                                                            onClick={this.searchFn.bind(this, 'clear')}>Clear</Button>


                                                    </InputGroupAddon>
                                                </InputGroup>
                                            </form>
                                        </Col>
                                    </Row>
                                </div>


                            </CardBody>
                        </Card>
                    </Col>
                </Row>


                <Row>


                    <Col xs={'12'} sm={'12'} md={'12'} lg={'12'}>


                        <Card>
                            <CardHeader>
                                Devices Location
                                <b> Total Devices <span className={'text-info'}>
                                    ({this.state.assestsList.length})
                                </span></b>

                                <div className="card-header-actions">
                                </div>
                            </CardHeader>
                            <CardBody>

                                <Row>

                                    <Col xs={'12'} sm={'12'} md={'12'} lg={'12'}>
                                        <div className="chart-wrapper" id={'map'} style={{'height': 500}}>

                                        </div>
                                    </Col>


                                    {this.state.cityGraphLabel && this.state.cityGraphLabel.length ?

                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'}>
                                            <div className="chart-wrapper">

                                                {/*<Bar data={
                                                    {
                                                        labels: this.state.cityGraphLabel,
                                                        datasets: [
                                                            {
                                                                label: 'City Wise Graph',
                                                                backgroundColor:'#FF6384',
                                                                borderColor: '#FF6384',
                                                                borderWidth: 1,
                                                                hoverBackgroundColor: '#FF6384',
                                                                hoverBorderColor: '#FF6384',
                                                                data: this.state.cityGraphData,
                                                            },],
                                                    }
                                                }

                                                     options={{
                                                         legend: {
                                                             display: true,
                                                             fullWidth:true
                                                         },
                                                         scales: {
                                                             xAxes: [
                                                                 {
                                                                     scaleLabel: {
                                                                         display: true,
                                                                         labelString: 'City',
                                                                     }}],
                                                             yAxes: [
                                                                 {
                                                                     scaleLabel: {
                                                                         display: true,
                                                                         labelString: 'Total No of Asset'
                                                                     },
                                                                     ticks: {
                                                                         suggestedMin: 0,
                                                                         beginAtZero: true,
                                                                     }
                                                                 }
                                                             ],
                                                         }
                                                     }}
                                                     onElementsClick={(e)=>{this.openModalByBarFn(e[0]._model)}}/>*/}


                                                {this.generateBarGraphFn('cityGraphData', '#FF6384', '#FF6384', 'City Wise Graph', 'cityGraphLabel')}

                                            </div>
                                        </Col>
                                        : ''}


                                </Row>
                            </CardBody>
                        </Card>


                    </Col>

                </Row>


                {/*<Row>
                    <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                        <Card>
                            <CardHeader>
                                EL Devices&nbsp;
                                <span className={'text-info'}>
                                    ({this.state.el301_pie_data})
                                </span>
                            </CardHeader>
                            <CardBody>
                                <Row>



                                    {this.state.el301_pie_chart_data && this.state.el301_pie_chart_data.length ?

                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'} style={{'text-align':'center'}}>


                                            {this.pieChartViewFn('el301_pie_chart_data')}

                                        </Col>
                                        : ''}


                                </Row>

                            </CardBody>
                        </Card>
                    </Col>
                    <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                        <Card>
                            <CardHeader>
                                ES Devices&nbsp;
                                <span className={'text-info'}>
                                    ({this.state.es200_pie_data})
                                </span>
                            </CardHeader>
                            <CardBody>
                                <Row>

                                    {this.state.es200_pie_chart_data && this.state.es200_pie_chart_data.length ?
                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'} style={{'text-align':'center'}}>

                                            {this.pieChartViewFn('es200_pie_chart_data')}
                                        </Col>
                                        : ''}



                                </Row>

                            </CardBody>
                        </Card>
                    </Col>


                    <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                        <Card>
                            <CardHeader>
                                ED Devices&nbsp;
                                <span className={'text-info'}>
                                    ({this.state.ed_pie_data})
                                </span>
                            </CardHeader>
                            <CardBody>
                                <Row>

                                    {this.state.ed_pie_chart_data && this.state.ed_pie_chart_data.length ?
                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'} style={{'text-align':'center'}}>

                                            {this.pieChartViewFn('ed_pie_chart_data')}

                                        </Col> : ''}

                                </Row>

                            </CardBody>
                        </Card>
                    </Col>
                </Row>*/}


                <Row>


                    <Col xs={'12'} sm={'12'} md={'12'} lg={'12'}>


                        <Card>
                            <CardHeader>
                                DoorPilor Operators
                                <div className="card-header-actions">
                                </div>
                            </CardHeader>
                            <CardBody>


                                {this.state.dataPointsLabel && this.state.dataPointsLabel.length ?
                                    <Row>
                                        <Col xs={'12'} sm={'12'} md={'6'} lg={'6'}>
                                            <div className="chart-wrapper">

                                                {this.generateBarGraphFn('first_data_points', '#f9db05', '#F9DB05', 'First Datapoints', 'dataPointsLabel')}
                                            </div>
                                        </Col>

                                        <Col xs={'12'} sm={'12'} md={'6'} lg={'6'}>
                                            <div className="chart-wrapper">

                                                {this.generateBarGraphFn('all_data_points', '#23CEAE', '#14967e', 'All Datapoints', 'dataPointsLabel')}


                                            </div>
                                        </Col>
                                    </Row>
                                    : ''}


                            </CardBody>
                        </Card>


                    </Col>

                </Row>


                <Row>


                    <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                        <Card>
                            <CardHeader>
                                All Devices
                            </CardHeader>
                            <CardBody>
                                <Row>


                                    {this.state.allDevicesPieChart && this.state.allDevicesPieChart.length ?
                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'} style={{'text-align': 'center'}}>


                                            {this.pieChartViewFn('allDevicesPieChart')}
                                            {/* <div className="chart-wrapper">

                                                <PieChart
                                                    size={280}
                                                    labels
                                                    data={this.state.allDevicesPieChart}
                                                    clickHandler={
                                                        (d) => this.setState({
                                                            success:true,
                                                            modal_data:d.data.data,
                                                        })
                                                    }
                                                    styles={{
                                                        '.chart_text': {
                                                            fontSize: '1em',
                                                            fill: '#fff'
                                                        }
                                                    }}
                                                />



                                            </div>*/}
                                        </Col>
                                        : ''}


                                </Row>

                            </CardBody>
                        </Card>
                    </Col>


                    <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                        <Card>
                            <CardHeader>
                                ES Error Devices
                            </CardHeader>
                            <CardBody>
                                <Row>


                                    {this.state.es200ErrorData && this.state.es200ErrorData.length ?
                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'} style={{'text-align': 'center'}}>

                                            {this.pieChartViewFn('es200ErrorData')}
                                            {/*     <div className="chart-wrapper">

                                                <PieChart
                                                    size={280}
                                                    labels
                                                    data={this.state.es200ErrorData}
                                                    clickHandler={
                                                        (d) => this.setState({
                                                            success:true,
                                                            modal_data:d.data.data,
                                                        })
                                                    }

                                                    styles={{
                                                        '.chart_text': {
                                                            fontSize: '1em',
                                                            fill: '#fff'
                                                        }
                                                    }}
                                                />

                                            </div>*/}
                                        </Col> : ''}


                                </Row>

                            </CardBody>
                        </Card>
                    </Col>


                    <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                        <Card>
                            <CardHeader>
                                ED Error Devices
                            </CardHeader>
                            <CardBody>
                                <Row>


                                    {this.state.edErrorData && this.state.edErrorData.length ?
                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'} style={{'text-align': 'center'}}>

                                            {this.pieChartViewFn('edErrorData')}
                                            {/* <div className="chart-wrapper">

                                                <PieChart
                                                    size={280}
                                                    labels
                                                    data={this.state.edErrorData}
                                                    clickHandler={
                                                        (d) => this.setState({
                                                            success:true,
                                                            modal_data:d.data.data,
                                                        })
                                                    }
                                                    styles={{
                                                        '.chart_text': {
                                                            fontSize: '1em',
                                                            fill: '#fff'
                                                        }
                                                    }}
                                                />



                                            </div>*/}
                                        </Col>
                                        : ''}


                                </Row>

                            </CardBody>
                        </Card>
                    </Col>


                </Row>


                <Modal isOpen={this.state.success}
                       className={'modal-success modal-lg'}>
                    <ModalHeader>Asset List</ModalHeader>
                    <ModalBody>


                        <Table responsive striped>
                            {
                                this.state.modal_data && this.state.modal_data.length ?
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Type</th>
                                        <th>Asset ID</th>
                                        <th>Customer</th>

                                    </tr>
                                    </thead> : ''}
                            <tbody>
                            {
                                this.state.modal_data && this.state.modal_data.length ? this.state.modal_data.map((data, index) => (
                                        <tr>
                                            <td>

                                                <i className={(data.model == 'EL301' && data.data && (data.data.serviceDue || data.data.serviceDue == 'true')) ?
                                                    'fa fa-circle fa-lg text-warning' : ((data && data.data && data.model != 'EL301'
                                                        && data.data.errors && (data.data.errors['0'] != '0' &&
                                                            data.data.errors['0'] != 0 && data.data.errors['0'] != 'noError'))
                                                        ? 'fa fa-circle fa-lg text-danger' : 'fa fa-circle fa-lg text-success')
                                                }>
                                                </i>
                                                {index + 1}

                                            </td>
                                            <td><a href="javascript:void 0">{data.model || '-'}</a>

                                            </td>
                                            <td><a href="javascript:void 0"
                                                   id={"Popover" + index}
                                                   onMouseOver={this.toggle.bind(this, index)}
                                                   onMouseOut={this.toggle.bind(this, index)}
                                                   onClick={this.redirect.bind(this, data)}>
                                                {data.id}
                                            </a>
                                                {data.popoverOpen ?
                                                    <Popover placement="left"
                                                             isOpen={data.popoverOpen}
                                                             target={"Popover" + index}
                                                             toggle={this.toggle.bind(this, index)}>
                                                        <PopoverBody>

                                                            {data.data && !_.isEmpty(data.data) ?

                                                                <span>

                                                                    {Object.keys(data.data).map(function (key) {
                                                                        if (key != 'errors')
                                                                            return <div>
                                                                                <b>{key}</b>:{(data.data[key] == 'false' || data.data[key] == false) ? 'false' : data.data[key]}
                                                                            </div>

                                                                    })}

                                                                </span>
                                                                : ''}
                                                        </PopoverBody>
                                                    </Popover> : ''}


                                            </td>
                                            <td><a
                                                href="javascript:void 0">{data.data_metadata && data.data_metadata.customer ?
                                                data.data_metadata.customer : (data.customer || '-')}</a></td>
                                        </tr>
                                    )
                                ) : <h4 className={'text-center'}><b>No Operator Found</b></h4>
                            }
                            </tbody>
                        </Table>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary"
                                id="Popover1"
                                onClick={() => {
                                    this.setState({success: false})
                                }}>Cancel</Button>
                    </ModalFooter>
                </Modal>

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
