import React, {Component} from 'react';
import {
    Badge, Card, CardBody, Button, Input, Form, Label, FormGroup, CardHeader, Col, CardFooter,
    Pagination, PaginationItem, PaginationLink, Row, Table
} from 'reactstrap';
import AdminPage from './AdminPage';
import {Redirect} from 'react-router-dom';
import './AdminAddPage.css'
import * as _ from "underscore";
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css"
import {NotificationContainer, NotificationManager} from 'react-notifications';
import LoadingScreen from "react-loading-screen";
import FileBase64 from 'react-file-base64';
import datepickerWrap from './AdminAddPage.css'

const commonHostUrl = require('../../../../src/constant');

//import "react-datepicker/dist/react-datepicker.css";

class AdminAddPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            isActive: true,
            emailValidation: false,
            mobileValidation: false,
            instituteValidation: false,
            adminValidation: false,
            tutorValidation: false,
            user: {},
            types: [
                {
                    value: 1,
                    label: 'Super Admin',
                    display: 'Select Type'
                },
                {
                    value: 2,
                    label: 'Admin',
                    display: 'Select Type'
                },
                {
                    value: 3,
                    label: 'Tutor',
                    display: 'Select Type'
                },
                {
                    value: 4,
                    label: 'Student',
                    display: 'Select Type'
                }
            ],
            tutors: [],
            selectedType: {
                label: 'Select',
            },
            selectedTutor: {
                //value: 4,
                label: 'Select',
            },
            selectedInstitute: {
                //value: 4,
                label: 'Select',
            },
            selectedAdmin: {
                //value: 4,
                label: 'Select',
            },
            validationError: "",
            selectDisable: false,
            tutorOpen: false,
            institutes: [],
            admins: [],
            adminOpen: true,
            files: [],
            startDate: new Date(),
            isImageLarge: false
        }
        this.handleChange = this.handleChange.bind(this);

    }


    /*componentWillReceiveProps(newProps) {
    }*/

    componentDidMount() {


        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let listBaseUrl = commonHostUrl.url + "api/user-list";
        let self = this;
        let lisPayload = {
            type: [2, 3]
        }

        fetch(listBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(lisPayload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    let userTutors

                    if (cachedUser && cachedUser.type === 3) {
                        let fullName = cachedUser.firstName + " " + cachedUser.lastName;
                        userTutors = [{label: fullName, firstName: cachedUser.firstName, lastName: cachedUser.lastName}]
                    }
                    else {
                        userTutors = _.filter(result.data, {type: 3});
                    }


                    let userAdmins = _.filter(result.data, {type: 2})

                    let myTutors = [];
                    _.each(userTutors, function (tutor) {
                        let fullName = tutor.firstName + " " + tutor.lastName
                        myTutors.push({
                            value: tutor._id,
                            label: fullName
                        })
                    })
                    let myAdmins = [];
                    _.each(userAdmins, function (tutor) {
                        let fullName = tutor.firstName + " " + tutor.lastName
                        myAdmins.push({
                            value: tutor._id,
                            label: fullName
                        })
                    })

                    this.setState({
                        tutors: myTutors,
                        admins: myAdmins
                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            })

        let apiBaseUrl = commonHostUrl.url + "api/institute-list";

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            // body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    let updateData = result.data;

                    let myInstitutes = [];
                    _.each(result.data, function (insti) {
                        myInstitutes.push({
                            value: insti._id,
                            label: insti.title
                        })
                    })


                    this.setState({
                        institutes: myInstitutes,

                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });


        //this.props.Logout();
    }

    redirectFn() {
        this.setState({redirect: true})
    }

    // Callback~
    getFiles(files) {
        this.setState({files: files})
        let imageSize = this.state.files[0].size;
        console.log("imageSize----", imageSize)
        if (imageSize <= 50) {
            console.log("In--SIze")
            this.setState({isImageLarge: true})
        }
    }

    handleChange(date) {
        this.setState({
            startDate: date
        });
    }

    setDataFn(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value})

        if (name == 'email') {
            let filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!filter.test(value)) {

                this.setState({emailValidation: true})

            } else {
                this.setState({emailValidation: false})
            }
        }
        if (name == 'mobile') {
            let filter = /^[0-9\b]+$/;
            if (!filter.test(value)) {

                this.setState({mobileValidation: true})

            } else {
                this.setState({mobileValidation: false})
            }
        }
        if (name == 'myImage') {
            this.setState({file: e.target.files[0]});
        }

    }

    setSelectFn(e) {
        const name = e.target.name;
        const value = parseInt(e.target.value);
        this.setState({selectedType: value})
    }

    handleCheck() {
        this.setState({isActive: !this.state.isActive});
    }

    resetFn() {
        this.setState({
            type: 4,
            firstName: '',
            lastName: '',
            email: '',
            mobile: '',
            //expiry_date:'',
            password: '',
            c_password: '',
            redirect: false,
            isActive: true,
            emailValidation: false,
            mobileValidation: false,
            tutorOpen: false,
            instituteValidation: false,
            adminValidation: false,
            tutorValidation: false,
            adminOpen: false,
            selectedType: {
                label: 'Select',
            },
            selectedTutor: {
                //value: 4,
                label: 'Select',
            },
            selectedInstitute: {
                //value: 4,
                label: 'Select',
            },
            selectedAdmin: {
                //value: 4,
                label: 'Select',
            },
        })
    }

    selectFn(key_name, e) {

        let loginUser = JSON.parse(localStorage.getItem('User'));
        if (loginUser && loginUser.type === 1) {
            if (key_name == 'clear') {
                this.setState({selectedType: 4});
            }
            else if (e.value === 4 && key_name === "selectedType") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, tutorOpen: true, adminOpen: true})
            }
            else if (e.value === 2 && key_name === "selectedType") {
                this.state[key_name] = e;
                this.setState({
                    [key_name]: e,
                    tutorOpen: false,
                    adminOpen: false,
                    adminValidation: true,
                    tutorValidation: true
                })
            }
            else if (e.value === 1 && key_name === "selectedType") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, adminOpen: false, adminValidation: true, tutorValidation: true})
            }
            else if (e.value === 3 && key_name === "selectedType") {
                this.state[key_name] = e;
                this.setState({
                    [key_name]: e,
                    tutorOpen: false,
                    adminOpen: true,
                    adminValidation: false,
                    tutorValidation: true
                })
            }
            else if (key_name === "selectedInstitute") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, instituteValidation: true})
            }
            else if (key_name === "selectedTutor") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, tutorOpen: true, adminOpen: true, tutorValidation: true})
            }
            else if (key_name === "selectedAdmin") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, adminValidation: true})
            }
            else {
                this.state[key_name] = e;
                this.setState({[key_name]: e, tutorOpen: false, adminOpen: true})
            }
        }
        else if (loginUser && loginUser.type === 2) {
            if (key_name == 'clear') {
                this.setState({selectedType: 4});
            }
            else if (e.value === 4 && key_name === "selectedType") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, tutorOpen: true, adminOpen: true, adminValidation: true,})
            }
            else if (e.value === 3 && key_name === "selectedType") {
                this.state[key_name] = e;
                this.setState({
                    [key_name]: e,
                    tutorOpen: false,
                    adminOpen: false,
                    adminValidation: true,
                    tutorValidation: true
                })
            }
            else if (key_name === "selectedInstitute") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, instituteValidation: true})
            }
            else if (key_name === "selectedTutor") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, tutorOpen: true, adminOpen: true, tutorValidation: true})
            }
            else if (key_name === "selectedAdmin") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, adminValidation: true})
            }
            else {
                this.state[key_name] = e;
                this.setState({[key_name]: e, tutorOpen: false, adminOpen: true})
            }
        }
        else if (loginUser && loginUser.type === 3) {
            if (key_name == 'clear') {
                this.setState({selectedType: 4});
            }
            else if (e.value === 4 && key_name === "selectedType") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, tutorOpen: true, adminOpen: true, adminValidation: true,})
            }
            else if (e.value === 3 && key_name === "selectedType") {
                this.state[key_name] = e;
                this.setState({
                    [key_name]: e,
                    tutorOpen: false,
                    adminOpen: false,
                    adminValidation: true,
                    tutorValidation: true
                })
            }
            else if (key_name === "selectedInstitute") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, instituteValidation: true})
            }
            else if (key_name === "selectedTutor") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, tutorOpen: true, adminOpen: true, tutorValidation: true})
            }
            else if (key_name === "selectedAdmin") {
                this.state[key_name] = e;
                this.setState({[key_name]: e, adminValidation: true})
            }
            else {
                this.state[key_name] = e;
                this.setState({[key_name]: e, tutorOpen: false, adminOpen: true})
            }
        }


    }


    RegisterFn() {
        let this_pointer = this;
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/user-register"

        let payload = {
            "firstName": this.state.firstName,
            "lastName": this.state.lastName,
            "email": this.state.email,
            "mobile": this.state.mobile,
            "password": this.state.password,
            "isActive": this.state.isActive,
            "type": this.state.selectedType.value,
            "tutorId": this.state.selectedTutor.value,
            "instituteId": this.state.selectedInstitute.value,
            "isAdmin": this.state.selectedAdmin.value,
            "class": this.state.class,
            "birthdate": this.state.startDate
        }

        if (this.state.files && this.state.files.length > 0) {
            let imageName = this.state.files[0].name;
            let imageType = this.state.files[0].type;
            let imageSize = this.state.files[0].size;
            let imageBase64 = this.state.files[0].base64;
            payload.profile = imageBase64
        }

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {
                    this.setState({
                        user: result.data,
                        redirect: true,
                        tutorOpen: false
                    });
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    this.setState({
                        redirect: false,
                        tutorOpen: false
                    });
                    NotificationManager.error(result.message);
                }
                else {
                    this.setState({
                        redirect: false,
                        tutorOpen: false
                    });
                    NotificationManager.error(result.message);
                }
            });
        //
        //this.props.addnew(this.state.user)
    }

    render() {
        let {types} = this.state;
        const {tutors} = this.state;
        const {institutes} = this.state;
        const {admins} = this.state;
        let loginUser = JSON.parse(localStorage.getItem('User'));

        if (loginUser && (loginUser.type === 2 || loginUser.type === 3)) {
            types = _.reject(types, function (type) {
                return type.value === 1
            })
        }
        if (loginUser && (loginUser.type === 2 || loginUser.type === 3)) {
            types = _.reject(types, function (type) {
                return type.value === 2
            })
        }
        if (loginUser && loginUser.type === 3) {
            types = _.reject(types, function (type) {
                return type.value === 3
            })
        }

        if (this.state.redirect) {
            return <Redirect to={{pathname: '/user'}}
                             render={<AdminPage/>}/>
        }
        return (
            <div className="animated fadeIn">
                <LoadingScreen
                    loading={this.state.loaded}
                    bgColor='#f1f1f1'
                    spinnerColor='#9ee5f8'
                    textColor='#676767'
                    text='Loading'>
                </LoadingScreen>
                <Row>

                    <Col xs="12" lg="12">
                        <form className="donationForm"
                              name={'form'}>
                            <Card>
                                <CardHeader>
                                    <Row>
                                        <Col xs="6" lg="6">
                                            <i className="fa fa-align-justify"></i> Add Users
                                        </Col>
                                        <Col xs="6" lg="6">
                                            <Button type="submit"
                                                    className="pull-right"
                                                    size="sm"
                                                    onClick={this.redirectFn.bind(this)}
                                                    color="primary">
                                                <i className="fa fa-chevron-left"></i> View User
                                            </Button>
                                        </Col>
                                    </Row>
                                </CardHeader>
                                <CardBody>
                                    {/*    <Row>
                                        <Col xs="12" sm="12" md={'11'} lg={'11'}>
                                        </Col>
                                        <Col xs="12" sm="12" md={'1'} lg={'1'}>
                                            <FormGroup check inline>
                                                <Input className="form-check-input"
                                                       type="checkbox"
                                                       onChange={this.handleCheck.bind(this)}
                                                       defaultChecked={this.state.isActive}/>
                                                <Label className="form-check-label"
                                                       check htmlFor="inline-checkbox1">Active</Label>
                                            </FormGroup>
                                        </Col>
                                    </Row>*/}
                                    <Row>
                                        <Col xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Type
                                                    <span
                                                        className={this.state.selectedType ? 'success' : 'err'}>*</span>
                                                </Label>
                                                <Select style={{width: 200}}
                                                        value={this.state.selectedType}
                                                        onChange={this.selectFn.bind(this, 'selectedType')}
                                                        options={
                                                            types
                                                        }
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col style={{
                                            marginRight: -13,
                                            paddingLeft: 7
                                        }} xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">First Name

                                                    <span className={this.state.firstName ? 'succes' : 'err'}>*</span>
                                                </Label>
                                                <Input type="text"
                                                       name="firstName"
                                                       ref={'firstName'}
                                                       value={this.state.firstName}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                        <Col style={{
                                            marginRight: -54,
                                            paddingLeft: 23
                                        }} xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Last Name
                                                    <span className={this.state.lastName ? 'succes' : 'err'}>*</span>
                                                </Label>
                                                <Input type="text"
                                                       name="lastName"
                                                       value={this.state.lastName}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col style={{
                                            marginLeft: 1
                                        }} xs="12" sm="12" md={'3'} lg={'3'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Image
                                                    <span>
                                                     {
                                                         this.state.isImageLarge ?
                                                             <span>Image Size Accessed, Use Under 40 Kb.</span> : ""

                                                     }
                                                </span>
                                                </Label>
                                                <FileBase64
                                                    multiple={true}
                                                    onDone={this.getFiles.bind(this)}/>
                                            </FormGroup>
                                        </Col>
                                        <Col style={{
                                            paddingRight: 0,
                                            marginLeft: 68
                                        }} xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Email
                                                    <span className={this.state.email ? 'succes' : 'err'}>
                                                    {!this.state.emailValidation ? '*' : ''}
                                                </span>
                                                    <span className={this.state.emailValidation ? 'err' : 'succes'}>

                                                    {this.state.emailValidation ? 'Please enter proper email address' : ''}

                                                </span>
                                                </Label>
                                                <Input type="text"
                                                       name="email"
                                                       value={this.state.email}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                        <Col style={{
                                            paddingLeft: 22,
                                        }} xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Mobile
                                                    {/*    <span className={this.state.mobile ? 'succes' : 'err'}>
                                                    {!this.state.mobileValidation ? '*' : ''}
                                                </span>
                                                    <span className={this.state.mobileValidation ? 'err' : 'succes'}>

                                                    {this.state.mobileValidation ? 'Please enter proper mobile number' : ''}

                                                </span>*/}
                                                </Label>
                                                <Input type="text"
                                                       name="mobile"
                                                       value={this.state.mobile}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                        {/* <Col xs="12" sm="12" md={'6'} lg={'6'}>
                                        <FormGroup>
                                            <Label htmlFor="name">Expiry Date
                                                <span className={this.state.expiry_date ? 'succes' : 'err'}>*</span>
                                            </Label>
                                            <Input type="date"
                                                   name="expiry_date"
                                                   value={this.state.expiry_date}
                                                   onChange={this.setDataFn.bind(this)}
                                                   required />
                                        </FormGroup>
                                    </Col>*/}
                                    </Row>

                                    <Row>
                                        {
                                            loginUser.type === 1 && this.state.adminOpen === true ?
                                                <Col xs="12" sm="12" md={'4'} lg={'4'}>
                                                    <FormGroup>
                                                        <Label htmlFor="name">Admin
                                                            <span
                                                                className={this.state.adminValidation ? 'success' : 'err'}>*</span>
                                                        </Label>
                                                        <Select style={{width: 200}}
                                                                value={this.state.selectedAdmin}
                                                                disabled={!this.state.selectDisable}
                                                                onChange={this.selectFn.bind(this, 'selectedAdmin')}
                                                                options={
                                                                    admins
                                                                }
                                                                required/>
                                                    </FormGroup>
                                                </Col> : ""

                                        }

                                        {
                                            !this.state.selectedType === 3 || this.state.tutorOpen ?
                                                <Col xs="12" sm="12" md={'4'} lg={'4'}>
                                                    <FormGroup>
                                                        <Label htmlFor="name">Tutor
                                                            <span
                                                                className={this.state.tutorValidation ? 'success' : 'err'}>*</span>
                                                        </Label>
                                                        <Select style={{width: 200}}
                                                                value={this.state.selectedTutor}
                                                                disabled={!this.state.selectDisable}
                                                                onChange={this.selectFn.bind(this, 'selectedTutor')}
                                                                options={
                                                                    tutors
                                                                }
                                                        />
                                                    </FormGroup>
                                                </Col> : ""

                                        }
                                        <Col xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Institute
                                                    <span
                                                        className={this.state.instituteValidation ? 'success' : 'err'}>*</span>
                                                </Label>
                                                <Select style={{width: 200}}
                                                        value={this.state.selectedInstitute}
                                                        disabled={!this.state.selectedInstitute}
                                                        onChange={this.selectFn.bind(this, 'selectedInstitute')}
                                                        options={
                                                            institutes
                                                        }
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="12" sm="12" md={'6'} lg={'6'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Class
                                                    <span className={this.state.class ? 'succes' : 'err'}>*</span>
                                                </Label>
                                                <Input type="class"
                                                       name="class"
                                                       value={this.state.class}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" sm="12" md={'6'} lg={'6'}>
                                            <div className="">
                                                <FormGroup>
                                                    <Label htmlFor="name">Birth Date
                                                        <span
                                                            className={this.state.startDate ? 'succes' : 'err'}>*</span>
                                                    </Label><br/>
                                                    <DatePicker
                                                        selected={this.state.startDate}
                                                        onChange={this.handleChange}
                                                        showMonthDropdown
                                                        showYearDropdown
                                                        dateFormatCalendar="MMMM"
                                                        scrollableYearDropdown
                                                        yearDropdownItemNumber={20}
                                                    />
                                                </FormGroup>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="12" sm="12" md={'6'} lg={'6'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Password
                                                    <span className={this.state.password ? 'succes' : 'err'}>*</span>
                                                </Label>
                                                <Input type="password"
                                                       name="password"
                                                       value={this.state.password}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" sm="12" md={'6'} lg={'6'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Confirm Password
                                                    <span className={!this.state.c_password ? 'err' : 'succes'}>
                                                    {!this.state.c_password ? '*' : ''}
                                                </span>
                                                    <span className={this.state.c_password && this.state.password &&
                                                    this.state.password == this.state.c_password ? 'succes' : 'err'}>

                                                    {
                                                        this.state.c_password && this.state.password &&
                                                        this.state.password != this.state.c_password ? 'Password and confirm password didnt match.' :
                                                            <span> {this.state.c_password && this.state.password &&
                                                            this.state.password == this.state.c_password ? '*' : ''}
                                                       </span>
                                                    }
                                                        </span>
                                                </Label>
                                                <Input type="password"
                                                       name="c_password"
                                                       value={this.state.c_password}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                    </Row>


                                </CardBody>

                                <CardFooter>
                                    <Button type="reset"
                                            className={'pull-right'}
                                            size="sm"
                                            onClick={this.resetFn.bind(this)}
                                            color="danger"> Reset</Button>
                                    <Button type="button"
                                            onClick={this.RegisterFn.bind(this)}
                                            className={!this.state.selectedType || !this.state.firstName || !this.state.lastName || !this.state.email
                                            || !this.state.password
                                            || !this.state.c_password || this.state.password != this.state.c_password ||
                                            !this.state.instituteValidation || !this.state.adminValidation || !this.state.tutorValidation || this.state.emailValidation ? 'btnDisbaled pull-right' : 'pull-right'}
                                            size="sm"
                                            style={{'marginRight': 10}}
                                        /* onSubmit={this.RegisterFn.bind(this)}*/
                                            color="primary">Save
                                    </Button>

                                </CardFooter>

                            </Card>
                        </form>
                    </Col>
                    <NotificationContainer/>
                </Row>


            </div>

        );
    }
}

export default AdminAddPage;
