import React, {Component} from 'react';
import '../../Pages/Login/login.css'
import {
    Button, Card, Label, CardBody,
    CardGroup, Col, Container, Input, InputGroup,
    Modal, ModalBody, ModalFooter,
    ModalHeader, FormGroup,
    InputGroupAddon, InputGroupText, Row
} from 'reactstrap';
import logoo from '../../Pages/Login/logo';
import {Redirect} from 'react-router-dom';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';


import LoadingScreen from 'react-loading-screen';
import {bindActionCreators} from 'redux'
import {ActCreators} from '../../../redux/bindActionCretor'
import {connect} from 'react-redux'
import Dashboard from "../../Dashboard";
import AdminPage from "../../Admin/User";
import * as _ from "underscore";

const commonHostUrl = require('../../../../src/constant');
var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const mapStateToProps = state => {
    console.log('hi', state);
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActCreators, dispatch)
};


class ForgotPassword extends Component {


    constructor(props) {
        super(props);
        this.state = {
            errorPassword: false,
            errorEmail: false,
            redirect: false,
            redirectPass: false,
            first_time_password_change: false,
            loaded: false,
            loginUser: null
        };
    }

    componentDidMount() {
        this.props.Logout();
    }

    //store success login data to localstorage
    onSetResult = (result, key) => {
        result.data.token = result.token
        this.setState({loginUser: result.data});
        localStorage.setItem('User', JSON.stringify(this.state.loginUser));
    }

    redirectPassFn() {
        this.setState({redirectPass: true})
    }

    LoginFn() {
        var this_pointer = this;
        //this.email.focus();
        if (this.state.password == null || this.state.password == "" || this.state.email == null || this.state.email == "") {
            if (this.state.password == null || this.state.password == "") {
                this.setState({errorPassword: true})
            } else {
                this.setState({errorPassword: false})
            }
            if (this.state.email == null || this.state.email == "") {
                this.setState({errorEmail: true})
            } else {
                this.setState({errorEmail: false})
            }
        } else {
            this.setState({errorPassword: false, errorEmail: false, loaded: true});

            //let apiBaseUrl = "http://192.168.0.120:1365/login";
            let apiBaseUrl = commonHostUrl.url + "login";
            let authenticationData = {
                email: this.state.email,
                password: this.state.password,
            };
            console.log("Aut-------", authenticationData)

            fetch(apiBaseUrl, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(authenticationData),
            })
                .then((res) => res.json())
                .then((result) => {
                    console.log("Data------", result);
                    if (result.code == 200) {
                        //generating localstorage
                        this_pointer.onSetResult(result, authenticationData)
                        //redirect to dashboard
                        this_pointer.props.Login({email: this_pointer.state.email});
                        this_pointer.setState({loaded: false, redirect: true,});

                        //this.props.history.push('/dashboard')
                        console.log("ForgotPassword successfull");
                        NotificationManager.success(result.message);
                    }
                    else if (result.code == 403) {
                        this_pointer.setState({loaded: false, redirect: false});
                        console.log("Username password do not match");
                        NotificationManager.error(result.message);
                    }
                    else {
                        console.log("Username does not exists");
                        NotificationManager.error(result.message);
                    }
                })

        }

    }


    changePasswordFn() {
        var this_pointer = this;
        var authenticationData = {
            Username: this.state.email,
            Password: this.state.password,
        };
        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
        var poolData = {
            UserPoolId: 'us-west-2_U24uJfY6N',
            ClientId: '7mm5cmo7rsj9m46q6s15p3lfl4'
        };


        var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
        var userData = {
            Username: authenticationData.Username,
            Pool: userPool
        };
        var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);


        cognitoUser.authenticateUser(authenticationDetails, {
            newPasswordRequired: (userAttributes, requiredAttributes) => {
                delete userAttributes.email_verified;
                userAttributes.family_name = "AutoComplete by app";
                userAttributes.name = "app user";

                cognitoUser.completeNewPasswordChallenge(
                    this.state.new_password,
                    userAttributes,
                    this
                );

                this_pointer.setState({password: '', first_time_password_change: false})
                NotificationManager.success('Please login again');


            },
            onSuccess: function (result) {
                console.log('result', result);
                this_pointer.setState({password: '', first_time_password_change: false})
                NotificationManager.success('Please login again');
            },

            onFailure: function (err) {
                console.log('err', err);
                NotificationManager.error(err.message);
            }
        });
    }


    checkErrorFn() {
        if (this.state.password == null || this.state.password == "" || this.state.email == null || this.state.email == "") {
            if (this.state.password == null || this.state.password == "") {
                this.setState({errorPassword: true})
            } else {
                this.setState({errorPassword: false})
            }
            if (this.state.email == null || this.state.email == "") {
                this.setState({errorEmail: true})
            } else {
                this.setState({errorEmail: false})
            }
        } else {
            this.setState({errorPassword: false})
            this.setState({errorEmail: false})
        }
    }


    render() {
        console.log("State-----", this.state)
        if (this.state.redirect) {
            return <Redirect to={{pathname: '/user'}}
                             render={<AdminPage/>}/>
        }
        if (this.state.redirectPass) {
            return <Redirect to={{pathname: '/user'}}
                             render={<AdminPage/>}/>
        }
        return (
            <div className="app flex-row align-items-center">

                <LoadingScreen
                    loading={this.state.loaded}
                    bgColor='#f1f1f1'
                    spinnerColor='#9ee5f8'
                    textColor='#676767'
                    text='Loading'>
                </LoadingScreen>

                <Container>
                    <Row className="justify-content-center">
                        <Col md="8">
                            <CardGroup>
                                <Card className="p-4">
                                    <CardBody>

                                        <Row>

                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                {/* <img src={logoo}
                             className="imgHeight"
                             alt="admin@bootstrapmaster.com" />*/}
                                            </Col>
                                        </Row>

                                        <h1>ForgotPassword</h1>
                                        <form className="donationForm"
                                              onSubmit={this.LoginFn.bind(this)}
                                              name={'form'}>

                                            <InputGroup className="mb-3">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="icon-user"></i>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input type="email"
                                                       value={this.state.email}
                                                       required
                                                       ref={this.email}
                                                       className={this.state.errorEmail ? 'errorBrd' : ''}
                                                       onChange={(e) => {
                                                           this.setState({email: e.target.value});
                                                           this.checkErrorFn()
                                                       }}
                                                       placeholder="Email"/>
                                            </InputGroup>
                                            <InputGroup className="mb-4">
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="icon-lock"></i>
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input type="password"
                                                       amazon-cognito-identity-js value={this.state.password}
                                                       required
                                                       className={this.state.errorPassword ? 'errorBrd' : ''}
                                                       onChange={(e) => {
                                                           this.setState({password: e.target.value});
                                                           this.checkErrorFn()
                                                       }}
                                                       placeholder="Password"/>
                                            </InputGroup>
                                            {/*<Row>

                                                <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-right">
                                                    <Input className="form-check-input"
                                                           type="checkbox" id="checkbox1"
                                                           name="checkbox1" value="option1"/>
                                                    <Label check className="form-check-label rememberMe"
                                                           htmlFor="checkbox1">Remember Me</Label>
                                                </Col>


                                            </Row>*/}
                                            <Row>

                                                <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                    <Button color="primary"
                                                            type={'submit'}
                                                            className="px-4">ForgotPassword</Button>
                                                    {/*onClick={this.LoginFn.bind(this)}*/}
                                                </Col>
                                            </Row>
                                        </form>
                                        <Row>

                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                <Button color="link" className="px-0"
                                                        onClick={this.redirectPassFn.bind(this)}>Forgot password?</Button>
                                            </Col>

                                            {/*<Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                <p className="text-muted">New to Entry as a Service? Sign up for
                                                    a <Button color="link" className="px-0">free account</Button>
                                                </p>
                                            </Col>*/}

                                        </Row>

                                    </CardBody>
                                </Card>
                            </CardGroup>
                            <NotificationContainer/>
                        </Col>
                    </Row>
                </Container>


                <Modal isOpen={this.state.first_time_password_change}
                       className={'modal-info modal-lg'}>
                    <ModalHeader>To continue, change the password</ModalHeader>
                    <ModalBody>

                        <Row>
                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                <FormGroup>
                                    <Label htmlFor="name">New Password</Label>
                                    <Input type="password"
                                           name="new_password"
                                           ref={'username'}
                                           onChange={(e) => {
                                               this.setState({new_password: e.target.value})
                                           }}
                                           value={this.state.new_password}
                                           required/>
                                </FormGroup>
                            </Col>
                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                <FormGroup>
                                    <Label htmlFor="name">Confirm Password
                                        {this.state.new_password && this.state.confirm_password &&
                                        (this.state.new_password != this.state.confirm_password) ?
                                            <span className={'text-danger'}>&nbsp;Password and confirm password must be same</span>
                                            : ''}

                                    </Label>
                                    <Input type="password"
                                           name="c_password"
                                           onChange={(e) => {
                                               this.setState({confirm_password: e.target.value})
                                           }}
                                           value={this.state.confirm_password}
                                           required/>
                                </FormGroup>
                            </Col>
                        </Row>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary"
                                onClick={this.changePasswordFn.bind(this)}
                                disabled={!this.state.new_password || !this.state.confirm_password
                                || this.state.new_password != this.state.confirm_password ? true : false}
                        >Submit</Button>
                        <Button color="secondary"
                                onClick={(e) => {
                                    this.setState({first_time_password_change: false})
                                }}>Cancel</Button>
                    </ModalFooter>
                </Modal>

            </div>
        );
    }
}

export default ForgotPassword;
