import React, {Component} from 'react';
import {
    Badge, Card, CardBody, Button, Input, Form, Label, FormGroup, CardHeader, Col, CardFooter,
    Pagination, PaginationItem, PaginationLink, Row, Table
} from 'reactstrap';
import AdminPage from './AdminPage';
import {Redirect} from 'react-router-dom';
import './AdminAddPage.css'
import * as _ from "underscore";
import Select from 'react-select';
import AdminAddPage from "./AdminAddPage";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import DatePicker from "react-datepicker/es";
import FileBase64 from "react-file-base64";

const commonHostUrl = require('../../../../src/constant');

class AdminEditPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            type: 4,
            firstName: '',
            lastName: '',
            email: '',
            mobile: '',
            expiry_date: '',
            password: '',
            c_password: '',
            redirect: false,
            isActive: true,
            emailValidation: false,
            mobileValidation: false,
            idEditable: false,
            types: [
                {
                    value: 1,
                    label: 'Super Admin',
                },
                {
                    value: 2,
                    label: 'Admin',
                },
                {
                    value: 3,
                    label: 'Tutor',
                },
                {
                    value: 4,
                    label: 'Student',
                }
            ],
            selectedType: {
                value: 4,
                label: 'Student',
            },
            selectedTutor: {
                //value: 4,
                label: 'Select',
            },
            selectedAdmin: {
                //value: 4,
                label: 'Select',
            },
            selectedInstitute: {
                //value: 4,
                label: 'Select',
            },
            validationError: "",
            tutors: [],
            selectDisable: false,
            tutorOpen: false,
            institutes: [],
            admins: [],
            tutorDisable: false,
            startDate: new Date(),
            files:[]
        }
        this.handleChange = this.handleChange.bind(this);

    }

    componentDidMount() {


        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let listBaseUrl = commonHostUrl.url + "api/user-list";
        let self = this;
        let lisPayload = {
            type: [2, 3]
        }

        fetch(listBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(lisPayload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    let userTutors = _.filter(result.data, {type: 3})
                    let userAdmins = _.filter(result.data, {type: 2})


                    let myTutors = [];
                    _.each(userTutors, function (tutor) {
                        let fullName = tutor.firstName + " " + tutor.lastName
                        myTutors.push({
                            value: tutor._id,
                            label: fullName
                        })
                    })

                    let myAdmins = [];
                    _.each(userAdmins, function (tutor) {
                        let fullName = tutor.firstName + " " + tutor.lastName
                        myAdmins.push({
                            value: tutor._id,
                            label: fullName
                        })
                    })


                    this.setState({
                        tutors: myTutors,
                        admins: myAdmins
                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            })

        let location_details = window.location.hash.split('/');
        let id = location_details[location_details.length - 1];
        //this.state.selectedType = 1;

        //let cachedUser = JSON.parse(localStorage.getItem('User'));
        //let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/user-detail/" + id;

        fetch(apiBaseUrl, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            // body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    let updateData = result.data;
                    let instituteData = updateData.instituteId ? updateData.instituteId : {
                        label: "Select",
                        value: null
                    }
                    let adminData = updateData.createdBy ? updateData.createdBy : {
                        label: "Select",
                        value: null
                    }

                    let adminFullName = adminData.firstName + " " + adminData.lastName

                    let tutorData
                    let fullName
                    let typeData = _.findWhere(this.state.types, {value: updateData.type});
                    if (typeData && (typeData.value === 3 || typeData.value === 2 || typeData.value === 1)) {
                        this.setState({tutorDisable: true})
                    }

                    updateData.tutorId ? tutorData = _.findWhere(this.state.tutors, {value: updateData.tutorId._id}) : {};

                    _.isEmpty(tutorData) ? tutorData = {
                        value: "123456",
                        lable: "Select"
                    } : fullName = updateData.tutorId.firstName + " " + updateData.tutorId.lastName

                    if (updateData.type == 3) {
                        this.setState({
                            selectDisable: true
                        })
                    }

                    let myBirthDate ;
                    if(updateData.birthdate){
                        myBirthDate = updateData.birthdate
                    }
                    else {
                        myBirthDate = new Date()
                    }


                    this.setState({
                        idEditable: true,
                        firstName: updateData.firstName,
                        lastName: updateData.lastName,
                        email: updateData.email,
                        mobile: updateData.mobile,
                        type: updateData.type,
                        class: updateData.class,
                        birthdate: myBirthDate,
                        startDate: myBirthDate,
                        selectedType: {
                            value: typeData.value,
                            label: typeData.label,
                        },
                        selectedTutor: {
                            value: tutorData.value,
                            label: fullName,
                        },
                        selectedInstitute: {
                            value: instituteData._id,
                            label: instituteData.title,
                        },
                        selectedAdmin: {
                            value: adminData._id,
                            label: adminFullName,
                        },
                    });
                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });

        let instBaseUrl = commonHostUrl.url + "api/institute-list";

        fetch(instBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            // body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    let updateData = result.data;

                    let myInstitutes = [];
                    _.each(result.data, function (insti) {
                        myInstitutes.push({
                            value: insti._id,
                            label: insti.title
                        })
                    })


                    this.setState({
                        institutes: myInstitutes,

                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });

        //this.props.Logout();
    }


    redirectFn() {
        this.setState({redirect: true})
    }

    handleChange(date) {
        this.setState({
            startDate: date
        });
    }

    setDataFn(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value})

        if (name == 'email') {
            let filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!filter.test(value)) {

                this.setState({emailValidation: true})

            } else {
                this.setState({emailValidation: false})
            }
        }
        if (name == 'mobile') {
            let filter = /^[0-9\b]+$/;
            if (!filter.test(value)) {

                this.setState({mobileValidation: true})

            } else {
                this.setState({mobileValidation: false})
            }
        }
        if (name == 'myImage') {
            this.setState({file: e.target.files[0]});
        }

    }

    handleCheck() {
        this.setState({isActive: !this.state.isActive});
    }


    selectFn(key_name, e) {
        if (key_name == 'clear') {
            this.setState({type: '1'});
        } else {
            this.state[key_name] = e;
            this.setState({[key_name]: e})
        }
    }

    // Callback~
    getFiles(files) {
        this.setState({files: files})
    }

    UpdateFn() {
        let this_pointer = this;
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;
        let editReqObj = this.props.location;
        let editReqId = editReqObj && editReqObj.state ? editReqObj.state.userId : null;

        let location_details = window.location.hash.split('/');
        let id = location_details[location_details.length - 1];

        let apiBaseUrl = commonHostUrl.url + "api/user-update/" + id

        let payload = {
            "firstName": this.state.firstName,
            "lastName": this.state.lastName,
            "email": this.state.email,
            "mobile": this.state.mobile,
            "isActive": this.state.isActive,
            "type": this.state.selectedType.value,
            "tutorId": this.state.selectedTutor.value,
            "instituteId": this.state.selectedInstitute.value,
            "isAdmin": this.state.selectedAdmin.value,
            "class": this.state.class,
            "birthdate": this.state.startDate
        }

        if (this.state.files && this.state.files.length > 0) {
            let imageName = this.state.files[0].name;
            let imageType = this.state.files[0].type;
            let imageSize = this.state.files[0].size;
            let imageBase64 = this.state.files[0].base64;
            payload.profile = imageBase64
        }

        fetch(apiBaseUrl, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {
                let mainData
                let firstName
                let lastName
                let fullName
                let myTutorId
                let updatedTutor
                if (result.data) {
                    mainData = result.data;
                    firstName = mainData.tutorId ? mainData.tutorId.firstName : ""
                    lastName = mainData.tutorId ? mainData.tutorId.lastName : ""
                    fullName = firstName + " " + lastName
                    myTutorId = mainData.tutorId ? mainData.tutorId._id : 0
                    updatedTutor = {
                        value: myTutorId,
                        label: fullName
                    }
                    result.data.selectedTutor = updatedTutor
                }

                if (result.code == 200) {
                    this.setState({
                        user: result.data,
                        redirect: true
                    });
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
                else {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
            });
        //
        //this.props.addnew(this.state.user)
    }

    render() {
        const {types} = this.state;
        const {tutors} = this.state;
        const {institutes} = this.state;
        const {admins} = this.state;
        let loginUser = JSON.parse(localStorage.getItem('User'));

        if (this.state.redirect) {
            return <Redirect to={{pathname: '/user'}}
                             render={<AdminPage/>}/>
        }
        return (
            <div className="animated fadeIn">
                <Row>

                    <Col xs="12" lg="12">
                        <form className="donationForm"

                              name={'form'}>
                            <Card>
                                <CardHeader>
                                    <Row>
                                        <Col xs="6" lg="6">
                                            <i className="fa fa-align-justify"></i> Edit User
                                        </Col>
                                        <Col xs="6" lg="6">
                                            <Button type="submit"
                                                    className="pull-right"
                                                    size="sm"
                                                    onClick={this.redirectFn.bind(this)}
                                                    color="primary">
                                                <i className="fa fa-chevron-left"></i> View User
                                            </Button>
                                        </Col>
                                    </Row>
                                </CardHeader>
                                <CardBody>
                                    {/*    <Row>
                                        <Col xs="12" sm="12" md={'11'} lg={'11'}>
                                        </Col>
                                        <Col xs="12" sm="12" md={'1'} lg={'1'}>
                                            <FormGroup check inline>
                                                <Input className="form-check-input"
                                                       type="checkbox"
                                                       onChange={this.handleCheck.bind(this)}
                                                       defaultChecked={this.state.isActive}/>
                                                <Label className="form-check-label"
                                                       check htmlFor="inline-checkbox1">Active</Label>
                                            </FormGroup>
                                        </Col>
                                    </Row>*/}
                                    <Row>
                                        <Col xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Type
                                                    <span
                                                        className={this.state.selectedType ? 'succes' : 'err'}>*</span>
                                                </Label>
                                                <Select style={{width: 200}}
                                                        value={this.state.selectedType}
                                                        onChange={this.selectFn.bind(this, 'selectedType')}
                                                        options={
                                                            types
                                                        }
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col style={{
                                            marginRight: -13,
                                            paddingLeft: 7
                                        }} xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">First Name

                                                    <span className={this.state.firstName ? 'succes' : 'err'}>*</span>
                                                </Label>
                                                <Input type="text"
                                                       name="firstName"
                                                       ref={'firstName'}
                                                       value={this.state.firstName}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                        <Col style={{
                                            marginRight: -54,
                                            paddingLeft: 23
                                        }} xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Last Name
                                                    <span className={this.state.lastName ? 'succes' : 'err'}>*</span>
                                                </Label>
                                                <Input type="text"
                                                       name="lastName"
                                                       value={this.state.lastName}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col style={{
                                            marginLeft: 1
                                        }} xs="12" sm="12" md={'3'} lg={'3'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Image</Label>
                                                <FileBase64
                                                    multiple={true}
                                                    onDone={this.getFiles.bind(this)}/>
                                            </FormGroup>
                                        </Col>
                                        <Col style={{
                                            paddingRight: 0,
                                            marginLeft: 68
                                        }} xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Email
                                                    <span className={this.state.email ? 'succes' : 'err'}>
                                                    {!this.state.emailValidation ? '*' : ''}
                                                </span>
                                                    <span className={this.state.emailValidation ? 'err' : 'succes'}>

                                                    {this.state.emailValidation ? 'Please enter proper email address' : ''}

                                                </span>
                                                </Label>
                                                <Input type="text"
                                                       name="email"
                                                       value={this.state.email}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                        <Col style={{
                                            paddingLeft: 22,
                                        }} xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup> 
                                                <Label htmlFor="name">Mobile
                                                {/*    <span className={this.state.mobile ? 'succes' : 'err'}>
                                                    {!this.state.mobileValidation ? '*' : ''}
                                                </span>
                                                    <span className={this.state.mobileValidation ? 'err' : 'succes'}>

                                                    {this.state.mobileValidation ? 'Please enter proper mobile number' : ''}

                                                </span>*/}
                                                </Label>
                                                <Input type="text"
                                                       name="mobile"
                                                       value={this.state.mobile}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                        {/* <Col xs="12" sm="12" md={'6'} lg={'6'}>
                                        <FormGroup>
                                            <Label htmlFor="name">Expiry Date
                                                <span className={this.state.expiry_date ? 'succes' : 'err'}>*</span>
                                            </Label>
                                            <Input type="date"
                                                   name="expiry_date"
                                                   value={this.state.expiry_date}
                                                   onChange={this.setDataFn.bind(this)}
                                                   required />
                                        </FormGroup>
                                    </Col>*/}
                                    </Row>


                                    <Row>
                                        {
                                            loginUser.type === 1 ?
                                                <Col xs="12" sm="12" md={'4'} lg={'4'}>
                                                    <FormGroup>
                                                        <Label htmlFor="name">Admin
                                                            <span
                                                                className={this.state.selectedAdmin ? 'success' : 'err'}>*</span>
                                                        </Label>
                                                        <Select style={{width: 200}}
                                                                value={this.state.selectedAdmin}
                                                                disabled={!this.state.selectDisable}
                                                                onChange={this.selectFn.bind(this, 'selectedAdmin')}
                                                                options={
                                                                    admins
                                                                }
                                                        />
                                                    </FormGroup>
                                                </Col> : ""

                                        }
                                        {
                                            !this.state.tutorDisable ?
                                                <Col xs="12" sm="12" md={'4'} lg={'4'}>
                                                    <FormGroup>
                                                        <Label htmlFor="name">Tutor
                                                            <span
                                                                className={this.state.selectedTutor ? 'success' : 'err'}>*</span>
                                                        </Label>
                                                        <Select style={{width: 200}}
                                                                value={this.state.selectedTutor}
                                                                disabled={!this.state.selectDisable}
                                                                onChange={this.selectFn.bind(this, 'selectedTutor')}
                                                                options={
                                                                    tutors
                                                                }
                                                        />
                                                    </FormGroup>
                                                </Col> : ""
                                        }

                                        <Col xs="12" sm="12" md={'4'} lg={'4'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Institute
                                                    <span
                                                        className={this.state.selectedInstitute ? 'success' : 'err'}>*</span>
                                                </Label>
                                                <Select style={{width: 200}}
                                                        value={this.state.selectedInstitute}
                                                        disabled={!this.state.selectedInstitute}
                                                        onChange={this.selectFn.bind(this, 'selectedInstitute')}
                                                        options={
                                                            institutes
                                                        }
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" sm="12" md={'6'} lg={'6'}>
                                            <FormGroup>
                                                <Label htmlFor="name">Class
                                                    <span className={this.state.class ? 'succes' : 'err'}>*</span>
                                                </Label>
                                                <Input type="class"
                                                       name="class"
                                                       value={this.state.class}
                                                       onChange={this.setDataFn.bind(this)}
                                                       required/>
                                            </FormGroup>
                                        </Col>
                                        <Col xs="12" sm="12" md={'6'} lg={'6'}>
                                            <FormGroup style={{
                                                borderRadius: 4,
                                                width: 220
                                            }}>
                                                <Label htmlFor="name">Birth Date
                                                    <span className={this.state.startDate ? 'succes' : 'err'}>*</span>
                                                </Label><br/>
                                                <DatePicker
                                                    selected={this.state.startDate}
                                                    onChange={this.handleChange}
                                                    showMonthDropdown
                                                    showYearDropdown
                                                    dateFormatCalendar="MMMM"
                                                    scrollableYearDropdown
                                                    yearDropdownItemNumber={20}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>


                                </CardBody>

                                <CardFooter>
                                    {/* <Button type="cancel"
                                            className={'pull-right'}
                                            size="sm"
                                            onClick={this.returnFn.bind(this)}
                                            color="danger"> Cancel</Button>*/}
                                    <Button type="button"
                                            onClick={this.UpdateFn.bind(this)}
                                            className={!this.state.firstName || !this.state.lastName || !this.state.email ||
                                            this.state.emailValidation ? 'btnDisbaled pull-right' : 'pull-right'}
                                            size="sm"
                                            style={{'marginRight': 10}}
                                        /* onSubmit={this.UpdateFn.bind(this)}*/
                                            color="primary">Update
                                    </Button>

                                </CardFooter>

                            </Card>
                        </form>
                    </Col>
                    <NotificationContainer/>
                </Row>


            </div>

        );
    }
}

export default AdminEditPage;
