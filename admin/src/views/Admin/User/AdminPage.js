import React, {Component} from 'react';
import {
    Badge,
    Card,
    CardBody,
    Button,
    Input,
    CardHeader,
    Col,
    Pagination,
    PaginationItem,
    PaginationLink,
    Row,
    Table,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    InputGroup,
    InputGroupAddon,
    FormGroup,
    Label
} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import AdminAddPage from './AdminAddPage';
import * as _ from 'underscore';
import {apiUrl} from "../../index";
//import {Accordion, AccordionItem} from '../../../cssLibrary/react-light-accordion';
//import Label from "reactstrap/src/Label";
//import FormGroup from "reactstrap/src/FormGroup";
import Select from 'react-select';
import * as moment from "moment";
import {Accordion, AccordionItem} from "../Task";
import {NotificationManager} from "react-notifications";
import ImageZoom from 'react-medium-image-zoom'
//import InputGroup from "reactstrap/src/InputGroup";
//import InputGroupAddon from "reactstrap/src/InputGroupAddon";
const commonHostUrl = require('../../../../src/constant');


class AdminPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            users: [],
            editState: false,
            id: null,
            tutors: [],
            deleteModel: false,
            confirm: false,
            deleteId: null,
            search: '',
            showModal: false,
            temImage: "https://i.kinja-img.com/gawker-media/image/upload/gd8ljenaeahpn0wslmlz.jpg",
            types: [
                {
                    value: 1,
                    label: 'Super Admin',
                    display: 'Select Type'
                },
                {
                    value: 2,
                    label: 'Admin',
                    display: 'Select Type'
                },
                {
                    value: 3,
                    label: 'Tutor',
                    display: 'Select Type'
                },
                {
                    value: 4,
                    label: 'Student',
                    display: 'Select Type'
                }
            ],
            selectedType: {
                value: 0,
                label: 'Select Filter',
                display: 'Select Type'
            },
            sortBy: 'desc',
            sorting: {},
            sortByVal: {},
            sort: {},
            currentFilter: null,
            previousFilter: null,
            sortOrder: false,
            loginToken: '',
            apiUrl: '',
            userDetails: [],
            detailPage: false,
            userSubjectTasks: [],
            userSubject: [],
            detailCollapse: false,
            isSubjectDisplay: false,
            isTaskDisplay: false,
            pagesCount: 0,
            pageSize: 15,
            currentPage: 0


        }

        this.addnew = this.newData.bind(this);
    }

    redirectFn() {
        this.setState({redirect: true})
    }

    Capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    userDetailFn(key, id) {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        //let location_details = window.location.hash.split('/');
        //let id = location_details[location_details.length - 1];
        //this.state.selectedType = 1;

        //let cachedUser = JSON.parse(localStorage.getItem('User'));
        //let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/user-detail/" + id;

        fetch(apiBaseUrl, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            // body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    //date formatter
                    _.map(result.data.subjects, function (res) {
                        if (res.createdDate) {
                            res.createdDate = moment(res.createdDate).format("MMM-DD-YYYY")
                        }
                        if (res.updatedDate) {
                            res.updatedDate = moment(res.updatedDate).format("MMM-DD-YYYY")
                        }
                    })
                    _.map(result.data.tasks, function (res) {
                        if (res.createdDate) {
                            res.createdDate = moment(res.createdDate).format("MMM-DD-YYYY")
                        }
                        if (res.updatedDate) {
                            res.updatedDate = moment(res.updatedDate).format("MMM-DD-YYYY")
                        }
                    })

                    if (result.data && result.data.subjects && result.data.subjects.length > 0) {
                        if (key === "subject") {
                            this.setState({
                                userSubject: result.data.subjects,
                                isSubjectDisplay: true,
                                isTaskDisplay: false
                            })
                        }
                    }
                    /*  else{
                          this.setState({isSubjectDisplay: false})
                      }*/
                    if (result.data && result.data.tasks && result.data.tasks.length > 0) {
                        if (key === "task") {
                            this.setState({
                                userSubjectTasks: result.data.tasks,
                                isTaskDisplay: true,
                                isSubjectDisplay: false
                            })
                        }

                    }
                    /* else {
                         this.setState({isTaskDisplay: false})
                     }*/
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });


        /*  this.setState({addEdit: true})
          this.setState({id: id})*/
    }

    editRedirectFn(key, id, tutors) {
        this.setState({editState: true})
        this.setState({id: id})
    }

    openModal(deleteId) {
        this.setState({deleteId: deleteId})
        this.setState({deleteModel: true})
    }

    deleteFn() {

        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/user-delete/" + this.state.deleteId;


        let payload = {}

        fetch(apiBaseUrl, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {
                if (result.code == 200) {

                    let cloneOfUsers = _.clone(this.state.users)
                    let deleteIndex = _.findIndex(this.state.users, {_id: this.state.deleteId});
                    if (deleteIndex >= 0) {
                        cloneOfUsers[deleteIndex].deleteModel = false;
                        cloneOfUsers.splice(deleteIndex, 1);
                    }

                    this.setState({
                        users: cloneOfUsers,
                        deleteModel: false
                    });

                    this.componentWillMount()
                    /*  this.props.history.push({
                          pathname: "/user"
                      });

                      return <Redirect to={{pathname: '/user'}}
                                       render={<AdminPage/>}/>
  */
                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });
        //
    }

    dataRender(payload, loginToken, apiBaseUrl) {

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    let allUsers = result.data;
                    _.each(allUsers, function (user) {
                        if (user.tutorId && user.tutorId._id) {
                            let firstName = user.tutorId.firstName ? user.tutorId.firstName : ""
                            let lastName = user.tutorId.lastName ? user.tutorId.lastName : "";
                            let fullName = firstName + " " + lastName;
                            user.tutorId.fullName = fullName
                        }
                    })


                    //date formatter
                    _.map(allUsers, function (res) {
                        if (res.birthdate) {
                            res.birthdate = moment(res.birthdate).format("MMM-DD-YYYY")
                        }
                    })

                    let myPagesCount = Math.ceil(result.data.length / this.state.pageSize);
                    this.setState({
                        users: result.data,
                        pagesCount: myPagesCount
                    });

                    //Getting tutors list
                    let myTutors = _.filter(result.data, {type: 3});
                    this.setState({
                        tutors: myTutors
                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            })
    }

    //getting login user data
    componentWillMount() {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/user-list";
        let self = this;
        this.setState({loginToken: loginToken, apiBaseUrl: apiBaseUrl})


        let payload = {
            //"email": this.state.email,
            //"password": this.state.password
        }

        /* if (this.state.search && this.state.search !== null) {
             let searchParams = {
                 keys: ['name'],
                 keyword: this.state.search
             }
             payload.search = searchParams
         }*/

        if (this.state.selectedType.value) {
            payload.type = [this.state.selectedType.value]
        }

        /* if (this.state.sort) {
             payload.sort = this.state.sort
         }*/

        //send it to api for call
        this.dataRender(payload, loginToken, apiBaseUrl)

    }

    //function for getting add object data to push into list
    newData(newdata) {
        let oldData = this.state.users
        oldData.push(newdata)

        this.setState({users: oldData});
    }

    //search for data
    searchFn(clear) {
        if (clear == 'clear') {
            this.setState({search: ''});
            this.dataRender({}, this.state.loginToken, this.state.apiBaseUrl)

        }
        else if (this.state.search && (!_.isNull(this.state.search)
            || !_.isUndefined(this.state.search) || this.state.search !== '')) {
            this.setState({search: this.state.search});
            this.componentWillMount()
        }
    }

    //select filter of type
    selectFn(key_name, e) {
        if (key_name == 'clear') {
            this.setState({selectedType: 0});
            this.dataRender({}, this.state.loginToken, this.state.apiBaseUrl)
        } else {
            this.state[key_name] = e;
            this.setState({[key_name]: e})
            this.componentWillMount()
        }
    }

    //sorting of data
    setSortFilter(key) {
        let prev = this.state.currentFilter;
        let order = this.state.sortOrder;
        let elements = this.state.users.slice();
        let sortingFunction = (a, b) => {
            let _a = a[key];
            let _b = b[key];
            if (typeof _a === 'string' || typeof _b === 'string') {
                _a = _a.toLowerCase();
                _b = _b.toLowerCase();
            }

            if (_a <= _b) return -1;
            return 1;
        };

        if (prev !== key) {
            order = true;
            elements = elements.sort(sortingFunction);
        } else {
            order = !order;
            elements.reverse();
        }

        this.setState({
            previousFilter: prev,
            currentFilter: key,
            sortOrder: order,
            users: elements
        });
    }

    subjectTaskFn(key, id) {
        let userData = _.first(this.state.userDetails);
        let userSubjectTask = _.filter(userData.tasks, {studentId: id});
        this.setState({userSubjectTasks: userSubjectTask, detailCollapse: true})
    }

    handleSearchChange = (e) => {
        this.setState({search: e.target.value});
    }


    render() {
        let {users} = this.state;
        const {types} = this.state;
        const {userDetails} = this.state;
        const {userSubjectTasks} = this.state;
        const {userSubject} = this.state;
        const {pageSize} = this.state;
        const {pagesCount} = this.state;
        const {currentPage} = this.state;

        //redirecting to Add page
        if (this.state.redirect) {
            this.props.history.push({
                pathname: "/user/add",
            });
        }

        //redirecting to edit page
        if (this.state.editState) {
            this.props.history.push({
                pathname: "/user/edit/" + this.state.id,
                state: {
                    userId: this.state.id,
                }
            });
        }
        //on type search changes
        let searchString = this.state.search.trim().toLowerCase();
        if (searchString.length > 0) {
            users = users.filter(function (i) {
                return i.name.toLowerCase().match(searchString);
            });
        }


        return (
            <div className="animated fadeIn">
                <Row>

                    <Col xs="12" lg="12">
                        <Card>
                            <CardHeader>
                                <Row>
                                    <Col xs="2" lg="2">
                                        <i className="fa fa-align-justify"></i> Users
                                    </Col>
                                    <Col xs="3" lg="3">
                                        <FormGroup>
                                            <Select style={{width: 200}}
                                                    value={this.state.selectedType}
                                                    onChange={this.selectFn.bind(this, 'selectedType')}
                                                    options={
                                                        types
                                                    }

                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col xs="5" lg="5">
                                        {/* <form
                                            name={'form'}
                                            onSubmit={this.searchFn.bind(this)}>
                                            <InputGroup>

                                                <Input style={{height: 38}} type="text"
                                                       value={this.state.search}
                                                       onChange={(e) => {
                                                           this.setState({search: e.target.value})
                                                       }}
                                                       placeholder="Search"/>


                                                <InputGroupAddon addonType="prepend">
                                                    <Button type="button"
                                                            onClick={this.searchFn.bind(this)}
                                                            color="primary"
                                                            size="sm">
                                                        <i className="fa fa-search"></i>
                                                        Search</Button>


                                                    <Button
                                                        type="button"
                                                        color="danger"
                                                        size="sm"
                                                        onClick={this.searchFn.bind(this, 'clear')}>Clear</Button>


                                                </InputGroupAddon>
                                            </InputGroup>
                                        </form>*/}
                                        <form
                                            name={'form'}>
                                            <InputGroup>

                                                <Input style={{height: 38}} type="text"
                                                       value={this.state.search}
                                                       onChange={this.handleSearchChange} placeholder="Type here..."/>
                                            </InputGroup>
                                        </form>
                                    </Col>
                                    <Col xs="2" lg="2">
                                        <Button type="submit"
                                                className="pull-right"
                                                onClick={this.redirectFn.bind(this)}
                                                size="sm"
                                                color="primary">
                                            <i className="fa fa-plus"></i> New User
                                        </Button>
                                    </Col>
                                    {
                                        this.state.selectedType.value > 0 ?
                                            <Col xs="2" lg="2">
                                                <Button type="submit"
                                                        size="sm"
                                                        className={'btn-pill btnPdng'}
                                                        onClick={this.selectFn.bind(this, 'clear')}
                                                        color="primary">
                                                    Clear all <i className="fa fa-close"></i>
                                                </Button>
                                            </Col>
                                            : ''}

                                </Row>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col xs="12" lg="12">
                                        <Table responsive striped>
                                            <thead>
                                            <tr>
                                                <th style={{width: '4%'}}>#</th>
                                                <th style={{width: '10%'}}>
                                                    <span className="data-short-left">
                                                        FirstName
                                                    </span>
                                                    {
                                                        <span className="data-short-left">
                                                        {
                                                            this.state.sortBy == 'asc' ?
                                                                <i className="fa fa-caret-up"
                                                                   onClick={this.setSortFilter.bind(this, 'firstName')}>

                                                                </i>
                                                                : <i className="fa fa-caret-down"
                                                                     onClick={this.setSortFilter.bind(this, 'firstName')}></i>
                                                        }


                                                    </span>
                                                    }

                                                </th>
                                                <th style={{width: '10%'}}>LastName</th>
                                                <th style={{width: '6%'}}>Type</th>
                                                <th style={{width: '5%'}}>Email</th>
                                                <th style={{width: '10%'}}>Mobile</th>
                                                <th style={{width: '12%'}}>Created By</th>
                                                <th style={{width: '40%'}}>Action</th>

                                            </tr>
                                            </thead>

                                            {
                                                users.map((user, index) => (
                                                        <tbody key={index}>
                                                        <tr>
                                                            <td style={{width: '4%'}}>{index + 1}&nbsp;&nbsp;
                                                                <span>

                                                                     <ImageZoom
                                                                         image={{
                                                                             src: user.profile,
                                                                             className: 'image--cover',
                                                                             style: {width: '3em'}
                                                                         }}
                                                                     />
                                                                    {/*{
                                                                     user.profile ?

                                                                        {



                                                                            /* <img
                                                                             src={user.profile}
                                                                             className="image--cover"></img>*!/
                                                                             :
                                                                         <img
                                                                             src="https://i.kinja-img.com/gawker-media/image/upload/gd8ljenaeahpn0wslmlz.jpg"
                                                                             className="image--cover"></img>
                                                                 }*/}
                                                            </span>
                                                            </td>
                                                            <td style={{width: '10%'}}>
                                                                <a data-toggle="tooltip" data-placement="top"
                                                                   style={{color: user.isDeleted ? '#FF0000' : '#20a8d8'}}>{this.Capitalize(user.firstName)}
                                                                </a>
                                                                {/*<span
                                                                    style={{color: user.isDeleted ? '#FF0000' : '#23282c'}}>{user.firstName} </span>*/}
                                                            </td>
                                                            <td style={{width: '10%'}}>{this.Capitalize(user.lastName)}</td>
                                                            <td style={{width: '6%'}}>
                                                                <span>
                                                                    {user.type == 1 ?
                                                                        <Badge color="danger">Super Admin</Badge> :
                                                                        user.type == 2 ?
                                                                            <Badge color="info">Admin</Badge> :
                                                                            user.type == 3 ?
                                                                                <Badge color="success">Tutor</Badge> :
                                                                                user.type == 4 ? <Badge
                                                                                        color="primary">Student</Badge> :
                                                                                    <Badge
                                                                                        color="primary">Student</Badge>
                                                                    }
                                                                </span>&nbsp;&nbsp;
                                                                <span>
                                                                    {
                                                                        user.type == 4 && user.tutorId ?
                                                                            <Badge data-toggle="tooltip"
                                                                                   data-placement="top"
                                                                                   title={user.tutorId.fullName}
                                                                                   color="warning">T</Badge> : null
                                                                    }

                                                                </span><br/>
                                                                <span>
                                                                    {
                                                                        user.followUpId ?
                                                                            <span data-toggle="tooltip"
                                                                                  data-placement="top"
                                                                                  title="Follow Up">
                                                                                {user.followUpId.firstName} {user.followUpId.lastName}
                                                                            </span> : null

                                                                    }

                                                                </span>
                                                            </td>
                                                            <td style={{width: '5%'}}>{user.email}
                                                                <br/>
                                                                {
                                                                    user.birthdate ?
                                                                        <span data-toggle="tooltip"
                                                                              data-placement="top"
                                                                              title="Birth Date">{user.birthdate}</span> : ""
                                                                }
                                                            </td>
                                                            <td style={{width: '10%'}}>{user.mobile}</td>
                                                            <td style={{width: '12%'}}>
                                                                {user.createdBy.firstName} {user.createdBy.lastName}<br/>
                                                                <span>
                                                                    {user.createdBy.type == 1 ?
                                                                        <Badge color="danger">Super Admin</Badge> :
                                                                        user.createdBy.type == 2 ?
                                                                            <Badge color="info">Admin</Badge> :
                                                                            user.createdBy.type == 3 ?
                                                                                <Badge color="success">Tutor</Badge> :
                                                                                user.createdBy.type == 4 ? <Badge
                                                                                        color="primary">Student</Badge> :
                                                                                    <Badge
                                                                                        color="primary">Student</Badge>}
                                                                </span>
                                                            </td>
                                                            <td style={{width: '40%'}}>
                                                                <span>
                                                                    {
                                                                        user.type == 4 && user.subjects.length > 0 ?
                                                                            <span>
                                                                            <span
                                                                                onClick={() => this.userDetailFn('subject', user._id)}
                                                                                className="badge badge-pill badge-info"
                                                                                style={{cursor: "pointer"}}>
                                                                            Subject
                                                                            </span> &nbsp;&nbsp;
                                                                                <span
                                                                                    onClick={() => this.userDetailFn('task', user._id)}
                                                                                    className="badge badge-pill badge-success"
                                                                                    style={{cursor: "pointer"}}>
                                                                                Task</span>
                                                                        </span> : null

                                                                    }
                                                                </span><br/>&nbsp;&nbsp;
                                                                <span style={{marginLeft: 9}}>
                                                                    <i style={{
                                                                        color: '#ff0000'
                                                                    }}
                                                                       onClick={() => this.openModal(user._id)}
                                                                       data-title="Delete"
                                                                       data-toggle="modal" data-target="#deleteModel"
                                                                       className="fa fa-trash ">
                                                                    </i>&nbsp;&nbsp;

                                                                    <i style={{
                                                                        color: '#20a8d8',
                                                                    }}
                                                                       onClick={() => this.editRedirectFn('edit', user._id, this.state.tutors)}
                                                                       data-title="Delete"
                                                                       data-toggle="modal" data-target="#edit"
                                                                       className="fa fa-edit">
                                                                        </i>
                                                                    </span>

                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    )
                                                )}
                                        </Table>

                                    </Col>
                                </Row>

                                <Modal isOpen={this.state.deleteModel}
                                       className={'modal-danger '}>
                                    <ModalHeader>Confirm </ModalHeader>
                                    <ModalBody>
                                        Are you sure, you want to delete this entry ?
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary"
                                                id="Popover1"
                                                onClick={() => {
                                                    this.setState({deleteModel: false})
                                                }}>No
                                        </Button>
                                        <Button color="success"
                                                id="Popover1"
                                                onClick={() => this.deleteFn(this)}>Yes
                                        </Button>
                                    </ModalFooter>
                                </Modal>

                                <Modal isOpen={this.state.isSubjectDisplay === true}
                                       className={'modal-info modal-lg'}>
                                    <ModalHeader>
                                        User Subject Detail
                                    </ModalHeader>
                                    <ModalBody>
                                        <Row>
                                            <Col xs="12" lg="12">

                                                <div className="card-body" style={{
                                                    height: 542,
                                                    overflow: 'auto',
                                                }}>
                                                    <Table responsive striped>
                                                        <thead>
                                                        <tr>
                                                            <th style={{width: '5%'}}>#</th>
                                                            <th>
                                                                        <span className="data-short-left">
                                                                            Title
                                                                        </span>
                                                            </th>
                                                            <th style={{width: '30%'}}>Block</th>
                                                            <th style={{width: '15%'}}>Created By</th>
                                                            <th style={{width: '18%'}}>Date</th>
                                                            {/*<th>Action</th>*/}

                                                        </tr>
                                                        </thead>

                                                        {
                                                            userSubject.map((subjectObj, index) => (
                                                                    <tbody key={index}>
                                                                    <tr>
                                                                        <td style={{width: '5%'}}>{index + 1}</td>
                                                                        <td>
                                                                           <span data-toggle="tooltip"
                                                                                 data-placement="top"
                                                                                 title="Subject Tasks"
                                                                                 style={{color: subjectObj.isDeleted ? '#FF0000' : '#20a8d8'}}>{subjectObj.title} </span><br/>
                                                                            <b>What happened so far? :</b>
                                                                            <span>
                                                                                 {subjectObj.title1}
                                                                            </span><br/>
                                                                            <b>What is happening these days far ? :</b>
                                                                            <span>
                                                                                {subjectObj.title2}
                                                                            </span><br/>
                                                                            <b>Goals :What do i want to achieve ? :</b>
                                                                            <span>
                                                                                {subjectObj.title3}
                                                                            </span><br/>
                                                                            <b>Measures :How will i know i succeed ? :</b>
                                                                            <span>
                                                                                {subjectObj.title4}
                                                                            </span><br/>
                                                                            <b>My action plan ? :</b>
                                                                            <span>
                                                                                {subjectObj.title5}
                                                                            </span>
                                                                        </td>
                                                                        <td style={{width: '20%'}}>{subjectObj.block.title}</td>
                                                                        <td style={{width: '15%'}}>{subjectObj.createdBy.firstName} {subjectObj.createdBy.lastName}</td>
                                                                        <td style={{width: '18%'}}>
                                                                            <span data-toggle="tooltip"
                                                                                  data-placement="top"
                                                                                  title="Created Date">
                                                                                CD:{subjectObj.createdDate}
                                                                            </span>
                                                                            {
                                                                                subjectObj.updatedDate ?
                                                                                    <span data-toggle="tooltip"
                                                                                          data-placement="top"
                                                                                          title="Updated Date">
                                                                                UD:{subjectObj.updatedDate}
                                                                            </span> : ""
                                                                            }

                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                )
                                                            )}
                                                    </Table>
                                                </div>

                                            </Col>
                                        </Row>


                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary"
                                                onClick={(e) => {
                                                    this.setState({
                                                        isSubjectDisplay: false,
                                                    })
                                                }}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                                <Modal isOpen={this.state.isTaskDisplay === true}
                                       className={'modal-info modal-lg'}>
                                    <ModalHeader>
                                        User Task Detail
                                    </ModalHeader>
                                    <ModalBody>
                                        <Row>
                                            <Col xs="12" lg="12">

                                                <div className="card-body" style={{
                                                    height: 542,
                                                    overflow: 'auto',
                                                }}>
                                                    <Table responsive striped>
                                                        <thead>
                                                        <tr>
                                                            <th style={{width: '5%'}}>#</th>
                                                            <th>
                                                                        <span className="data-short-left">
                                                                            Title
                                                                        </span>
                                                            </th>
                                                            <th>Subject</th>
                                                            <th style={{width: '15%'}}>Created By</th>
                                                            <th style={{width: '18%'}}>Date</th>
                                                            {/* <th style={{width: '16%'}}>Updated At</th>*/}
                                                            {/*<th>Action</th>*/}

                                                        </tr>
                                                        </thead>

                                                        {
                                                            userSubjectTasks.map((subjectObj, index) => (
                                                                    <tbody key={index}>
                                                                    <tr>
                                                                        <td style={{width: '5%'}}>{index + 1}</td>
                                                                        <td>
                                                                            <span
                                                                                style={{color: subjectObj.isDeleted ? '#FF0000' : '#20a8d8'}}>{subjectObj.title} </span><br/>
                                                                            <b>What do i do? :</b>
                                                                            <span>
                                                                                {subjectObj.questionOne}
                                                                            </span>
                                                                            <br/>
                                                                            <b>What am i planning to do ? :</b>
                                                                            <span>
                                                                                {subjectObj.questionTwo}
                                                                            </span>
                                                                            <br/>
                                                                            <b>My next milestone ? :</b>
                                                                            <span>
                                                                               {subjectObj.questionThree}
                                                                            </span>
                                                                            <br/>
                                                                            <b>Who do i follow up with ? :</b>
                                                                            <span>
                                                                               {subjectObj.questionThree}
                                                                             </span>
                                                                        </td>
                                                                        <td>
                                                                            <span
                                                                                style={{color: subjectObj.subjectId.isDeleted ? '#FF0000' : '#20a8d8'}}>{subjectObj.subjectId.title} </span><br/>
                                                                            <b>What happened so far? :</b>
                                                                            <span>
                                                                                {subjectObj.subjectId.title1}
                                                                            </span>
                                                                            <br/>
                                                                            <b>What is happening these days far ? :</b>
                                                                            <span>
                                                                                {subjectObj.subjectId.title2}
                                                                            </span>
                                                                            <br/>
                                                                            <b>Goals :What do i want to achieve ? :</b>
                                                                            <span>
                                                                                {subjectObj.subjectId.title3}
                                                                            </span>
                                                                            <br/>
                                                                            <b>Measures :How will i know i succeed ? :</b>
                                                                            <span>
                                                                                {subjectObj.subjectId.title4}
                                                                            </span>
                                                                            <br/>
                                                                            <b>My action plan ? :</b>
                                                                            <span>
                                                                                {subjectObj.subjectId.title5}
                                                                            </span>
                                                                        </td>
                                                                        <td style={{width: '15%'}}>{subjectObj.createdBy.firstName} {subjectObj.createdBy.lastName}</td>
                                                                        <td style={{width: '18%'}}>
                                                                            <span data-toggle="tooltip"
                                                                                  data-placement="top"
                                                                                  title="Created Date">
                                                                                CD:{subjectObj.createdDate}
                                                                            </span>
                                                                            {
                                                                                subjectObj.updatedDate ?
                                                                                    <span data-toggle="tooltip"
                                                                                          data-placement="top"
                                                                                          title="Updated Date">
                                                                                UD:{subjectObj.updatedDate}
                                                                            </span> : ""
                                                                            }

                                                                        </td>
                                                                        {/* <td style={{width: '16%'}}>{subjectObj.updatedDate}</td>*/}
                                                                    </tr>
                                                                    </tbody>
                                                                )
                                                            )}
                                                    </Table>
                                                </div>

                                            </Col>
                                        </Row>


                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary"
                                                onClick={(e) => {
                                                    this.setState({
                                                        isTaskDisplay: false,
                                                    })
                                                }}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                                {/*<Pagination>
                                    <PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
                                    <PaginationItem active>
                                        <PaginationLink tag="button">1</PaginationLink>
                                    </PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
                                </Pagination>*/}
                                {/* <div className="pagination-wrapper">
                                    <Pagination aria-label="Page navigation example">

                                        <PaginationItem disabled={currentPage <= 0}>

                                            <PaginationLink
                                                onClick={e => this.handleClick(e, currentPage - 1)}
                                                previous
                                                href="#"
                                            />

                                        </PaginationItem>

                                        {[this.pagesCount].map((page, i) =>
                                            <PaginationItem active={i === currentPage} key={i}>
                                                <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                                                    {i + 1}
                                                </PaginationLink>
                                            </PaginationItem>
                                        )}

                                        <PaginationItem disabled={currentPage >= pagesCount - 1}>

                                            <PaginationLink
                                                onClick={e => this.handleClick(e, currentPage + 1)}
                                                next
                                                href="#"
                                            />

                                        </PaginationItem>

                                    </Pagination>
                                </div>*/}
                            </CardBody>
                        </Card>
                    </Col>
                </Row>


            </div>

        );
    }
}

export default AdminPage;
