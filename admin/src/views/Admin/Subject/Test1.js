<div className="accordion" id="accordion">
    <div className="card">
        <div className="card-header" id="headingOne">
            <h5 className="mb-0">
                <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                        aria-expanded="true" aria-controls="collapseOne">
                    Collapsible Group Item #1
                </button>
            </h5>
        </div>

        <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div className="card-body">
                Sed feugiat egestas nisl sed faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id
                augue. Vivamus porta elit id nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque
                est, vel lacinia ipsum efficitur sed. Morbi porttitor porta commodo. Sed feugiat egestas nisl sed
                faucibus. Fusce nisi metus, accumsan eget ullamcorper eget, iaculis id augue. Vivamus porta elit id
                nulla varius, in sodales massa bibendum. Nullam condimentum pellentesque est, vel lacinia ipsum
                efficitur sed. Morbi porttitor porta commodo.
            </div>
        </div>
    </div>
</div>