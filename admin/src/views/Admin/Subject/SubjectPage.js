import React, {Component} from 'react';
import {
    Badge,
    Card,
    CardBody,
    Button,
    Input,
    CardHeader,
    Col,
    Pagination,
    PaginationItem,
    PaginationLink,
    Row,
    Table,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    InputGroup,
    InputGroupAddon,
    FormGroup,
    Label,
    InputGroupText
} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import * as _ from 'underscore';
import * as moment from 'moment';
import {apiUrl} from "../../index";
//import Label from "reactstrap/src/Label";
//import FormGroup from "reactstrap/src/FormGroup";
import Select from 'react-select';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import FileBase64 from 'react-file-base64';
//import InputGroup from "reactstrap/src/InputGroup";
//import InputGroupAddon from "reactstrap/src/InputGroupAddon";
const commonHostUrl = require('../../../../src/constant');


class SubjectPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            subjects: [],
            blocks: [],
            editState: false,
            id: null,
            tutors: [],
            deleteModel: false,
            confirm: false,
            deleteId: null,
            search: '',
            showModal: false,
            sortBy: 'desc',
            sorting: {},
            sortByVal: {},
            sort: {},
            currentFilter: null,
            previousFilter: null,
            sortOrder: false,
            loginToken: '',
            apiUrl: '',
            addEdit: false,
            editForm: false,
            user: {},
            errorTitle: false,
            subjectPage: false,
            subjectTasks: [],
            subsExist: false,
            selectedType: null,
            files: []
        }

        this.addnew = this.newData.bind(this);
    }

    redirectFn() {
        this.setState({redirect: true})
    }

    editRedirectFn(key, id) {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/subject-detail/" + id;

        fetch(apiBaseUrl, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            // body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    let updateData = result.data;

                    let detailSelected = {};
                    if (updateData.block && updateData.block._id) {
                        detailSelected.value = updateData.block._id,
                            detailSelected.label = updateData.block.title
                    }


                    this.setState({
                        title: updateData.title,
                        selectedType: detailSelected,
                        user: updateData,
                        addEdit: true,
                        id: id,
                    });


                    let payload = {}
                    // for getting block data
                    let cachedUser = JSON.parse(localStorage.getItem('User'));
                    let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

                    let apiBaseUrl = commonHostUrl.url + "api/block-list";
                    let self = this;
                    this.setState({loginToken: loginToken, apiBaseUrl: apiBaseUrl})


                    this.blockEditData(payload, loginToken, apiBaseUrl)
                }
                else if (result.code == 403) {
                    //alert("Failed to create User")
                    NotificationManager.error(result.message);
                }
                else {
                    // alert("Username does not exist");
                    NotificationManager.error(result.message);
                }
            });
    }

    subjectTaskFn(key, id) {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/subject-task-list";
        let payload = {
            subjectId: id
        }

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    //date formatter
                    _.map(result.data, function (res) {
                        if (res.createdDate) {
                            res.createdDate = moment(res.createdDate).format("MMM-DD-YYYY")
                        }
                        if (res.updatedDate) {
                            res.updatedDate = moment(res.updatedDate).format("MMM-DD-YYYY")
                        }
                        if (res.questionThree) {
                            res.questionThree = moment(res.questionThree).format("MMM-DD-YYYY")
                        }
                    })

                    let myBlockSubjects
                    if (result.data && result.data.length) {
                        myBlockSubjects = result.data;
                        this.setState({subsExist: true})
                    }
                    else {
                        myBlockSubjects = [];
                        this.setState({subsExist: false})
                    }


                    this.setState({
                        subjectTasks: myBlockSubjects,
                        subjectPage: true,
                        id: id
                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });


        /*  this.setState({addEdit: true})
          this.setState({id: id})*/
    }

    openModal(deleteId) {
        this.setState({deleteId: deleteId})
        this.setState({deleteModel: true})
    }

    // Callback~
    getFiles(files) {
        this.setState({files: files})
    }


    redirectPassFn() {
        //open Add/Edit Form
        this.setState({addEdit: true});

        // for getting block data
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/block-list";
        let self = this;
        this.setState({loginToken: loginToken, apiBaseUrl: apiBaseUrl})


        let payload = {
            //"email": this.state.email,
            //"password": this.state.password
        }

        if (this.state.search && this.state.search !== null) {
            let searchParams = {
                keys: ['title'],
                keyword: this.state.search
            }
            payload.search = searchParams
        }

        //send it to api for call
        this.blockData(payload, loginToken, apiBaseUrl)


    }

    commonRedirect(id) {
        if (id && id !== null) {
            this.updateTitleFn()
        }
        else {
            this.addTitleFn()
        }
    }

    deleteFn() {

        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/subject-delete/" + this.state.deleteId;


        let payload = {}

        fetch(apiBaseUrl, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {
                if (result.code == 200) {

                    let cloneOfUsers = _.clone(this.state.subjects)
                    let deleteIndex = _.findIndex(this.state.subjects, {_id: this.state.deleteId});
                    if (deleteIndex >= 0) {
                        cloneOfUsers[deleteIndex].deleteModel = false;
                        cloneOfUsers.splice(deleteIndex, 1);
                    }

                    this.setState({
                        subjects: cloneOfUsers,
                        deleteModel: false
                    });

                    this.componentWillMount()
                    /*  this.props.history.push({
                          pathname: "/user"
                      });

                      return <Redirect to={{pathname: '/user'}}
                                       render={<SubjectPage/>}/>
  */
                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });
        //
    }

    checkErrorFn() {

        if (this.state.title == null || this.state.title == "") {
            this.setState({errorTitle: true})
        }

    }

    dataManupalation(payload, loginToken, apiBaseUrl) {

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    //date formatter
                    _.map(result.data, function (res) {
                        if (res.createdDate) {
                            res.createdDate = moment(res.createdDate).format("MMM-DD-YYYY")
                        }
                        if (res.updatedDate) {
                            res.updatedDate = moment(res.updatedDate).format("MMM-DD-YYYY")
                        }
                    })

                    this.setState({
                        subjects: result.data
                    });

                    //Getting tutors list
                    let myTutors = _.filter(result.data, {type: 3});
                    this.setState({
                        tutors: myTutors
                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error("Data not found");
                }
                else {
                    NotificationManager.error("Data not found");
                }
            })
    }

    blockData(payload, loginToken, apiBaseUrl) {

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    //date formatter
                    _.map(result.data, function (res) {
                        if (res.createdDate) {
                            res.createdDate = moment(res.createdDate).format("MMM-DD-YYYY")
                        }
                        if (res.updatedDate) {
                            res.updatedDate = moment(res.updatedDate).format("MMM-DD-YYYY")
                        }
                    })
                    let mySelected

                    if (payload.key === 'add') {
                        let byDefaultSelected = _.first(result.data);
                        mySelected = {
                            value: byDefaultSelected._id,
                            label: byDefaultSelected.title
                        }
                    }


                    let myBlocks = [];
                    _.each(result.data, function (block) {
                        myBlocks.push({
                            value: block._id,
                            label: block.title
                        })
                    })


                    this.setState({
                        blocks: myBlocks,
                        selectedType: mySelected
                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error("Data not found");
                }
                else {
                    NotificationManager.error("Data not found");
                }
            })
    }

    blockEditData(payload, loginToken, apiBaseUrl) {

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    //date formatter
                    _.map(result.data, function (res) {
                        if (res.createdDate) {
                            res.createdDate = moment(res.createdDate).format("MMM-DD-YYYY")
                        }
                        if (res.updatedDate) {
                            res.updatedDate = moment(res.updatedDate).format("MMM-DD-YYYY")
                        }
                    })

                    let myBlocks = [];
                    _.each(result.data, function (block) {
                        myBlocks.push({
                            value: block._id,
                            label: block.title
                        })
                    })


                    this.setState({
                        blocks: myBlocks,
                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error("Data not found");
                }
                else {
                    NotificationManager.error("Data not found");
                }
            })
    }

    //getting login user data
    componentWillMount() {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/subject-list";
        let self = this;
        this.setState({loginToken: loginToken, apiBaseUrl: apiBaseUrl})


        let payload = {
            //"email": this.state.email,
            //"password": this.state.password
        }

        if (this.state.search && this.state.search !== null) {
            let searchParams = {
                keys: ['title'],
                keyword: this.state.search
            }
            payload.search = searchParams
        }

        //send it to api for call
        this.dataManupalation(payload, loginToken, apiBaseUrl)

    }

    //function for getting add object data to push into list
    newData(newdata) {
        let oldData = this.state.subjects
        oldData.push(newdata)

        this.setState({subjects: oldData});
    }

    //search for data
    searchFn(clear) {
        if (clear == 'clear') {
            this.setState({search: ''});
            this.dataManupalation({}, this.state.loginToken, this.state.apiBaseUrl)

        }
        else if (this.state.search && (!_.isNull(this.state.search)
            || !_.isUndefined(this.state.search) || this.state.search !== '')) {
            this.setState({search: this.state.search});
            this.componentWillMount()
        }
    }

    //select filter of type
    selectFn(key_name, e) {
        if (key_name == 'clear') {
            this.setState({selectedType: 0});
            this.dataManupalation({}, this.state.loginToken, this.state.apiBaseUrl)
        } else {
            this.state[key_name] = e;
            this.setState({[key_name]: e})
            this.componentWillMount()
        }
    }

    //sorting of data
    setSortFilter(key) {
        let prev = this.state.currentFilter;
        let order = this.state.sortOrder;
        let elements = this.state.subjects.slice();
        let sortingFunction = (a, b) => {
            let _a = a[key];
            let _b = b[key];
            if (typeof _a === 'string' || typeof _b === 'string') {
                _a = _a.toLowerCase();
                _b = _b.toLowerCase();
            }

            if (_a <= _b) return -1;
            return 1;
        };

        if (prev !== key) {
            order = true;
            elements = elements.sort(sortingFunction);
        } else {
            order = !order;
            elements.reverse();
        }

        this.setState({
            previousFilter: prev,
            currentFilter: key,
            sortOrder: order,
            subjects: elements
        });
    }

    addTitleFn() {
        let this_pointer = this;
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/subject"

        let payload = {
            "title": this.state.title,
            "block": this.state.selectedType.value
        }

        if (this.state.files && this.state.files.length > 0) {
            let imageName = this.state.files[0].name;
            let imageType = this.state.files[0].type;
            let imageSize = this.state.files[0].size;
            let imageBase64 = this.state.files[0].base64;
            payload.image = imageBase64
        }


        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {
                    this.setState({
                        user: result.data,
                        addEdit: false,
                        title: ''
                    });
                    this.componentWillMount()
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
                else {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
            });
        //
        //this.props.addnew(this.state.user)
    }

    updateTitleFn() {
        let this_pointer = this;
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/subject-update/" + this.state.id

        let payload = {
            "title": this.state.title,
            "block": this.state.selectedType.value
        }

        if (this.state.files && this.state.files.length > 0) {
            let imageName = this.state.files[0].name;
            let imageType = this.state.files[0].type;
            let imageSize = this.state.files[0].size;
            let imageBase64 = this.state.files[0].base64;
            payload.image = imageBase64
        }


        fetch(apiBaseUrl, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {
                    this.setState({
                        user: result.data,
                        addEdit: false,
                        title: '',
                        id: null
                    });
                    this.componentWillMount()
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
                else {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
            });
        //
        //this.props.addnew(this.state.user)
    }

    handleSearchChange = (e) => {
        this.setState({search: e.target.value});
    }

    render() {
        let {subjects} = this.state;
        const {subjectTasks} = this.state;
        const {blocks} = this.state;
        //on type search changes
        let searchString = this.state.search.trim().toLowerCase();
        if (searchString.length > 0) {
            subjects = subjects.filter(function (i) {
                return i.title.toLowerCase().match(searchString);
            });
        }


        return (
            <div className="animated fadeIn">
                <Row>

                    <Col xs="12" lg="12">
                        <Card>
                            <CardHeader>
                                <Row>
                                    <Col xs="2" lg="2">
                                        <i className="fa fa-align-justify"></i> Subjects
                                    </Col>
                                    <Col xs="3" lg="3">

                                    </Col>
                                    <Col xs="5" lg="5">
                                        {/*<form
                                            name={'form'}
                                            onSubmit={this.searchFn.bind(this)}>
                                            <InputGroup>

                                                <Input style={{height: 38}} type="text"
                                                       value={this.state.search}
                                                       onChange={(e) => {
                                                           this.setState({search: e.target.value})
                                                       }}
                                                       placeholder="Search"/>


                                                <InputGroupAddon addonType="prepend">
                                                    <Button type="button"
                                                            onClick={this.searchFn.bind(this)}
                                                            color="primary"
                                                            size="sm">
                                                        <i className="fa fa-search"></i>
                                                        Search</Button>


                                                    <Button
                                                        type="button"
                                                        color="danger"
                                                        size="sm"
                                                        onClick={this.searchFn.bind(this, 'clear')}>Clear</Button>


                                                </InputGroupAddon>
                                            </InputGroup>
                                        </form>*/}
                                        <form
                                            name={'form'}>
                                            <InputGroup>

                                                <Input style={{height: 38}} type="text"
                                                       value={this.state.search}
                                                       onChange={this.handleSearchChange} placeholder="Type here..."/>
                                            </InputGroup>
                                        </form>
                                    </Col>
                                    <Col xs="2" lg="2">
                                        {/*<Button type="submit"
                                                className="pull-right"
                                                onClick={this.redirectPassFn.bind(this)}
                                                size="sm"
                                                color="primary">
                                            <i className="fa fa-plus"></i> New Subject
                                        </Button>*/}
                                    </Col>
                                </Row>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col xs="12" lg="12">
                                        <Table responsive striped>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>
                                                    <span className="data-short-left">
                                                        Title
                                                    </span>
                                                    {
                                                        <span className="data-short-left">
                                                        {
                                                            this.state.sortBy == 'asc' ?
                                                                <i className="fa fa-caret-up"
                                                                   onClick={this.setSortFilter.bind(this, 'title')}>

                                                                </i>
                                                                : <i className="fa fa-caret-down"
                                                                     onClick={this.setSortFilter.bind(this, 'title')}></i>
                                                        }


                                                    </span>
                                                    }

                                                </th>
                                                <th>Block</th>
                                                <th>Created By</th>
                                                <th>Date</th>
                                                {/* <th>Updated At</th>*/}
                                                {/*   <th>Action</th>*/}

                                            </tr>
                                            </thead>

                                            {
                                                subjects.map((subject, index) => (
                                                        <tbody key={index}>
                                                        <tr>
                                                            <td>{index + 1}</td>
                                                            <td>
                                                                <a data-toggle="tooltip" data-placement="top"
                                                                   title="Subject Tasks"
                                                                   onClick={() => this.subjectTaskFn('taskdetail', subject._id)}
                                                                   style={{color: subject.isDeleted ? '#FF0000' : '#20a8d8'}}>{subject.title} </a><br/>
                                                                <b>What happened so far? :</b>
                                                                <span>
                                                                    {subject.title1}
                                                                </span>
                                                                <br/>
                                                                <b>What is happening these days far ? :</b>
                                                                <span>
                                                                    {subject.title2}
                                                                </span>
                                                                <br/>
                                                                <b>Goals :What do i want to achieve ? :</b>
                                                                <span>
                                                                    {subject.title3}
                                                                </span>
                                                                <br/>
                                                                <b>Measures :How will i know i succeed ? :</b>
                                                                <span>
                                                                    {subject.title4}
                                                                </span>
                                                                <br/>
                                                                <b>My action plan ? :</b>
                                                                <span>
                                                                    {subject.title5}
                                                                </span>
                                                            </td>
                                                            <td>{subject.block.title}</td>
                                                            <td>{subject.createdBy.firstName} {subject.createdBy.lastName}</td>
                                                            {/*<td>{subject.createdDate}</td>*/}
                                                            <td>
                                                                            <span data-toggle="tooltip"
                                                                                  data-placement="top"
                                                                                  title="Created Date">
                                                                                CD:{subject.createdDate}
                                                                            </span>
                                                                {
                                                                    subject.updatedDate ?
                                                                        <span data-toggle="tooltip"
                                                                              data-placement="top"
                                                                              title="Updated Date">
                                                                                UD:{subject.updatedDate}
                                                                            </span> : ""
                                                                }

                                                            </td>
                                                            {/* <td>{subject.updatedDate}</td>*/}
                                                            {/* <td>
                                                                <span>
                                                                       <i style={{
                                                                           paddingLeft: 1,
                                                                           marginLeft: 6,
                                                                           marginRight: 10
                                                                       }}
                                                                          onClick={() => this.openModal(subject._id)}
                                                                          data-title="Delete"
                                                                          data-toggle="modal" data-target="#deleteModel"
                                                                          className="fa fa-trash">
                                                                </i>
                                                            </span>


                                                                <i style={{paddingLeft: 1, marginLeft: 6, marginRight: 10}}
                                                                   onClick={() => this.editRedirectFn('edit', subject._id)}
                                                                   data-title="Edit"
                                                                   data-toggle="modal" data-target="#edit"
                                                                   className="fa fa-edit">
                                                                </i>

                                                            </td>*/}
                                                        </tr>
                                                        </tbody>
                                                    )
                                                )}
                                        </Table>

                                    </Col>
                                </Row>

                                <Modal isOpen={this.state.deleteModel}
                                       className={'modal-danger '}>
                                    <ModalHeader>Confirm </ModalHeader> <ModalBody>
                                    Are you sure, you want to delete this entry ?
                                </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary"
                                                id="Popover1"
                                                onClick={() => {
                                                    this.setState({deleteModel: false})
                                                }}>No
                                        </Button>
                                        <Button color="success"
                                                id="Popover1"
                                                onClick={() => this.deleteFn(this)}>Yes
                                        </Button>
                                    </ModalFooter>
                                </Modal>
                                <Modal isOpen={this.state.addEdit}
                                       className={'modal-info modal-fade'}>
                                    {
                                        this.state.id === null ?
                                            <ModalHeader>
                                                Add Subject
                                            </ModalHeader>
                                            : <ModalHeader>
                                                Edit Subject
                                            </ModalHeader>
                                    }

                                    <ModalBody>
                                        <Row>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                                <FormGroup>
                                                    <Label htmlFor="name">Block
                                                        <span
                                                            className={this.state.selectedType ? 'success' : 'err'}>*</span>
                                                    </Label>
                                                    <Select style={{width: 200}}
                                                            value={this.state.selectedType}
                                                            onChange={this.selectFn.bind(this, 'selectedType')}
                                                            options={
                                                                blocks
                                                            }
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                                <FormGroup>
                                                    <Label htmlFor="name">Title

                                                        <span
                                                            className={this.state.title ? 'succes' : 'err'}>*</span>
                                                    </Label>
                                                    <Input type="text"
                                                           name="title"
                                                           ref={'title'}
                                                           value={this.state.title}
                                                           onChange={(e) => {
                                                               this.setState({title: e.target.value});
                                                               this.checkErrorFn()
                                                           }}
                                                           required/>
                                                </FormGroup>
                                            </Col>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                                <FormGroup>
                                                    <Label htmlFor="name">Icon</Label>
                                                    <FileBase64
                                                        multiple={true}
                                                        onDone={this.getFiles.bind(this)}/>
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                    </ModalBody>
                                    <ModalFooter>
                                        {
                                            this.state.id ?
                                                <Button color="primary"
                                                        onClick={this.commonRedirect.bind(this, this.state.id)}
                                                        className={!this.state.title ? 'btnDisbaled pull-right' : 'pull-right'}>
                                                    Update</Button>
                                                :
                                                <Button color="primary"
                                                        onClick={this.commonRedirect.bind(this, null)}
                                                        className={!this.state.title ? 'btnDisbaled pull-right' : 'pull-right'}>
                                                    Save</Button>
                                        }

                                        <Button color="secondary"
                                                onClick={(e) => {
                                                    this.setState({
                                                        addEdit: false,
                                                        title: null,
                                                        id: null,
                                                        selectedType: null
                                                    })
                                                }}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                                <Modal isOpen={this.state.subjectPage && this.state.subsExist}
                                       className={'modal-info modal-lg'}>
                                    <ModalHeader>
                                        Subject Task Detail
                                    </ModalHeader>
                                    <ModalBody>
                                        <Row>
                                            <Col xs="12" lg="12" style={{
                                                height: 642,
                                                overflow: 'auto',
                                            }}>
                                                <Table responsive striped>
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Title</th>
                                                        <th>Student</th>
                                                        <th>Block</th>
                                                        <th>Created By</th>
                                                        {/* <th>Created At</th>
                                                        <th>Updated At</th>*/}
                                                    </tr>
                                                    </thead>

                                                    {
                                                        subjectTasks.map((task, index) => (
                                                                <tbody key={index}>
                                                                <tr>
                                                                    <td>{index + 1}</td>
                                                                    <td>
                                                                        <a style={{color: task.isDeleted ? '#FF0000' : '#20a8d8'}}>{task.title} </a><br/>
                                                                        <b>What do i do? :</b>
                                                                        <span>
                                                                         {task.questionOne}
                                                                        </span>
                                                                        <br/>
                                                                        <b>What am i planning to do ? :</b>
                                                                        <span>
                                                                            {task.questionTwo}
                                                                        </span>
                                                                        <br/>
                                                                        <b>My next milestone ? :</b>
                                                                        <span>
                                                                            {task.questionThree}
                                                                        </span><br/>
                                                                        <b>Who do i follow up with ? :</b>
                                                                        <span>
                                                                                          {
                                                                                              task.followUpId ?
                                                                                                  <span>
                                                                                                    {task.followUpId.firstName} {task.followUpId.lastName}
                                                                                                  </span> : null
                                                                                          }
                                                                                        </span>

                                                                    </td>
                                                                    <td>{task.studentId.firstName} {task.studentId.lastName}</td>
                                                                    <td>{task.blockId.title}</td>
                                                                    <td>
                                                                        {
                                                                            task.createdByStudent ?
                                                                                <span>
                                                                                                    {task.createdByStudent.firstName} {task.createdByStudent.lastName}
                                                                                                </span> : task.createdByTutor ?
                                                                                <span>
                                                                                                {task.createdByTutor.firstName} {task.createdByTutor.lastName}
                                                                                                </span> :
                                                                                <span>
                                                                                                    {task.createdBy.firstName} {task.createdBy.lastName}
                                                                                                </span>
                                                                        }
                                                                    </td>
                                                                    {/* <td>{task.createdDate}</td>
                                                                    <td>{task.updatedDate}</td>*/}
                                                                </tr>
                                                                </tbody>
                                                            )
                                                        )}
                                                </Table>

                                            </Col>
                                        </Row>


                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary"
                                                onClick={(e) => {
                                                    this.setState({
                                                        addEdit: false,
                                                        title: null,
                                                        id: null,
                                                        subjectPage: false
                                                    })
                                                }}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                                {/* <Pagination>
                                    <PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
                                    <PaginationItem active>
                                        <PaginationLink tag="button">1</PaginationLink>
                                    </PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
                                </Pagination>*/}
                            </CardBody>
                        </Card>
                    </Col>
                    <NotificationContainer/>
                </Row>


            </div>

        );
    }
}

export default SubjectPage;
