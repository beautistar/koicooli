// info.jsx
var Info = ({ val, children }) => (
    <div className='info'>{children}: {val}</div>
);

// controls.jsx
var TableControl = React.createClass({
    getDefaultProps: () => ({
        type: 'get',
        text: 'Get',
        handler: () => { console.log('Default action'); }
    }),
    render: function() {
        var cls = 'control control--' + this.props.type;
        return (
            <span className={cls}
                  onClick={this.props.handler}>{this.props.children}</span>
        );
    }
});

var Checkbox = React.createClass({
    getDefaultProps: () => ({
        status: false,
        onClick: () => console.log('checkbox')
    }),
    render: function() {
        return (
            <span className='checkbox' onClick={this.props.onClick}>
				<CheckboxIcon status={this.props.status} />
			</span>
        );
    }
});

// icons.js
var SVG = ({ href, ...other }) => {
    return (
        <svg {...other}>
            <use xlinkHref={href} />
        </svg>
    );
}

var TriangleIcon = (props) => {
    var visibility = 'triangle triangle--' + (props.visibility ? 'visible' : 'hidden');
    var order = '#triangle-' + ((props.order || !props.visibility) ? 'up' : 'down');

    return <SVG href={order} className={visibility} />
};

var CheckboxIcon = (props) => {
    var type = props.status ? '#checkbox-active' : '#checkbox-inactive';
    return <SVG href={type} />;
};

// body.jsx
var TableCell = ({ children }) => <td>{children}</td>;

var TableRow = React.createClass({
    render: function() {

        return (
            <tr>
                <TableCell>{this.props.index}</TableCell>
                <td><Checkbox status={this.props.status}
                              onClick={this.props.checkboxHandler} /></td>
                <TableCell>{this.props.product}</TableCell>
                <TableCell>{this.props.category}</TableCell>
                <TableCell>{'$'}{this.props.price}</TableCell>
                <td>
                    <TableControl handler={this.props.deleteHandler}
                                  type='delete'>Delete</TableControl>
                    <TableControl handler={this.props.editHandler}
                                  type='get'>Get</TableControl>
                </td>
            </tr>
        );
    }
});

// head.jsx
var HeadCell = (props) => {
    var cls = 'filter filter--' + props.current;
    return (
        <th>
			<span className={cls} onClick={props.handler}>
				<span className='filter__key'>{props.children}</span>
				<TriangleIcon visibility={props.icon.visibility}
                              order={props.icon.order} />
			</span>
        </th>
    );
};

var TableHead = (props) => (
    <thead>
    <tr>
        <HeadCell handler={props.handlers[0]}
                  icon={props.icon[0]}>
            {props.filters[0].text}
        </HeadCell>
        <th>
            <Checkbox status={props.checkboxStatus}
                      onClick={props.checkboxHandler} />
        </th>
        {props.filters.slice(1).map((element, i) => (
            <HeadCell handler={props.handlers[i + 1]}
                      icon={props.icon[i + 1]}>
                {element.text}
            </HeadCell>
        ))}
        <th>Controls</th>
    </tr>
    </thead>
);

// table.jsx
var Table = React.createClass({
    getInitialState: function() {
        return {
            currentFilter: null,
            previousFilter: null,
            sortOrder: false,
            checkboxStatus: false,
            elements: this.props.elements.map((element, i) => {
                element.index = i;
                element.status = false;
                return element;
            })
        };
    },
    resetElements: function() {
        this.setState(this.getInitialState());
    },
    editElement: function(i) {
        var res = [];
        var element = this.state.elements[i];
        var status;
        for (var p in element) {

            res.push(p + ': ' + (p === 'status' ? (element[p] ? 'checked' : 'unchecked') : element[p]));
        }
        alert(res.join('\n'));
    },
    removeElement: function(i) {
        this.setState({
            elements: this.state.elements.filter((x, index) => index != i)
        });
    },
    toggleCommonCheckbox: function() {
        var checkboxStatus = !this.state.checkboxStatus;

        var elements = this.state.elements.map((element) => {
            element.status = checkboxStatus;
            return element;
        });

        this.setState({ checkboxStatus: checkboxStatus, elements: elements });
    },
    toggleCheckbox: function(i) {
        var element = this.state.elements.slice()[i];
        var status = !element.status;
        this.setState({
            elements: this.state.elements.map((element, index) => {
                if (index === i) element.status = status;
                return element;
            })
        });
    },
    setFilter: function(filter) {
        var prev = this.state.currentFilter;
        var order = this.state.sortOrder;
        var elements = this.state.elements.slice();
        var sortingFunction = (a, b) => {
            var _a = a[filter];
            var _b = b[filter];
            if (typeof _a === 'string' || typeof _b === 'string') {
                _a = _a.toLowerCase();
                _b = _b.toLowerCase();
            }

            if (_a <= _b) return -1;
            return 1;
        };

        if (prev !== filter) {
            order = true;
            elements = elements.sort(sortingFunction);
        } else {
            order = !order;
            elements.reverse();
        }

        this.setState({
            previousFilter: prev,
            currentFilter: filter,
            sortOrder: order,
            elements: elements
        });
    },
    render: function() {
        var display = a => a === null ? 'null' : a;
        var info = [
            { msg: 'Number of elements', val: this.state.elements.length },
            { msg: 'Previous filter', val: display(this.state.previousFilter) },
            { msg: 'Current filter', val: display(this.state.currentFilter) },
            { msg: 'Sort order', val: this.state.sortOrder ? 'true' : 'false' }
        ];
        var filters = [
            { type: 'product', text: 'Product' },
            { type: 'index', text: 'Index' },
            { type: 'category', text: 'Category' },
            { type: 'price', text: 'Price' }
        ];

        return (
            <section className='table-container'>
                <table className='table'>
                    <TableHead filters={filters}
                               handlers={filters.map((element) =>
                                   this.setFilter.bind(this, element.type))}
                               icon={filters.map((element) => ({
                                   visibility: element.type === this.state.currentFilter,
                                   order: (element.type === this.state.currentFilter) && this.state.sortOrder
                               }))}
                               checkboxStatus={this.state.checkboxStatus}
                               checkboxHandler={this.toggleCommonCheckbox} />
                    <tbody>
                    {this.state.elements.map((element, i) =>
                        <TableRow key={element.product}
                                  index={element.index + 1}
                                  status={element.status}
                                  product={element.product}
                                  category={element.category}
                                  price={element.price}
                                  checkboxHandler={this.toggleCheckbox.bind(this, i)}
                                  editHandler={this.editElement.bind(this, i)}
                                  deleteHandler={this.removeElement.bind(this, i)} />)}
                    </tbody>
                </table>

                <div className='table-info'>
                    {info.map(item => <Info val={item.val}>{item.msg}</Info>)}
                </div>
                <br />
                <TableControl type='reset'
                              handler={this.resetElements}>Reset</TableControl>
            </section>
        );
    }
});

// data.js
var elements = [
    { product: 'Bread', category: 'Food', price: 1 },
    { product: 'F. Dostoyevsky "Crime and Punishment"', category: 'Books', price: 8 },
    { product: 'iPhone', category: 'Electronics', price: 699 },
    { product: 'Milk', category: 'Food', price: 2 },
    { product: 'TV', category: 'Electronics', price: 1099 }
];

// app.jsx
ReactDOM.render(
    <Table elements={elements} />,
    document.getElementById('table')
);
