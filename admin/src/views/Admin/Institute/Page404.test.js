import React from 'react';
import ReactDOM from 'react-dom';
import AdminPage from './InstitutePage';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<InstitutePage />, div);
  ReactDOM.unmountComponentAtNode(div);
});
