import React, {Component} from 'react';
import {
    Badge,
    Card,
    CardBody,
    Button,
    Input,
    CardHeader,
    Col,
    Pagination,
    PaginationItem,
    PaginationLink,
    Row,
    Table,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    InputGroup,
    InputGroupAddon,
    FormGroup,
    Label,
    InputGroupText
} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import * as _ from 'underscore';
import * as moment from 'moment';
import {apiUrl} from "../../../index";
import FileBase64 from 'react-file-base64';
import Img from 'react-image'
import VisibilitySensor from 'react-visibility-sensor'
//import Label from "reactstrap/src/Label";
//import FormGroup from "reactstrap/src/FormGroup";
import Select from 'react-select';
import {NotificationContainer, NotificationManager} from 'react-notifications';
//import InputGroup from "reactstrap/src/InputGroup";
//import InputGroupAddon from "reactstrap/src/InputGroupAddon";
const commonHostUrl = require('../../../../src/constant');


class InstitutePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            institutes: [],
            editState: false,
            id: null,
            tutors: [],
            deleteModel: false,
            confirm: false,
            deleteId: null,
            search: '',
            showModal: false,
            sortBy: 'desc',
            sorting: {},
            sortByVal: {},
            sort: {},
            currentFilter: null,
            previousFilter: null,
            sortOrder: false,
            loginToken: '',
            apiUrl: '',
            addEdit: false,
            editForm: false,
            user: {},
            errorTitle: false,
            errorAddress: false,
            subjectPage: false,
            blockSubjects: [],
            subsExist: false,
            files: []


        }

        this.addnew = this.newData.bind(this);
    }

    redirectFn() {
        this.setState({redirect: true})
    }

    editRedirectFn(key, id) {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        //let location_details = window.location.hash.split('/');
        //let id = location_details[location_details.length - 1];
        //this.state.selectedType = 1;

        //let cachedUser = JSON.parse(localStorage.getItem('User'));
        //let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/institute-detail/" + id;

        fetch(apiBaseUrl, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            // body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    let updateData = result.data;


                    this.setState({
                        title: updateData.title,
                        address: updateData.address,
                        user: updateData,
                        addEdit: true,
                        id: id
                    });

                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });


        /*  this.setState({addEdit: true})
          this.setState({id: id})*/
    }


    openModal(deleteId) {
        this.setState({deleteId: deleteId})
        this.setState({deleteModel: true})
    }

    redirectPassFn() {
        this.setState({addEdit: true})
    }

    commonRedirect(id) {
        if (id && id !== null) {
            this.updateTitleFn()
        }
        else {
            this.addTitleFn()
        }
    }

    deleteFn() {

        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/institute-delete/" + this.state.deleteId;


        let payload = {}

        fetch(apiBaseUrl, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {
                if (result.code == 200) {

                    let cloneOfUsers = _.clone(this.state.blocks)
                    let deleteIndex = _.findIndex(this.state.blocks, {_id: this.state.deleteId});
                    if (deleteIndex >= 0) {
                        cloneOfUsers[deleteIndex].deleteModel = false;
                        cloneOfUsers.splice(deleteIndex, 1);
                    }

                    this.setState({
                        blocks: cloneOfUsers,
                        deleteModel: false
                    });

                    this.componentWillMount()
                    /*  this.props.history.push({
                          pathname: "/user"
                      });

                      return <Redirect to={{pathname: '/user'}}
                                       render={<BlockPage/>}/>
  */
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });
        //
    }

    checkErrorFn() {

        if (this.state.title == null || this.state.title == "") {
            this.setState({errorTitle: true})
        }
        if (this.state.address == null || this.state.address == "") {
            this.setState({errorAddress: true})
        }

    }

    dataManupalation(payload, loginToken, apiBaseUrl) {

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    //date formatter
                    _.map(result.data, function (res) {
                        if (res.createdDate) {
                            res.createdDate = moment(res.createdDate).format("MMM-DD-YYYY")
                        }
                        if (res.updatedDate) {
                            res.updatedDate = moment(res.updatedDate).format("MMM-DD-YYYY")
                        }
                    })

                    this.setState({
                        institutes: result.data
                    });

                    //Getting tutors list
                    let myTutors = _.filter(result.data, {type: 3});
                    this.setState({
                        tutors: myTutors
                    });
                    NotificationManager.success(result.message);

                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error("Data not found");
                }
            })
    }

    //getting login user data
    componentWillMount() {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/institute-list";
        let self = this;
        this.setState({loginToken: loginToken, apiBaseUrl: apiBaseUrl})


        let payload = {}

        /* if (this.state.search && this.state.search !== null) {
             let searchParams = {
                 keys: ['title'],
                 keyword: this.state.search
             }
             payload.search = searchParams
         }*/

        //send it to api for call
        this.dataManupalation(payload, loginToken, apiBaseUrl)

    }

    //function for getting add object data to push into list
    newData(newdata) {
        let oldData = this.state.institutes
        oldData.push(newdata)

        this.setState({institutes: oldData});
    }

    //search for data
    searchFn(clear) {
        if (clear == 'clear') {
            this.setState({search: ''});
            this.dataManupalation({}, this.state.loginToken, this.state.apiBaseUrl)

        }
        else if (this.state.search && (!_.isNull(this.state.search)
            || !_.isUndefined(this.state.search) || this.state.search !== '')) {
            this.setState({search: this.state.search});
            this.componentWillMount()
        }
    }

    //select filter of type
    selectFn(key_name, e) {
        if (key_name == 'clear') {
            this.setState({selectedType: 0});
            this.dataManupalation({}, this.state.loginToken, this.state.apiBaseUrl)
        } else {
            this.state[key_name] = e;
            this.setState({[key_name]: e})
            this.componentWillMount()
        }
    }

    // Callback~
    getFiles(files) {
        this.setState({files: files})
    }

    //sorting of data
    setSortFilter(key) {
        let prev = this.state.currentFilter;
        let order = this.state.sortOrder;
        let elements = this.state.institutes.slice();
        let sortingFunction = (a, b) => {
            let _a = a[key];
            let _b = b[key];
            if (typeof _a === 'string' || typeof _b === 'string') {
                _a = _a.toLowerCase();
                _b = _b.toLowerCase();
            }

            if (_a <= _b) return -1;
            return 1;
        };

        if (prev !== key) {
            order = true;
            elements = elements.sort(sortingFunction);
        } else {
            order = !order;
            elements.reverse();
        }

        this.setState({
            previousFilter: prev,
            currentFilter: key,
            sortOrder: order,
            institutes: elements
        });
    }

    addTitleFn() {
        let this_pointer = this;
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/institute"

        let payload = {
            "title": this.state.title,
            "address": this.state.address,
        }

        if (this.state.files && this.state.files.length > 0) {
            //let imageName = this.state.files[0].name;
            //let imageType = this.state.files[0].type;
            //let imageSize = this.state.files[0].size;
            let imageBase64 = this.state.files[0].base64;
            payload.image = imageBase64
        }

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {
                    this.setState({
                        user: result.data,
                        addEdit: false,
                        title: ''
                    });
                    this.componentWillMount()
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
                else {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
            });
        //
        //this.props.addnew(this.state.user)
    }

    updateTitleFn() {
        let this_pointer = this;
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/institute-update/" + this.state.id

        let payload = {
            "title": this.state.title,
        }

        if (this.state.files && this.state.files.length > 0) {
            let imageName = this.state.files[0].name;
            let imageType = this.state.files[0].type;
            let imageSize = this.state.files[0].size;
            let imageBase64 = this.state.files[0].base64;
            payload.image = imageBase64
        }

        fetch(apiBaseUrl, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {
                    this.setState({
                        user: result.data,
                        addEdit: false,
                        title: '',
                        id: null
                    });
                    this.componentWillMount()
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
                else {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
            });
        //
        //this.props.addnew(this.state.user)
    }

    handleSearchChange = (e) => {
        this.setState({search: e.target.value});
    }

    render() {
        let {institutes} = this.state;
        const {blockSubjects} = this.state;
        let loginUser = JSON.parse(localStorage.getItem('User'));

        //on type search changes
        let searchString = this.state.search.trim().toLowerCase();
        if (searchString.length > 0) {
            institutes = institutes.filter(function (i) {
                return i.title.toLowerCase().match(searchString);
            });
        }


        return (
            <div className="animated fadeIn">
                <Row>

                    <Col xs="12" lg="12">
                        <Card>
                            <CardHeader>
                                <Row>
                                    <Col xs="2" lg="2">
                                        <i className="fa fa-align-justify"></i> Institutes
                                    </Col>
                                    <Col xs="3" lg="3">

                                    </Col>
                                    <Col xs="5" lg="5">
                                        {/*  <form
                                            name={'form'}
                                            onSubmit={this.searchFn.bind(this)}>
                                            <InputGroup>

                                                <Input style={{height: 38}} type="text"
                                                       value={this.state.search}
                                                       onChange={(e) => {
                                                           this.setState({search: e.target.value})
                                                       }}
                                                       placeholder="Search"/>


                                                <InputGroupAddon addonType="prepend">
                                                    <Button type="button"
                                                            onClick={this.searchFn.bind(this)}
                                                            color="primary"
                                                            size="sm">
                                                        <i className="fa fa-search"></i>
                                                        Search</Button>


                                                    <Button
                                                        type="button"
                                                        color="danger"
                                                        size="sm"
                                                        onClick={this.searchFn.bind(this, 'clear')}>Clear</Button>


                                                </InputGroupAddon>
                                            </InputGroup>
                                        </form>*/}
                                        <form
                                            name={'form'}>
                                            <InputGroup>

                                                <Input style={{height: 38}} type="text"
                                                       value={this.state.search}
                                                       onChange={this.handleSearchChange} placeholder="Type here..."/>
                                            </InputGroup>
                                        </form>
                                    </Col>
                                    <Col xs="2" lg="2">
                                        {
                                            loginUser && loginUser.type === 1 ?
                                                <Button type="submit"
                                                        className="pull-right"
                                                        onClick={this.redirectPassFn.bind(this)}
                                                        size="sm"
                                                        color="primary">
                                                    <i className="fa fa-plus"></i> New Institute
                                                </Button> : ""
                                        }

                                    </Col>
                                </Row>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col xs="12" lg="12">
                                        <Table responsive striped>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th style={{width: '30%'}}>
                                                    <span className="data-short-left">
                                                        Title
                                                    </span>
                                                    {
                                                        <span className="data-short-left">
                                                        {
                                                            this.state.sortBy == 'asc' ?
                                                                <i className="fa fa-caret-up"
                                                                   onClick={this.setSortFilter.bind(this, 'title')}>

                                                                </i>
                                                                : <i className="fa fa-caret-down"
                                                                     onClick={this.setSortFilter.bind(this, 'title')}></i>
                                                        }


                                                    </span>
                                                    }

                                                </th>
                                                <th style={{width: '30%'}}>Address</th>
                                                <th>Created By</th>
                                                <th>Date</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>

                                            {
                                                institutes.map((institute, index) => (
                                                        <tbody key={index}>
                                                        <tr>
                                                            <td>{index + 1}</td>
                                                            <td style={{width: '30%'}}>
                                                                <a data-toggle="tooltip" data-placement="top"
                                                                   style={{color: institute.isDeleted ? '#FF0000' : '#20a8d8'}}>{institute.title}
                                                                </a>
                                                            </td>
                                                            <td style={{width: '30%'}}>{institute.address}</td>
                                                            <td>{institute.createdBy.firstName} {institute.createdBy.lastName}</td>
                                                            <td>
                                                                            <span data-toggle="tooltip"
                                                                                  data-placement="top"
                                                                                  title="Created Date">
                                                                                CD:{institute.createdDate}
                                                                            </span><br/>
                                                                {
                                                                    institute.updatedDate ?
                                                                        <span data-toggle="tooltip"
                                                                              data-placement="top"
                                                                              title="Updated Date">
                                                                                UD:{institute.updatedDate}
                                                                            </span> : ""
                                                                }

                                                            </td>
                                                            <td>
                                                                <span>
                                                                       <i style={{
                                                                           paddingLeft: 1,
                                                                           marginLeft: 6,
                                                                           marginRight: 10,
                                                                           color: '#ff0000'
                                                                       }}
                                                                          onClick={() => this.openModal(institute._id)}
                                                                          data-title="Delete"
                                                                          data-toggle="modal" data-target="#deleteModel"
                                                                          className="fa fa-trash">
                                                                </i>
                                                            </span>


                                                                <i style={{
                                                                    paddingLeft: 1,
                                                                    marginLeft: 6,
                                                                    marginRight: 10,
                                                                    color: '#20a8d8'
                                                                }}
                                                                   onClick={() => this.editRedirectFn('edit', institute._id)}
                                                                   data-title="Edit"
                                                                   data-toggle="modal" data-target="#edit"
                                                                   className="fa fa-edit">
                                                                </i>

                                                            </td>
                                                        </tr>
                                                        < div className="modal fade" id="deleteModel" tabIndex="-1"
                                                              role="dialog" aria-labelledby="exampleModalLabel"
                                                              aria-hidden="true">
                                                            <div className="modal-dialog" role="document">
                                                                <div className="modal-content">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title"
                                                                            id="exampleModalLabel">Modal title</h5>
                                                                        <button type="button" className="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        ...
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button"
                                                                                className="btn btn-secondary"
                                                                                data-dismiss="modal">Close
                                                                        </button>
                                                                        <button type="button"
                                                                                className="btn btn-primary">Save changes
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tbody>
                                                    )
                                                )}
                                        </Table>

                                    </Col>
                                </Row>

                                <Modal isOpen={this.state.deleteModel}
                                       className={'modal-danger '}>
                                    <ModalHeader>Confirm </ModalHeader> <ModalBody>
                                    Are you sure, you want to delete this entry ?
                                </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary"
                                                id="Popover1"
                                                onClick={() => {
                                                    this.setState({deleteModel: false})
                                                }}>No
                                        </Button>
                                        <Button color="success"
                                                id="Popover1"
                                                onClick={() => this.deleteFn(this)}>Yes
                                        </Button>
                                    </ModalFooter>
                                </Modal>
                                <Modal isOpen={this.state.addEdit}
                                       className={'modal-info modal-fade'}>
                                    {
                                        this.state.id === null ?
                                            <ModalHeader>
                                                Add Institute
                                            </ModalHeader>
                                            : <ModalHeader>
                                                Edit Institute
                                            </ModalHeader>
                                    }

                                    <ModalBody>
                                        <Row>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                                <FormGroup>
                                                    <Label htmlFor="name">Title

                                                        <span
                                                            className={this.state.title ? 'succes' : 'err'}>*</span>
                                                    </Label>
                                                    <Input type="text"
                                                           name="title"
                                                           ref={'title'}
                                                           value={this.state.title}
                                                           onChange={(e) => {
                                                               this.setState({title: e.target.value});
                                                               this.checkErrorFn()
                                                           }}
                                                           required/>
                                                </FormGroup>
                                            </Col>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                                <FormGroup>
                                                    <Label htmlFor="name">Address</Label>
                                                    <textarea
                                                        style={{height: 106}}
                                                        className="form-control"
                                                        name="address"
                                                        ref={'address'}
                                                        value={this.state.address}
                                                        onChange={(e) => {
                                                            this.setState({address: e.target.value});
                                                            this.checkErrorFn()
                                                        }}
                                                        required/>
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                    </ModalBody>
                                    <ModalFooter>
                                        {
                                            this.state.id ?
                                                <Button color="primary"
                                                        onClick={this.commonRedirect.bind(this, this.state.id)}
                                                        className={!this.state.title ? 'btnDisbaled pull-right' : 'pull-right'}>
                                                    Update</Button>
                                                :
                                                <Button color="primary"
                                                        onClick={this.commonRedirect.bind(this, null)}
                                                        className={!this.state.title ? 'btnDisbaled pull-right' : 'pull-right'}>
                                                    Save</Button>
                                        }

                                        <Button color="secondary"
                                                onClick={(e) => {
                                                    this.setState({addEdit: false, title: null, id: null})
                                                }}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                                {/* <Pagination>
                                    <PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
                                    <PaginationItem active>
                                        <PaginationLink tag="button">1</PaginationLink>
                                    </PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
                                </Pagination>*/}
                            </CardBody>
                        </Card>
                    </Col>
                    <NotificationContainer/>
                </Row>


            </div>

        );
    }
}

export default InstitutePage;
