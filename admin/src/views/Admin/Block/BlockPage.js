import React, {Component} from 'react';
import {
    Badge,
    Card,
    CardBody,
    Button,
    Input,
    CardHeader,
    Col,
    Pagination,
    PaginationItem,
    PaginationLink,
    Row,
    Table,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    InputGroup,
    InputGroupAddon,
    FormGroup,
    Label,
    InputGroupText
} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import * as _ from 'underscore';
import * as moment from 'moment';
import {apiUrl} from "../../index";
import FileBase64 from 'react-file-base64';
import Img from 'react-image'
import VisibilitySensor from 'react-visibility-sensor'
//import Label from "reactstrap/src/Label";
//import FormGroup from "reactstrap/src/FormGroup";
import Select from 'react-select';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import ImageZoom from 'react-medium-image-zoom'
//import InputGroup from "reactstrap/src/InputGroup";
//import InputGroupAddon from "reactstrap/src/InputGroupAddon";
const commonHostUrl = require('../../../../src/constant');
let imagePath1 = require('../../../../src/icons/icons/blockIcon1.png');
let imagePath2 = require('../../../../src/icons/icons/blockIcon2.png');
let imagePath3 = require('../../../../src/icons/icons/blockIcon3.png');
let imagePath4 = require('../../../../src/icons/icons/blockIcon4.png');
let imagePath5 = require('../../../../src/icons/icons/blockIcon5.png');
let imagePath6 = require('../../../../src/icons/icons/blockIcon6.png');
let imagePath7 = require('../../../../src/icons/icons/blockIcon7.png');
let imagePath8 = require('../../../../src/icons/icons/blockIcon8.png');
let imagePath9 = require('../../../../src/icons/icons/blockIcon9.png');

//import ImageZoom from 'react-medium-image-zoom'


class BlockPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            blocks: [],
            editState: false,
            id: null,
            tutors: [],
            deleteModel: false,
            confirm: false,
            deleteId: null,
            search: '',
            showModal: false,
            sortBy: 'desc',
            sorting: {},
            sortByVal: {},
            sort: {},
            currentFilter: null,
            previousFilter: null,
            sortOrder: false,
            loginToken: '',
            apiUrl: '',
            addEdit: false,
            editForm: false,
            user: {},
            errorTitle: false,
            subjectPage: false,
            blockSubjects: [],
            subsExist: false,
            files: []


        }

        this.addnew = this.newData.bind(this);
    }

    redirectFn() {
        this.setState({redirect: true})
    }

    editRedirectFn(key, id) {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        //let location_details = window.location.hash.split('/');
        //let id = location_details[location_details.length - 1];
        //this.state.selectedType = 1;

        //let cachedUser = JSON.parse(localStorage.getItem('User'));
        //let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/block-detail/" + id;

        fetch(apiBaseUrl, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            // body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    let updateData = result.data;


                    this.setState({
                        title: updateData.title,
                        user: updateData,
                        addEdit: true,
                        id: id
                    });

                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });


        /*  this.setState({addEdit: true})
          this.setState({id: id})*/
    }

    blockSubjectsFn(key, id) {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        //let location_details = window.location.hash.split('/');
        //let id = location_details[location_details.length - 1];
        //this.state.selectedType = 1;

        //let cachedUser = JSON.parse(localStorage.getItem('User'));
        //let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/block--subjects/" + id;

        fetch(apiBaseUrl, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            // body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    //date formatter
                    _.map(result.data, function (res) {
                        if (res.createdDate) {
                            res.createdDate = moment(res.createdDate).format("MMM-DD-YYYY")
                        }
                        if (res.updatedDate) {
                            res.updatedDate = moment(res.updatedDate).format("MMM-DD-YYYY")
                        }
                    })

                    let myBlockSubjects
                    if (result.data && result.data.length) {
                        myBlockSubjects = result.data;
                        this.setState({subsExist: true})
                    }
                    else {
                        myBlockSubjects = [];
                        this.setState({subsExist: false})
                    }


                    this.setState({
                        blockSubjects: myBlockSubjects,
                        subjectPage: true,
                        id: id
                    });

                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });


        /*  this.setState({addEdit: true})
          this.setState({id: id})*/
    }

    openModal(deleteId) {
        this.setState({deleteId: deleteId})
        this.setState({deleteModel: true})
    }

    redirectPassFn() {
        this.setState({addEdit: true})
    }

    commonRedirect(id) {
        if (id && id !== null) {
            this.updateTitleFn()
        }
        else {
            this.addTitleFn()
        }
    }

    deleteFn() {

        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/block-delete/" + this.state.deleteId;


        let payload = {}

        fetch(apiBaseUrl, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {
                if (result.code == 200) {

                    let cloneOfUsers = _.clone(this.state.blocks)
                    let deleteIndex = _.findIndex(this.state.blocks, {_id: this.state.deleteId});
                    if (deleteIndex >= 0) {
                        cloneOfUsers[deleteIndex].deleteModel = false;
                        cloneOfUsers.splice(deleteIndex, 1);
                    }

                    this.setState({
                        blocks: cloneOfUsers,
                        deleteModel: false
                    });

                    this.componentWillMount()
                    /*  this.props.history.push({
                          pathname: "/user"
                      });

                      return <Redirect to={{pathname: '/user'}}
                                       render={<BlockPage/>}/>
  */
                }
                else if (result.code == 403) {
                    NotificationManager.error(result.message);
                }
                else {
                    NotificationManager.error(result.message);
                }
            });
        //
    }

    checkErrorFn() {

        if (this.state.title == null || this.state.title == "") {
            this.setState({errorTitle: true})
        }

    }

    dataManupalation(payload, loginToken, apiBaseUrl) {

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {

                    //date formatter
                    _.map(result.data, function (res) {
                        if (res.createdDate) {
                            res.createdDate = moment(res.createdDate).format("MMM-DD-YYYY")
                        }
                        if (res.updatedDate) {
                            res.updatedDate = moment(res.updatedDate).format("MMM-DD-YYYY")
                        }
                    })

                    this.setState({
                        blocks: result.data
                    });

                    //Getting tutors list
                    let myTutors = _.filter(result.data, {type: 3});
                    this.setState({
                        tutors: myTutors
                    });

                }
                else if (result.code == 403) {
                    NotificationManager.error("Data not found");
                }
                else {
                    NotificationManager.error("Data not found");
                }
            })
    }

    //getting login user data
    componentWillMount() {
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/block-list";
        let self = this;
        this.setState({loginToken: loginToken, apiBaseUrl: apiBaseUrl})


        let payload = {}

        /*  if (this.state.search && this.state.search !== null) {
              let searchParams = {
                  keys: ['title'],
                  keyword: this.state.search
              }
              payload.search = searchParams
          }
  */
        //send it to api for call
        this.dataManupalation(payload, loginToken, apiBaseUrl)

    }

    //function for getting add object data to push into list
    newData(newdata) {
        let oldData = this.state.blocks
        oldData.push(newdata)

        this.setState({blocks: oldData});
    }

    //search for data
    searchFn(clear) {
        if (clear == 'clear') {
            this.setState({search: ''});
            this.dataManupalation({}, this.state.loginToken, this.state.apiBaseUrl)

        }
        else if (this.state.search && (!_.isNull(this.state.search)
            || !_.isUndefined(this.state.search) || this.state.search !== '')) {
            this.setState({search: this.state.search});
            this.componentWillMount()
        }
    }

    //select filter of type
    selectFn(key_name, e) {
        if (key_name == 'clear') {
            this.setState({selectedType: 0});
            this.dataManupalation({}, this.state.loginToken, this.state.apiBaseUrl)
        } else {
            this.state[key_name] = e;
            this.setState({[key_name]: e})
            this.componentWillMount()
        }
    }

    // Callback~
    getFiles(files) {
        this.setState({files: files})
    }

    //sorting of data
    setSortFilter(key) {
        let prev = this.state.currentFilter;
        let order = this.state.sortOrder;
        let elements = this.state.blocks.slice();
        let sortingFunction = (a, b) => {
            let _a = a[key];
            let _b = b[key];
            if (typeof _a === 'string' || typeof _b === 'string') {
                _a = _a.toLowerCase();
                _b = _b.toLowerCase();
            }

            if (_a <= _b) return -1;
            return 1;
        };

        if (prev !== key) {
            order = true;
            elements = elements.sort(sortingFunction);
        } else {
            order = !order;
            elements.reverse();
        }

        this.setState({
            previousFilter: prev,
            currentFilter: key,
            sortOrder: order,
            blocks: elements
        });
    }

    addTitleFn() {
        let this_pointer = this;
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/block"

        let payload = {
            "title": this.state.title,
        }

        if (this.state.files && this.state.files.length > 0) {
            let imageName = this.state.files[0].name;
            let imageType = this.state.files[0].type;
            let imageSize = this.state.files[0].size;
            let imageBase64 = this.state.files[0].base64;
            payload.image = imageBase64
        }

        fetch(apiBaseUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {
                    this.setState({
                        user: result.data,
                        addEdit: false,
                        title: ''
                    });
                    this.componentWillMount()
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
                else {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
            });
        //
        //this.props.addnew(this.state.user)
    }

    updateTitleFn() {
        let this_pointer = this;
        let cachedUser = JSON.parse(localStorage.getItem('User'));
        let loginToken = (cachedUser && cachedUser.token) ? cachedUser.token : null;

        let apiBaseUrl = commonHostUrl.url + "api/block-update/" + this.state.id

        let payload = {
            "title": this.state.title,
        }

        if (this.state.files && this.state.files.length > 0) {
            let imageName = this.state.files[0].name;
            let imageType = this.state.files[0].type;
            let imageSize = this.state.files[0].size;
            let imageBase64 = this.state.files[0].base64;
            payload.image = imageBase64
        }

        fetch(apiBaseUrl, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token: loginToken
            },
            body: JSON.stringify(payload),
        })
            .then((res) => res.json())
            .then((result) => {

                if (result.code == 200) {
                    this.setState({
                        user: result.data,
                        addEdit: false,
                        title: '',
                        id: null
                    });
                    this.componentWillMount()
                    NotificationManager.success(result.message);
                }
                else if (result.code == 403) {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
                else {
                    this.setState({
                        redirect: false
                    });
                    NotificationManager.error(result.message);
                }
            });
        //
        //this.props.addnew(this.state.user)
    }

    handleSearchChange = (e) => {
        this.setState({search: e.target.value});
    }


    render() {
        let {blocks} = this.state;
        const {blockSubjects} = this.state;

        //redirecting to edit page
        if (this.state.editState) {
            this.props.history.push({
                pathname: "/user/edit/" + this.state.id,
                state: {
                    userId: this.state.id,
                }
            });
        }

        //on type search changes
        let searchString = this.state.search.trim().toLowerCase();
        if (searchString.length > 0) {
            blocks = blocks.filter(function (i) {
                return i.title.toLowerCase().match(searchString);
            });
        }


        return (
            <div className="animated fadeIn">
                <Row>

                    <Col xs="12" lg="12">
                        <Card>
                            <CardHeader>
                                <Row>
                                    <Col xs="2" lg="2">
                                        <i className="fa fa-align-justify"></i> Blocks
                                    </Col>
                                    <Col xs="3" lg="3">

                                    </Col>
                                    <Col xs="5" lg="5">
                                        {/* <form
                                            name={'form'}
                                            onSubmit={this.searchFn.bind(this)}>
                                            <InputGroup>

                                                <Input style={{height: 38}} type="text"
                                                       value={this.state.search}
                                                       onChange={(e) => {
                                                           this.setState({search: e.target.value})
                                                       }}
                                                       placeholder="Search"/>


                                                <InputGroupAddon addonType="prepend">
                                                    <Button type="button"
                                                            onClick={this.searchFn.bind(this)}
                                                            color="primary"
                                                            size="sm">
                                                        <i className="fa fa-search"></i>
                                                        Search</Button>


                                                    <Button
                                                        type="button"
                                                        color="danger"
                                                        size="sm"
                                                        onClick={this.searchFn.bind(this, 'clear')}>Clear</Button>


                                                </InputGroupAddon>
                                            </InputGroup>
                                        </form>*/}
                                        <form
                                            name={'form'}>
                                            <InputGroup>

                                                <Input style={{height: 38}} type="text"
                                                       value={this.state.search}
                                                       onChange={this.handleSearchChange} placeholder="Type here..."/>
                                            </InputGroup>
                                        </form>
                                    </Col>
                                    <Col xs="2" lg="2">
                                        {/*<Button type="submit"
                                                className="pull-right"
                                                onClick={this.redirectPassFn.bind(this)}
                                                size="sm"
                                                color="primary">
                                            <i className="fa fa-plus"></i> New Block
                                        </Button>*/}
                                    </Col>
                                </Row>
                            </CardHeader>
                            <CardBody>
                                <Row>
                                    <Col xs="12" lg="12">
                                        <Table responsive striped>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>
                                                    <span className="data-short-left">
                                                        Title
                                                    </span>
                                                    {
                                                        <span className="data-short-left">
                                                        {
                                                            this.state.sortBy == 'asc' ?
                                                                <i className="fa fa-caret-up"
                                                                   onClick={this.setSortFilter.bind(this, 'title')}>

                                                                </i>
                                                                : <i className="fa fa-caret-down"
                                                                     onClick={this.setSortFilter.bind(this, 'title')}></i>
                                                        }


                                                    </span>
                                                    }

                                                </th>
                                                <th>Created By</th>
                                                <th>Created At</th>
                                                <th>Updated At</th>
                                                {/*<th>Action</th>*/}

                                            </tr>
                                            </thead>

                                            {
                                                blocks.map((block, index) => (
                                                        <tbody key={index}>
                                                        <tr>
                                                            <td>{index + 1}
                                                                <span>
                                                                    {block.sequence == 1 ?
                                                                        <ImageZoom
                                                                            image={{
                                                                                src: imagePath1,
                                                                                className: 'image--cover',
                                                                                style: {width: '3em'}
                                                                            }}
                                                                        /> :
                                                                        block.sequence == 2 ?
                                                                            <ImageZoom
                                                                                image={{
                                                                                    src: imagePath2,
                                                                                    className: 'image--cover',
                                                                                    style: {width: '3em'}
                                                                                }}
                                                                            /> :
                                                                            block.sequence == 3 ?
                                                                                <ImageZoom
                                                                                    image={{
                                                                                        src: imagePath3,
                                                                                        className: 'image--cover',
                                                                                        style: {width: '3em'}
                                                                                    }}
                                                                                /> :
                                                                                block.sequence == 4 ?
                                                                                    <ImageZoom
                                                                                        image={{
                                                                                            src: imagePath4,
                                                                                            className: 'image--cover',
                                                                                            style: {width: '3em'}
                                                                                        }}
                                                                                    /> :
                                                                                    block.sequence == 5 ?
                                                                                        <ImageZoom
                                                                                            image={{
                                                                                                src: imagePath5,
                                                                                                className: 'image--cover',
                                                                                                style: {width: '3em'}
                                                                                            }}
                                                                                        /> :
                                                                                        block.sequence == 6 ?
                                                                                            <ImageZoom
                                                                                                image={{
                                                                                                    src: imagePath6,
                                                                                                    className: 'image--cover',
                                                                                                    style: {width: '3em'}
                                                                                                }}
                                                                                            /> :
                                                                                            block.sequence == 7 ?
                                                                                                <ImageZoom
                                                                                                    image={{
                                                                                                        src: imagePath7,
                                                                                                        className: 'image--cover',
                                                                                                        style: {width: '3em'}
                                                                                                    }}
                                                                                                /> :
                                                                                                block.sequence == 8 ?
                                                                                                    <ImageZoom
                                                                                                        image={{
                                                                                                            src: imagePath8,
                                                                                                            className: 'image--cover',
                                                                                                            style: {width: '3em'}
                                                                                                        }}
                                                                                                    /> :
                                                                                                    <ImageZoom
                                                                                                        image={{
                                                                                                            src: imagePath9,
                                                                                                            className: 'image--cover',
                                                                                                            style: {width: '3em'}
                                                                                                        }}
                                                                                                    />
                                                                    }
                                                                </span>
                                                                {/*  {
                                                                    block.image ?
                                                                        <img
                                                                            src={block.image}
                                                                            className="image--cover"></img> :
                                                                        <img
                                                                            src="https://i.kinja-img.com/gawker-media/image/upload/gd8ljenaeahpn0wslmlz.jpg"
                                                                            className="image--cover"></img>
                                                                }*/}
                                                            </td>
                                                            <td>
                                                                <a data-toggle="tooltip" data-placement="top"
                                                                   title="Block Subjects"
                                                                   onClick={() => this.blockSubjectsFn('subjectdetail', block._id)}
                                                                   style={{color: block.isDeleted ? '#FF0000' : '#20a8d8'}}>{block.title}
                                                                </a>
                                                            </td>
                                                            <td>{block.createdBy.firstName} {block.createdBy.lastName}</td>
                                                            <td>{block.createdDate}</td>
                                                            <td>{block.updatedDate}</td>
                                                         {/*   <td>
                                                                <span>
                                                                       <i style={{
                                                                           paddingLeft: 1,
                                                                           marginLeft: 6,
                                                                           marginRight: 10,
                                                                           color: '#ff0000'
                                                                       }}
                                                                          onClick={() => this.openModal(block._id)}
                                                                          data-title="Delete"
                                                                          data-toggle="modal" data-target="#deleteModel"
                                                                          className="fa fa-trash">
                                                                </i>
                                                            </span>


                                                                <i style={{
                                                                    paddingLeft: 1,
                                                                    marginLeft: 6,
                                                                    marginRight: 10,
                                                                    color: '#20a8d8'
                                                                }}
                                                                   onClick={() => this.editRedirectFn('edit', block._id)}
                                                                   data-title="Edit"
                                                                   data-toggle="modal" data-target="#edit"
                                                                   className="fa fa-edit">
                                                                </i>

                                                            </td>*/}
                                                        </tr>
                                                        < div className="modal fade" id="deleteModel" tabIndex="-1"
                                                              role="dialog" aria-labelledby="exampleModalLabel"
                                                              aria-hidden="true">
                                                            <div className="modal-dialog" role="document">
                                                                <div className="modal-content">
                                                                    <div className="modal-header">
                                                                        <h5 className="modal-title"
                                                                            id="exampleModalLabel">Modal title</h5>
                                                                        <button type="button" className="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div className="modal-body">
                                                                        ...
                                                                    </div>
                                                                    <div className="modal-footer">
                                                                        <button type="button"
                                                                                className="btn btn-secondary"
                                                                                data-dismiss="modal">Close
                                                                        </button>
                                                                        <button type="button"
                                                                                className="btn btn-primary">Save changes
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tbody>
                                                    )
                                                )}
                                        </Table>

                                    </Col>
                                </Row>

                                <Modal isOpen={this.state.deleteModel}
                                       className={'modal-danger '}>
                                    <ModalHeader>Confirm </ModalHeader> <ModalBody>
                                    Are you sure, you want to delete this entry ?
                                </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary"
                                                id="Popover1"
                                                onClick={() => {
                                                    this.setState({deleteModel: false})
                                                }}>No
                                        </Button>
                                        <Button color="success"
                                                id="Popover1"
                                                onClick={() => this.deleteFn(this)}>Yes
                                        </Button>
                                    </ModalFooter>
                                </Modal>
                                <Modal isOpen={this.state.addEdit}
                                       className={'modal-info modal-fade'}>
                                    {
                                        this.state.id === null ?
                                            <ModalHeader>
                                                Add Block
                                            </ModalHeader>
                                            : <ModalHeader>
                                                Edit Block
                                            </ModalHeader>
                                    }

                                    <ModalBody>
                                        <Row>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                                <FormGroup>
                                                    <Label htmlFor="name">Title

                                                        <span
                                                            className={this.state.title ? 'succes' : 'err'}>*</span>
                                                    </Label>
                                                    <Input type="text"
                                                           name="title"
                                                           ref={'title'}
                                                           value={this.state.title}
                                                           onChange={(e) => {
                                                               this.setState({title: e.target.value});
                                                               this.checkErrorFn()
                                                           }}
                                                           required/>
                                                </FormGroup>
                                            </Col>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                                <FormGroup>
                                                    <Label htmlFor="name">Icon</Label>
                                                    <FileBase64
                                                        multiple={true}
                                                        onDone={this.getFiles.bind(this)}/>
                                                    {/* <input type="file" name="image" onChange={(e) => {
                                                        this.setState({image: e.target.value});
                                                    }}/>*/}
                                                    {/* <img src={require('/one.jpeg')} />*/}
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                    </ModalBody>
                                    <ModalFooter>
                                        {
                                            this.state.id ?
                                                <Button color="primary"
                                                        onClick={this.commonRedirect.bind(this, this.state.id)}
                                                        className={!this.state.title ? 'btnDisbaled pull-right' : 'pull-right'}>
                                                    Update</Button>
                                                :
                                                <Button color="primary"
                                                        onClick={this.commonRedirect.bind(this, null)}
                                                        className={!this.state.title ? 'btnDisbaled pull-right' : 'pull-right'}>
                                                    Save</Button>
                                        }

                                        <Button color="secondary"
                                                onClick={(e) => {
                                                    this.setState({addEdit: false, title: null, id: null})
                                                }}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                                <Modal isOpen={this.state.subjectPage && this.state.subsExist}
                                       className={'modal-info modal-fade'}>
                                    <ModalHeader>
                                        Block Subject Detail
                                    </ModalHeader>
                                    <ModalBody>
                                        <Row>
                                            <Col xs="12" lg="12">
                                                <Table responsive striped>
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Title</th>
                                                        <th>Created At</th>
                                                        <th>Updated At</th>
                                                    </tr>
                                                    </thead>

                                                    {
                                                        blockSubjects.map((block, index) => (
                                                                <tbody key={index}>
                                                                <tr>
                                                                    <td>{index + 1}</td>
                                                                    <td>
                                                                        <a style={{color: block.isDeleted ? '#FF0000' : '#23282c'}}>{block.title} </a>
                                                                    </td>
                                                                    <td>{block.createdDate}</td>
                                                                    <td>{block.updatedDate}</td>
                                                                </tr>
                                                                < div className="modal fade" id="deleteModel" tabIndex="-1"
                                                                      role="dialog" aria-labelledby="exampleModalLabel"
                                                                      aria-hidden="true">
                                                                    <div className="modal-dialog" role="document">
                                                                        <div className="modal-content">
                                                                            <div className="modal-header">
                                                                                <h5 className="modal-title"
                                                                                    id="exampleModalLabel">Modal title</h5>
                                                                                <button type="button" className="close"
                                                                                        data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div className="modal-body">
                                                                                ...
                                                                            </div>
                                                                            <div className="modal-footer">
                                                                                <button type="button"
                                                                                        className="btn btn-secondary"
                                                                                        data-dismiss="modal">Close
                                                                                </button>
                                                                                <button type="button"
                                                                                        className="btn btn-primary">Save
                                                                                    changes
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </tbody>
                                                            )
                                                        )}
                                                </Table>

                                            </Col>
                                        </Row>


                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary"
                                                onClick={(e) => {
                                                    this.setState({
                                                        addEdit: false,
                                                        title: null,
                                                        id: null,
                                                        subjectPage: false
                                                    })
                                                }}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                                {/* <Pagination>
                                    <PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
                                    <PaginationItem active>
                                        <PaginationLink tag="button">1</PaginationLink>
                                    </PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
                                    <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
                                </Pagination>*/}
                            </CardBody>
                        </Card>
                    </Col>
                    <NotificationContainer/>
                </Row>


            </div>

        );
    }
}

export default BlockPage;
