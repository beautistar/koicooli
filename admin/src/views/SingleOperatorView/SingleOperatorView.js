import React, { Component } from 'react';

import {
    Col,
    Row,
    Card,CardHeader,CardBody,
    Modal, ModalBody, ModalFooter,Button,FormGroup,Label,Input
} from 'reactstrap';
import {CSVLink,CSVDownload} from 'react-csv';

import  { Redirect } from 'react-router-dom';
import * as _ from 'underscore';
import * as mom from 'moment'
import * as moment from 'moment-timer'

import { Line} from 'react-chartjs-2';
import Dashboard from "../Dashboard";
import '../../App.css'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {ActCreators} from '../../redux/bindActionCretor'
import LoadingScreen from 'react-loading-screen';
import './SingleOperatorView.css'


let headers = [];
let dynamic_graph_data={};
let dynamic_graph_perhour_data={};
let same_date_data=false;

let dashboard_data={};
let asset_detail={};
const mapStateToProps=state=>{
    asset_detail=  state.graph_data;
};

const mapDispatchToProps=dispatch=>{
    return bindActionCreators(ActCreators, dispatch)
};


let months_wise=['people_in_per_day_data','people_out_per_day_data',
    'total_cycle_per_day_data','cycle_after_maintenance_day',
    'operating_hours_day','operating_hours_battery_day'];

let month_wise_field=['total_people_in_data_report','total_people_out_data_report'
    ,'total_cycle_data_report','total_cycle_maintenance','total_operating_hours',
'total_operating_hours_battery'];

let day_wise=['people_in_per_hour_data','people_out_per_hour_data',
    'total_cycle_per_hour_data','cycle_after_maintenance',
    'operating_hours','operating_hours_battery'];

let day_wise_field=['per_hour_in_step_data','per_hour_out_step_data'
    ,'per_hour_total_cycle_step_data','cycle_after_maintenance_hours','operating_hours_result',
    'operating_hours_battery_result']

let graph_fields=['peopleIn','peopleOut','totalCycles','cyclesAfterMaintenance','operatingHours','operatingHoursBattery']

let previous_graph_data={};
let filter_date_data=[];

let timezone_hours=0;
let timezone_min=0;
class SingleOperatorView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded:true,
            success:false,
            changeFilterDate:false,
            firstTimeDateChange:false,
            firstTimeLoad:true,
            dynamic_graph_data:{},
            dynamic_graph_perhour_data:{},
            filter_date:mom().format('YYYY-MM-DD'),
            todaDate:mom().format('YYYY-MM-DD'),
            filter_from_date:mom('2017-01-01').format('YYYY-MM-DD'),
            filter_to_date:mom().format('YYYY-MM-DD'),
            first_data_point_date:mom().format('YYYY-MM-DD'),
            csvData:[],
            total_cycle_data_report:0,
            total_people_in_data_report:0,
            total_people_out_data_report:0,
            total_cycle_maintenance:0,
            total_operating_hours:0,
            total_operating_hours_battery:0,
            per_hour_in_step_data:0,
            per_hour_out_step_data:0,
            per_hour_total_cycle_step_data:0,
            cycle_after_maintenance_hours:0,
            operating_hours_result:0,
            operating_hours_battery_result:0,
            people_in_per_day_data:[],
            people_in_per_hour_data:[],
            people_out_per_day_data:[],
            people_out_per_hour_data:[],
            total_cycle_per_day_data:[],
            total_cycle_per_hour_data:[],
            cycle_after_maintenance_day:[],
            cycle_after_maintenance:[],
            operating_hours:[],
            operating_hours_day:[],
            operating_hours_battery:[],
            operating_hours_battery_day:[],
            single_operator_data:[],
            export_all_data:[],
            graph_generation:[


                {
                    name:'total_cycle_per_day',
                    type:'Day',
                    label:'Total No Of Cycle',
                    graph_label:'Total Cycle',
                    graph_color:'#4dbd74',
                    graph_data:'total_cycle_per_day_data',
                    total_value:'total_cycle_data_report'
                },

                {
                    name:'people_in_per_day',
                    type:'Day',
                    label:'Total No Of People IN',
                    show:false,
                    graph_label:'People In',
                    graph_color:'#FF6384',
                    graph_data:'people_in_per_day_data',
                    total_value:'total_people_in_data_report'
                },

                {
                    name:'people_out_per_day',
                    type:'Day',
                    label:'Total No Of People Out',
                    show:false,
                    graph_label:'People Out',
                    graph_color:'#36A2EB',
                    graph_data:'people_out_per_day_data',
                    total_value:'total_people_out_data_report'
                },


                                            {
                                                name:'cycle_after_maintenance_day',
                                                type:'Day',
                                                label:'Total No Of Cycle After Maintenance',
                                                show:true,
                                                graph_label:'Cycle After Maintenance',
                                                graph_color:'#FF6384',
                                                graph_data:'cycle_after_maintenance_day',
                                                total_value:'total_cycle_maintenance'
                                            },


                                            {
                                                name:'operating_hours_day',
                                                type:'Day',
                                                label:'Total No Of Operating Hours',
                                                show:true,
                                                graph_label:'Operating Hours',
                                                graph_color:'#36A2EB',
                                                graph_data:'operating_hours_day',
                                                total_value:'total_operating_hours'
                                            },



                                            {
                                                name:'operating_hours_battery_day',
                                                type:'Day',
                                                label:'Total No Of Operating Hours Battery',
                                                show:true,
                                                graph_label:'Operating Hours Battery',
                                                graph_color:'#fbfb0e',
                                                graph_data:'operating_hours_battery_day',
                                                total_value:'total_operating_hours_battery'
                                            },

            ],
            graph_generation_per_hour:[

                {
                    name:'total_cycle_per_hour',
                    type:'Hour',
                    label:'Total No Of Cycle',
                    graph_label:'Total Cycle',
                    graph_color:'#4dbd74',
                    graph_data:'total_cycle_per_hour_data',
                    total_value:'per_hour_total_cycle_step_data'
                },
                {
                    name:'people_in_per_hour',
                    type:'Hour',
                    label:'Total No Of People IN',
                    show:false,
                    graph_label:'People In',
                    graph_color:'#FF6384',
                    graph_data:'people_in_per_hour_data',
                    total_value:'per_hour_in_step_data'
                },
                {
                    name:'people_out_per_hour',
                    type:'Hour',
                    label:'Total No Of People Out',
                    show:false,
                    graph_label:'People Out',
                    graph_color:'#36A2EB',
                    graph_data:'people_out_per_hour_data',
                    total_value:'per_hour_out_step_data'
                },

                {
                    name:'cycle_after_maintenance',
                    type:'Hour',
                    label:'Total No Of Cycle After Maintenance',
                    show:true,
                    graph_label:'Cycle After Maintenance',
                    graph_color:'#FF6384',
                    graph_data:'cycle_after_maintenance',
                    total_value:'cycle_after_maintenance_hours'
                },

                {
                    name:'operating_hours',
                    type:'Hour',
                    label:'Total No Of Operating Hours',
                    show:true,
                    graph_label:'Operating Hours',
                    graph_color:'#36A2EB',
                    graph_data:'operating_hours',
                    total_value:'operating_hours_result'
                },



                {
                    name:'operating_hours_battery',
                    type:'Hour',
                    label:'Total No Of Operating Hours Battery',
                    show:true,
                    graph_label:'Operating Hours Battery',
                    graph_color:'#fbfb0e',
                    graph_data:'operating_hours_battery',
                    total_value:'operating_hours_battery_result'
                },



            ],
            reset:{
                load_time:true,
                from_date:'',
                to_date:'',
            }
        };
    }

    componentDidMount(){
        dashboard_data={};
        var location_data=window.location.hash.split('/');
        dashboard_data.id=location_data[location_data.length-1];
        filter_date_data=[];
        dynamic_graph_data={};
        dynamic_graph_perhour_data={};
        same_date_data=false;


        if(asset_detail && !_.isEmpty(asset_detail) && asset_detail.data_metadata
            && !_.isEmpty(asset_detail.data_metadata)){
            var location= asset_detail.data_metadata.geolocation.split(',');

            if(location && location.length){
                fetch('https://maps.googleapis.com/maps/api/timezone/json?location='+location[0]+','+location[1]+'&timestamp='+ parseInt(mom().valueOf()/1000)+'&key=AIzaSyBgx7dDMktRN_ziLp70t6LNDC9sY70Pflo', {

                }).then((response) => response.text())
                    .then((responseText) => {
                        var res = JSON.parse(responseText);
                        var total_mins=((res.dstOffset+res.rawOffset)/60);
                        timezone_hours=parseInt(total_mins/60);
                        timezone_min=total_mins-(timezone_hours*60);
                    }).catch(err => err);
            }

        }


        this.GraphDataFn()
    }

    calculationFn(array,type,data_size){
                var this_pointer=this;
        var exist_data={};
        //var exist_data_index;

               /* if(this_pointer.state.filter_date==this_pointer.state.filter_from_date){
                    exist_data= _.findWhere(filter_date_data,{
                       date: mom(this_pointer.state.filter_date).format('YYYY-MM-DD')
                    });
                    exist_data_index= _.findIndex(filter_date_data,{
                       date: mom(this_pointer.state.filter_date).format('YYYY-MM-DD')
                    });
                }

                if(this_pointer.state.filter_date==this_pointer.state.filter_to_date){
                    exist_data= _.findWhere(filter_date_data,{
                       date:  mom(this_pointer.state.filter_date).subtract(1,'days').format('YYYY-MM-DD')
                    });

                    exist_data_index= _.findIndex(filter_date_data,{
                       date:  mom(this_pointer.state.filter_date).subtract(1,'days').format('YYYY-MM-DD')
                    });

                    if(!exist_data || _.isEmpty(exist_data)){
                        exist_data= _.findWhere(filter_date_data,{
                            date: mom(this_pointer.state.filter_date).format('YYYY-MM-DD')
                        });
                    }
                    if(exist_data_index && exist_data_index<0){
                        exist_data_index= _.findIndex(filter_date_data,{
                            date: mom(this_pointer.state.filter_date).format('YYYY-MM-DD')
                        });
                    }
                }

                if(this_pointer.state.filter_date>this_pointer.state.filter_to_date){
                    if(filter_date_data && filter_date_data.length){
                        exist_data= _.findWhere(filter_date_data,{
                            date:  filter_date_data[filter_date_data.length-1].date
                        });
                        exist_data_index= _.findIndex(filter_date_data,{
                            date:  filter_date_data[filter_date_data.length-1].date
                        });
                    }

                }

                if(this_pointer.state.filter_date<this_pointer.state.filter_from_date){
                    if(filter_date_data && filter_date_data.length) {
                        exist_data = _.findWhere(filter_date_data, {
                            date: filter_date_data[0].date
                        });
                        exist_data_index = _.findIndex(filter_date_data, {
                            date: filter_date_data[0].date
                        });
                    }
                }

                if(this_pointer.state.filter_date>this_pointer.state.filter_from_date &&
                    this_pointer.state.filter_date<this_pointer.state.filter_to_date ){
                    if(filter_date_data && filter_date_data.length) {
                        exist_data = _.findWhere(filter_date_data, {
                            date: filter_date_data[0].date
                        });
                        exist_data_index = _.findIndex(filter_date_data, {
                            date: filter_date_data[0].date
                        });
                    }
                }*/

     /*   var exist_data=_.findWhere(filter_date_data,{
            date:(this_pointer.state.filter_date==this_pointer.state.filter_from_date ||
                this_pointer.state.filter_date==this_pointer.state.filter_to_date) ?
                mom(this_pointer.state.filter_date).format('YYYY-MM-DD') :
                (this_pointer.state.filter_date>this_pointer.state.filter_to_date ?
                    filter_date_data[filter_date_data.length-1].date :
                    (this_pointer.state.filter_date<this_pointer.state.filter_from_date ?
                        filter_date_data[0].date
                    : mom(this_pointer.state.filter_date).subtract(1,'days').format('YYYY-MM-DD'))
                   )
        });
        var exist_data_index=_.findIndex(filter_date_data,{
            date:(this_pointer.state.filter_date==this_pointer.state.filter_from_date ||
                this_pointer.state.filter_date==this_pointer.state.filter_to_date) ?
                mom(this_pointer.state.filter_date).format('YYYY-MM-DD') :
                (this_pointer.state.filter_date>this_pointer.state.filter_to_date ?
                        filter_date_data[filter_date_data.length-1].date :
                        (this_pointer.state.filter_date<this_pointer.state.filter_from_date ?
                            filter_date_data[0].date
                            : mom(this_pointer.state.filter_date).subtract(1,'days').format('YYYY-MM-DD'))
                )
        });*/

            _.each(type=='month' ? months_wise : day_wise,function(data,index){


                if(data_size){
                    if(type=='month'){
                        this_pointer.state[data].push(previous_graph_data && previous_graph_data[data] ? previous_graph_data[data] : undefined)
                    }else{

                      /*  this_pointer.state[data].push(
                            ((this_pointer.state.filter_date==this_pointer.state.filter_from_date || this_pointer.state.filter_date==this_pointer.state.filter_to_date)
                            && !same_date_data) ? 0 : (this_pointer.state.filter_date < this_pointer.state.filter_from_date ?
                                same_date_data ? (exist_data && exist_data[data] ? (exist_data[data]) : 0) : 0
                            : (exist_data && exist_data[data] ? (exist_data[data]) : 0))
                        )*/

                        this_pointer.state[data].push(
                            ((this_pointer.state.filter_date==this_pointer.state.filter_from_date)
                            && !same_date_data) ? undefined :(exist_data && exist_data[data] ? parseInt(exist_data[data]) : undefined))

                    }

                }else{

                    var name=graph_fields[index];


                    var exist_array_data=_.max(array,function(exist_data){
                                            return exist_data.timestamp
                                        });

                   if(type=='month'){
                       //previous_graph_data[data]=array[array.length-1].data[name] ? array[array.length-1].data[name] : 0;
                       previous_graph_data[data]=exist_array_data && exist_array_data.data && exist_array_data.data[name] ? exist_array_data.data[name] : 0;
                   }else{
                       same_date_data=true;
                       exist_data[data]=exist_array_data && exist_array_data.data && exist_array_data.data[name] ? exist_array_data.data[name] : 0;
                          //filter_date_data[exist_data_index==-1 ? 0 : exist_data_index][data]=exist_array_data && exist_array_data.data && exist_array_data.data[name] ? exist_array_data.data[name] : 0;
                   }
                   this_pointer.state[data].push(exist_array_data && exist_array_data.data && exist_array_data.data[name] ? parseInt(exist_array_data.data[name]) : 0)
                }

            })
    }

    totalSumCalculationFn(type){
        var this_pointer=this;
        _.each(type=='month' ? months_wise : day_wise,function(data,index){
            var name=type=='month' ? month_wise_field[index] : day_wise_field[index];
            this_pointer.state[name]=(this_pointer.state[data][this_pointer.state[data].length-1] ? this_pointer.state[data][this_pointer.state[data].length-1] : 0)
        });

    }
    changeFilterDateFn(){
        this.state.filter_from_date=this.state.graph_date;
        this.setState({filter_from_date:this.state.graph_date,success:false})
        this.GraphDataFn()
    }


    exportAllDataFn(){
       this.setState({loaded:true})
        fetch('https://xwwhnoga55.execute-api.us-west-2.amazonaws.com/Prod/api/Assets/'+dashboard_data.id, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "beginTimestamp": parseInt(mom('2018-01-01').valueOf()/1000),
                "endTimestamp": parseInt(mom().valueOf()/1000),
                "id": dashboard_data.id,
            }),

        }).then((response) => response.text())
            .then((responseText) => {


                var res=JSON.parse(responseText);

                if(res){
                    this.state.export_all_data =[];

                    let customer_information_data=[];
                    let customer_information_headers=[];
                    let export_all_data=[];
                    let export_all_headers=[];


                    var dashboarddata={};
                    dashboarddata=res.asset;



                    if(_.isObject(res.asset.metadata)){
                        dashboarddata.data_metadata=res.asset.metadata;
                    }else{
                        dashboarddata.data_metadata=JSON.parse(res.asset.metadata);
                    }


                    if(dashboarddata && dashboarddata.data_metadata && !_.isEmpty(dashboarddata.data_metadata)){
                        if(dashboarddata.id){
                            customer_information_headers.push('id');
                            customer_information_data.push(dashboarddata.id)
                        }


                        _.each(dashboarddata.data_metadata,function(val,key){
                            customer_information_headers.push(key);
                            customer_information_data.push(val)
                        });
                    }

                    export_all_data.push(customer_information_headers);
                    export_all_data.push(customer_information_data);
                    export_all_data.push([]);



                    if(res && res.asset && res.asset.data){
                        _.each(res.asset.data,function(val,key){
                            if(key!='UUID' && key!='errors' ){
                                export_all_headers.push(key)
                            }
                        });
                    }


                    if(res.history && res.history.length){
                        this.setState({filter_to_date:mom(res.asset.timestamp*1000).format('YYYY-MM-DD')});
                        this.state.filter_to_date=mom(res.asset.timestamp*1000).format('YYYY-MM-DD')
                    }



                    var from_filter_date=mom(this.state.filter_from_date).format('DD');
                    var to_filter_date=mom(this.state.filter_to_date).format('DD');
                    var from_month=mom(this.state.filter_from_date).format('MM');
                    var to_month=mom(this.state.filter_to_date).format('MM');
                    var from_year=mom( this.state.filter_from_date).format('YYYY');
                    var to_year=mom( this.state.filter_to_date).format('YYYY');

                    var month_count_loop=mom([to_year, to_month, to_filter_date]).diff(mom([from_year, from_month, from_filter_date]), 'days')+2;


                    export_all_data.push(export_all_headers);
                    //month wise data
                    for(var i=1;i<=(_.isNaN(month_count_loop) ? 2 : month_count_loop);i++) {
                        var excel_data={};
                        if(i<=9){
                            i='0'+i
                        }
                        if(((from_filter_date).toString()).length==1){
                            from_filter_date='0'+from_filter_date
                        }
                        if((from_month.toString()).length==1){
                            from_month='0'+from_month
                        }

                        var total_days_in_month=mom(from_year+'-'+from_month, "YYYY-MM").daysInMonth()

                        var date=from_year + '-' + from_month + '-' + from_filter_date;
                        if(res.history && res.history.length){
                            var exist_data = _.filter(res.history, function(data){
                                if(data && data.data && data.data.timeStamp){
                                    return mom(data.data.timeStamp).format('YYYY-MM-DD')==date
                                }
                            });

                            if (exist_data && exist_data.length) {

                                _.each(exist_data,function(data){
                                    var excelData=[];
                                    excel_data={};


                                    _.each(export_all_headers,function(header_key){
                                            excelData.push(
                                                (data.data[header_key] == 'true' || data.data[header_key] == true ? data.data[header_key] : (
                                                    data.data[header_key] == 'false' || data.data[header_key] == false ? 'false' : (
                                                        data.data[header_key] ? data.data[header_key] : '-'
                                                    )
                                                ))
                                            )
                                    });
                                    export_all_data.push(excelData);

                                })
                            }
                        }




                        if(parseInt(total_days_in_month)==parseInt(from_filter_date)){
                            if(parseInt(from_month)==12){
                                from_year=parseInt(from_year)+1
                                from_month=1;
                            }else{
                                from_month=parseInt(from_month)+1;
                            }
                            from_filter_date=1;

                        }else{
                            from_filter_date++;
                        }

                    }
                    this.setState({export_all_data:export_all_data,loaded:false});
                    document.getElementById("csvlink").click();

                }

            }).catch(err => err);
    }


    GraphDataFn(keyname,e){
        var this_pointer=this;
        var sec=0;
        var after_api=0;
        var response_time=true;
        previous_graph_data={};
        filter_date_data=[];
        this.state.loaded=true;
        same_date_data=false;
        if(e && keyname){
            this.setState({[keyname]:e.target.value})
            this.state[keyname]=e.target.value;
        }


        fetch('https://xwwhnoga55.execute-api.us-west-2.amazonaws.com/Prod/api/Assets/'+dashboard_data.id, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "beginTimestamp": parseInt(mom(this.state.filter_from_date).startOf('day').valueOf()/1000),
                "endTimestamp": parseInt(mom(this.state.filter_to_date).endOf('day').valueOf()/1000),
                "id": dashboard_data.id,
            }),

        }).then((response) => response.text())
            .then((responseText) => {
                response_time=false;
                var res=JSON.parse(responseText);
                if(res){

                    res.history=_.filter(res.history,function(data){
                        return !_.isNull(data.data)
                    });
                    if(res.history && res.history.length || this.state.firstTimeDateChange){


                        console.log(res)

                        if(_.isObject(res.asset.metadata)){
                            res.asset.data_metadata=res.asset.metadata;
                        }else{
                            res.asset.data_metadata=JSON.parse(res.asset.metadata);
                        }
                        dashboard_data=res.asset;


                        if(dashboard_data.model=='EL301'){
                            dashboard_data.showes200=false
                        }else{
                            dashboard_data.showes200=true
                        }
                        var last_datapoints=_.last(_.sortBy(res.history,'timestamp'));

                        if(last_datapoints && !_.isEmpty(last_datapoints)){

                            this.setState({
                                filter_to_date:mom(last_datapoints.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
                                filter_date:mom(last_datapoints.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
                            });


                            this.state.filter_to_date=mom(last_datapoints.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD');
                            this.state.filter_date=mom(last_datapoints.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD');
                        }

                        var first_datapoints=_.first(_.sortBy(res.history,'timestamp'));
                        if(this.state.firstTimeLoad){
                            this.setState({
                                first_data_point_date:first_datapoints && !_.isEmpty(first_datapoints) ? mom(first_datapoints.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD') : this_pointer.state.filter_from_date,
                                todaDate:mom(res.asset.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
                                firstTimeLoad:false
                            })
                        }
                        this.getMonthWiseGraphDataFn(res)
                        this.getHourlyGraphDataFn(res)

                    }
                    else if(!this.state.firstTimeDateChange){
                        this.setState({
                            firstTimeDateChange:true,
                            filter_from_date:mom(res.asset.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
                            filter_to_date:mom(res.asset.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
                            filter_date:mom(res.asset.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
                            first_data_point_date:mom(res.asset.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
                            todaDate:mom(res.asset.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
                        });
                        this.state.filter_from_date=mom(res.asset.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD');
                        this.state.filter_to_date=mom(res.asset.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD');
                        this.state.filter_date=mom(res.asset.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD');
                        this.state.firstTimeDateChange=true;
                        this.GraphDataFn();
                    }

                }else {
                    this.setState({loaded:false})
                }
            }).catch(err => err);
    }



    getMonthWiseGraphDataFn(historyData){
        var this_pointer=this;
        var graph_data=this.state.graph_generation;
        var graph_data_hour=this.state.graph_generation_per_hour;
        if(dashboard_data.model=='EL301'){
            graph_data_hour[0]['show']=false;
            graph_data[0]['show']=false;
        }else if(dashboard_data.model=='ES200'){
            graph_data_hour[0]['show']=true;
            graph_data[0]['show']=true;
        }else{
            graph_data_hour[0]['show']=true;
            graph_data[0]['show']=true;
            graph_data_hour[5]['show']=false;
            graph_data[5]['show']=false;
        }

        if(this.state.reset.load_time){

            var reset_data=this.state.reset;
            reset_data.from_date=this.state.filter_from_date;
            reset_data.to_date=this.state.filter_to_date;
            reset_data.load_time=false;
            this.setState({reset:reset_data,
            })

        }

        let customer_info=[];
        let customer_info_headers=[];
        let excel_all_data=[];
        let export_info_headers=[];

        var month_wise_data=false;

        if(dashboard_data && dashboard_data.data_metadata && !_.isEmpty(dashboard_data.data_metadata)){
            if(dashboard_data.id){
                customer_info_headers.push('id');
                customer_info.push(dashboard_data.id)
            }


            _.each(dashboard_data.data_metadata,function(val,key){
                customer_info_headers.push(key);
                customer_info.push(val)
            });
        }

        excel_all_data.push(customer_info_headers);
        excel_all_data.push(customer_info);
        excel_all_data.push([]);


        if(historyData && historyData.asset && historyData.asset.data){
            _.each(historyData.asset.data,function(val,key){
                if(key!='UUID' && key!='errors' ){
                    export_info_headers.push(key)
                }
            });
        }

        excel_all_data.push(export_info_headers);

        var last_datapoints=_.last(_.sortBy(historyData.history,'timestamp'));

        if(last_datapoints && !_.isEmpty(last_datapoints)){

            this.setState({
                filter_to_date:mom(last_datapoints.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
            });


            this.state.filter_to_date=mom(last_datapoints.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD');
        }


        var from_filter_date=mom(this.state.filter_from_date).format('DD');
        var to_filter_date=mom(this.state.filter_to_date).format('DD');
        var from_month=mom(this.state.filter_from_date).format('MM');
        var to_month=mom(this.state.filter_to_date).format('MM');
        var from_year=mom( this.state.filter_from_date).format('YYYY');
        var to_year=mom( this.state.filter_to_date).format('YYYY');

        var month_count_loop=mom([to_year, to_month, to_filter_date]).diff(mom([from_year, from_month, from_filter_date]), 'days')+2;


        this.setState({
            people_in_per_day_data:[],
            people_out_per_day_data:[],
            total_cycle_per_day_data:[],
            cycle_after_maintenance_day:[],
            operating_hours_day:[],
            operating_hours_battery_day:[],
            graph_generation:graph_data,
            graph_generation_per_hour:graph_data_hour
        });

        let graph_dynamic_x_axis_label=[];
        //month wise data
        for(var i=1;i<=(_.isNaN(month_count_loop) ? 2 : month_count_loop);i++) {
            var excel_data={}
            if(i<=9){
                i='0'+i
            }
            if(((from_filter_date).toString()).length==1){
                from_filter_date='0'+from_filter_date
            }
            if((from_month.toString()).length==1){
                from_month='0'+from_month
            }

            var total_days_in_month=mom(from_year+'-'+from_month, "YYYY-MM").daysInMonth()

            var date=from_year + '-' + from_month + '-' + from_filter_date;
            if(historyData.history && historyData.history.length){
                var exist_data = _.filter(historyData.history, function(data){
                    if(data && data.data && data.data.timeStamp){
                        return mom(data.data.timeStamp).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD')==date
                    }
                });

                if (exist_data && exist_data.length) {
                    month_wise_data=true;
                    graph_dynamic_x_axis_label.push(date);
                    this.state.filter_from_date=graph_dynamic_x_axis_label[0];
                    filter_date_data.push({
                        date:date,
                        people_in_per_hour_data:exist_data[exist_data.length-1].data['peopleIn'] ? exist_data[exist_data.length-1].data['peopleIn'] : 0,
                        people_out_per_hour_data:exist_data[exist_data.length-1].data['peopleOut'] ? exist_data[exist_data.length-1].data['peopleOut'] : 0,
                        total_cycle_per_hour_data:exist_data[exist_data.length-1].data['totalCycles'] ? exist_data[exist_data.length-1].data['totalCycles'] : 0,
                        cycle_after_maintenance:exist_data[exist_data.length-1].data['cyclesAfterMaintenance'] ? exist_data[exist_data.length-1].data['cyclesAfterMaintenance'] : 0,
                        operating_hours:exist_data[exist_data.length-1].data['operatingHours'] ? exist_data[exist_data.length-1].data['operatingHours'] : 0,
                        operating_hours_battery:exist_data[exist_data.length-1].data['operatingHoursBattery'] ? exist_data[exist_data.length-1].data['operatingHoursBattery'] : 0,
                    });
                    this_pointer.calculationFn(exist_data,'month');

                    _.each(exist_data,function(data){
                        var excelData=[];
                        excel_data={};


                        _.each(export_info_headers,function(header_key){
                            excelData.push(
                                (data.data[header_key] == 'true' || data.data[header_key] == true ? data.data[header_key] : (
                                    data.data[header_key] == 'false' || data.data[header_key] == false ? 'false' : (
                                        data.data[header_key] ? data.data[header_key] : '-'
                                    )
                                ))
                            )
                        });
                        excel_all_data.push(excelData);
                    })
                } else if(month_wise_data) {
                    graph_dynamic_x_axis_label.push(date);
                    this.state.filter_from_date=graph_dynamic_x_axis_label[0];
                    filter_date_data.push({
                        date:date,
                        people_in_per_hour_data:previous_graph_data['people_in_per_day_data'] ? previous_graph_data['people_in_per_day_data'] : 0,
                        people_out_per_hour_data:previous_graph_data['people_out_per_day_data'] ? previous_graph_data['people_out_per_day_data'] : 0,
                        total_cycle_per_hour_data:previous_graph_data['total_cycle_per_day_data'] ? previous_graph_data['total_cycle_per_day_data'] : 0,
                        cycle_after_maintenance:previous_graph_data['cycle_after_maintenance_day'] ? previous_graph_data['cycle_after_maintenance_day'] : 0,
                        operating_hours:previous_graph_data['operating_hours_day'] ? previous_graph_data['operating_hours_day'] : 0,
                        operating_hours_battery:previous_graph_data['operating_hours_battery_day'] ? previous_graph_data['operating_hours_battery_day'] : 0,
                    });
                    this_pointer.calculationFn(exist_data,'month','no_data');
                }
            }else{
                graph_dynamic_x_axis_label.push(date);
                this.state.filter_from_date=graph_dynamic_x_axis_label[0];
                filter_date_data.push({
                    date:date,
                    people_in_per_hour_data:previous_graph_data['people_in_per_day_data'] ? previous_graph_data['people_in_per_day_data'] : 0,
                    people_out_per_hour_data:previous_graph_data['people_out_per_day_data'] ? previous_graph_data['people_out_per_day_data'] : 0,
                    total_cycle_per_hour_data:previous_graph_data['total_cycle_per_day_data'] ? previous_graph_data['total_cycle_per_day_data'] : 0,
                    cycle_after_maintenance:previous_graph_data['cycle_after_maintenance_day'] ? previous_graph_data['cycle_after_maintenance_day'] : 0,
                    operating_hours:previous_graph_data['operating_hours_day'] ? previous_graph_data['operating_hours_day'] : 0,
                    operating_hours_battery:previous_graph_data['operating_hours_battery_day'] ? previous_graph_data['operating_hours_battery_day'] : 0,
                });
                this_pointer.calculationFn(exist_data,'month','no_data');
            }




            if(parseInt(total_days_in_month)==parseInt(from_filter_date)){
                if(parseInt(from_month)==12){
                    from_year=parseInt(from_year)+1
                    from_month=1;
                }else{
                    from_month=parseInt(from_month)+1;
                }
                from_filter_date=1;

            }else{
                from_filter_date++;
            }

        }
        this.totalSumCalculationFn('month');


        _.each(this.state.graph_generation,function(graph_value){
            this_pointer.renderGraphViewFn(graph_value,graph_dynamic_x_axis_label,'dynamic_graph_data')
        });


        this.setState({
            dynamic_graph_data:dynamic_graph_data,
            csvData:excel_all_data,
            loaded:false,
            filter_from_date:graph_dynamic_x_axis_label && graph_dynamic_x_axis_label.length ? graph_dynamic_x_axis_label[0] : this_pointer.state.filter_from_date,
        });

    }


    getHourlyGraphDataFn(historyData){
        var this_pointer=this;
        this.setState({
            people_in_per_hour_data:[],
            people_out_per_hour_data:[],
            total_cycle_per_hour_data:[],
            cycle_after_maintenance:[],
            operating_hours:[],
            operating_hours_battery:[],
        });


        var filter_dte=mom(this.state.filter_date).format('YYYY-MM-DD');
        var today_date=mom(this.state.filter_date).format('DD');
        var today_month=mom(this.state.filter_date).format('MM');
        var today_year=mom(this.state.filter_date).format('YYYY');

        //hour wise data
        var exist_per_hour_data = _.filter(historyData.history, function(data){
            if(data && data.data && data.data.timeStamp){
                return mom(data.data.timeStamp).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD')==filter_dte
            }
        });

        var last_datapoints=_.last(_.sortBy(exist_per_hour_data,'timestamp'));

        if(last_datapoints && !_.isEmpty(last_datapoints)){
            this.setState({
                filter_date:mom(last_datapoints.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD'),
            });

            this.state.filter_date=mom(last_datapoints.timestamp*1000).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD');

            var last_datapoints_hour= parseInt(mom(last_datapoints.data.timeStamp).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('HH'))
        }


        for(var i=1;i<=(last_datapoints_hour && last_datapoints_hour>=0 ? last_datapoints_hour+1 :24);i++) {

            var from=i-1;
            if(from<=9){
                from='0'+from
            }
            var from_date = today_year + '-' + today_month + '-' + today_date + 'T' + (from) + ':00:00.000Z';
            var to_date = today_year + '-' + today_month + '-' + today_date + 'T' + (i<=9 ? i='0'+i : i) + ':00:00.000Z';
            if (exist_per_hour_data && exist_per_hour_data.length) {


                var time_wise_data = _.filter(exist_per_hour_data, function (data) {
                    return mom(data.data.timeStamp).add(timezone_hours,'hour').add(timezone_min,'min').utc().isBetween(from_date, to_date)
                });

                if (time_wise_data && time_wise_data.length) {


                    this_pointer.calculationFn(time_wise_data,'day');


                } else {
                    this_pointer.calculationFn(time_wise_data,'day','no_data');
                }
            }else{
                this_pointer.calculationFn([],'day','no_data');
            }

        }
        this.totalSumCalculationFn('day');


        _.each(this.state.graph_generation_per_hour,function(graph_value){
            this_pointer.renderGraphViewFn(graph_value,[],'dynamic_graph_perhour_data')
        });

        this.setState({
            dynamic_graph_perhour_data:dynamic_graph_perhour_data,
            loaded:false
        });
    }


monthGraphFn(keyname,e){
    var response_time=true;
    previous_graph_data={};
    filter_date_data=[];
    this.state.loaded=true;
    same_date_data=false;
    if(e && keyname){
        this.setState({[keyname]:e.target.value})
        this.state[keyname]=e.target.value;
    }


    fetch('https://xwwhnoga55.execute-api.us-west-2.amazonaws.com/Prod/api/Assets/'+dashboard_data.id, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "beginTimestamp": parseInt(mom(this.state.filter_from_date).startOf('day').valueOf()/1000),
            "endTimestamp": parseInt(mom(this.state.filter_to_date).endOf('day').valueOf()/1000),
            "id": dashboard_data.id,
        }),

    }).then((response) => response.text())
        .then((responseText) => {
            response_time=false;
            var res=JSON.parse(responseText);
            if(res){

                res.history=_.filter(res.history,function(data){
                    return !_.isNull(data.data)
                });
                    this.getMonthWiseGraphDataFn(res)


            }else {
                this.setState({loaded:false})
            }
        }).catch(err => err);
}


    hourlyGraphFn(keyname,e){
        var this_pointer=this;
        same_date_data=false;
        this.state[keyname]=e.target.value;
        this.setState({loaded:true,filter_date:e.target.value});
        fetch('https://xwwhnoga55.execute-api.us-west-2.amazonaws.com/Prod/api/Assets/'+dashboard_data.id, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "beginTimestamp": parseInt(mom(this.state.filter_date).startOf('days').valueOf()/1000),
                "endTimestamp": parseInt(mom(this.state.filter_date).endOf('days').valueOf()/1000),
                "id": dashboard_data.id,
            }),

        }).then((response) => response.text())
            .then((responseText) => {

                var res=JSON.parse(responseText);
                if(res) {
                    console.log(res);
                    res.history=_.filter(res.history,function(data){
                        return !_.isNull(data.data)
                    });
                    this.getHourlyGraphDataFn(res)
                /*    this.setState({
                        people_in_per_hour_data:[],
                        people_out_per_hour_data:[],
                        total_cycle_per_hour_data:[],
                        cycle_after_maintenance:[],
                        operating_hours:[],
                        operating_hours_battery:[],
                    });
                    var filter_dte=mom(this.state.filter_date).format('YYYY-MM-DD');
                    var today_date=mom(this.state.filter_date).format('DD');
                    var today_month=mom(this.state.filter_date).format('MM');
                    var today_year=mom(this.state.filter_date).format('YYYY');

                    //hour wise data
                    var exist_per_hour_data = _.filter(res.history, function(data){
                        if(data && data.data && data.data.timeStamp){
                            return mom(data.data.timeStamp).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('YYYY-MM-DD')==filter_dte
                        }
                    });
                    var last_datapoints=_.last(_.sortBy(exist_per_hour_data,'timestamp'));


                    if(last_datapoints && !_.isEmpty(last_datapoints)){
                        var last_datapoints_hour= parseInt(mom(last_datapoints.data.timeStamp).add(timezone_hours,'hour').add(timezone_min,'min').utc().format('HH'))
                    }
                    for(var i=1;i<=(last_datapoints_hour && last_datapoints_hour>=0 ? last_datapoints_hour+1 :24);i++) {

                        var from=i-1;
                        if(from<=9){
                            from='0'+from
                        }
                        var from_date = today_year + '-' + today_month + '-' + today_date + 'T' + (from) + ':00:00.000Z';
                        var to_date = today_year + '-' + today_month + '-' + today_date + 'T' + (i<=9 ? i='0'+i : i) + ':00:00.000Z';
                        if (exist_per_hour_data && exist_per_hour_data.length) {


                            var time_wise_data = _.filter(exist_per_hour_data, function (data) {
                                return mom(data.data.timeStamp).add(timezone_hours,'hour').add(timezone_min,'min').utc().isBetween(from_date, to_date)
                            });

                            if (time_wise_data && time_wise_data.length) {


                                this_pointer.calculationFn(time_wise_data,'day');


                            } else {
                                this_pointer.calculationFn(time_wise_data,'day','no_data');
                            }
                        }else{
                            this_pointer.calculationFn([],'day','no_data');
                        }

                    }
                    this.totalSumCalculationFn('day');

                    _.each(this.state.graph_generation_per_hour,function(graph_value){
                        this_pointer.renderGraphViewFn(graph_value,[],'dynamic_graph_perhour_data')
                    });

                    this.setState({
                        dynamic_graph_perhour_data:dynamic_graph_perhour_data,
                        loaded:false
                    })*/

                }else {
                    this.setState({loaded:false})
                }
            }).catch(err => err);
    }


    renderGraphViewFn(graph_value,graph_dynamic_x_axis_label,graphDataName){

        var graph_attributes=  {
            labels: graph_value.type=='Day' ?
                graph_dynamic_x_axis_label :
                ['1am','2am','3am','4am','5am','6am','7am',
                    '8am','9am','10am','11am','12pm','1pm',
                    '2pm','3pm','4pm','5pm','6pm','7pm',
                    '8pm','9pm','10pm','11pm','12am']
            ,
            datasets: [
                {
                    label: graph_value.graph_label,
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: graph_value.graph_color,
                    borderColor: graph_value.graph_color,
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth:4,
                    pointBorderColor: graph_value.graph_color,
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: graph_value.graph_color,
                    pointHoverBorderColor: graph_value.graph_color,
                    pointHoverBorderWidth: 2,
                    pointRadius: 2,
                    pointHitRadius: 10,
                    spanGaps:true,
                    data: this.state[graph_value.graph_data],
                }],

        };
        graphDataName=='dynamic_graph_perhour_data' ? dynamic_graph_perhour_data[graph_value.name]=graph_attributes : dynamic_graph_data[graph_value.name]=graph_attributes;

    }
    renderGraphDataFn(data,graphName){
        var this_point=this;
       var max_data=parseInt(_.max(this_point.state[data.graph_data]));
       var min_data=parseInt(_.min(this_point.state[data.graph_data]));

        return (
            <Col
            style={{'margin-top':'20px'}}
            xs={'12'} sm={'12'} md={'12'} lg={'12'}>
            <div className="chart-wrapper">
                {!_.isEmpty(this.state.dynamic_graph_perhour_data) || !_.isEmpty(this.state.dynamic_graph_data) ?

                    <Line data={graphName=='dynamic_graph_perhour_data' ? this.state.dynamic_graph_perhour_data[data.name] :this.state.dynamic_graph_data[data.name]}
                          options={
                              data.name=='total_cycle_per_day' ?

                                  {
                                      legend: {
                                          display: true,
                                          fullWidth:true
                                      },
                                      tooltips: {
                                          callbacks: {
                                              label: function(tooltipItem) {
                                                  return 'Total Cycle:'+tooltipItem.yLabel +','+
                                                      'Avg Cycle:'+ ((this_point.state.total_cycle_per_day_data[tooltipItem.index]-
                                                          this_point.state.total_cycle_per_day_data[tooltipItem.index==0 ? tooltipItem.index :tooltipItem.index-1])/2);
                                              }
                                          }
                                      },
                                      scales: {
                                          xAxes: [
                                              {
                                                  scaleLabel: {
                                                      display: true,
                                                      labelString: [data.type],
                                                      ticks: {
                                                          autoSkip: true,
                                                          autoSkipPadding: 10
                                                      }
                                                  }}],
                                          yAxes: [
                                              {
                                                  scaleLabel: {
                                                      display: true,
                                                      labelString: [data.label]
                                                  },
                                                  ticks: {
                                                      autoSkip: true,
                                                      autoSkipPadding: 20,
                                                      max:max_data==0 ? 1 : parseInt(max_data + (max_data/4)),
                                                      suggestedMin: max_data==0 ? 0 :
                                                          ((min_data-(min_data/4))>0 ? parseInt(min_data-(min_data/4)) : 0),    // minimum will be 0, unless there is a lower value.
                                                      stepSize: max_data==0 ? 1 :parseInt(max_data/3)
                                                  }
                                              }
                                          ],
                                      }
                                  }
                                  :  {
                                      legend: {
                                          display: true,
                                          fullWidth:true
                                      },

                                      scales: {
                                          xAxes: [
                                              {
                                                  scaleLabel: {
                                                      display: true,
                                                      labelString: [data.type],
                                                      ticks: {
                                                          autoSkip: true,
                                                          autoSkipPadding: 10
                                                      }
                                                  }}],
                                          yAxes: [
                                              {
                                                  scaleLabel: {
                                                      display: true,
                                                      labelString: [data.label]
                                                  },
                                                  ticks: {
                                                      autoSkip: true,
                                                      autoSkipPadding: 20,
                                                      max:max_data==0 ? 1 : parseInt(max_data + (max_data/4)),
                                                      suggestedMin: max_data==0 ? 0 :
                                                          ((min_data-(min_data/4))>0 ? parseInt(min_data-(min_data/4)) : 0),    // minimum will be 0, unless there is a lower value.
                                                      stepSize: max_data==0 ? 1 :parseInt(max_data/3)
                                                  }
                                              }
                                          ],
                                      }
                                  }
                          }/>
                    : ''}

            </div>

        </Col>)
    }


    renderFilterDateViewFn(labelName,value,maxDate,minDate){
        return(
            <FormGroup>
            <Label htmlFor="name">
                <b>
                    {labelName}
                </b>
            </Label>
            <Input type="date"
        style={{'padding-left': '0px',
            'font-size': '13px',
        }}
        name="expiry_date"
        value={this.state[value]}
                   min={minDate ? this.state[minDate] : undefined}
        max={this.state[maxDate]}
        onChange={ labelName=='Date (per hour data)' ? this.hourlyGraphFn.bind(this,value) : this.monthGraphFn.bind(this,value)}
        required />
            </FormGroup>
        )
    }

    redirectFn(){
        this.setState({redirect:true})
    }
    setValueFn(key_name,e){
        this.setState({[key_name]:e.target.value})
    }
    resetDateFn(){


        this.setState({filter_from_date:this.state.reset.from_date,
            filter_to_date:this.state.reset.to_date});
this.state.filter_from_date=this.state.reset.from_date;
this.state.filter_to_date=this.state.reset.to_date;
        this.GraphDataFn();

    }

    render() {
        const this_point=this;
        if(this.state.redirect){
            return <Redirect to={{pathname:'/dashboard'}}
                             render={<Dashboard />}/>
        }
        return (


                <div className="animated fadeIn">
                    <LoadingScreen
                        loading={this.state.loaded}
                        bgColor='#f1f1f1'
                        spinnerColor='#9ee5f8'
                        textColor='#676767'
                        text='Loading'>
                    </LoadingScreen>
                    <Row>


                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'} >


                            <Card>
                                <CardHeader>

                                    <Row>
                                        <Col xs="10" lg="10">
                                             <h4>
                                                 {dashboard_data.id ?
                                                     <b>{dashboard_data.id}</b>
                                                     :''}
                                                 {dashboard_data.data_metadata && dashboard_data.data_metadata.city ?
                                                     <b style={{'color':'#63c2de'}}>
                                                         ({dashboard_data.data_metadata.city})
                                                     </b>
                                                     :''}
                                                 {dashboard_data.model  ?
                                                     <b style={{'color':'#63c2de'}}>({dashboard_data.model})</b>
                                                     :''}
                                             </h4>
                                        </Col>
                                        <Col xs="2" lg="2">
                                            <Button type="submit"
                                                    className="pull-right"
                                                    onClick={this.redirectFn.bind(this)}
                                                    size="sm"
                                                    color="primary">
                                                <i className="fa fa-chevron-left"></i> Back To Dashboard
                                            </Button>
                                        </Col>
                                    </Row>




                                </CardHeader>
                                <CardBody>


                                    <Row>
                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'}>

                                            <div className="chart-wrapper">

                                                <h5><b>{this.state.filter_from_date}&nbsp;to&nbsp;
                                                    {this.state.filter_to_date}</b></h5>

                                                {this.state.demoKey ? this.state.demoKey : ''}
                                            </div>
                                        </Col>
                                    </Row>




                                            <Row style={{'border-bottom':'2px dashed #b7b7b7'}}>

                                                <Col xs={'12'} sm={'12'} md={'3'} lg={'3'}>
                                                    <div className="chart-wrapper">
                                                        <h1 style={{'color':'#4dbd74'}}>
                                                            {this.state.total_cycle_data_report}
                                                            </h1>
                                                        <p>TotalCycles</p>
                                                    </div>
                                                </Col>
                                                {
                                                    dashboard_data.showes200 ?

                                                        <Col xs={'12'} sm={'12'} md={'9'} lg={'9'} style={{'padding':'0px'}}>
                                                            <Row>
                                                            <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                                                                <div className="chart-wrapper">

                                                                    <h1>
                                                      <span style={{'color':'#FF6384'}}>
                                                      {this.state.total_cycle_maintenance}
                                                      </span>
                                                                    </h1>
                                                                    <p>Total Cycles After Maintenance</p>
                                                                </div>

                                                            </Col>
                                                            <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                                                                <div className="chart-wrapper">

                                                                    <h1>
                                                                <span style={{'color':'#36A2EB'}}>
                                                      {this.state.total_operating_hours}

                                                      </span>
                                                                    </h1>
                                                                    <p>Operating Hours</p>
                                                                </div>

                                                            </Col>
                                                            {dashboard_data.model=='ES200'
                                                                ?
                                                                <Col xs={'12'} sm={'12'} md={'4'} lg={'4'}>

                                                                    <div className="chart-wrapper">

                                                                        <h1>
                                                                <span style={{'color':'#fbfb0e'}}>
                                                      {this.state.total_operating_hours_battery}

                                                      </span>
                                                                        </h1>
                                                                        <p>Operating Hours Battery</p>
                                                                    </div>

                                                                </Col>
                                                                : ''}
                                                            </Row>
                                                        </Col>



                                                        :
                                                        <Col xs={'12'} sm={'12'} md={'9'} lg={'9'}>

                                                            <div className="chart-wrapper">

                                                                <h1>
                                                      <span style={{'color':'#FF6384'}}>
                                                      {this.state.total_people_in_data_report}
                                                      </span>
                                                                    /
                                                                    <span style={{'color':'#36A2EB'}}>
                                                      {this.state.total_people_out_data_report}

                                                      </span>
                                                                </h1>
                                                                <p>People In /
                                                                    People Out</p>
                                                            </div>

                                                        </Col>
                                                }

                                            </Row>



                                            <Row style={{'margin-top':'10px'}}>
                                                <Col xs={'12'} sm={'12'} md={'2'} lg={'2'}>
                                                    {this.renderFilterDateViewFn('Date (per hour data)','filter_date','todaDate','first_data_point_date')}
                                                </Col>
                                                <Col xs={'12'} sm={'12'} md={'2'} lg={'2'}>
                                                    {this.renderFilterDateViewFn('From Date','filter_from_date','filter_to_date','first_data_point_date')}
                                                </Col>
                                                <Col xs={'12'} sm={'12'} md={'2'} lg={'2'}>
                                                    {this.renderFilterDateViewFn('To Date','filter_to_date','todaDate','filter_from_date')}
                                                </Col>


                                                <Col xs={'12'} sm={'12'} md={'1'} lg={'1'}>
                                                    <Button type="submit"
                                                            size="md"
                                                            onClick={this.resetDateFn.bind(this)}
                                                            style={{'margin-top':30}}
                                                            color="primary">
                                                        Reset
                                                    </Button>
                                                </Col>

                                                <Col xs={'12'} sm={'12'} md={'2'} lg={'2'}>
                                                    <CSVLink data={this.state.csvData}
                                                             target="_blank"
                                                             filename= {dashboard_data.id ? dashboard_data.id+'.csv' : 'graphData.csv'}>
                                                        <Button type="submit"
                                                                size="md"
                                                                style={{'margin-top':30}}
                                                                color="primary">
                                                            <i className="fa fa-file-excel-o"></i>&nbsp;Export Excel
                                                        </Button>
                                                    </CSVLink>
                                                </Col>
                                                <Col xs={'12'} sm={'12'} md={'2'} lg={'2'}
                                                     style={{'padding':'0'}}>

                                                    <CSVLink data={this.state.export_all_data}
                                                             id="csvlink"
                                                             target="_blank"
                                                             asyncOnClick={true}
                                                             onClick={(event,done) => {
                                                                console.log('event',event);
                                                                 done();
                                                             }}
                                                             filename= {dashboard_data.id ? dashboard_data.id+'.csv' : 'graphData.csv'}>
                                                    </CSVLink>



                                                        <Button type="submit"
                                                                size="md"
                                                                onClick={this.exportAllDataFn.bind(this)}
                                                                style={{'margin-top':30}}
                                                                color="primary">
                                                            <i className="fa fa-file-excel-o"></i>&nbsp;    Export All
                                                        </Button>

                                                </Col>
                                            </Row>




                                            <Row>



                                                <Col xs={'12'} sm={'12'} md={'6'} lg={'6'} style={{'padding':'0px'}}>
                                                {this.state.graph_generation_per_hour && this.state.graph_generation_per_hour.length ?
                                                    this.state.graph_generation_per_hour.map((data,index)=>(
                                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'} style={{'padding':'0px'}}>
                                                            {dashboard_data.showes200==data.show ?

                                                             this.renderGraphDataFn(data,'dynamic_graph_perhour_data')

                                                                : ''}




                                                        </Col>





                                                    ))
                                                    : ''}
                                                </Col>

                                                <Col xs={'12'} sm={'12'} md={'6'} lg={'6'} style={{'padding':'0px'}}>
                                                {this.state.graph_generation && this.state.graph_generation.length ?
                                                    this.state.graph_generation.map((data,index)=>(
                                                        <Col xs={'12'} sm={'12'} md={'12'} lg={'12'} style={{'padding':'0px'}}>
                                                            {dashboard_data.showes200==data.show ?

                                                             this.renderGraphDataFn(data,'dynamic_graph_data')

                                                                : ''}




                                                        </Col>





                                                    ))
                                                    : ''}
                                                </Col>



                                            </Row>


                                </CardBody>
                            </Card>

                        </Col>


                    </Row>


                    <Modal isOpen={this.state.success}
                           className={'modal-primary'}>
                        <ModalBody>

                            <p>
                            <b>
                                {this.state.modalMessage}
                                <span className={'text-primary'}>
                                    {this.state.graph_date}.
                                </span>
                            </b>
                            </p>


{this.state.changeFilterDate ?
    <span>
    <p>
        <b>If you want to change the from date  <span className={'text-primary'}>
                                {this.state.filter_from_date}&nbsp;
                            </span>
            to  <span className={'text-primary'}>
                                    &nbsp;{this.state.graph_date}
                                </span>

        </b>
    </p>


    <h5 className={'text-center'}>
        <a href="javascript:void 0" onClick={this.changeFilterDateFn.bind(this)}>Click here.</a>
    </h5>
    </span>: ''}


                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={()=>{this.setState({success:false})}}>Ok</Button>
                        </ModalFooter>
                    </Modal>

                </div>
        );
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(SingleOperatorView);
