
Message List
Loading history...

AmitKumar Bhomihar [3:48 PM]
image upload ka koi dena base 64 wala

AmitKumar Bhomihar [1:43 PM]
tu free nhi hua

Anand Soni [1:44 PM]
abhi ek-2 din bhot busy rahunga or bhai

AmitKumar Bhomihar [1:44 PM]
usko msg krne ke lie tim ehai kmine

Anand Soni [1:45 PM]
bhai kisko

AmitKumar Bhomihar [1:45 PM]
aur kisko

Anand Soni [1:45 PM]
abh m kisi ko msg nhi krta
sab khtm
sab band

AmitKumar Bhomihar [1:45 PM]
:face_with_rolling_eyes:

    Anand Soni [1:45 PM]
ye sab moh maya thi
sab band hogya h

AmitKumar Bhomihar [1:45 PM]
kyu

Anand Soni [1:46 PM]
kyu ki wajah thori hoti h bas hogya khtm

AmitKumar Bhomihar [1:46 PM]
ok

Anand Soni [1:46 PM]
tuje pta h na m kitna dur bhagta hu ladkiyo se

AmitKumar Bhomihar [1:46 PM]
ha wo to mujhe pata hia

Anand Soni [1:46 PM]
hahha.. toh bas yaha bhi same case hua
m free hot hu
dn help krta hu
tere

AmitKumar Bhomihar [1:47 PM]
ok

Anand Soni [11:02 AM]
whatsap par aa
tera vo issue solve hogya

Anand Soni [11:14 AM]
react-onevent
Untitled
import React, { Component } from 'react';
import './login.css'
import { Button, Card, Label,CardBody,
    CardGroup, Col, Container, Input, InputGroup,
    Modal, ModalBody, ModalFooter,
    ModalHeader,FormGroup,
    InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import logoo from './logo';
import { Redirect } from 'react-router-dom';
import AdminPage from '../../AdminPage/AdminPage';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import onEnter from 'react-onenterkeydown';
import OnEvent from 'react-onevent';
import LoadingScreen from 'react-loading-screen';
import { bindActionCreators } from 'redux'
import {ActCreators} from '../../../redux/bindActionCretor'
import {connect} from 'react-redux'
import Dashboard from "../../Dashboard";
import * as _ from "underscore";
var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const mapStateToProps=state=>{
    console.log('hi',state);
};
const mapDispatchToProps=dispatch=>{
    return bindActionCreators(ActCreators, dispatch)
};
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errorPassword:false,
            errorEmail:false,
            redirect:false,
            first_time_password_change:false,
            loaded:false
        };
    }
    componentDidMount(){
        this.props.Logout();
    }
    LoginFn(){
        //this.state.loaded=true;
        var this_pointer=this;
        //this_pointer.preventDefault();
        //this.email.focus();
        if(this.state.password==null || this.state.password=="" || this.state.email==null || this.state.email==""){
            if(this.state.password==null || this.state.password==""){
                this.setState({errorPassword:true})
                NotificationManager.error('Please enter the password');
            }else{
                this.setState({errorPassword:false})
            }
            if(this.state.email==null || this.state.email==""){
                this.setState({errorEmail:true})
                NotificationManager.error('Please enter the email');
            }else{
                this.setState({errorEmail:false})
            }
        }else{
            this.setState({errorPassword:false,errorEmail:false,loaded:true});
            /*  var authenticationData = {
               Username : 'support@jacksolutions.biz',
               Password : 'jack1234',
             };*/
            var authenticationData = {
                Username : this.state.email,
                Password : this.state.password,
            };
            var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
            var poolData = {
                UserPoolId: 'us-west-2_U24uJfY6N',
                ClientId:'7mm5cmo7rsj9m46q6s15p3lfl4'
            };
            var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
            var userData = {
                Username: authenticationData.Username,
                Pool: userPool
            };
            var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: function (result) {
                    var accessToken = result.getAccessToken().getJwtToken();
                    this_pointer.props.Login({email:this_pointer.state.email,idToken:result.idToken.jwtToken,
                        accessToken:accessToken});
                    console.log('accessToken',result.idToken.jwtToken)
                    this_pointer.setState({loaded:false,redirect:true})
                },
                onFailure: function(err) {
                    console.log('err',err);
                    if(err.code=='UnknownError'){
                        this_pointer.setState({loaded:false,
                            first_time_password_change:true,
                            confirm_password:'',
                            new_password:'',
                        })
                    }else{
                        this_pointer.setState({loaded:false})
                        NotificationManager.error(err.message);
                    }
                }
            });
            /*cognitoUser.changePassword('jack1234', 'jack', function(err, result) {
              if (err) {
                console.log('err',err);
                return;
              }
              console.log('call result: ' + result);
            });*/
            /*   if(this.state.email=='admin@dormakaba.com' && this.state.password=='123456'){
                this.props.Login({email:'admin@dormakaba.com'});
                this.setState({redirect:true})
              }else {
                NotificationManager.error('User Not Registered');
              }*/
        }
    }
    changePasswordFn(){
        var this_pointer=this;
        var authenticationData = {
            Username : this.state.email,
            Password : this.state.password,
        };
        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
        var poolData = {
            UserPoolId: 'us-west-2_U24uJfY6N',
            ClientId:'7mm5cmo7rsj9m46q6s15p3lfl4'
        };
        var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
        var userData = {
            Username: authenticationData.Username,
            Pool: userPool
        };
        var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            newPasswordRequired: (userAttributes, requiredAttributes) => {
                delete userAttributes.email_verified;
                userAttributes.family_name = "AutoComplete by app";
                userAttributes.name = "app user";
                cognitoUser.completeNewPasswordChallenge(
                    this.state.new_password,
                    userAttributes,
                    {
                        onSuccess: (user) => {
                            console.log('success', user);
                            this_pointer.setState({password:'',first_time_password_change:false})
                            NotificationManager.success('Please login again');
                        },
                        onFailure: (error) => {
                            console.log(error);
                            NotificationManager.error(error.message);
                        },
                    },
                );
                /* cognitoUser.onSuccess(function(result){
                   console.log('result',result);
                   this_pointer.setState({password:'',first_time_password_change:false})
                   NotificationManager.success('Please login again');
                 });
                 cognitoUser.onFailure(function(err){
                   console.log('err',err);
                   NotificationManager.error(err.message);
                 });*/
            }
        });
    }
    checkErrorFn(){
        if(this.state.password==null || this.state.password=="" || this.state.email==null || this.state.email==""){
            if(this.state.password==null || this.state.password==""){
                this.setState({errorPassword:true})
            }else{
                this.setState({errorPassword:false})
            }
            if(this.state.email==null || this.state.email==""){
                this.setState({errorEmail:true})
            }else{
                this.setState({errorEmail:false})
            }
        }else{
            this.setState({errorPassword:false})
            this.setState({errorEmail:false})
        }
    }
    render() {
        if(this.state.redirect){
            this.props.history.push({pathname:'/dashboard'})
            /*  return <Redirect to={{pathname:'/dashboard'}}
                       render={<Dashboard />}/>*/
        }
        return (
            <div className="app flex-row align-items-center">
                <LoadingScreen
                    loading={this.state.loaded}
                    bgColor='#f1f1f1'
                    spinnerColor='#9ee5f8'
                    textColor='#676767'
                    text='Loading'>
                </LoadingScreen>
                <Container>
                    <Row className="justify-content-center">
                        <Col md="8">
                            <CardGroup>
                                <Card className="p-4">
                                    <CardBody>
                                        <Row>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                <img src={logoo}
                                                     className="imgHeight"
                                                     alt="admin@bootstrapmaster.com" />
                                            </Col>
                                        </Row>
                                        <h1>Login</h1>
                                        <InputGroup className="mb-3">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="icon-user"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <OnEvent enter={ this.LoginFn.bind(this)}>
                                                <Input type="email"
                                                       value={this.state.email}
                                                       required
                                                       ref={this.email}
                                                       className={this.state.errorEmail ? 'errorBrd':''}
                                                       onChange={(e)=>{this.setState({email:e.target.value});this.checkErrorFn()}}
                                                       placeholder="Email" />
                                            </OnEvent>
                                        </InputGroup>
                                        <InputGroup className="mb-4">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="icon-lock"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <OnEvent enter={ this.LoginFn.bind(this)}>
                                                <Input type="password"
                                                       value={this.state.password}
                                                       required
                                                       className={this.state.errorPassword ? 'errorBrd':''}
                                                       onChange={(e)=>{this.setState({password:e.target.value});this.checkErrorFn()}}
                                                       placeholder="Password" />
                                            </OnEvent>
                                        </InputGroup>
                                        <Row>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-right">
                                                <Input className="form-check-input"
                                                       type="checkbox" id="checkbox1"
                                                       name="checkbox1" value="option1" />
                                                <Label check className="form-check-label rememberMe"
                                                       htmlFor="checkbox1">Remember Me</Label>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                <Button color="primary"
                                                        type={'button'}
                                                        onClick={this.LoginFn.bind(this)}
                                                        className="px-4">Login</Button>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                <Button color="link" className="px-0">Forgot password?</Button>
                                            </Col>
                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                <p className="text-muted">New to Entry as a Service? Sign up for a
                                                    <Button color="link" className="px-0">free account</Button>
                                                </p>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                </Card>
                            </CardGroup>
                            <NotificationContainer/>
                        </Col>
                    </Row>
                </Container>
                <Modal isOpen={this.state.first_time_password_change}
                       className={'modal-info modal-lg'}>
                    <ModalHeader>To continue, change the password</ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                <FormGroup>
                                    <Label htmlFor="name">New Password</Label>
                                    <Input type="password"
                                           name="new_password"
                                           ref={'username'}
                                           onChange={(e)=>{this.setState({new_password:e.target.value})}}
                                           value={this.state.new_password}
                                           required />
                                </FormGroup>
                            </Col>
                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                <FormGroup>
                                    <Label htmlFor="name">Confirm Password
                                        {this.state.new_password && this.state.confirm_password &&
                                        (this.state.new_password!=this.state.confirm_password) ?
                                            <span className={'text-danger'}>&nbsp;Password and confirm password must be same</span>
                                            : ''}
                                    </Label>
                                    <Input type="password"
                                           name="c_password"
                                           onChange={(e)=>{this.setState({confirm_password:e.target.value})}}
                                           value={this.state.confirm_password}
                                           required />
                                </FormGroup>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary"
                                onClick={this.changePasswordFn.bind(this)}
                                disabled={!this.state.new_password || !this.state.confirm_password
                                || this.state.new_password!=this.state.confirm_password ? true : false}
                        >Submit</Button>
                        <Button color="secondary"
                                onClick={(e)=>{this.setState({first_time_password_change:false})}}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Login);
