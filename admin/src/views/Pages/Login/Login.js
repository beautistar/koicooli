import React, {Component} from 'react';
import './login.css'
import {
    Button, Card, Label, CardBody,
    CardGroup, Col, Container, Input, InputGroup,
    Modal, ModalBody, ModalFooter,
    ModalHeader, FormGroup,
    InputGroupAddon, InputGroupText, Row, Form
} from 'reactstrap';
import logoo from './logo';
import OnEvent from 'react-onevent'
import {Redirect} from 'react-router-dom';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

import LoadingScreen from 'react-loading-screen';
import {bindActionCreators} from 'redux'
import {ActCreators} from '../../../redux/bindActionCretor'
import {connect} from 'react-redux'
import Dashboard from "../../Dashboard";
import AdminPage from "../../Admin/User";
import ForgotPassword from "../../Admin/User";
import * as _ from "underscore";
//import ForgotPassword from "../../Admin/User/ForgotPassword";

const commonHostUrl = require('../../../../src/constant');
var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const mapStateToProps = state => {
    console.log('hi', state);
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActCreators, dispatch)
};


class Login extends Component {


    constructor(props) {
        super(props);
        this.state = {
            errorPassword: false,
            errorEmail: false,
            redirect: false,
            forgotPassoword: false,
            first_time_password_change: false,
            loaded: false,
            loginUser: null,
            email1: null,
            emailValidation: false,
            hasError: false,
            hasErrorInfo: ""
        };
    }

    /*componentDidMount() {
        this.props.Logout();
    }*/

    //store success login data to localstorage
    onSetLoginLocalStorage = (result, key) => {
        result.data.token = result.token
        this.setState({loginUser: result.data});
        localStorage.setItem('User', JSON.stringify(this.state.loginUser));
    }

    redirectPassFn() {
        this.setState({forgotPassword: true})
    }

    LoginFn() {
        var this_pointer = this;
        //this.email.focus();
        if (this.state.password == null || this.state.password == "" || this.state.email == null || this.state.email == "") {
            if (this.state.password == null || this.state.password == "") {
                this.setState({errorPassword: true})
            } else {
                this.setState({errorPassword: false})
            }
            if (this.state.email == null || this.state.email == "") {
                this.setState({errorEmail: true})
            } else {
                this.setState({errorEmail: false})
            }
        }
        else {
            this.setState({errorPassword: false, errorEmail: false, loaded: true});

            //let apiBaseUrl = "http://192.168.0.120:1365/login";
            let apiBaseUrl = commonHostUrl.url + "login";
            let authenticationData = {
                email: this.state.email,
                password: this.state.password,
            };
            console.log("Aut-------", authenticationData)

            fetch(apiBaseUrl, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(authenticationData),
            }).then((res) => res.json())
                .then((result) => {
                    if (result.code == 200) {
                        /* //generating localstorage
                         this.onSetLoginLocalStorage(result, authenticationData)
                         //redirect to dashboard
                         this.props.Login({email: this_pointer.state.email});
                         this.setState({loaded: false, redirect: true,});

                         NotificationManager.success("Logged In Successfully");*/
                        if (result.data && (result.data.type === 3 || result.data.type === 4)) {
                            this.setState({loaded: false, redirect: false,});
                            NotificationManager.error("Un-Authorized Access");
                        }
                        else {
                            //generating localstorage
                            this.onSetLoginLocalStorage(result, authenticationData)
                            //redirect to dashboard
                            this.props.Login({email: this_pointer.state.email});
                            this.setState({loaded: false, redirect: true,});

                            NotificationManager.success("Logged In Successfully");
                        }
                        //this.props.history.push('/dashboard')
                        console.log("Login successfull", result.data);
                    }
                    else if (result.code == 403) {
                        this.setState({loaded: false, redirect: false});
                        console.log("Username password do not match");
                        NotificationManager.error(result.message);
                    }
                    else {
                        NotificationManager.error("Username does not exists");
                    }
                }).catch((error) => {
                console.log("Err In Login---------", error);
            });

        }

    }

    changePasswordFn() {
        var this_pointer = this;
        //this.email.focus();
        if (this.state.email1 == null || this.state.email1 == "" || this_pointer.state.email1 == '') {
            this.setState({errorEmail: true})
        } else {
            this.setState({errorEmail: false, loaded: true});

            //let apiBaseUrl = "http://192.168.0.120:1365/login";
            let apiBaseUrl = commonHostUrl.url + "forgot-password";
            let authenticationData = {
                email: this.state.email1,
            };
            console.log("Aut---Email----", authenticationData)

            fetch(apiBaseUrl, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(authenticationData),
            })
                .then((res) => res.json())
                .then((result) => {
                    if (result.code == 200) {
                        //generating localstorage
                        this_pointer.setState({loaded: false, redirect: true});
                        NotificationManager.success(result.message);
                        this.props.history.push('/login')
                    }
                    else if (result.code == 403) {
                        this_pointer.setState({loaded: false, redirect: false, forgotPassword: false});
                        NotificationManager.error(result.message);
                    }
                    else {
                        NotificationManager.error("User does not exist");
                    }


                })

        }

    }


    checkErrorFn() {
        if (this.state.password == null || this.state.password == "" || this.state.email == null || this.state.email == "") {
            if (this.state.password == null || this.state.password == "") {
                this.setState({errorPassword: true})
            } else {
                this.setState({errorPassword: false})
            }
            if (this.state.email == null || this.state.email == "") {
                this.setState({errorEmail: true})
            }
            else if (this.state.email1 == null || this.state.email1 == "") {
                this.setState({errorEmail: true})
            }
            else {
                this.setState({errorEmail: false})
            }
        } else {
            this.setState({errorPassword: false})
            this.setState({errorEmail: false})
        }
    }

    setDataFn(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value})

        if (name == 'email1') {
            let filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!filter.test(value)) {

                this.setState({emailValidation: true})

            } else {
                this.setState({emailValidation: false})
            }
        }

    }


    render() {
        console.log("State-----", this.state)
        if (this.state.redirect) {
            this.props.history.push({
                pathname: "/block",
            });
            /* return <Redirect to={{pathname: '/user'}}
                              render={<AdminPage/>}/>*/
        }
        if (this.state.redirectPass) {
            return <Redirect to={{pathname: '/user/forgot-password'}}
                             render={<ForgotPassword/>}/>
        }

        return (
            <div className="app flex-row align-items-center">

                <LoadingScreen
                    loading={this.state.loaded}
                    bgColor='#f1f1f1'
                    spinnerColor='#9ee5f8'
                    textColor='#676767'
                    text='Loading'>
                </LoadingScreen>

                <Container>
                    <Row className="justify-content-center">
                        <Col md="8">
                            <CardGroup>
                                <Card className="p-4">
                                    <CardBody>

                                        <Row>

                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                {/* <img src={logoo}
                             className="imgHeight"
                             alt="admin@bootstrapmaster.com" />*/}
                                            </Col>
                                        </Row>

                                        <h1>Login</h1>
                                        <InputGroup className="mb-3">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="icon-user"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <OnEvent enter={this.LoginFn.bind(this)}>
                                                <Input type="email"
                                                       value={this.state.email}
                                                       required
                                                       ref={this.email}
                                                       className={this.state.errorEmail ? 'errorBrd' : ''}
                                                       onChange={(e) => {
                                                           this.setState({email: e.target.value});
                                                           this.checkErrorFn()
                                                       }}
                                                       placeholder="Email"/>
                                            </OnEvent>
                                        </InputGroup>
                                        <InputGroup className="mb-4">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="icon-lock"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <OnEvent enter={this.LoginFn.bind(this)}>
                                                <Input type="password"
                                                       amazon-cognito-identity-js value={this.state.password}
                                                       required
                                                       className={this.state.errorPassword ? 'errorBrd' : ''}
                                                       onChange={(e) => {
                                                           this.setState({password: e.target.value});
                                                           this.checkErrorFn()
                                                       }}
                                                       placeholder="Password"/>
                                            </OnEvent>
                                        </InputGroup>
                                        {/*<Row>

                                                <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-right">
                                                    <Input className="form-check-input"
                                                           type="checkbox" id="checkbox1"
                                                           name="checkbox1" value="option1"/>
                                                    <Label check className="form-check-label rememberMe"
                                                           htmlFor="checkbox1">Remember Me</Label>
                                                </Col>


                                            </Row>*/}
                                        <Row>

                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                <Button color="primary"
                                                        type={'button'}
                                                        onClick={this.LoginFn.bind(this)}
                                                        className="px-4">Login</Button>
                                            </Col>
                                        </Row>
                                        <Row>

                                            <Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                <Button color="link" className="px-0"
                                                        onClick={this.redirectPassFn.bind(this)}>Forgot
                                                    password?</Button>
                                            </Col>

                                            {/*<Col xs="12" sm="12" md={'12'} lg={'12'} className="text-center">
                                                <p className="text-muted">New to Entry as a Service? Sign up for
                                                    a <Button color="link" className="px-0">free account</Button>
                                                </p>
                                            </Col>*/}

                                        </Row>

                                    </CardBody>
                                </Card>
                            </CardGroup>
                            <NotificationContainer/>
                        </Col>
                    </Row>
                </Container>


                {/* <Modal isOpen={this.state.first_time_password_change}
                       className={'modal-info modal-lg'}>
                    <ModalHeader>To continue, change the password</ModalHeader>
                    <ModalBody>

                        <Row>
                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                <FormGroup>
                                    <Label htmlFor="name">New Password</Label>
                                    <Input type="password"
                                           name="new_password"
                                           ref={'username'}
                                           onChange={(e) => {
                                               this.setState({new_password: e.target.value})
                                           }}
                                           value={this.state.new_password}
                                           required/>
                                </FormGroup>
                            </Col>
                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                <FormGroup>
                                    <Label htmlFor="name">Confirm Password
                                        {this.state.new_password && this.state.confirm_password &&
                                        (this.state.new_password != this.state.confirm_password) ?
                                            <span className={'text-danger'}>&nbsp;Password and confirm password must be same</span>
                                            : ''}

                                    </Label>
                                    <Input type="password"
                                           name="c_password"
                                           onChange={(e) => {
                                               this.setState({confirm_password: e.target.value})
                                           }}
                                           value={this.state.confirm_password}
                                           required/>
                                </FormGroup>
                            </Col>
                        </Row>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary"
                                onClick={this.changePasswordFn.bind(this)}
                                disabled={!this.state.new_password || !this.state.confirm_password
                                || this.state.new_password != this.state.confirm_password ? true : false}
                        >Submit</Button>
                        <Button color="secondary"
                                onClick={(e) => {
                                    this.setState({first_time_password_change: false})
                                }}>Cancel</Button>
                    </ModalFooter>
                </Modal>*/}

                <Modal isOpen={this.state.forgotPassword}
                       className={'modal-info modal-md modal-dialog-centered'}>
                    <ModalHeader>To continue, enter valid and register email</ModalHeader>
                    <ModalBody>

                        <Row>
                            <Col xs="12" sm="12" md={'12'} lg={'12'}>
                                 <span className={this.state.email1 ? 'succes' : 'err'}>
                                                    {!this.state.emailValidation ? '*' : ''}
                                                </span>
                                <span className={this.state.emailValidation ? 'err' : 'succes'}>

                                                    {this.state.emailValidation ? 'Please enter proper email address' : ''}

                                                </span>
                                <InputGroup className="mb-3">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <i className="icon-user"></i>
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text"
                                           name="email1"
                                           value={this.state.email1}
                                           required
                                           onChange={this.setDataFn.bind(this)}
                                           placeholder="Email"/>
                                </InputGroup>
                            </Col>
                        </Row>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary"
                                onClick={this.changePasswordFn.bind(this)}
                                className={!this.state.email1 ||
                                this.state.emailValidation ? 'btnDisbaled pull-right' : 'pull-right'}
                        >Submit</Button>
                        <Button color="secondary"
                                onClick={(e) => {
                                    this.setState({forgotPassword: false, email1: null})
                                }}>Cancel</Button>
                    </ModalFooter>
                </Modal>

            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
